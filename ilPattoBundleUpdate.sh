bundleName="ilpatto-bundle"

dbpod=$(kubectl -n entando get pods | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)
kubectl -n entando delete EntandoDeBundle/$bundleName
kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_dedb -d metroca_db -c $'delete from installed_entando_bundles where id=\'$bundleName\';'"
sleep 1
ent bundler from-git --repository=https://gitlab.com/hidden-cmca/ilpatto-bundle.git --namespace=entando --name=$bundleName
