dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)


kubectl cp widget/cagliari_widget_lista_multicontent.sql entando/$dbpod:/opt/app-root/src/cagliari_widget_lista_multicontent.sql

kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/cagliari_widget_lista_multicontent.sql";
exit 1
