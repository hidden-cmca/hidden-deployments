update sysconfig set config='<?xml version="1.0" encoding="UTF-8"?>
<Params>
<Param name="urlStyle">classic</Param>
<Param name="hypertextEditor">fckeditor</Param>
<Param name="treeStyle_page">request</Param>
<Param name="treeStyle_category">classic</Param>
<Param name="startLangFromBrowser">false</Param>
<Param name="firstTimeMessages">false</Param>
<Param name="baseUrl">request</Param>
<Param name="baseUrlContext">true</Param>
<Param name="useJsessionId">false</Param>
<Param name="gravatarIntegrationEnabled">false</Param>
<Param name="editEmptyFragmentEnabled">false</Param>
<Param name="argon2">true</Param>
<SpecialPages>
<Param name="notFoundPageCode">notfound</Param>
<Param name="homePageCode">homepage</Param>
<Param name="errorPageCode">errorpage</Param>
<Param name="loginPageCode">login</Param>
</SpecialPages>
<FeaturesOnDemand>
<Param name="groupsOnDemand">true</Param>
<Param name="categoriesOnDemand">true</Param>
<Param name="contentTypesOnDemand">true</Param>
<Param name="contentModelsOnDemand">true</Param>
<Param name="apisOnDemand">true</Param>
<Param name="resourceArchivesOnDemand">true</Param>
</FeaturesOnDemand>
<ExtendendPrivacyModule>
<Param name="extendedPrivacyModuleEnabled">false</Param>
<Param name="maxMonthsSinceLastAccess">6</Param>
<Param name="maxMonthsSinceLastPasswordChange">3</Param>
</ExtendendPrivacyModule>
<ExtraParams>
<Param name="page_preview_hash">hi0m6IcQcPvIk1LhEoae</Param>
<Param name="aspect_ratio">600:600</Param>
</ExtraParams>
<Param name="pagina_argomento">arg_01_dett</Param>
<Param name="nodo_argomenti">arg_argomenti</Param>
</Params>
' where item='params';
