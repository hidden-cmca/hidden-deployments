insert into categories (catcode, parentcode, titles) values ('home', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="it">Generale</property>
<property key="en">All</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('evd_nvt', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">NovitÃ  - sezione in evidenza</property>
<property key="it">NovitÃ  - sezione in evidenza</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_documenti', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Documenti  - (sezioni terzo livello)</property>
<property key="it">Documenti  - (sezioni terzo livello)</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2333', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Elderly person</property>
<property key="it">Anziano</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('758', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Child</property>
<property key="it">Fanciullo</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1528', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Young person</property>
<property key="it">Giovane</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2806', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Family</property>
<property key="it">Famiglia</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('897', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Student</property>
<property key="it">Studente</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2944', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Association</property>
<property key="it">Associazione</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('27', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Municipality</property>
<property key="it">Comune</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('3206', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Education</property>
<property key="it">Istruzione</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1690', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Housing</property>
<property key="it">Abitazione</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1919', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Domestic animal</property>
<property key="it">Animale domestico</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1469', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Social integration</property>
<property key="it">Integrazione sociale</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2836', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Social protection</property>
<property key="it">Protezione sociale</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2792', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Action programme</property>
<property key="it">Programma d''azione</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2846', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Construction and town planning</property>
<property key="it">Urbanistica e edilizia</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1074', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Vocational training</property>
<property key="it">Formazione professionale</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('4416', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Organisation of work and working conditions</property>
<property key="it">Condizioni e organizzazione del lavoro</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('48', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Transport</property>
<property key="it">Trasporto</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1818', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Marriage</property>
<property key="it">Matrimonio</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('416', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Electoral procedure and voting</property>
<property key="it">Procedura elettorale e voto</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1700', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Leisure</property>
<property key="it">Tempo libero</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('317', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Culture</property>
<property key="it">Cultura</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1302', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Immigration</property>
<property key="it">Immigrazione</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2524', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Pollution</property>
<property key="it">Inquinamento</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('5429', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Parking area</property>
<property key="it">Area di parcheggio</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('6849', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Town traffic</property>
<property key="it">Traffico urbano</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('597', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Water</property>
<property key="it">Acqua</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1158', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Waste management</property>
<property key="it">Gestione dei rifiuti</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('2841', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Health</property>
<property key="it">Salute</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('4045', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Public safety</property>
<property key="it">Sicurezza pubblica</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('816', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">International security</property>
<property key="it">Sicurezza internazionale</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('861', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Green area</property>
<property key="it">Spazio verde</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('4245', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Sport</property>
<property key="it">Sport</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('4533', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Road transport</property>
<property key="it">Trasporto stradale</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_01', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Puc elaborati</property>
<property key="it">Puc elaborati</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('4470', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Tourism</property>
<property key="it">Turismo</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('66', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Energy</property>
<property key="it">Energia</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('3236', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Information technology and data processing</property>
<property key="it">Informatica e trattamento dei dati</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_ambiti', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Ambiti disciplinari</property>
<property key="it">Ambiti disciplinari</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('7050', 'cat_ambiti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">neuropsychiatric and rehabilitation nursing sciences</property>
<property key="it">scienze infermieristiche e tecniche neuro-psichiatriche e riabilitative</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('evd', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Argomenti - sezione in evidenza</property>
<property key="it">Argomenti - sezione in evidenza</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('evd_bnd', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Bandi - sezione in evidenza</property>
<property key="it">Bandi - sezione in evidenza</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('1759', 'arg_argomenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Coronavirus</property>
<property key="it">Coronavirus</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('evd_uff', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Uffici - sezione in evidenza</property>
<property key="it">Uffici - sezione in evidenza</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02', 'cat_documenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Pianificazione urbanistica</property>
<property key="it">Pianficazione urbanistica</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_02', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">PUC varianti</property>
<property key="it">PUC varianti</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_04', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Piano particolareggiato centro storico (Ppcs)</property>
<property key="it">Piano particolareggiato centro storico (Ppcs)</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('arg_argomenti', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Argomenti  - (utilizzare solo in Tassonomia argomenti)</property>
<property key="it">Argomenti  - (utilizzare solo in Tassonomia argomenti)</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_ufficitta', 'home', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Uffici di cittÃ </property>
<property key="it">Uffici di cittÃ  (solo per tipo organizzazione)</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_06', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Piani di risanamento</property>
<property key="it">Piani di risanamento</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_07', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Pianificazione attuativa</property>
<property key="it">Pianificazione attuativa</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_08', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Pianificazione in itinere</property>
<property key="it">Pianificazione in itinere</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_09', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Valutazione ambientale strategica</property>
<property key="it">Valutazione ambientale strategica</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_10', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Fondi documentali digitali</property>
<property key="it">Fondi documentali digitali</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_03', 'cat_documenti', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Pianificazione di settore</property>
<property key="it">Pianificazione di settore</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_03_01', 'cat_doc_07_03', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Piano di utilizzo del litorale (Pul)</property>
<property key="it">Piano di utilizzo del litorale (Pul)</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_03_02', 'cat_doc_07_03', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Piano di classificazione acustica (Pca)</property>
<property key="it">Piano di classificazione acustica (Pca)</property>
</properties>

');
insert into categories (catcode, parentcode, titles) values ('cat_doc_07_02_05', 'cat_doc_07_02', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Pianificazione storica</property>
<property key="it">Pianificazione storica</property>
</properties>

');
