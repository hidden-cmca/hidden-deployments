dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)

cat content/contentmodels* >> content/contentmodels.sql

kubectl cp content/contentmodels.sql entando/$dbpod:/opt/app-root/src/contentmodels.sql

kubectl exec -it $dbpod -n entando -- bash -c "psql -U metroca_portdb -d metroca_db -c $'delete from contentmodels;'"
sleep 1


kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/contentmodels.sql";

rm content/contentmodels.sql

exit 1
