dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)


kubectl cp widget/update_motorericerca_keycloak.sql entando/$dbpod:/opt/app-root/src/update_motorericerca_keycloak.sql

kubectl cp widget/cittametro_navbar_footer_parents.sql entando/$dbpod:/opt/app-root/src/cittametro_navbar_footer_parents.sql

kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/update_motorericerca_keycloak.sql";
kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/cittametro_navbar_footer_parents.sql";  
exit 1
