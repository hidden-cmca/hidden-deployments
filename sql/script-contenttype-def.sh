dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)


kubectl cp content/contenttypes-def.sql entando/$dbpod:/opt/app-root/src/contenttypes-def.sql

kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/contenttypes-def.sql";
exit 1
