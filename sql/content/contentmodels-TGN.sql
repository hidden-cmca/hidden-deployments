INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200011, 'TGN', 'Intro sezioni (titolo + abs+pulsantiverdi)', '
<h2>$content.titolo.text</h2>
<p>$content.descr.text</p>
#if ($content.link.size() > 0)
<p>
#foreach ($bottone in $content.link)
	<a class="btn btn-default btn-verde" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPAGE") $bottone.text" href="$bottone.destination">$bottone.text</a>
#end
</p>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200002, 'TGN', 'Intro sezioni (titolo + abs)', '
<h2>$content.titolo.text</h2>
<p>$content.descr.text</p>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200003, 'TGN', 'Sito tematico (foto)', '
<article class="scheda scheda-round">
	<div class="scheda-testo nopadd">
  	#if ($content.link_pag.destination != "")
		<a href="$content.link_pag.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.link.get(0).text">
    	#if ($content.immagine.getImagePath(''0'') != "")
			<img src="$content.immagine.getImagePath(''3'')" alt="$content.immagine.text" class="img-fluid" />
      #else
      	$content.titolo.text
      #end
		</a>
    #else
    <a>
    	#if ($content.immagine.getImagePath(0) != "")
			<img src="$content.immagine.getImagePath(3)" alt="$content.immagine.text" class="img-fluid" />
      #else
      	$content.titolo.text
      #end
		</a>
    #end
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200009, 'TGN', 'Sezioni - in evidenza (titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
						<h4>
            		<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
            </h4>
						<p>$content.descr.text</p>
					</div>
					<div class="scheda-footer">
            		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$linkpage" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
					</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200006, 'TGN', 'Sezioni - in evidenza (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
            <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200005, 'TGN', 'Sito tematico (titolo+abs+ico)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<div class="scheda-gestione">
	<p>
		<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text"><strong>$content.titolo.text</strong></a><br />
		<span>$content.descr.text</span>
	</p>
	<div class="thumbs-round thumbs-round-icon">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
	</div>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200004, 'TGN', 'Sito tematico (titolo+abs+ico+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p class="scheda-testo-w70">$content.descr.text</p>
		<div class="scheda-icona scheda-icona-inline">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
		</div>
	</div>
	<div class="scheda-footer">
			<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_SHOWMORE") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200010, 'TGN', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
#set($icona = "ca-subject")
#if ($content.icona.text != "")
	#set($icona = $content.icona.text)
#end
<article>
	<div class="cerca-risultato-item">
  	#if ($content.sezione.mapKey != "")
			<a href="$content.getPageURL($content.sezione.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue" class="categoria">
    #else
    	<a class="categoria">
    #end
      	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$icona"></use></svg>
    #if ($content.sezione.mapKey != "")
        <span>$content.sezione.mapValue</span>
    #end
      </a>
		<h4><a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>

','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200001, 'TGN', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
          <p>$content.descr.text</p>
				</div>
			</div>
      <div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						#if ($content.argomenti.values != "[]")
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
							<div class="argomenti-sezione-elenco">
								#set ($argomenti = $content.argomenti.values)
								#foreach ($Category in $content.getCategories())
									#if($argomenti.contains($Category.code))
										<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
									#end
								#end
							</div>
						#end
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.paragrafo && $content.paragrafo.size() > 0)
	<section id="articolo-dettaglio-testo">
		<div class="container">	
			<div class="row">
				<div class="linetop-lg"></div>
				<div class="col-lg-3 col-md-4 lineright">
					<aside id="menu-sinistro">
						<h4 class="dropdown">
							<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
								$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
							</a>
						</h4>
						<div class="menu-separatore"><div class="bg-oro"></div></div>
						<div id="lista-paragrafi" class="list-group collapse show">
							#foreach ($paragrafo in $content.paragrafo)
								#if ($paragrafo.titolo.text != "")
									<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-$foreach.count" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $paragrafo.titolo.text">$paragrafo.titolo.text</a>
								#end
							#end
							#if ($content.allegati && $content.allegati.size() > 0)
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-allegati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_TGN_allegati")">$i18n.getLabel("CITTAMETRO_CONTENT_TGN_allegati")</a>
							#end
							#if ($content.link && $content.link.size() > 0)
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkcorrelati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_TGN_linkcorrelati")">$i18n.getLabel("CITTAMETRO_CONTENT_TGN_linkcorrelati")</a>
							#end
						</div>
					</aside>
				</div>
				<div class="col-lg-9 col-md-8 pt8">
					#if (($content.paragrafo && $content.paragrafo.size() > 0) || ($content.link && $content.link.size() > 0) || ($content.allegati && $content.allegati.size() > 0))
						<div class="articolo-paragrafi articolo-paragrafi-link">
							#foreach ($paragrafo in $content.paragrafo)
								#if ($paragrafo.titolo.text != "")
									<div class="row">
										<div class="offset-md-1 col-md-11 paragrafo">
											<a id="articolo-par-$foreach.count"> </a>
											<h4>
												#if ($paragrafo.link.destination != "")
													#if ($paragrafo.link.destination.contains(''mailto''))
														#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAILTO"))
													#else 
														#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"))
													#end
													<a href="$paragrafo.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $paragrafo.link.text">$paragrafo.titolo.text</a>
												#else
													$paragrafo.titolo.text
												#end
											</h4>
										</div>
									</div>
								#end
								#if (($paragrafo.testo.text != "") || ($paragrafo.link_txt.destination != ""))
									<div class="row">
									  <div class="offset-md-1 col-md-8 testolungo">
										$paragrafo.testo.text
										#if ($paragrafo.link_txt.destination != "")
											#if ($paragrafo.link_txt.destination.contains(''mailto''))
												#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAILTO"))
											#else 
												#set ($titleintro = $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"))
											#end
											<p><a href="$paragrafo.link_txt.destination" title="$titleintro: $paragrafo.link_txt.text" class="wbreak" #if($paragrafo.link_txt.symbolicLink.destType == 1)target="_blank" #end>$paragrafo.link_txt.text</a></p>
										#end
									  </div>
									</div>
								#end
							#end
              
              #if ($content.video.destination != "")
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo" data-ng-cloak data-ng-controller="FiltriController">
									<div class="videoplayer">
									#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
									</div>
								</div>
							</div>
							#end
              
							#if ($content.allegati && $content.allegati.size() > 0)
								<div class="row">
									<div class="offset-md-1 col-md-11 paragrafo">
										<a id="articolo-par-allegati"> </a>
										<h4>$i18n.getLabel("CITTAMETRO_CONTENT_TGN_allegati")</h4>
									</div>
								</div>
								<div class="row schede">
									<div class="offset-md-1 col-md-11">
										#set ($contadoc = 1)
										#foreach ($allegato in $content.allegati)
											#if ($contadoc % 2 != 0)
												<div class="row allegati-riga">
											#end
											#if ($allegato.attachPath != '''')
												<div class="col-lg-6">
													<article class="allegato">
														<div class="scheda-allegato">
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
																	aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
																	$allegato.text
																</a>
															</h4>
														</div>
													</article>
												</div>
												#set ($contadoc = $contadoc + 1)
											#end
											#if ($contadoc % 2 != 0 || $foreach.count==$content.allegati.size())
												</div>
											#end
										#end
									</div>
								</div>
							#end
							#if ($content.link && $content.link.size() > 0)
								<div class="row">
									<div class="offset-md-1 col-md-11 paragrafo">
										<a id="articolo-par-linkcorrelati"> </a>
										<h4>$i18n.getLabel("CITTAMETRO_CONTENT_TGN_linkcorrelati")</h4>
									</div>
								</div>
								<div class="row articolo-ulterioriinfo">
									<div class="offset-md-1 col-md-8">
										#set ($contalink = 1)
										#foreach ($altrilink in $content.link)
											#if ($altrilink.destination != '''')
												#if ($foreach.count % 2 != 0)
													<div class="row">
												#end
												<div class="col-lg-6">
													<article class="scheda-verde-link scheda-round">
														<a href="$altrilink.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $altrilink.text" #if($altrilink.symbolicLink.destType == 1)target="_blank" #end><strong>$altrilink.text</strong></a>
													</article>
												</div>
												#set ($contalink = $contalink + 1)
											#end
											#if ($contalink % 2 != 0 || $foreach.count==$content.link.size())
												</div>
											#end
										#end
										</p>
									</div>
								</div>
							#end
						</div>
					#end
				</div>
			</div>
		</div>
	</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200007, 'TGN', 'Area personale - cittadino (ico+cat+tit+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end

<article class="scheda scheda-slide">
	<div class="scheda-icona">
		<a>
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg> 
		</a>
	</div>
	<div class="scheda-icona-testo">$i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_CITIZEN")</div>
	<div class="scheda-testo">
		<h4>
			<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
		</h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200012, 'TGN', 'Area personale - residente (ico+cat+tit+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end

<article class="scheda scheda-slide">
	<div class="scheda-icona">
		<a>
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg> 
		</a>
	</div>
	<div class="scheda-icona-testo">$i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_RESIDENT")</div>
	<div class="scheda-testo">
		<h4>
			<a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
		</h4>
		<p>$content.descr.text</p>
	</div>
</article>

','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200012
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200015, 'TGN', 'Sezioni - in evd residente (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
			$i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_RESIDENT")
	</div>
	<div class="scheda-testo-small">
			<h4>
            <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200015
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200014, 'TGN', 'Sezioni - in evd cittadino (ico+cat+tit+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = $content.getContentLink())
#if ($content.link_pag.destination != "")
	#set($linkpage = $content.link_pag.destination)
#end
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
      $i18n.getLabel("CITTAMETRO_PORTAL_SERVICE_CITIZEN")
	</div>
	<div class="scheda-testo-small">
			<h4>
            <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$linkpage" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200014
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200008, 'TGN', 'Intro sezioni (titolo+testo+argomenti)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")		
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-7">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						#if ($content.argomenti.values != "[]")
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
							<div class="argomenti-sezione-elenco">
								#set ($argomenti = $content.argomenti.values)
								#foreach ($Category in $content.getCategories())
									#if($argomenti.contains($Category.code))
										<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
									#end
								#end
							</div>
						#end
					</div>
				</aside>
			</div>
		</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200008
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 200013, 'TGN', 'Intro sezioni (condividi+azioni+argomenti)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")				
        <aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						#if ($content.argomenti.values != "[]")
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
							<div class="argomenti-sezione-elenco">
								#set ($argomenti = $content.argomenti.values)
								#foreach ($Category in $content.getCategories())
									#if($argomenti.contains($Category.code))
										<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
									#end
								#end
							</div>
						#end
					</div>
				</aside>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 200013
) LIMIT 1;

