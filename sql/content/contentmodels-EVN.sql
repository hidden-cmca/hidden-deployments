INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210005, 'EVN', 'Sito tematico (titolo+abs)', '
<h3>$content.titolo.text</h3>
<p>$content.descr.text</p>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210004, 'EVN', 'Homepage - Argomenti (cat+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg>
		$content.titolo.text
</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210002, 'EVN', 'Evento padre (ico+cat+titolo+img+data)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda scheda-evento scheda-round">
		<div class="scheda-icona-small mr130">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $content.tipologia.mapValue<br />
      #if ($content.luogo.sch_luogo.destination != "")
      	<span>$content.luogo.sch_luogo.text</span>
      #else
      	<span>$content.luogo.nome.text</span>
      #end
		</div>
		<div class="scheda-testo">
			<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
		</div>
    #if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("4")" alt="$content.immagine.text" />
 			</figure>
      <div class="scheda-data">
				<strong>$content.data_iniz.getFormattedDate("dd")</strong>
				$content.data_iniz.getFormattedDate("MMM")
			</div>
		</div>
    #end
	</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210006, 'EVN', 'Homepage - Calendario (cat+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
$content.tipologia.mapValue - 
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOAPPOINTMENT"): $content.titolo.text">
	$content.titolo.text
</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210003, 'EVN', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210009, 'EVN', 'Sezioni - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($sezionePage = ''novita'')
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg>
    #if ($content.tipologia.mapKey != "")
    	$content.tipologia.mapValue
    #else 
    	$i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
    #end
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210010, 'EVN', 'Sezioni - In evidenza (titolo+abs)', '
<article class="scheda scheda-round scheda-news">	
	<div class="scheda-testo mt32 no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210007, 'EVN', 'Homepage - In evidenza (titolo+abs+arg+img)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($sezionePage = ''novita'')
<article>
	#if ($content.immagine.getImagePath("0") != "")
	<div class="novita-foto img-fit-cover"> 
		<img src="$content.immagine.getImagePath("4")" alt="$content.immagine.text" />
	</div>
  #end
	<div class="novita-testo">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">$content.titolo.text</a></h2>
					<p>$content.descr.text</p>
					<div class="argomenti">
						#set ($argomenti = $content.argomenti.values)
						#foreach ($Category in $content.getCategories())
							#if($argomenti.contains($Category.code))
								<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
							#end
						#end
					</div>
				<a href="$content.getPageURL($sezionePage)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
				</div>
			</div>
		</div>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210001, 'EVN', 'Dettaglio completo', '
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						#if (!$content.genitore.booleanValue)
							#if ($content.parte_di.destination != "")
							<li class="breadcrumb-item"><a href="$content.parte_di.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.parte_di.text"><strong>$content.parte_di.text</strong></a><span class="separator">/</span></li>
							#end
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#else
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#end
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					#if($content.sottotit.text != "")
					<h4>$content.sottotit.text</h4>
					#end
					<p>$content.descr.text</p>
					#if($content.stato.text == "Disattivo")
					<div class="alert alert-warning mt16" role="alert">
						<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $content.stato_mot.text
					</div>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
						#if ($content.vedi_cal.booleanValue)
						<div class="mt32">
							<a class="btn btn-default btn-celeste" title="$i18n.getLabel("CITTAMETRO_CONTENT_EVN_gotocal")" href="#">
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-calendar_today"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVN_gotocal")
							</a>
						</div>
						#end
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if (($content.dim_img.booleanValue) && ($content.immagine.getImagePath("0") != ""))
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
		<figcaption>$content.immagine.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.intro.text != "" || $content.desc_dest.text != "" || $content.galleria.destination != "" || $content.video.destination != "" || $content.persone.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-intro" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_intro")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_intro")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luogo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_luogo")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_luogo")</a>
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-date_orari" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_date_orari")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_date_orari")</a>
						#if ($content.prezzo.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-prezzo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_prezzo")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_prezzo")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_contatti")</a>
						#if ($content.figli && $content.figli.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-figli" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_figli")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_figli")</a>
						#end
						#if ($content.parte_di.destination != "" && ! $content.genitore.booleanValue)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-parte_di" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_EVN_parte_di")">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_parte_di")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_MIGHT_INTERESTED")">$i18n.getLabel("CITTAMETRO_MIGHT_INTERESTED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					<div data-ng-cloak data-ng-controller="FiltriController">
						#if ($content.intro.text != "" || $content.desc_dest.text != "" || $content.galleria.destination != "" || $content.video.destination != "" || $content.persone.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-intro"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_intro")</h4>
								</div>
							</div>
							#if ($content.intro.text != "")
								<div class="row">
									<div class="offset-md-1 col-md-8 testolungo">
										$content.intro.text
									</div>
								</div>
							#end
							#if (! $content.dim_img.booleanValue)
								<div class="row">
									<div class="offset-lg-1 col-lg-11 col-md-12 articolo-foto-interna img-fit-cover">
										<figure>
											<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
										</figure>
									</div>
								</div>
							#end
							#if ($content.galleria.destination != "")
								<div class="row">
									<div class="col-md-12 mt32">
										<div class="galleriasfondo"></div>
									</div>
									<div class="offset-md-1 col-md-11 mt32">
										<div class="galleriaslide">
											<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
											#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
										</div>
									</div>
								</div>
							#end
							#if ($content.video.destination != "")
								<div class="row">
									<div class="offset-md-1 col-md-11 paragrafo">
										<div class="videoplayer">
											#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
										</div>
									</div>
								</div>
							#end
							#if ($content.desc_dest.text != "")
								<div class="row">
									<div class="offset-md-1 col-md-8 testolungo">
										<span class="mt24 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_desc_dest")</span>
										$content.desc_dest.text
									</div>
								</div>
							#end
							#if ($content.persone && $content.persone.size() > 0)
								<div class="row">
									<div class="offset-md-1 col-md-8 testolungo">
										<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_persone")</p>
									</div>
								</div>
								<div class="row articolo-ulterioriinfo">
									<div class="offset-md-1 col-md-11">
										<div class="argomenti persone">
											#foreach ($persona in $content.persone)
												#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
												<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
											#end
										</div>
									</div>
								</div>
							#end	
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luogo"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_luogo")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 listaluoghi">
								<div class="row">
									#if ($content.luogo.sch_luogo.destination != "")
										#set($linkScheda = $content.luogo.sch_luogo.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("LGO"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''120004'')" data-ng-bind-html="renderContent[''$linkScheda''][''120004'']"></div>
										#end
									#else
										<div class="col-md-9">
											<article class="scheda-ufficio-contatti scheda-round">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-place"></use></svg>
												<div class="scheda-ufficio-testo">
													#if ($content.luogo.nome.text != "")
														<h4 class="mb24"><a>$content.luogo.nome.text</a>
															#if ($content.luogo.indirizzo.text != "" || $content.luogo.cap.text != "")
																<br /><span>
																#if ($content.luogo.indirizzo.text != "")
																	$content.luogo.indirizzo.text
																#end
																#if ($content.luogo.cap.text != "")
																	#if ($content.luogo.indirizzo.text != "")
																		-
																	#end
																	$content.luogo.cap.text
																#end
																</span>
															#end
														</h4>
													#end
													<p>
														#if ($content.luogo.quartiere.text != "")
															$content.luogo.quartiere.text
														#end
														#if ($content.luogo.circoscr.text != "")
															#if ($content.luogo.quartiere.text != "")
																<br />
															#end
															$content.luogo.circoscr.text
														#end
													</p>
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
					</div>
					#if ($content.luogo.gps_lat.text != "" && $content.luogo.gps_lon.text != "")
						<div id="schedamappa" class="mt32" style="height:368px"></div>
						<div id="schedamappah" class="mt32"></div>
						<script>
							var latIn =  $content.luogo.gps_lat.text;
							var lngIn =  $content.luogo.gps_lon.text;
							var fscala =  "16";
							var mapCa = L.map(''schedamappa'', {
							  layers:[OSM]
							});
							var baseMaps = {
								"CartoDB": CartoDB,
								"Open Street Map": OSM,
								"Google Sat": Google,
								"Esri Sat": Esri_sat
							};
							var icona = L.icon({
								iconUrl: ''$imgURL/marker-icon.png'',
								iconSize: [25, 34],
								iconAnchor: [13, 34],
								popupAnchor: [-3, -76],
								shadowUrl: ''$imgURL/marker-shadow.png'',
								shadowSize: [41, 34],
								shadowAnchor: [13, 34]
							});
							mapCa.setView([latIn, lngIn], fscala);
							L.marker([latIn, lngIn], {icon: icona}).addTo(mapCa);
						</script>
					#end
					<div data-ng-cloak data-ng-controller="FiltriController">
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-date_orari"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_date_orari")</h4>
							</div>
						</div>
						<div class="row stepper mt12">
							<div class="offset-md-1 col-md-11">
								#if ($content.data_iniz.getFullDate() != $content.data_fine.getFullDate())
									<div class="step">
										<div class="date-step">
											<span class="date-step-giorno">$content.data_iniz.getFormattedDate("dd")</span><br><span class="date-step-mese">$content.data_iniz.getFormattedDate("MMM")/$content.data_iniz.getFormattedDate("yy")</span>
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_inizio_evento")</p>
											</div>
										</div>
									</div>
									<div class="step">
										<div class="date-step">
											<span class="date-step-giorno">$content.data_fine.getFormattedDate("dd")</span><br><span class="date-step-mese">$content.data_fine.getFormattedDate("MMM")/$content.data_fine.getFormattedDate("yy")</span>
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_fine_evento")</p>
											</div>
										</div>
									</div>
								#else 
									<div class="step">
										<div class="date-step">
											<span class="date-step-giorno">$content.data_iniz.getFormattedDate("dd")</span><br><span class="date-step-mese">$content.data_iniz.getFormattedDate("MMM")/$content.data_iniz.getFormattedDate("yy")</span>
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_inizio_fine_evento")</p>
											</div>
										</div>
									</div>
								#end
							</div>
						</div>
						#if ($content.ricorrenza.ric_lun.booleanValue || $content.ricorrenza.ric_mar.booleanValue || $content.ricorrenza.ric_mer.booleanValue || $content.ricorrenza.ric_gio.booleanValue || $content.ricorrenza.ric_ven.booleanValue || $content.ricorrenza.ric_sab.booleanValue || $content.ricorrenza.ric_dom.booleanValue)
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<span class="mt24 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_giorni")</span>
								<p>
								#set($contag = 0)
								#if ($content.ricorrenza.ric_lun.booleanValue)
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_lun")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_mar.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_mar")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_mer.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_mer")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_gio.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_gio")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_ven.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_ven")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_sab.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_sab")
									#set($contag = $contag+1)
								#end
								#if ($content.ricorrenza.ric_dom.booleanValue)
									#if ($contag > 0)-#end
									$i18n.getLabel("CITTAMETRO_CONTENT_EVN_ric_dom")
								#end
								</p>
							</div>
						</div>
						#end
						#if ($content.orari.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<span class="mt24 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_orari")</span>
								$content.orari.text
								
								#if ($content.agg_cal.booleanValue)
									<script nonce="$content.nonce" type="text/javascript" src="https://addevent.com/libs/atc/1.6.1/atc.min.js" async defer></script>
									<div title="$i18n.getLabel("CITTAMETRO_CONTENT_EVN_agg_cal")" class="addeventatc btn btn-default btn-celeste" data-styling="none" role="button">
										<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add_circle"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVN_agg_cal")
										<span class="start">$content.data_iniz.getFormattedDate("yyyy/MM/dd") $content.data_iniz.getFormattedDate("HH:mm")</span>
										<span class="end">$content.data_fine.getFormattedDate("yyyy/MM/dd") $content.data_fine.getFormattedDate("HH:mm")</span>
										<span class="timezone">Europe/Rome</span>
										<span class="title">$content.titolo.text</span>
										<span class="description">$content.descr.text</span>
									</div>
								#end
							</div>
						</div>
						#end	
						#if ($content.prezzo.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-prezzo"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_prezzo")</h4>
								</div>
							</div>
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									#set ($giagratuito = 0)
									#foreach ($biglietto in $content.prezzo)
										#if (($biglietto.tipo.text != "Gratuito") || ($giagratuito == 0))
											<div class="callout sp callout-highlight">
												#if ($biglietto.tipo.text == "Gratuito")
													<div class="callout-title"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_gratis")</strong></div>
													#set ($giagratuito = 1)
												#else
													#set ($costoappo = $biglietto.costo.text)
													#set ($numerovirgola = $costoappo.length() - 3)
													#set ($decimali = $costoappo.substring($numerovirgola,$costoappo.length()))
													#set ($numerofinale = $decimali)
													#set ($numerocar = $numerovirgola - 1)
													#set ($cicli = 0)
													#foreach ($i in [$numerocar..0])
														#if ($cicli == 3)
															#set ($numerofinale = "." + $numerofinale)
															#set ($cicli = 0)
														#end
														#set ($numerofinale = $costoappo.charAt($i) + $numerofinale)
														#set ($cicli = $cicli + 1)
													#end
													<div class="callout-title"><span>$biglietto.tipo.text</span><br /><strong>$numerofinale</strong> euro</div>
												#end
												#if ($biglietto.descr.text != '''')
													$biglietto.descr.text
												#end
											</div>
										#end
									#end
								</div>
							</div>
						#end
						#if ($content.info_bigl.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.info_bigl.text
								</div>
							</div>
						#end
						#if ($content.link_bigl.destination != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="mt16">
										<a class="btn btn-default btn-iscriviti" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.link_bigl.text" href="$content.link_bigl.destination">
											$content.link_bigl.text
										</a>
									</p>
								</div>
							</div>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-documenti"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_documenti")</h4>
								</div>
							</div>
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									#set ($contadoc = 1)
									#foreach ($allegato in $content.documenti)
										#if ($contadoc % 2 != 0)
											<div class="row allegati-riga">
										#end
											#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
												<div class="col-md-6">
													<article class="allegato">
														<div class="scheda-allegato">
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#if ($allegato.documento.attachPath != '''')
																	#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																	#set ($last = $fileNameSplit.size() - 1)
																	#set ($fileExt = $fileNameSplit[$last])
																	<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																		$allegato.documento.text
																	</a>
																#end
																#if ($allegato.link.destination != '''')
																<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.link.text">
																	$allegato.link.text
																</a>
																#end
																<br />
																<span>$allegato.descr.text</span>
															</h4>
														</div>
													</article>
												</div>
												#set ($contadoc = $contadoc + 1)
											#end
										#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
											</div>
										#end
									#end
								</div>
							</div>
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									#if ($content.organizz.link_uff.destination != "")
										#set($linkScheda = $content.organizz.link_uff.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''220003'')" data-ng-bind-html="renderContent[''$linkScheda''][''220003'']"></div>
										#end
									#else
										<div class="col-md-9">
											<article class="scheda-ufficio-contatti scheda-round">
												<div class="scheda-ufficio-testo">
													#if ($content.organizz.soggetto.text != "")
														<h4 class="mb24"><a>$content.organizz.soggetto.text</a></h4>
													#end
													#if (($content.organizz.persona.text != "") || ($content.organizz.telefono.text != "") || ($content.organizz.reperib.text != "") || ($content.organizz.email.text != "") || ($content.organizz.sito_web.destination != ""))
														<p>
															#if ($content.organizz.persona.text != "")
																$i18n.getLabel("CITTAMETRO_CONTENT_EVN_organizz_referente"): $content.organizz.persona.text<br />
															#end
															#if ($content.organizz.telefono.text != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.organizz.telefono.text<br />
															#end
															#if ($content.organizz.reperib.text != "")
																$i18n.getLabel("CITTAMETRO_CONTENT_EVN_organizz_reperib"): $content.organizz.reperib.text<br />
															#end
															#if ($content.organizz.email.text != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.organizz.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.organizz.email.text</a><br />
															#end
															#if ($content.organizz.sito_web.destination != "")
																$i18n.getLabel("CITTAMETRO_PORTAL_WEBSITE"): <a href="$content.organizz.sito_web.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.organizz.sito_web.text">$content.organizz.sito_web.text</a>
															#end
														</p>
													#end
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
						#if ($content.supporto.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_EVN_supporto")</p>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									#set($linkScheda = $content.supporto.destination.replaceAll("#|C;|!",""))
									#if ($linkScheda.contains("ORG"))
										<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''220003'')" data-ng-bind-html="renderContent[''$linkScheda''][''220003'']"></div>
									#end
								</div>
							</div>
						</div>
						#end
						#if ($content.figli && $content.figli.size() > 0)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-figli"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_figli")</h4>
								</div>
							</div>
							<div class="row">
								<div class="offset-md-1 col-md-11 lista-eventi-figli">
									<div class="row">
									#foreach ($figlio in $content.figli)
										#set($linkEvento = $figlio.destination.replaceAll("#|C;|!",""))
										#if ($linkEvento.contains("EVN"))
											<div class="col-lg-6 col-md-12 mb16" data-ng-init="getContent(''$linkEvento'',''210002'')" data-ng-bind-html="renderContent[''$linkEvento''][''210002'']"></div>
										#end
									#end
									</div>
								</div>
							</div>
						#end
						#if ($content.parte_di.destination != "" && ! $content.genitore.booleanValue)
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-parte_di"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_parte_di")</h4>
								</div>
							</div>
							<div class="row">
								<div class="offset-md-1 col-md-11 lista-eventi-figli">
									<div class="row">
										#set($linkEvento = $content.parte_di.destination.replaceAll("#|C;|!",""))
										#if ($linkEvento.contains("EVN"))
											<div class="col-lg-6 col-md-12 mb16" data-ng-init="getContent(''$linkEvento'',''210002'')" data-ng-bind-html="renderContent[''$linkEvento''][''210002'']"></div>
										#end
									</div>
								</div>
							</div>
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-info"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
							</div>
						</div>
						#if ($content.ult_info.text != "")
							<div class="row articolo-ulterioriinfo">
								<div class="offset-md-1 col-md-8">
									$content.ult_info.text
								</div>
							</div>
						#end
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#if ($content.patro_den.text != "" || $content.patro_link.destination != "")
									<p>
										<strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_patro"):</strong><br />
										#if ($content.patro_link.destination != "")
											<a href="$content.patro_link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.patro_link.text" class="scheda-logo">
										#end
										#if ($content.patro_logo.getImagePath("2") != "")
											<img src="$content.patro_logo.getImagePath("2")" alt="$content.patro_logo.text" />
										#else
											#if ($content.patro_den.text != "")
												$content.patro_den.text
											#else
												$content.patro_link.text
											#end
										#end
										#if ($content.patro_link.destination != "")
											</a>
										#end
									</p>
								#end
								#if ($content.sponsor && $content.sponsor.size() > 0)
									<p class="mt32">
										<strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_sponsor"):</strong><br />
										#foreach ($spons in $content.sponsor)
											#if ($spons.sitoweb.destination != "")
												<a href="$spons.sitoweb.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $spons.sitoweb.text" class="scheda-logo-piccolo">
											#end
											#if ($spons.logo.getImagePath("2") != "")
												<img src="$spons.logo.getImagePath("2")" alt="$spons.logo.text" />
											#else
												#if ($spons.denominaz.text != "")
													$spons.denominaz.text
												#else
													$spons.sitoweb.text
												#end
											#end
											#if ($spons.sitoweb.destination != "")
												</a>
											#end
										#end
									</p>
								#end
								#if ($content.str_polit && $content.str_polit.size() > 0)
									<p class="mt32 mb0"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_str_polit")</strong></p>
									<div class="listaluoghi">
										<div class="row">
											#foreach ($pol in $content.str_polit)
												<div class="col-lg-6">
													<article class="scheda-ufficio-contatti scheda-round">
														<div class="scheda-ufficio-testo">
															#if ($pol.pol_scheda.destination != "")
																<h4><a href="$pol.pol_scheda.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $pol.pol_scheda.text">$pol.pol_scheda.text</a></h4>
															#else
																<h4><a>$pol.pol_nome.text</a></h4>
															#end
														</div>
													</article>
												</div>
											#end
										</div>
									</div>
								#end
								
								#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
									<div class="callout important sp">
										<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
										#foreach ($aiuto in $content.box_aiuto)
											#if ($aiuto.testo.text != "")
												$aiuto.testo.text
											#end
											#if ($aiuto.link.destination != '''')
												<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
											#end
										#end
									</div>
								#end
								<div class="row">
									<div class="col-md-12 mt16">
										<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
										<p class="data-articolo">
											<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_MIGHT_INTERESTED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210014, 'EVN', 'Homepage - In evidenza (cat+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $content.tipologia.mapValue
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNOVITA")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210014
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210008, 'EVN', 'Sezioni - In evidenza (img+cat+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
			</figure>
			#if ($content.data_iniz != "")
				<div class="scheda-data">
					<strong>$content.data_iniz.getFormattedDate("d")</strong>
					$content.data_iniz.getFormattedDate("MMM")
				</div>
			#end
		</div>
	#end
	<div class="scheda-icona-small">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
	</div>
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
		</div>
	#end
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210008
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210011, 'EVN', 'Sezioni - In evidenza (img+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($sezionePage = ''novita'')
<article class="scheda scheda-round scheda-news">
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
				<figure>
					<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
				</figure>
				#if ($content.data_iniz != "")
					<div class="scheda-data">
						<strong>$content.data_iniz.getFormattedDate("d")</strong>
						$content.data_iniz.getFormattedDate("MMM")
					</div>
				#end
		</div>
	#end
	<div class="scheda-testo mt32">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>	
	<div class="scheda-argomenti">
		#set ($argomenti = $content.argomenti.values)
		#foreach ($Category in $content.getCategories())
			#if($argomenti.contains($Category.code))
				<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
			#end
		#end
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210013, 'EVN', 'Sezioni - In evidenza (img+cat+dt+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.immagine.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" class="img-fluid objpos-$content.pos_img.mapKey" />
			</figure>
			#if ($content.data_iniz != "")
				<div class="scheda-data">
					<strong>$content.data_iniz.getFormattedDate("d")</strong>
					$content.data_iniz.getFormattedDate("MMM")
				</div>
			#end
		</div>
	#end
	<div class="scheda-icona-small #if ($content.foto.getImagePath("0") != "") mr114 #end">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
	</div>
	<div class="scheda-testo #if($content.immagine.getImagePath("0") == "")mr-0#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
		<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
		</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210013
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 210012, 'EVN', 'Sezioni - In evidenza (cat+data+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-event"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_EVENTS")
  	- $content.data_iniz.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOEVENTS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 210012
) LIMIT 1;



