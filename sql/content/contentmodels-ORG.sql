INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220014, 'ORG', 'Luogo (ico+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="col-md-9">
	<article class="scheda-ufficio-contatti scheda-round mt8">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
			</h4>
    </div>
	</article>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220014
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220005, 'ORG', 'Gestione (ico+titolo+indirizzo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda-ufficio-contatti scheda-round barretta-verde">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">    
     <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
       <strong>$content.titolo.text</strong>
     </a><br />
     <span>
       #if ($content.indirizzo.text != "")
         $content.indirizzo.text
       #end
       #if ($content.cap.text != "" && $content.cap.text != "-")
         - $content.cap.text
       #end
       #if ($content.ind_altro.text != "")
         <br />$content.ind_altro.text
       #end
     </span>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220013, 'ORG', 'Intro organizzazione (titolo)', '
<article class="scheda scheda-brick scheda-round">
	<div class="scheda-testo mt16">
		<h4>
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
    		$content.titolo.text
      </a>
    </h4>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220013
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220017, 'ORG', 'Titolo (titolo+link)', '
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220017
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220011, 'ORG', 'Dettaglio completo - card + badge', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.competenze.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-competenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</a>
						#end
						#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</a>
						#end
						#if ($content.persone && $content.persone.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-persone" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</a>
						#end
						#if ($content.tipologia.mapKey.equals("amm_02") || ($content.tipologia.mapKey.equals("amm_03")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-sedi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.competenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-competenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.competenze.text
							</div>
						</div>
					#end
					#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</h4>
							</div>
						</div>						
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								#if ($content.resp.destination != "")
									<div class="row">
										<div class="col-md-6 scheda-persona-singola scheda-staff mt12">
											#set($linkP = $content.resp.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkP'',''230009'')" data-ng-bind-html="renderContent[''$linkP''][''230009'']"></div>
										</div>
									</div>
								#end
								#if ($content.legami && $content.legami.size() > 0)
									<div class="row">
										<div class="col-md-12">
											<article class="scheda-staff">
												<p class="ampio">
													#if ($content.tipologia.mapValue.contains("Aree"))
														$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_uffici")<br />
													#else 
														#if ($content.tipologia.mapValue.contains("Uffici"))
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_dipende")<br />
														#else
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_altre")<br />
														#end
													#end
													#foreach ($legame in $content.legami)
														#set($linkCont = $legame.destination.replaceAll("#|C;|!",""))
														<span data-ng-init="getContent(''$linkCont'',''220017'')" data-ng-bind-html="renderContent[''$linkCont''][''220017'']"></span>
														<br />
													#end
												</p>
											</article>
										</div>
									</div>
								#end
								#if ($content.assessor.size() > 0)
									<div class="row">
										#if ($content.assessor.size() > 1)
											<div class="col-sm-12 lista-persone">
												<span class="mb8 mt0 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessori")</span>
												<div class="row">
													#foreach ($ass in $content.assessor)
														<div class="col-lg-6 col-md-12 mb16">
															#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
														</div>
													#end
												</div>
											</div>
										#else
											<div class="col-md-6 scheda-persona-singola">
												<span class="mb8 mt12 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessore")</span>
												#foreach ($ass in $content.assessor)
													#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
												#end
											</div>
										#end
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.persone && $content.persone.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-persone"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="argomenti persone mt12">
									#foreach ($persona in $content.persone)
										#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
										<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
									#end
								</div>
							</div>
						</div>
					#end
					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo #if ($content.persone && $content.persone.size() > 0)mt20#end">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</h4>
							</div>
						</div>
						#if ($content.tipologia.mapKey.equals("amm_02"))
							#set ($fieldsearch = ''area'')
						#else
							#set ($fieldsearch = ''uffici'')
						#end
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''800002'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''800002'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''800002'')" data-ng-bind-html="renderContent[elem.$][''800002'']"></div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo #if ($content.servizi && $content.servizi.size() > 0)mt8#end">
							<a id="articolo-par-sedi"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-11 listaluoghi">	
							<div class="row">
								<div class="col-lg-9 col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										#if ($content.sede.destination != "")
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
										#end
										<div class="scheda-ufficio-testo">
											<h4 class="mb24">
												#if ($content.sede.destination != "")
													<a href="$content.sede.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.sede.text">$content.sede.text</a><br />
												#end
												#if ($content.indirizzo.text != "" || $content.cap.text != "")
													<span>
													#if ($content.indirizzo.text != "")
														$content.indirizzo.text
													#end
													#if ($content.ind_altro.text != "")
														- $content.ind_altro.text
													#end
													#if ($content.cap.text != "" && $content.cap.text != "-")
														 - $content.cap.text
													#end
													</span>
												#end
											</h4>
											#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
											#end
											#if (($content.telefono.text != "") || ($content.email.text != "") || ($content.pec.text != ""))
											<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($content.pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
											</p>
											#end
											#if ($content.sch_persone.size() > 0)
											<p>
												#foreach ($gente in $content.sch_persone)
													#if ($gente.destination != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="$gente.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $gente.text"><strong>$gente.text</strong></a>
													#end
												#end
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
										#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
															#else
																#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																	$allegato.documento.text
																</a>
															#end
															<br />
															<span>$allegato.descr.text</span>
														</h4>
													</div>
												</article>
											</div>
											#set ($contadoc = $contadoc + 1)
										#end
									#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end

					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220010, 'ORG', 'Dettaglio completo - card', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.competenze.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-competenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</a>
						#end
						#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</a>
						#end
						#if ($content.persone && $content.persone.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-persone" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</a>
						#end
						#if ($content.tipologia.mapKey.equals("amm_02") || ($content.tipologia.mapKey.equals("amm_03")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-sedi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.competenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-competenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.competenze.text
							</div>
						</div>
					#end
					#if (($content.legami && $content.legami.size() > 0) || ($content.resp.destination != "") || ($content.assessor.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</h4>
							</div>
						</div>						
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								#if ($content.resp.destination != "")
									<div class="row">
										<div class="col-md-6 scheda-persona-singola scheda-staff mt12">
											#set($linkP = $content.resp.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkP'',''230009'')" data-ng-bind-html="renderContent[''$linkP''][''230009'']"></div>
										</div>
									</div>
								#end
								#if ($content.legami && $content.legami.size() > 0)
									<div class="row">
										<div class="col-md-12">
											<article class="scheda-staff">
												<p class="ampio">
													#if ($content.tipologia.mapValue.contains("Aree"))
														$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_uffici")<br />
													#else 
														#if ($content.tipologia.mapValue.contains("Uffici"))
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_dipende")<br />
														#else
															$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_altre")<br />
														#end
													#end
													#foreach ($legame in $content.legami)
														#set($linkCont = $legame.destination.replaceAll("#|C;|!",""))
														<span data-ng-init="getContent(''$linkCont'',''220017'')" data-ng-bind-html="renderContent[''$linkCont''][''220017'']"></span>
														<br />
													#end
												</p>
											</article>
										</div>
									</div>
								#end
								#if ($content.assessor.size() > 0)
									<div class="row">
										#if ($content.assessor.size() > 1)
											<div class="col-sm-12 lista-persone">
												<span class="mb8 mt0 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessori")</span>
												<div class="row">
													#foreach ($ass in $content.assessor)
														<div class="col-lg-6 col-md-12 mb16">
															#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
														</div>
													#end
												</div>
											</div>
										#else
											<div class="col-md-6 scheda-persona-singola">
												<span class="mb8 mt12 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessore")</span>
												#foreach ($ass in $content.assessor)
													#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
												#end
											</div>
										#end
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.persone && $content.persone.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-persone"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</h4>
							</div>
						</div>
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11 lista-persone">
								<div class="row">
									#foreach ($persona in $content.persone)
										<div class="col-lg-6 col-md-12 mb16">
											#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
											<div data-ng-init="getContent(''$linkDipendente'',''230005'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230005'']"></div>
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo #if ($content.persone && $content.persone.size() > 0)mt20#end">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</h4>
							</div>
						</div>
						#if ($content.tipologia.mapKey.equals("amm_02"))
							#set ($fieldsearch = ''area'')
						#else
							#set ($fieldsearch = ''uffici'')
						#end
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''800002'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''800002'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''800002'')" data-ng-bind-html="renderContent[elem.$][''800002'']"></div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo #if ($content.servizi && $content.servizi.size() > 0)mt8#end">
							<a id="articolo-par-sedi"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-11 listaluoghi">	
							<div class="row">
								<div class="col-lg-9 col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										#if ($content.sede.destination != "")
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
										#end
										<div class="scheda-ufficio-testo">
											<h4 class="mb24">
												#if ($content.sede.destination != "")
													<a href="$content.sede.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.sede.text">$content.sede.text</a><br />
												#end
												#if ($content.indirizzo.text != "" || $content.cap.text != "")
													<span>
													#if ($content.indirizzo.text != "")
														$content.indirizzo.text 
													#end
													#if ($content.ind_altro.text != "")
														- $content.ind_altro.text
													#end
													#if ($content.cap.text != "" && $content.cap.text != "-")
														- $content.cap.text
													#end													
													</span>
												#end
											</h4>
											#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
											#end
											#if (($content.telefono.text != "") || ($content.email.text != "") || ($content.pec.text != ""))
											<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($content.pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
											</p>
											#end
											#if ($content.sch_persone.size() > 0)
											<p>
												#foreach ($gente in $content.sch_persone)
													#if ($gente.destination != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="$gente.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $gente.text"><strong>$gente.text</strong></a>
													#end
												#end
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
										#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
											<div class="col-md-6">
												<article class="allegato">
													<div class="scheda-allegato">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
															#else
																#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																	$allegato.documento.text
																</a>
															#end
															<br />
															<span>$allegato.descr.text</span>
														</h4>
													</div>
												</article>
											</div>
											#set ($contadoc = $contadoc + 1)
										#end
									#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end

					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
                    <p><a href="$aiuto.link.destination"   #if($aiuto.link.symbolicLink.destType == 1)target="_blank" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $aiuto.link.text"                     #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $aiuto.link.text" #end > 
                    <strong>$aiuto.link.text</strong></a></p>
										#end     
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220010
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220016, 'ORG', 'Titolo h4 (titolo+link)', '
<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220016
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220018, 'ORG', 'Servizio (ico+titolo+indirizzo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br />
        #if ($content.indirizzo.text != "" || $content.cap.text != "")
        <span>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text 
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
        		- $content.cap.text
         #end
         #if ($content.ind_altro.text != "")
         	<br />$content.ind_altro.text
         #end
        </span>
        #end
			</h4>
    </div>
	</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220018
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220022, 'ORG', 'Widget Help (orario)', '
<p>
												#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != ""))
												#end
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
      								

','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220022
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220012, 'ORG', 'Servizio (titolo+indirizzo+contatti) - col-6', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
			<h4 class="mb24">
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br />
        #if ($content.indirizzo.text != "" || $content.cap.text != "")
        <span>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
         	- $content.cap.text
         #end
        
         #if ($content.ind_altro.text != "")
         	<br />$content.ind_altro.text
         #end
        </span>
        #end
			</h4>
      <p>
      #if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
     	 $i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
      #end
      #set($prec = false)
      	#if ($info.getConfigParameter("orario_invernale") == ''true'')
      	#if ($content.orario.inv_matt.text != "")
      		#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
      		$orarioinvmatt
      		#set($prec = true)
      #end
      #if ($content.orario.inv_pom.text != "")
      	#if ($prec)<br />#end
      		#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
      		$orarioinvpom
      		#set($prec = true)
      	#end
      #else
      	#if ($content.orario.est_matt.text != "")
      		#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
      		$orarioestmatt
      		#set($prec = true)
      	#else 
      	#if ($content.orario.inv_matt.text != "")
      		#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
      		$orarioinvmatt														
      		#set($prec = true)
      	#end
      #end
      #if ($content.orario.est_pom.text != "")
      	#if ($prec)<br />#end														
      	#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
      	$orarioestpom
      	#set($prec = true)
      #else
      #if ($content.orario.inv_pom.text != "")
      	#if ($prec)<br />#end
      		#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
      		$orarioinvpom
      		#set($prec = true)
      	#end
      #end
     #end
     #if ($content.orario.continuo.text != "")
      #if ($prec)<br />#end
      #set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
      $orariocontinuo													
      #end
      </p>
      <p>	
		#if ($content.telefono.text != "")
			$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
		#end
		#if ($content.email.text != "")
			$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
		#end
		#if ($pec.text != "")
			$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
		#end	
	</p>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220012
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220019, 'ORG', 'Persona (ico+cat+titolo) - col-6', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda scheda-round">
  	<div class="scheda-icona-small">
    	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
  			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
      </a>
    </div>
    <div class="scheda-testo-small">
			<h4>
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
			</h4>
    </div>
	</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220019
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220009, 'ORG', 'Intro sezioni (titolo+testo+gestione)', '
		<div class="row">
			<div class="offset-lg-1 col-lg-5 col-md-7">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-4 offset-lg-1 offset-md-1 col-md-4">
			</div>
		</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220002, 'ORG', 'Sezioni - in evidenza (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220021, 'ORG', 'Lista (titolo + indirizzo - email + area)', '
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	<p>
		<br/>
    #if ($content.indirizzo.text != "" || $content.cap.text != "")
    	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_STR_indirizzo"):</strong>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text 
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
        		- $content.cap.text
         #end
      <br />
    #end
    #if ($content.email.text != "")
    	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_STR_email"):</strong> $content.email.text <br />
    #end
		#if (($content.tipologia.mapValue.contains("Uffici")) && ($content.legami.size() > 0))
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_STR_area"):</strong>								
			#foreach ($legame in $content.legami)
      	#set($linkStruttura = $legame.destination.replaceAll("#|C;|!",""))
      	#if ($linkStruttura.contains("ORG"))
        	<a href="$legame.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $legame.text">$legame.text</a>
				#end
      	</div>
      #end
		#end
	</p>
</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220021
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220001, 'ORG', 'Dettaglio completo - badge - pagina volante', '
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#if ($content.tipologia.mapKey.equals("amm_02"))
	#set ($fieldsearch = ''area'')
#else
	#set ($fieldsearch = ''uffici'')
#end
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.competenze.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-competenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</a>
						#end
						#if (($content.tipologia.mapKey.equals("amm_02")) && ($content.legami.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura_uffici" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_ufficio")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_ufficio")</a>
						#end
						#if (($content.tipologia.mapKey.equals("amm_03")) && ($content.legami.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura_area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_area")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_area")</a>
						#end
						#if (($content.resp.destination != "") || ($content.assessor.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-struttura" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</a>
						#end
						#if (($content.tipologia.mapKey.equals("amm_01_02_02")) || ($content.tipologia.mapKey.equals("amm_01_02_03")) || ($content.tipologia.mapKey.equals("amm_01_03")) || ($content.tipologia.mapKey.equals("amm_01_03_02")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-persone" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-sedi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</a>
						#if ($content.tipologia.mapKey.equals("amm_02") || ($content.tipologia.mapKey.equals("amm_03")))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</a>
						#end
						#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luoghi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-bandi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_bandi")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_bandi")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-ordinanze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_ordinanze")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_ordinanze")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-modulistica" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_modulistica")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_modulistica")</a>
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc-regolamenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_STR_regolamenti")">$i18n.getLabel("CITTAMETRO_CONTENT_STR_regolamenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.competenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-competenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_competenze")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.competenze.text
							</div>
						</div>
					#end
					#if (($content.tipologia.mapKey.equals("amm_02")) && ($content.legami.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura_uffici"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_uffici")</h4>
							</div>
						</div>
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11 listaluoghi">	
								<div class="row">
									#foreach ($legame in $content.legami)
										<div class="col-lg-6">
											<article class="scheda-ufficio-contatti scheda-round">
												<div class="scheda-ufficio-testo">
													#set($linkCont = $legame.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
					#end 
					#if (($content.tipologia.mapKey.equals("amm_03")) && ($content.legami.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura_area"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_area")</h4>
							</div>
						</div>
						<div class="row listaluoghi" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									#foreach ($legame in $content.legami)
										<div class="col-lg-9 col-md-12">
											#set($linkStruttura = $legame.destination.replaceAll("#|C;|!",""))
											#if ($linkStruttura.contains("ORG"))
												<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
											#end
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#if (($content.resp.destination != "") || ($content.assessor.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-struttura"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura")</h4>
							</div>
						</div>						
						<div class="row" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								#if ($content.assessor.size() > 0)
									<div class="row">
										#if ($content.assessor.size() > 1)
											<div class="col-sm-12 lista-persone">
												<span class="mb8 mt0 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessori")</span>
												<div class="row">
													#foreach ($ass in $content.assessor)
														<div class="col-lg-6 col-md-12 mb16">
															#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
														</div>
													#end
												</div>
											</div>
										#else
											<div class="col-lg-6 scheda-persona-singola">
												<span class="mb8 mt12 articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_struttura_assessore")</span>
												#foreach ($ass in $content.assessor)
													#set($linkAss = $ass.destination.replaceAll("#|C;|!",""))
													<div data-ng-init="getContent(''$linkAss'',''230005'')" data-ng-bind-html="renderContent[''$linkAss''][''230005'']"></div>
												#end
											</div>
										#end
									</div>
								#end
								#if ($content.resp.destination != "")
									<div class="row #if ($content.assessor.size() > 0 && $content.assessor.size() < 2)mt12#end">
										<div class="col-md-12">
											<article>
												#set($linkP = $content.resp.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkP'',''230010'')" data-ng-bind-html="renderContent[''$linkP''][''230010'']"></div>
											</article>
										</div>
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.persone && $content.persone.size() > 0)
						#if (($content.tipologia.mapKey.equals("amm_01_02_02")) || ($content.tipologia.mapKey.equals("amm_01_02_03")) || ($content.tipologia.mapKey.equals("amm_01_03")) || ($content.tipologia.mapKey.equals("amm_01_03_02")))
							<div class="row">
								<div class="offset-md-1 col-md-11 paragrafo">
									<a id="articolo-par-persone"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone")</h4>
								</div>
							</div>
						#else
							<div class="row mt12">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone_funzionari")</p>
								</div>
							</div>
						#end
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="argomenti persone">
									#foreach ($persona in $content.persone)
										#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
										<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
									#end
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-sedi"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_contatti")</h4>
						</div>
					</div>
					<div class="row">
						<div class="offset-md-1 col-md-11 listaluoghi">	
							<div class="row">
								<div class="col-lg-9 col-md-12">
									<article class="scheda-ufficio-contatti scheda-round">
										#if ($content.sede.destination != "")
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
										#end
										<div class="scheda-ufficio-testo">
											<h4 class="mb24">
												#if ($content.sede.destination != "")
													<a href="$content.sede.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.sede.text">$content.sede.text</a><br />
												#end
												#if ($content.indirizzo.text != "" || $content.cap.text != "")
													<span>
													#if ($content.indirizzo.text != "")
														$content.indirizzo.text 
													#end
													#if ($content.ind_altro.text != "")
														- $content.ind_altro.text
													#end
													#if ($content.cap.text != "" && $content.cap.text != "-")
														- $content.cap.text
													#end
													</span>
												#end
											</h4>
											
											#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
												<p>$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
											#end
											#if (($content.telefono.text != "") || ($content.email.text != "") || ($content.pec.text != ""))
											<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($content.pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
											</p>
											#end
										</div>
									</article>
								</div>
							</div>
						</div>
					</div>
					#if ($content.sch_persone.size() > 0)
						<div class="row mt12">
							<div class="offset-md-1 col-md-11">
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_STR_persone_contattare")</p>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo" data-ng-cloak data-ng-controller="FiltriController">
							<div class="offset-md-1 col-md-11">
								<div class="lista-persone-ruoli">
									#foreach ($gente in $content.sch_persone)
										#if ($gente.destination != "")
											<div class="persona-ruolo">
												#set($linkDipendente = $gente.destination.replaceAll("#|C;|!",""))
												<span data-ng-init="getContent(''$linkDipendente'',''230012'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230012'']"></span> - $gente.text
											</div>
										#end
									#end
								</div>
							</div>
						</div>
					#end

					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=micro;value=false)+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''800002'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''800002'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''800002'')" data-ng-bind-html="renderContent[elem.$][''800002'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_servizi_empty")</p>
							</div>
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
										#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
															#else
																#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																	$allegato.documento.text
																</a>
															#end
															<br />
															<span>$allegato.descr.text</span>
														</h4>
													</div>
												</article>
											</div>
											#set ($contadoc = $contadoc + 1)
										#end
									#if ($contadoc % 2 != 0 || $foreach.count == $content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.tipologia.mapKey.equals("amm_02") || $content.tipologia.mapKey.equals("amm_03"))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luoghi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''LGO'', '''', '''', '' '', '''', ''(attributeFilter=true;key=s_str_link;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height riga-mono">
									<div class="col-lg-6 col-md-6" data-ng-repeat="elem in contents" data-ng-init="getContent(elem.$,''120009'')" data-ng-bind-html="renderContent[elem.$][''120009'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_luoghi_empty")</p>
							</div>
						</div>

						#if ($content.tipologia.mapKey.equals("amm_03"))
							#set ($fieldsearch = ''ufficio'')
						#end
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-bandi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_bandi")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=tipologia;value=doc_09)+(attributeFilter=true;start=today;key=data_scad)+(order=DESC;attributeFilter=true;key=data_pubb)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
						
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-ordinanze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_ordinanze")</h4>
							</div>
						</div>
						
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=tipologia;value=doc_10)+(order=DESC;attributeFilter=true;key=data_pubb)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
						
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-modulistica"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_modulistica")</h4>
							</div>
						</div>
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=tipologia;value=doc_02)+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
						
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc-regolamenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_STR_regolamenti")</h4>
							</div>
						</div>
						
						<div class="row schede" data-ng-cloak data-ng-controller="FiltriController" data-ng-init="setParameters(''999'', ''999'', ''DOC'', '''', '''', '' '', '''', ''(attributeFilter=true;key=$fieldsearch;value=$content.getId())+(attributeFilter=true;key=sottotip;value=doc_04_02)+(attributeFilter=true;key=data_scad;nullValue=true)+(order=ASC;attributeFilter=true;key=titolo)'')">
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length > 0">
								<div class="row row-eq-height allegati-riga" data-ng-repeat="elem in contents" data-ng-if="($index+1)%2 == 0 || ($index+1) == contents.length || ($index+1) == ''999''">
									<div class="col-lg-6" data-ng-if="($index+1)%2 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''700010'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''700010'']"></div>
									<div class="col-lg-6" data-ng-init="getContent(elem.$, ''700010'')" data-ng-bind-html="renderContent[elem.$][''700010'']"></div>
								</div>
							</div>
							<div class="offset-md-1 col-md-11" data-ng-if="contents.length == 0">
								<p class="mt0">$i18n.getLabel("CITTAMETRO_CONTENT_STR_documento_empty")</p>
							</div>
						</div>
					#end
					
					
					
					
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")<br/>$aiuto.testo.text #end 
                    
										#if ($aiuto.link.destination != '''')
                                        <p><a href="$aiuto.link.destination"   #if($aiuto.link.symbolicLink.destType == 1)target="_blank" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $aiuto.link.text" #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $aiuto.link.text" #end > 
                                         <strong>$aiuto.link.text</strong></a></p>
										#end                    
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220003, 'ORG', 'Servizio (ico+titolo+indirizzo+contatti)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
	<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">
			<h4 class="mb24">
				<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
				<br />
        #if ($content.indirizzo.text != "" || $content.cap.text != "")
        <span>
         #if ($content.indirizzo.text != "")
         	$content.indirizzo.text 
         #end
         #if ($content.cap.text != "" && $content.cap.text != "-")
        		- $content.cap.text
         #end
         #if ($content.ind_altro.text != "")
         	<br />$content.ind_altro.text
         #end
        </span>
        #end
			</h4>
      <p>
												#if (($content.orario.inv_matt.text != "") || ($content.orario.inv_pom.text != "") || ($content.orario.est_matt.text != "") || ($content.orario.est_pom.text != "") || ($content.orario.continuo.text != "")) 
													$i18n.getLabel("CITTAMETRO_CONTENT_STR_orari"):<br />
												#end
												#set($prec = false)
												#if ($info.getConfigParameter("orario_invernale") == ''true'')
													#if ($content.orario.inv_matt.text != "")
														#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														$orarioinvmatt
														#set($prec = true)
													#end
													#if ($content.orario.inv_pom.text != "")
														#if ($prec)<br />#end
														#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
														$orarioinvpom
														#set($prec = true)
													#end
												#else
													#if ($content.orario.est_matt.text != "")
														#set($orarioestmatt = $content.orario.est_matt.text.replaceAll("\|","<br />"))
														$orarioestmatt
														#set($prec = true)
													#else 
														#if ($content.orario.inv_matt.text != "")
															#set($orarioinvmatt = $content.orario.inv_matt.text.replaceAll("\|","<br />"))
														    $orarioinvmatt														
															#set($prec = true)
														#end
													#end
													#if ($content.orario.est_pom.text != "")
														#if ($prec)<br />#end														
														#set($orarioestpom = $content.orario.est_pom.text.replaceAll("\|","<br />"))
														$orarioestpom
														#set($prec = true)
													#else
														#if ($content.orario.inv_pom.text != "")
															#if ($prec)<br />#end
															#set($orarioinvpom = $content.orario.inv_pom.text.replaceAll("\|","<br />"))
															$orarioinvpom
															#set($prec = true)
														#end
													#end
												#end
												#if ($content.orario.continuo.text != "")
													#if ($prec)<br />#end
													#set($orariocontinuo = $content.orario.continuo.text.replaceAll("\|","<br />"))
													$orariocontinuo													
												#end
											</p>
      								<p>	
												#if ($content.telefono.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text<br />
												#end
												#if ($content.email.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a><br />
												#end
												#if ($pec.text != "")
													$i18n.getLabel("CITTAMETRO_PORTAL_PEC"): <a href="mailto:$content.pec.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.pec.text</a><br />
												#end	
			</p>
    </div>
	</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220006, 'ORG', 'Intro sezioni (titolo+testo+argomenti)', '
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-7">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 offset-lg-1 offset-md-1 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220004, 'ORG', 'Uffici (ico+titolo+indirizzo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda-ufficio-contatti scheda-round">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    <div class="scheda-ufficio-testo">    
     <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
       <strong>$content.titolo.text</strong>
     </a><br />
     <span>
       #if ($content.indirizzo.text != "")
         $content.indirizzo.text
       #end
       #if ($content.cap.text != "" && $content.cap.text != "-")
         - $content.cap.text
       #end
       #if ($content.ind_altro.text != "")
         <br />$content.ind_altro.text
       #end
     </span>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220020, 'ORG', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220020
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220007, 'ORG', 'Homepage - In evidenza (cat+titolo+abs+link) ', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLAMMINISTRAZIONE")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220007
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220008, 'ORG', 'Argomento (ico+cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220008
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 220015, 'ORG', 'Sezioni - in evidenza (ico+cat+titoletto+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 220015
) LIMIT 1;
