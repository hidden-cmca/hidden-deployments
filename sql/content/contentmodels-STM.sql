INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 170002, 'STM', 'Argomento - In evidenza  (ico+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda-sito $content.colore.text">
	#if ($content.icona.text != '''')
    <div class="icona-sito">
      <svg class="icon $content.icona.text"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
    </div>
  #end
	<p>
		<a href="$content.url_sito.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text [$i18n.getLabel("CITTAMETRO_PORTAL_EXTERNALSITE")]">
			<strong>$content.titolo.text</strong><br>
			$content.descr.text
		</a>
	</p>
</article>

','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 170002
) LIMIT 1;

INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 170003, 'STM', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="bg-argomento $content.colore.text">
</div>
<div class="container sitotematico-titolo">
	<div class="row">
		<div class="offset-lg-1 col-lg-10 col-md-12">
			<h2>
				#if ($content.icona.text != '')
					<svg class="icon $content.icona.text"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
				#end
				$content.titolo.text
		  </h2>
		</div>
	</div>
</div>
<div class="container" data-ng-cloak data-ng-controller="FiltriController">
	<div class="box-argomento box-sitotematico">
		<div class="row">
			<div class="col-lg-7 col-md-12" id="briciole">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Home"><strong>Home</strong></a><span class="separator">/</span></li>
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
				  </ol>
				</nav>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7 col-md-12">
				<div class="titolo-sezione">
					#set($linkNotizia = $content.notizia.destination.replaceAll("#|C;|!",""))
					<div data-ng-init="getContent(''$linkNotizia'',''100011'')" data-ng-bind-html="renderContent[''$linkNotizia''][''100011'']"></div>
				</div>
			</div>
			<div class="col-lg-5 col-md-12">
				#if($content.gest_pag.size() > 0)
					<aside id="menu-area">
						<p>$i18n.getLabel("CITTAMETRO_CONTENT_MANAGE_THEMES")</p>
							#foreach ($gest in $content.gest_pag)
								#set($linkGest = $gest.destination.replaceAll("#|C;|!",""))
								#if ($linkGest.contains("ORG"))
									<div data-ng-init="getContent(''$linkGest'',''220005'')" data-ng-bind-html="renderContent[''$linkGest''][''220005'']"></div>
								#end
							#end
					</aside>
				#end
			</div>
		</div>
	</div>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 170003
) LIMIT 1;

INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 170001, 'STM', 'Homepage - In evidenza (ico+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($linkpage = "")
#if ($content.url_sito.destination != '''')
	#set($linkpage = $content.url_sito.destination)
#end

<div class="scheda-sito $content.colore.text">
		#if ($content.icona.text != '''')
    	<div class="icona-sito">
        <svg class="icon $content.icona.text"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
      </div>
    #end
	<p>
  	#if ($content.url_sito.destination != '''')
		<a href="$content.url_sito.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text [$i18n.getLabel("CITTAMETRO_PORTAL_EXTERNALSITE")]" #if($content.url_sito.symbolicLink.destType == 1)target="_blank" #end>
    #else 
    <a>
    #end
			<strong>$content.titolo.text</strong><br>
			$content.descr.text
		</a>
	</p>
</div>

','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 170001
) LIMIT 1;
