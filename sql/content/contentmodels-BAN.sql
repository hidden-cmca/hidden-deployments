INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 180001, 'BAN', 'In evidenza (sfondo+titolo+abs)', '
<article class="scheda-app">
	#if ($content.immagine.getImagePath(''0'') != "")
		<div class="bg-full">
			<img src="$content.immagine.getImagePath(''3'')" alt="$content.immagine.text" />
		</div>
  #end
	<p>
  	#if ($content.link.destination != "")
		<a href="$content.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text">
			<strong>$content.titolo.text</strong><br />
			$content.descr.text
		</a>
    #else
    <a>
    <strong>$content.titolo.text</strong><br />
			$content.descr.text
    </a>
    #end
	</p>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 180001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 180002, 'BAN', 'Banner largo (sfondo+abs+titolo)	', '
	#if ($content.immagine.getImagePath(''0'') != "")
		<div class="bg-centroinfo">
			<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
		</div>
  #end
	<div class="container">
		<div class="row row-centroinfo">
			<div class="col-md-8">
				<div class="testo-centroinfo text-center">
					<h4>$content.descr.text</h4>
				</div>
			</div>
      #if ($content.num_verde.text != "")
       <div class="col-md-4">
         <div class="box-numeroverde">
         		#if ($content.link.destination != "")
						<a href="$content.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $content.titolo.text">
            #end
           		$content.num_verde.text
            #if ($content.link.destination != "")
						</a>
            #end
         </div>
       </div>
      #end
		</div>
	</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 180002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 180003, 'BAN', 'Banner largo immagine', '
#if ($content.immagine.getImagePath(''0'') != "")
<div class="container">
	<div class="img-banner-largo img-fit-cover">
		<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" class="img-fluid" />
	</div>
</div>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 180003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 180004, 'BAN', 'Homepage - Banner speciale', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set ($linktitolo = "#")
#if ($content.link_arg.mapKey != "")
	#set ($linktitolo = $content.getPageURL($info.getConfigParameter("pagina_argomento")) + "?categoryCode=" + $content.link_arg.mapKey)
#else
	#if ($content.link.destination != "")
  	#set ($linktitolo = $content.link.destination)
  #end
#end
<div class="allerta-meteo allerta3 $content.sfondo.mapKey">
	<article>
		#if ($content.immagine.getImagePath(''0'') != "")
			<div class="bg-allerta">
				<div class="bg-allerta-img">
					<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
				</div>
		  </div>
		#end
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-8 col-9">
					<h2><a href="$linktitolo" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h2>
				</div>
			</div>
			#if ($content.link_puls.size() > 0)
			<div class="row">
				<div class="col-12 mt8 mb8">
					#foreach ($pulsante in $content.link_puls)
						<a class="btn btn-gen bg-giallo" href="$pulsante.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $pulsante.text" #if($pulsante.symbolicLink.destType == 1)target="_blank" #end>$pulsante.text</a>
					#end
				</div>
			</div>
			#end
		</div>
	</article>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 180004
) LIMIT 1;


