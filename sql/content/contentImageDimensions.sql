UPDATE sysconfig SET config ='<Dimensions>
	<Dimension>
		<id>1</id>
		<dimx>130</dimx>
		<dimy>130</dimy>
	</Dimension>
	<Dimension>
		<id>2</id>
		<dimx>220</dimx>
		<dimy>220</dimy>
	</Dimension>
	<Dimension>
		<id>3</id>
		<dimx>500</dimx>
		<dimy>500</dimy>
	</Dimension>
	<Dimension>
		<id>4</id>
		<dimx>800</dimx>
		<dimy>800</dimy>
	</Dimension>
	<Dimension>
		<id>5</id>
		<dimx>1920</dimx>
		<dimy>1920</dimy>
	</Dimension>
</Dimensions>
' where item='imageDimensions';
