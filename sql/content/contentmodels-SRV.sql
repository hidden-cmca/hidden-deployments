INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800003, 'SRV', 'Homepage - In evidenza (cat+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLSERVIZI")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800005, 'SRV', 'Sezioni - in evidenza (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800008, 'SRV', 'Dettaglio completo', '
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						#if ($content.micro.booleanValue)
							<li class="breadcrumb-item"><a href="$content.parte_di.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.parte_di.text"><strong>$content.parte_di.text</strong></a><span class="separator">/</span></li>
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#else
							<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
						#end
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>

#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					#if($content.sottotit.text != "")
					<h4>$content.sottotit.text</h4>
					#end
					<p>$content.descr.text</p>
					#if($content.stato.text == "Disattivo")
					<div class="alert alert-warning mt16" role="alert">
						<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $content.stato_mot.text
					</div>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
		<figcaption>$content.immagine.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.descr_est.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descr_est" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_descr_est")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_descr_est")</a>
						#end
						#if ($content.desc_dest.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-desc_dest" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_desc_dest")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_desc_dest")</a>
						#end
						#if (($content.come_si_fa.text != "") || ($content.esito.text != "") || ($content.proc_esito.testo.text != "") || ($content.can_digit.testo.text != ""))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-come_si_fa" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_come_si_fa")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_come_si_fa")</a>
						#end
						#if ($content.uffici.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-uffici" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_uffici")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_uffici")</a>
						#end
						#if ($content.area.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_area")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_area")</a>
						#end
						#if ($content.cosa_serve && $content.cosa_serve.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-cosa_serve" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_cosa_serve")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_cosa_serve")</a>
						#end
						#if (($content.costi.text != "") || ($content.vincoli.text != ""))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-costi_vinc" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_costi_vinc")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_costi_vinc")</a>
						#end
						#if ($content.scadenze.text != "" || ($content.scad_fasi && $content.scad_fasi.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-scadenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_scadenze")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_scadenze")</a>
						#end
						#if ($content.casi_part.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-casi_part" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_casi_part")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_casi_part")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_documenti")</a>
						#end
						#if ($content.link && $content.link.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-link" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_link")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_link")</a>
						#end
						<div data-ng-init="setParameters(''999'', ''999'', ''SRV'', '''', '''', '' '', '''', ''(attributeFilter=true;key=parte_di;value=$content.getId())+(order=ASC;attributeFilter=true;key=titolo)'')">
							<a data-ng-if="contents.length > 0" class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-micro_serv" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_micro_serv")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_micro_serv")</a>
						</div>
						#if ($content.parte_di.destination != "" && $content.micro.booleanValue)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-parte_di" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_parte_di")">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_parte_di")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.descr_est.text != "")							
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descr_est"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_descr_est")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.descr_est.text								
							</div>							
						</div>
					#end
					#if ($content.desc_dest.text != "")							
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-desc_dest"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_desc_dest")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.desc_dest.text								
							</div>							
						</div>
					#end
					#if ($content.cop_geo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.cop_geo.text								
							</div>							
						</div>
					#end
					#if (($content.come_si_fa.text != "") || ($content.esito.text != "") || ($content.proc_esito.testo.text != "") || ($content.can_digit.testo.text != ""))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-come_si_fa"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_come_si_fa")</h4>
							</div>
						</div>
						#if ($content.come_si_fa.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.come_si_fa.text								
								</div>							
							</div>
						#end
						#if ($content.esito.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_esito")</p>
									<div class="articolo-mt0">
										$content.esito.text		
									</div>								
								</div>							
							</div>
						#end
						#if ($content.proc_esito.testo.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_proc_esito")</p>
									<div class="articolo-mt0">
										$content.proc_esito.testo.text
									</div>
									#if ($content.proc_esito.link_sch.destination != "")
										<div class="row">
										#set($linkScheda = $content.proc_esito.link_sch.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''220018'')" data-ng-bind-html="renderContent[''$linkScheda''][''220018'']"></div>
										#end
										#if ($linkScheda.contains("LGO"))
											<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''120004'')" data-ng-bind-html="renderContent[''$linkScheda''][''120004'']"></div>
										#end
										</div>
									#end
								</div>							
							</div>
						#end
						#if ($content.can_digit.testo.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_can_digit")</p>
									<div class="articolo-mt0">
										$content.can_digit.testo.text
									</div>
									#if ($content.can_digit.link.destination != "")
										<p class="mt16 mb32">
											<a class="btn btn-default btn-iscriviti" href="$content.can_digit.link.destination" #if($content.can_digit.link.symbolicLink.destType == 1) target="_blank"#end>$content.can_digit.link.text</a>
										</p>
									#end
								</div>							
							</div>
						#end
					#end
					#if ($content.autenticaz.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.autenticaz.text								
							</div>							
						</div>
					#end
					#if (($content.can_fisico.text != "") || ($content.can_prenot.destination != ""))
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_SRV_can_fisico")</p>
								<div class="articolo-mt0">
									$content.can_fisico.text
								</div>
								#if ($content.can_prenot.destination != "")
									<div class="row">
										<div class="col-md-12">
											<p class="mt16">
												<a class="btn btn-default btn-iscriviti" href="$content.can_prenot.destination">$content.can_prenot.text</a>
											</p>
										</div>
									</div>
								#end
							</div>
						</div>
					#end
					#if ($content.uffici.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-uffici"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_uffici")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">	
								<div class="row">
									#foreach ($sede in $content.uffici)
										#if ($sede.destination != "")
											#set($linkScheda = $sede.destination.replaceAll("#|C;|!",""))
											#if ($linkScheda.contains("ORG"))
												<div class="col-xl-6 col-lg-12 col-md-12">
													<article class="scheda-ufficio-contatti scheda-round">
													  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
															<div class="scheda-ufficio-testo">
																<div data-ng-init="getContent(''$linkScheda'',''220012'')" data-ng-bind-html="renderContent[''$linkScheda''][''220012'']"></div>
															</div>
													</article>
												</div>
											#end
										#end
									#end
								</div>
							</div>
						</div>
					#end
					#if ($content.area.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-area"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_area")</h4>
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									<div class="col-lg-12">
										#set($linkStruttura = $content.area.destination.replaceAll("#|C;|!",""))
										#if ($linkStruttura.contains("ORG"))
											<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					#if ($content.cosa_serve && $content.cosa_serve.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-cosa_serve"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_cosa_serve")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8">
								<div class="callout important callout-punti">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($box in $content.cosa_serve)
										<div class="callout-punto">
											<div class="callout-punto-icona">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
											</div>
											<div class="callout-punto-testo">
												$box.testo.text
												#if ($box.documento.attachPath != '''')
													<p>
														#set ($fileNameSplit = $box.documento.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$box.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $box.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $box.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $box.documento.resource.instance.fileLength)" target="_blank">
															<strong>$box.documento.text</strong>
														</a>
													</p>
												#end
												#if ($box.link.destination != '''')
													<p>
														<a href="$box.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $box.link.text" class="wbreak">
															<strong>$box.link.text</strong>
														</a>
													</p>
												#end
											</div>
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#if (($content.costi.text != "") || ($content.vincoli.text != ""))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-costi_vinc"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_costi_vinc")</h4>
							</div>
						</div>
						#if ($content.costi.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.costi.text									
								</div>								
							</div>
						#end
						#if ($content.vincoli.text != "")
							<div class="row">
								<div class="offset-md-1 col-md-8 testolungo">
									$content.vincoli.text									
								</div>								
							</div>
						#end
					#end
					#if (($content.scadenze.text != "") || ($content.scad_fasi && $content.scad_fasi.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-scadenze"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_scadenze")</h4>
							</div>
						</div>
						#if ($content.scadenze.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.scadenze.text
							</div>
						</div>
						#end
						#if ($content.scad_fasi && $content.scad_fasi.size() > 0)
						<div class="row steppertime">
							<div class="offset-md-1 col-md-11">
								#foreach ($fase in $content.scad_fasi)
									<div class="step">										
										<div class="date-step">
											#if ($fase.data.fullDate != "")
											<span class="date-step-giorno">$fase.data.getFormattedDate("dd")</span><span class="date-step-mese">$fase.data.getFormattedDate("MMM")/$fase.data.getFormattedDate("yy")</span>
											#else
											<span class="date-step-giorno"></span><span class="date-step-mese"></span>
											#end
											<span class="pallino"></span>
										</div>
										<div class="testo-step">
											<div class="scheda-step">
												<div class="scheda-step-testo">
													$fase.testo.text
													#if ($fase.micro_serv.destination != "")
														<p>
															<a href="$fase.micro_serv.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $content.microserv.link.text">
																<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
																$fase.micro_serv.text
															</a>
														</p>
													#end
												</div>
											</div>
										</div>
									</div>
								#end
							</div>
						</div>
						#end
					#end
					#if ($content.casi_part.text != "")							
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-casi_part"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_casi_part")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.casi_part.text								
							</div>							
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
										<div class="col-md-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($allegato.link.destination != '''')
															<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $allegato.link.text">
																$allegato.link.text
															</a>
														#else
															#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																$allegato.documento.text
															</a>
														#end
														<br />
														<span>$allegato.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.link && $content.link.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-link"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_link")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								#foreach ($sitoesterno in $content.link)
									#if ($sitoesterno.destination != '''')
										#set ($tipofile = $sitoesterno.symbolicLink.getDestType())
										#if ($tipofile == 1)
											#set ($icona = "ponmetroca.svg#ca-open_in_new")
										#end
										#if ($tipofile > 1)
											#set ($icona = "ponmetroca.svg#ca-link")
										#end
										<a href="$sitoesterno.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $sitoesterno.text" #if ($tipofile == 1)target="_blank" #end><svg class="icon icon-link"><use xlink:href="$imgURL/$icona"></use></svg>$sitoesterno.text</a><br />
									#end
								#end
							</div>
						</div>
					#end
					<div data-ng-if="contents.length > 0">
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-micro_serv"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_micro_serv")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli">
								<div class="row">
									<div class="col-lg-6 col-md-12 mb16" data-ng-repeat="elem in contents" data-ng-init="getContent(elem.$,''800009'')" data-ng-bind-html="renderContent[elem.$][''800009'']"></div>
								</div>
							</div>
						</div>
					</div>
					
					#if ($content.parte_di.destination != "" && $content.micro.booleanValue)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-parte_di"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_SRV_parte_di")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli">
								<div class="row">
									<div class="col-lg-6 col-md-12 mb16">
										#set($linkServizio = $content.parte_di.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<span class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy,H:mm")</strong>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800008
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800002, 'SRV', 'Sezioni - In evidenza (cat+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800006, 'SRV', 'Argomento - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800009, 'SRV', 'Altri servizi (cat+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800001, 'SRV', 'Sezioni - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800010, 'SRV', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="#if($content.tipologia.mapKey!='')$content.getPageURL($content.tipologia.mapKey)#end" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800004, 'SRV', 'Argomento (ico+cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> #if ($content.tipologia.mapKey != "") $content.tipologia.mapValue #end
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800007, 'SRV', 'Sezioni - in evidenza (ico+cat+titoletto+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
		#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg> $content.tipologia.mapValue
		</a>
    #else
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
    #end
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 800011, 'SRV', 'Link lista (titolo)', '
<a href="$content.getContentLink()">$content.titolo.text</a><br />
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 800011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 8000013, 'SRV', 'Lista (Tabella), titolo,abs,area (per test)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<th class="tabella-campo-coll" data-th="SERVIZIO" data-thead="SERVIZIO"><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></th>
<td class="tabella-campo-coll" data-th="DESCRIZIONE" data-thead="DESCRIZIONE">$content.descr.text</td>
<td class="tabella-campo-coll" data-th="AREA" data-thead="AREA">$content.area.text</td>
<td class="tabella-campo-coll" data-th="ARGOMENTO" data-thead="ARGOMENTO">	
   <div class="argomenti-sezione-elenco"> 	#set ($argomenti = $content.argomenti.values)
	#foreach ($Category in $content.getCategories())  #if($argomenti.contains($Category.code))($Category.title) #end
	#end
    </div>
</td>
<td class="tabella-campo-coll" data-th="DOCUMENTI" data-thead="DOCUMENTI">	
#if ($content.cosa_serve && $content.cosa_serve.size() > 0)
	#foreach ($box in $content.cosa_serve)
    #if ($box.link.destination != '')
		<a href="$box.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $box.link.text" class="wbreak">$box.link.text</a><br/>
		#end
    #if ($box.documento.attachPath != '')
		  #set ($fileNameSplit = $box.documento.resource.instance.fileName.split("\."))
			#set ($last = $fileNameSplit.size() - 1)
			#set ($fileExt = $fileNameSplit[$last])	
			<a href="$box.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $box.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $box.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $box.documento.resource.instance.fileLength)" target="_blank">
$box.documento.text</a><br/>
		  #end
   #end
#end
</td>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 8000013
) LIMIT 1;
