INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 190002, 'VID', 'Player interno', '
#if (($content.cod_video.text != "") || ($content.cod_canale.text != ""))
  #if ($content.cod_video.text != "")
	<script>
		if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
			$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/$content.cod_video.text");
		}else{
			$("#iframe-$content.getId()").attr("src", "https://www.youtube-nocookie.com/embed/$content.cod_video.text");
		}	
	</script>
  #else
	<script>
		if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
			$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
		}else{
			$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
		}	
	</script>
  #end
<div class="video-interno">
<iframe id="iframe-$content.getId()" width="100%" height="480" src="" frameborder="0" allowfullscreen></iframe>
</div>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 190002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 190003, 'VID', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="intro" data-ng-controller="FiltriController">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
		<div class="row mt40">
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 col-md-3">
				<p class="data-articolo"><span>$i18n.getLabel("CITTAMETRO_CONTENT_DATE"):</span><br /><strong>$content.data.getFormattedDate("dd MMMM yyyy")</strong></p>
			</div>
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 offset-md-1 col-md-3">
				
			</div>
		</div>
	</div>
</section>

<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_VID_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_VID_documenti")</a>
						#end
						#if ($content.link && $content.link.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkcorrelati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_VID_linkcorrelati")">$i18n.getLabel("CITTAMETRO_CONTENT_VID_linkcorrelati")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if (($content.cod_video.text != "") || ($content.cod_canale.text != ""))
						<div id="videoplayer">
							#if ($content.cod_video.text != "")
								<script>
									$(document).ready(function () {
										if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
											$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/$content.cod_video.text");
										}else{
											$("#iframe-$content.getId()").attr("src", "https://www.youtube-nocookie.com/embed/$content.cod_video.text");
										}
									});
								</script>
							#else
								<script>
									$(document).ready(function () {
										if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept"){
											$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
										}else{
											$("#iframe-$content.getId()").attr("src", "https://www.youtube.com/embed/live_stream?channel=$content.cod_canale.text");
										}
									});
								</script>
							#end
							<div class="video-interno">
								<iframe id="iframe-$content.getId()" width="100%" height="480" src="" frameborder="0" allowfullscreen></iframe>
							</div>
						</div>
						<div id="videoplayerh"></div>
					#end
					#if ($content.testo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.testo.text
							</div>
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_VID_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.documenti.attachPath != '''' || $allegato.link.destination != '''')
										<div class="col-md-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($allegato.documenti.attachPath != '''')
															#set ($fileNameSplit = $allegato.documenti.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$allegato.documenti.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documenti.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documenti.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documenti.resource.instance.fileLength)" target="_blank">
																$allegato.documenti.text
															</a>
														#end
														#if ($allegato.link.destination != '''')
														<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.link.text">
															$allegato.link.text
														</a>
														#end
														<br />
														<span>$allegato.desc.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.link && $content.link.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-linkcorrelati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_VID_linkcorrelati")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								#set ($contalink = 1)
								#foreach ($altrilink in $content.link)
									#if ($altrilink.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$altrilink.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $altrilink.text" #if($altrilink.symbolicLink.destType == 1)target="_blank" #end><strong>$altrilink.text</strong></a>
											</article>
										</div>
										#set ($contalink = $contalink + 1)
									#end
									#if ($contalink % 2 != 0 || $foreach.count==$content.link.size())
										</div>
									#end
								#end
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 190003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 190001, 'VID', 'Elenco (ico+titolo+abs+img)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-video scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
      $content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo">
  	
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 	
	</div>
	<div class="scheda-anteprima img-fit-cover">
		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.foto.text">
        <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-play"></use></svg>
 			</figure>
 		</a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 190001
) LIMIT 1;
