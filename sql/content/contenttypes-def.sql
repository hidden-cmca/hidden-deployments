UPDATE sysconfig SET config = '<?xml version="1.0" encoding="UTF-8"?>
<contenttypes>
         <contenttype typecode="BNR" typedescr="Banner - Ent" viewpage="**NULL**" listmodel="10023" defaultmodel="10003">
                 <attributes>
                         <attribute name="title" attributetype="Text" description="Title" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">title</property>
                                         </properties>
                                 </names>
                                 <validations>
                                         <required>true</required>
                                 </validations>
                                 <roles>
                                         <role>jacms:title</role>
                                 </roles>
                         </attribute>
                         <attribute name="abstract" attributetype="Hypertext" description="Abstract" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">abstract</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="link" attributetype="Link" description="Link">
                                 <names>
                                         <properties>
                                                 <property key="en">link</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="image" attributetype="Image" description="Image">
                                 <names>
                                         <properties>
                                                 <property key="en">image</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="placement" attributetype="Enumerator" description="Image Placement" separator=",">
                                 <names>
                                         <properties>
                                                 <property key="en">placement</property>
                                         </properties>
                                 </names>
                                 <![CDATA[LEFT,RIGHT]]>
                         </attribute>
                 </attributes>
         </contenttype>
         <contenttype typecode="NWS" typedescr="News - Ent" viewpage="**NULL**" listmodel="10022" defaultmodel="10002">
                 <attributes>
                         <attribute name="date" attributetype="Date" searchable="true">
                                 <names>
                                         <properties>
                                                 <property key="en">date</property>
                                         </properties>
                                 </names>
                                 <validations>
                                         <required>true</required>
                                 </validations>
                         </attribute>
                         <attribute name="title" attributetype="Text" searchable="true" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">title</property>
                                         </properties>
                                 </names>
                                 <validations>
                                         <required>true</required>
                                 </validations>
                                 <roles>
                                         <role>jacms:title</role>
                                 </roles>
                         </attribute>
                         <attribute name="abstract" attributetype="Hypertext" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">abstract</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="body" attributetype="Hypertext" description="Main Body" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">body</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="image" attributetype="Image">
                                 <names>
                                         <properties>
                                                 <property key="en">image</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="link" attributetype="Link" description="Link">
                                 <names>
                                         <properties>
                                                 <property key="en">link</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="category" attributetype="Text" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">category</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="direction" attributetype="Enumerator" description="Direction or orientation of the content" separator=",">
                                 <names>
                                         <properties>
                                                 <property key="en">direction</property>
                                         </properties>
                                 </names>
                                 <![CDATA[VERTICAL,HORIZONTAL]]>
                         </attribute>
                         <attribute name="argomenti" attributetype="EnumeratorMapCategory" description="Tassonomia argomenti" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
                                 <validations />
                         </attribute>
                 </attributes>
         </contenttype>
         <contenttype typecode="TCL" typedescr="2 columns" viewpage="**NULL**" listmodel="10024" defaultmodel="10004">
                 <attributes>
                         <attribute name="image" attributetype="Image" description="Image">
                                 <names>
                                         <properties>
                                                 <property key="en">image</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="title" attributetype="Text" description="Title" searchable="true" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">title</property>
                                         </properties>
                                 </names>
                                 <validations>
                                         <required>true</required>
                                 </validations>
                                 <roles>
                                         <role>jacms:title</role>
                                 </roles>
                         </attribute>
                         <attribute name="subtitle" attributetype="Hypertext" description="Subtitle" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">subtitle</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="col1" attributetype="Hypertext" description="Column 1 content" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">col1</property>
                                         </properties>
                                 </names>
                         </attribute>
                         <attribute name="col2" attributetype="Hypertext" description="Column 2 content" indexingtype="TEXT">
                                 <names>
                                         <properties>
                                                 <property key="en">col2</property>
                                         </properties>
                                 </names>
                         </attribute>
                 </attributes>                
		</contenttype>   
        <contenttype typecode="GAL" typedescr="Galleria immagini" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="data" attributetype="Date" description="Data" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="anteprima" attributetype="Image" description="Anteprima">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<list name="immagini" attributetype="Monolist" description="Immagini">
				<nestedtype>
					<attribute name="immagini" attributetype="Composite">
						<attributes>
							<attribute name="immagine" attributetype="Image" description="Immagine" />
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
        <contenttype typecode="ORG" typedescr="Organizzazione" viewpage="organizzazione" listmodel="220020" defaultmodel="220001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_01_01;amm_01_02;amm_01_02_02;amm_01_02_03;amm_01_03;amm_01_03_02;amm_02;amm_03;amm_06" showChildsPage="false">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="competenze" attributetype="Hypertext" description="Competenze" indexingtype="TEXT" />
			<list name="assessor" attributetype="Monolist" description="Assessore di riferimento">
				<nestedtype>
					<attribute name="assessor" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="resp" attributetype="Link" description="Responsabile">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="persone" attributetype="Monolist" description="Persone che compongono la struttura">
				<nestedtype>
					<attribute name="persone" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="legami" attributetype="Monolist" description="Legami con altre strutture">
				<nestedtype>
					<attribute name="legami" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="sede" attributetype="Link" description="Sede">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="indirizzo" attributetype="Text" description="Indirizzo" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="ind_altro" attributetype="Text" description="Piano/Scala/Stanza" indexingtype="TEXT" />
			<attribute name="orario" attributetype="Composite" description="Orario per il pubblico">
				<attributes>
					<attribute name="inv_matt" attributetype="Text" description="Invernale mattina" />
					<attribute name="inv_pom" attributetype="Text" description="Invernale pomeriggio" />
					<attribute name="est_matt" attributetype="Text" description="Estivo mattina" />
					<attribute name="est_pom" attributetype="Text" description="Estivo pomeriggio" />
					<attribute name="continuo" attributetype="Text" description="Continuato" />
				</attributes>
			</attribute>
			<attribute name="gps_lat" attributetype="Monotext" description="Latitudine" indexingtype="TEXT" />
			<attribute name="gps_lon" attributetype="Monotext" description="Longitudine" indexingtype="TEXT" />
			<attribute name="cap" attributetype="Monotext" description="CAP" indexingtype="TEXT" />
			<attribute name="telefono" attributetype="Monotext" description="Telefono" indexingtype="TEXT" />
			<attribute name="email" attributetype="Monotext" description="E-mail" indexingtype="TEXT" />
			<attribute name="pec" attributetype="Monotext" description="PEC" indexingtype="TEXT" />
			<list name="sch_persone" attributetype="Monolist" description="Persone da contattare">
				<nestedtype>
					<attribute name="sch_persone" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento" />
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT" />
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype> 
	<contenttype typecode="NVT" typedescr="News" viewpage="notizia" listmodel="100013" defaultmodel="100001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo della news" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="nov_01;nov_02;nov_04" showChildsPage="false">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="allerta" attributetype="EnumeratorMap" description="Tipologia di allerta meteo" searchable="true" indexingtype="TEXT" separator=";"><![CDATA[1=Gialla;2=Arancione;3=Rossa;]]></attribute>
			<attribute name="foto" attributetype="Image" description="Immagine in evidenza">
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="pos_foto" attributetype="EnumeratorMap" description="Posizione immagine" separator=";"><![CDATA[top=Allineamento in alto;center=Centrata;bottom=Allineamento in basso;]]></attribute>
			<attribute name="dim_foto" attributetype="Boolean" description="Immagine grande" />
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="data" attributetype="Date" description="Data della news" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<list name="persone" attributetype="Monolist" description="Persone">
				<nestedtype>
					<attribute name="persone" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="luoghi" attributetype="Monolist" description="Luoghi">
				<nestedtype>
					<attribute name="luoghi" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="testo" attributetype="Hypertext" description="Corpo della news" indexingtype="TEXT" />
			<attribute name="galleria" attributetype="Link" description="Galleria di immagini">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="video" attributetype="Link" description="Video evento">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento" />
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="link_est" attributetype="Monolist" description="Link utili">
				<nestedtype>
					<attribute name="link_est" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="ARG" typedescr="Argomento" viewpage="arg_01_dett" listmodel="400011" defaultmodel="400004">
		<attributes>
			<attribute name="icona" attributetype="Text" description="Icona" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
				</roles>
			</attribute>
			<attribute name="argomento" attributetype="EnumeratorMapCategory" description="Argomento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:argomento</role>
				</roles>
			</attribute>
			<list name="gestione" attributetype="Monolist" description="Gestione">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
				<nestedtype>
					<attribute name="gestione" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<list name="cont_home" attributetype="Monolist" description="Contenuti homepage">
				<nestedtype>
					<attribute name="cont_home" attributetype="Link" />
				</nestedtype>
			</list>
			<attribute name="link_pag" attributetype="Link" description="Link a pagina" />
		</attributes>
	</contenttype>
	<contenttype typecode="DOC" typedescr="Documento" viewpage="documento" listmodel="700008" defaultmodel="700007">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="documenti" showChildsPage="true">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="sottotip" attributetype="EnumeratorMapPage" description="Sottotipologia del documento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="doc_01;doc_04;doc_07;doc_07_02;doc_09" showChildsPage="true">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(((!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07") &&!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09") && #attribute.getMapKey().equals(""))) || (((#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09"))) && (!#attribute.getMapKey().equals(""))) && (#attribute.getMapKey().contains(#entity.getAttribute(''tipologia'').getMapKey())))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario selezionare una Sottotipologia solo in caso di tipologia "Albo pretorio" o "Atti normativi" o "Documenti (tecnici) di supporto" o "Bandi" e che siano congruenti con il campo tipologia. Selezionare un valore differente.]]></errormessage>
						<helpmessage><![CDATA[Inserire una diversa Sottotipologia oppure cambiare la scelta della Tipologia.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="oggetto" attributetype="Longtext" description="Oggetto" indexingtype="TEXT" />
			<attribute name="descr_est" attributetype="Hypertext" description="Informazioni e modalità" indexingtype="TEXT" />
			<attribute name="data_pubb" attributetype="Date" description="Data di pubblicazione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="data_scad" attributetype="Date" description="Data di scadenza" searchable="true" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate()))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, Data di scadenza obbligatoria per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
						<helpmessage><![CDATA[Inserire la Data di scadenza (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
					</expression>
				</validations>
				<roles>
					<role>jpattributeextended:datascadenza</role>
				</roles>
			</attribute>
			<attribute name="data_esito" attributetype="Date" description="Data esito bando (esclusivo per i bandi)" searchable="true" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null)]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, Data esito è valida solo per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
						<helpmessage><![CDATA[Inserire la Data esito corretta (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Documenti principali">
				<validations>
					<required>true</required>
				</validations>
				<nestedtype>
					<attribute name="documenti" attributetype="Attach">
						<validations />
					</attribute>
				</nestedtype>
			</list>
			<list name="formati" attributetype="Monolist" description="Formati disponibili">
				<nestedtype>
					<attribute name="formati" attributetype="Enumerator" separator=",">
						<validations>
							<required>true</required>
						</validations>
						<![CDATA[PDF,PDF editabile,ODT,ODS,DOC,DOCX,XLS,XLSX,RTF,Altro,online]]>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="canal_link" attributetype="Link" description="Canale digitale servizio collegato" />
			<list name="fasi" attributetype="Monolist" description="Fasi intermedie">
				<nestedtype>
					<attribute name="fasi" attributetype="Composite">
						<attributes>
							<attribute name="fs_accorpa" attributetype="Boolean" description="Accorpa alla riga precedente" />
							<attribute name="fs_data" attributetype="Date" description="Data fase">
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(((toString() != "") && (!#parent.getAttribute(''fs_accorpa'').booleanValue)) || ((#parent.getAttribute(''fs_accorpa'').booleanValue) && (toString() == "")))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, la data è obbligatoria se non accorpata alla precedente.]]></errormessage>
										<helpmessage><![CDATA[Inserire la data della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_testo" attributetype="Text" description="Descrizione">
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[((text != "") || (#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, la descrizione è obbligatoria. Inserire un testo.]]></errormessage>
										<helpmessage><![CDATA[Inserire la descrizione della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_alleg" attributetype="Attach" description="Allegato">
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(((attachPath != "") || (#parent.getAttribute(''fs_link'').destination != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, un allegato o link è obbligatorio. Inserire un file.]]></errormessage>
										<helpmessage><![CDATA[Inserire un allegato alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_link" attributetype="Link" description="Link">
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(((destination != "") || (#parent.getAttribute(''fs_alleg'').attachPath != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, un link o alllegato è obbligatorio. Inserire un link.]]></errormessage>
										<helpmessage><![CDATA[Inserire un link alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_datasc" attributetype="Date" description="Data scadenza file" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="allegati" attributetype="Monolist" description="Documenti allegati">
				<nestedtype>
					<attribute name="allegati" attributetype="Composite">
						<attributes>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="allegato" attributetype="Attach" description="Allegato" />
							<attribute name="descr" attributetype="Text" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="servizi" attributetype="Monolist" description="Servizi collegati">
				<nestedtype>
					<attribute name="servizi" attributetype="Composite">
						<attributes>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione" />
							<attribute name="link" attributetype="Link" description="Link">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''SRV'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Servizio non valido. Inserire un link a contenuto di tipo Servizio (SRV).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Servizio.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ufficio" attributetype="LinkSearchable" description="Ufficio responsabile del documento" searchable="true" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="area" attributetype="LinkSearchable" description="Area responsabile del documento" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="dataset" attributetype="Monolist" description="Dataset">
				<nestedtype>
					<attribute name="dataset" attributetype="Link" />
				</nestedtype>
			</list>
			<attribute name="autori" attributetype="Text" description="Autore/i" indexingtype="TEXT" />
			<attribute name="licenza" attributetype="Text" description="Licenza di distribuzione" indexingtype="TEXT" />
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT" />
			<list name="rif_norma" attributetype="Monolist" description="Riferimenti normativi">
				<nestedtype>
					<attribute name="rif_norma" attributetype="Link" />
				</nestedtype>
			</list>
			<attribute name="protoc" attributetype="Monotext" description="Protocollo" indexingtype="TEXT" />
			<attribute name="protoc_dt" attributetype="Date" description="Data protocollo" />
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="galleria" attributetype="Link" description="Immagini">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="EVN" typedescr="Evento" viewpage="evento" listmodel="210003" defaultmodel="210001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="sottotit" attributetype="Text" description="Titolo alternativo/Sottotitolo" indexingtype="TEXT" />
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia dell evento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="nov_03" showChildsPage="true">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="pos_img" attributetype="EnumeratorMap" description="Posizione immagine" separator=";"><![CDATA[top=Allineamento in alto;center=Centrata;bottom=Allineamento in basso;]]></attribute>
			<attribute name="dim_img" attributetype="Boolean" description="Immagine grande" />
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="genitore" attributetype="Boolean" description="Evento genitore">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="vedi_cal" attributetype="Boolean" description="Vedi il calendario eventi" />
			<attribute name="parte_di" attributetype="Link" description="Parte di">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''EVN'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Evento genitore. Inserire un link a contenuto di tipo Evento (EVN).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Evento.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="data_pubb" attributetype="Date" description="Data di pubblicazione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="intro" attributetype="Hypertext" description="Introduzione" indexingtype="TEXT" />
			<attribute name="galleria" attributetype="Link" description="Galleria di immagini">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="video" attributetype="Link" description="Video evento">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="desc_dest" attributetype="Hypertext" description="Descrizione dei destinatari" indexingtype="TEXT" />
			<list name="persone" attributetype="Monolist" description="Persone dell amministrazione">
				<nestedtype>
					<attribute name="persone" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="luogo" attributetype="Composite" description="Luogo">
				<validations>
					<required>true</required>
				</validations>
				<attributes>
					<attribute name="sch_luogo" attributetype="Link" description="Scheda luogo">
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="nome" attributetype="Text" description="Nome del luogo" indexingtype="TEXT">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata.]]></errormessage>
								<helpmessage><![CDATA[Inserire il nome del Luogo se non presente la scheda collegata.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="indirizzo" attributetype="Text" description="Indirizzo" indexingtype="TEXT">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "" && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata oppure se Macro evento.]]></errormessage>
								<helpmessage><![CDATA[Inserire l indirizzo del Luogo se non presente la scheda collegata oppure impostare Macro evento.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="cap" attributetype="Monotext" description="CAP" indexingtype="TEXT">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata.]]></errormessage>
								<helpmessage><![CDATA[Inserire il CAP del Luogo se non presente la scheda collegata.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="quartiere" attributetype="Text" description="Quartiere" indexingtype="TEXT" />
					<attribute name="circoscr" attributetype="Text" description="Circoscrizione" indexingtype="TEXT" />
					<attribute name="gps_lat" attributetype="Monotext" description="Latitudine" indexingtype="TEXT">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "" && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata oppure se Macro evento.]]></errormessage>
								<helpmessage><![CDATA[Inserire le coordinate GPS del Luogo se non presente la scheda collegata oppure impostare Macro evento.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="gps_lon" attributetype="Monotext" description="Longitudine" indexingtype="TEXT">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "" && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata oppure se Macro evento.]]></errormessage>
								<helpmessage><![CDATA[Inserire le coordinate GPS del Luogo se non presente la scheda collegata oppure impostare Macro evento.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="data_iniz" attributetype="Date" description="Data di inizio evento" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="data_fine" attributetype="Date" description="Data di fine evento" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<rangestart attribute="data_iniz" />
				</validations>
				<roles>
					<role>jpattributeextended:datascadenza</role>
				</roles>
			</attribute>
			<attribute name="ricorrenza" attributetype="Composite" description="Ricorrenza settimanale">
				<attributes>
					<attribute name="ric_lun" attributetype="CheckBox" description="Lunedì" />
					<attribute name="ric_mar" attributetype="CheckBox" description="Martedì" />
					<attribute name="ric_mer" attributetype="CheckBox" description="Mercoledì" />
					<attribute name="ric_gio" attributetype="CheckBox" description="Giovedì" />
					<attribute name="ric_ven" attributetype="CheckBox" description="Venerdì" />
					<attribute name="ric_sab" attributetype="CheckBox" description="Sabato" />
					<attribute name="ric_dom" attributetype="CheckBox" description="Domenica" />
				</attributes>
			</attribute>
			<attribute name="orari" attributetype="Hypertext" description="Orari" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="agg_cal" attributetype="Boolean" description="Aggiungi al calendario" />
			<list name="prezzo" attributetype="Monolist" description="Prezzo">
				<nestedtype>
					<attribute name="prezzo" attributetype="Composite">
						<attributes>
							<attribute name="tipo" attributetype="Enumerator" description="Tipo biglietto" separator=",">
								<validations>
									<required>true</required>
								</validations>
								<![CDATA[Intero,Ridotto,Gruppi,Cumulativo,Gratuito]]>
							</attribute>
							<attribute name="costo" attributetype="Monotext" description="Costo">
								<validations>
									<required>true</required>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[((text.matches("^([0-9]+,[0-9][0-9]$)") && !text.equals("0,00") && !#parent.getAttribute("tipo").text.equals("Gratuito")) || (text.equals("0") && #parent.getAttribute("tipo").text.equals("Gratuito")))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, inserire il valore col formato ''xx,yy'' o ''0'' in caso di tipo Gratuito.]]></errormessage>
										<helpmessage><![CDATA[Inserire un valore corretto (''0'' oppure ''100,50''.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="info_bigl" attributetype="Hypertext" description="Info biglietti" />
			<attribute name="link_bigl" attributetype="Link" description="Link acquisto biglietti" />
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento" />
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="organizz" attributetype="Composite" description="Organizzato da">
				<attributes>
					<attribute name="link_uff" attributetype="Link" description="Scheda ufficio">
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="soggetto" attributetype="Text" description="Soggetto" indexingtype="TEXT">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''organizz'').getAttribute(''link_uff'').destination.equals("") && text == "") || (#entity.getAttribute(''organizz'').getAttribute(''link_uff'').destination.equals("") && text != "")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda dell Organizzazione collegata.]]></errormessage>
								<helpmessage><![CDATA[Inserire il nome del Soggetto se non presente la scheda collegata.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="persona" attributetype="Text" description="Persona" indexingtype="TEXT" />
					<attribute name="telefono" attributetype="Monotext" description="Telefono" indexingtype="TEXT" />
					<attribute name="reperib" attributetype="Text" description="Reperibilità" indexingtype="TEXT" />
					<attribute name="email" attributetype="Monotext" description="Email" indexingtype="TEXT" />
					<attribute name="sito_web" attributetype="Link" description="Sito web" />
				</attributes>
			</attribute>
			<attribute name="supporto" attributetype="Link" description="Supportato da">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="figli" attributetype="Monolist" description="Lista eventi figli">
				<nestedtype>
					<attribute name="figli" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[(destination.contains(''EVN'') && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Evento non valido o il contenuto non può avere figli. Inserire un link a contenuto di tipo Evento (EVN) oppure impostare il campo Genitore.]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Evento oppure modificare il valore del campo Genitore.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT" />
			<attribute name="patro_den" attributetype="Text" description="Ente che patrocina l evento" indexingtype="TEXT" />
			<attribute name="patro_link" attributetype="Link" description="Ente che patrocina l evento - sito web" />
			<attribute name="patro_logo" attributetype="Image" description="Ente che patrocina l evento - logo" />
			<list name="sponsor" attributetype="Monolist" description="Sponsor">
				<nestedtype>
					<attribute name="sponsor" attributetype="Composite">
						<attributes>
							<attribute name="denominaz" attributetype="Text" description="Denominazione" />
							<attribute name="sitoweb" attributetype="Link" description="Sito web" />
							<attribute name="logo" attributetype="Image" description="Immagine logo" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="str_polit" attributetype="Monolist" description="Struttura politica coinvolta">
				<nestedtype>
					<attribute name="str_polit" attributetype="Composite">
						<attributes>
							<attribute name="pol_nome" attributetype="Text" description="Nome" />
							<attribute name="pol_scheda" attributetype="Link" description="Link a scheda">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="PRS" typedescr="Persona" viewpage="persona" listmodel="230014" defaultmodel="230011">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="nome" attributetype="Text" description="Nome" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="cognome" attributetype="Text" description="Cognome" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Ruolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Foto della persona">
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione di appartenenza" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_01_01;amm_01_02;amm_01_03;amm_05;" showChildsPage="false">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMap" description="Tipologia persona" searchable="true" indexingtype="TEXT" separator=";">
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[PP=(PP) Persona Politica;PA=(PA) Persona Amministrativa]]>
			</attribute>
			<attribute name="ruolo" attributetype="EnumeratorMap" description="Ruolo nell organizzazione" searchable="true" separator=";">
				<validations>
					<required>true</required>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#attribute.getMapKey().contains(#entity.getAttribute(''tipologia'').getMapKey()))]]></ognlexpression>
						<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
						<helpmessage><![CDATA[Valorizzare il ruolo corretto in base alla scelta della Tipologia di persona]]></helpmessage>
					</expression>
				</validations>
				<![CDATA[PP1=(PP) Consigliere;PP2=(PP) Consigliere Municipalità;PP3=(PP) Assessore;PP4=(PP) Sindaco;PP5=(PP) Vicesindaco;PP6=(PP) Presidente del Consiglio; PP7=(PP) Vice Presidente del Consiglio;PP8=(PP) Vice Presidente Vicario del Consiglio;PP9=(PP) Presidente Municipalità;PA1=(PA) Assistente sociale;PA2=(PA) Avvocato;PA3=(PA) Collaboratore amministrativo;PA4=(PA) Collaboratore tecnico;PA5=(PA) Comandante polizia municipale;PA6=(PA) Direttore generale;PA7=(PA) Dirigente;PA8=(PA) Dirigente tecnico;PA9=(PA) Funzionario tecnico titolare di posizione organizzativa;PA10=(PA) Funzionario titolare di posizione organizzativa;PA11=(PA) Funzionario amministrativo addetto stampa;PA12=(PA) Funzionario amministrativo;PA13=(PA) Funzionario psicologo;PA14=(PA) Funzionario tecnico;PA15=(PA) Funzionario veterinario;PA16=(PA) Istruttore informatico programmatore;PA17=(PA) Istruttore amministrativo addetto stampa;PA18=(PA) Istruttore tecnico;PA19=(PA) Istruttore agente di polizia municipale;PA20=(PA) Istruttore amministrativo;PA21=(PA) Segretario generale;PA22=(PA) Operatore;PA23=(PA) Esecutore amministrativo;PA24=(PA) Esecutore tecnico;PA25=(PA) Esecutore guardia giurata;PA26=(PA) Titolare di posizione organizzativa con delega dirigenziale;]]>
			</attribute>
			<list name="coll_liv1" attributetype="Monolist" description="Collegamenti all organizzazione (I livello)">
				<nestedtype>
					<attribute name="coll_liv1" attributetype="Link">
						<validations>
							<required>true</required>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="coll_liv2" attributetype="Monolist" description="Collegamenti all organizzazione (II livello)">
				<nestedtype>
					<attribute name="coll_liv2" attributetype="Link">
						<validations>
							<required>true</required>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ruolo_desc" attributetype="Hypertext" description="Competenze" />
			<attribute name="deleghe" attributetype="Hypertext" description="Deleghe" />
			<attribute name="data_insed" attributetype="Date" description="Data inizio" searchable="true">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[((#entity.getAttribute(''tipologia'').getMapKey() == ''PP'') && #attribute.getDate() != null) || (#entity.getAttribute(''tipologia'').getMapKey() == ''PA'')]]></ognlexpression>
						<errormessage><![CDATA[Attributo obbligatorio per il tipo di persona scelto]]></errormessage>
						<helpmessage><![CDATA[Inserire la data oppure variare la scelta della Tipologia di persona]]></helpmessage>
					</expression>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="data_fine" attributetype="Date" description="Data fine" searchable="true">
				<validations>
					<rangestart attribute="data_insed" />
				</validations>
				<roles>
					<role>jpattributeextended:datascadenza</role>
				</roles>
			</attribute>
			<attribute name="galleria" attributetype="Link" description="Foto attività politica">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="telefono" attributetype="Monotext" description="Numero di telefono" searchable="true" indexingtype="TEXT" />
			<attribute name="email" attributetype="Monotext" description="Indirizzo email" indexingtype="TEXT" />
			<list name="atto_nom" attributetype="Monolist" description="Atto di nomina / proclamazione / incarico">
				<nestedtype>
					<attribute name="atto_nom" attributetype="Attach" />
				</nestedtype>
			</list>
			<attribute name="cv" attributetype="Attach" description="Curriculum Vitae" />
			<list name="compensi" attributetype="Monolist" description="Compensi connessi all assunzione della carica">
				<nestedtype>
					<attribute name="compensi" attributetype="Attach" />
				</nestedtype>
			</list>
			<list name="importi" attributetype="Monolist" description="Spese di missione">
				<nestedtype>
					<attribute name="importi" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="altre_car" attributetype="Monolist" description="Altre cariche e incarichi">
				<nestedtype>
					<attribute name="altre_car" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="patrim_sit" attributetype="Monolist" description="Situazione patrimoniale e variazioni">
				<nestedtype>
					<attribute name="patrim_sit" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA26'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="redditi" attributetype="Monolist" description="Dichiarazione dei redditi titolare">
				<nestedtype>
					<attribute name="redditi" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA26'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="patrim_var" attributetype="Monolist" description="Situazione patrimoniale dopo cessazione">
				<nestedtype>
					<attribute name="patrim_var" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() !=''PA26'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="redditi_pa" attributetype="Monolist" description="Dichiarazione dei redditi coniuge e parenti">
				<nestedtype>
					<attribute name="redditi_pa" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA26'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="spese_elet" attributetype="Monolist" description="Spese elettorali">
				<nestedtype>
					<attribute name="spese_elet" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[((((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == '')) && (#entity.getAttribute(''tipologia'').getMapKey() == ''PP''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="insussist" attributetype="Monolist" description="Dich. insussistenza cause di inconf. e incompat.">
				<nestedtype>
					<attribute name="insussist" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="emolumenti" attributetype="Monolist" description="Emolumenti complessivi percepiti">
				<nestedtype>
					<attribute name="emolumenti" attributetype="Attach">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT" />
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="SRV" typedescr="Servizio" viewpage="servizio" listmodel="800010" defaultmodel="800008">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="sottotit" attributetype="Text" description="Titolo alternativo/Sottotitolo" indexingtype="TEXT" />
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia del servizio" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="servizi" showChildsPage="true">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="stato" attributetype="Enumerator" description="Stato" separator=",">
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[Attivo,Disattivo]]>
			</attribute>
			<attribute name="stato_mot" attributetype="Longtext" description="Motivo dello stato">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#entity.getAttribute("stato").getText().equals("Attivo")) || (#entity.getAttribute("stato").getText().equals("Disattivo") && (! #attribute.getText().equals("")))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario inserire il Motivo dello stato Disattivo.]]></errormessage>
						<helpmessage><![CDATA[Inserire una motivazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="micro" attributetype="Boolean" description="Micro servizio" searchable="true">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="parte_di" attributetype="LinkSearchable" description="Parte di" searchable="true" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''micro'').booleanValue && destination.equals("")) || (destination.contains(''SRV'') && #entity.getAttribute(''micro'').booleanValue)]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Servizio macro. Inserire un link a contenuto di tipo Servizio (SRV) oppure impostare il servizio come Macro servizio.]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento ad un Macro servizio, oppure cambia il valore a Macro servizio.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="descr_est" attributetype="Hypertext" description="Descrizione estesa" indexingtype="TEXT" />
			<attribute name="desc_dest" attributetype="Hypertext" description="Descrizione dei destinatari" indexingtype="TEXT" />
			<attribute name="cop_geo" attributetype="Hypertext" description="Copertura geografica" indexingtype="TEXT" />
			<attribute name="come_si_fa" attributetype="Hypertext" description="Come si fa" indexingtype="TEXT" />
			<attribute name="esito" attributetype="Hypertext" description="Output/Cosa si ottiene" indexingtype="TEXT" />
			<attribute name="proc_esito" attributetype="Composite" description="Procedure collegate all esito">
				<attributes>
					<attribute name="testo" attributetype="Hypertext" description="Testo" indexingtype="TEXT" />
					<attribute name="link_sch" attributetype="Link" description="Link scheda">
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[(destination.contains(''ORG'')) || (destination.contains(''LGO''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione o Luogo non valido. Inserire un link a contenuto di tipo Organizzazione (ORG) o Luogo (LGO).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione o Luogo.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="can_digit" attributetype="Composite" description="Canale digitale">
				<attributes>
					<attribute name="testo" attributetype="Hypertext" description="Testo" indexingtype="TEXT" />
					<attribute name="link" attributetype="Link" description="Link" />
				</attributes>
			</attribute>
			<attribute name="autenticaz" attributetype="Hypertext" description="Autenticazione" indexingtype="TEXT" />
			<attribute name="can_fisico" attributetype="Hypertext" description="Canale fisico" indexingtype="TEXT" />
			<attribute name="can_prenot" attributetype="Link" description="Canale fisico - prenotazione" />
			<list name="cosa_serve" attributetype="Monolist" description="Cosa serve (documenti richiesti)">
				<nestedtype>
					<attribute name="cosa_serve" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Descrizione" indexingtype="TEXT" />
							<attribute name="documento" attributetype="Attach" description="Documento" />
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="costi" attributetype="Hypertext" description="Costi" indexingtype="TEXT" />
			<attribute name="vincoli" attributetype="Hypertext" description="Vincoli" indexingtype="TEXT" />
			<attribute name="scadenze" attributetype="Hypertext" description="Fasi e scadenze" indexingtype="TEXT" />
			<list name="scad_fasi" attributetype="Monolist" description="Fasi e scadenze">
				<nestedtype>
					<attribute name="scad_fasi" attributetype="Composite">
						<attributes>
							<attribute name="data" attributetype="Date" description="Data" />
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="micro_serv" attributetype="Link" description="Macro o Micro servizio">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''SRV'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Servizio genitore. Inserire un link a contenuto di tipo Servizio (SRV).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Servizio.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="casi_part" attributetype="Hypertext" description="Casi particolari" indexingtype="TEXT" />
			<list name="uffici" attributetype="Monolist" description="Uffici">
				<validations>
					<required>true</required>
				</validations>
				<nestedtype>
					<attribute name="uffici" attributetype="LinkSearchable" searchable="true" indexingtype="TEXT">
						<validations>
							<required>true</required>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="area" attributetype="LinkSearchable" description="Area" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Altri documenti">
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento" />
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="Link a siti esterni">
				<nestedtype>
					<attribute name="link" attributetype="Link" />
				</nestedtype>
			</list>
			<attribute name="cod_ipa" attributetype="EnumeratorMap" description="Codice dell’Ente erogatore (IPA)" separator=";">
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[c_b354=Comune di Cagliari;cmdca=Citta Metropolitana di Cagliari;r_sardeg=Regione Autonoma della Sardegna;uds_ca=Universita degli Studi di Cagliari]]>
			</attribute>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="TGN" typedescr="Testo generico" viewpage="**NULL**" listmodel="200010" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<list name="paragrafo" attributetype="Monolist" description="Paragrafo">
				<nestedtype>
					<attribute name="paragrafo" attributetype="Composite">
						<attributes>
							<attribute name="titolo" attributetype="Text" description="Titolo" />
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="immagine" attributetype="Image" description="Immagine" />
							<attribute name="link" attributetype="Link" description="Link" />
							<attribute name="link_txt" attributetype="Link" description="Link testo" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="allegati" attributetype="Monolist" description="Allegati">
				<nestedtype>
					<attribute name="allegati" attributetype="Attach" />
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="Link">
				<nestedtype>
					<attribute name="link" attributetype="Link" />
				</nestedtype>
			</list>
			<attribute name="video" attributetype="Link" description="Video">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine" />
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione di appartenenza" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="homepage;amministrazione;amm_01;servizi;novita;documenti;argomenti;" showChildsPage="true">
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="icona" attributetype="Text" description="Icona" />
			<attribute name="link_pag" attributetype="Link" description="Link a pagina di dettaglio" />
		</attributes>
	</contenttype>
	<contenttype typecode="VID" typedescr="Video" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="tipologia video" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="arc_vid_01;arc_vid_02" showChildsPage="false">
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Anteprima">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="data" attributetype="Date" description="Data" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="cod_video" attributetype="Monotext" description="Codice video">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!text.equals("") && #entity.getAttribute(''cod_canale'').text.equals("")) || (text.equals("") && !#entity.getAttribute(''cod_canale'').text.equals(""))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario inserire solo una sorgente video.]]></errormessage>
						<helpmessage><![CDATA[Compilare solo uno dei campi sorgente video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="cod_canale" attributetype="Monotext" description="Codice canale per streaming">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!text.equals("") && #entity.getAttribute(''cod_video'').text.equals("")) || (text.equals("") && !#entity.getAttribute(''cod_video'').text.equals(""))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario inserire solo una sorgente video.]]></errormessage>
						<helpmessage><![CDATA[Compilare solo uno dei campi sorgente video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="testo" attributetype="Hypertext" description="Testo" />
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<attributes>
							<attribute name="documenti" attributetype="Attach" description="Documento" />
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento ad una scheda documento (DOC)]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="desc" attributetype="Text" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="Altri link">
				<nestedtype>
					<attribute name="link" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="BAN" typedescr="Banner" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine" />
			<attribute name="sfondo" attributetype="EnumeratorMap" description="Colore sfondo" separator=";"><![CDATA[bg1=Verde;bg2=Blu;bg3=Arancio;bg4=Rosso;bg5=Petrolio;bg6=Mattone;bg7=Carta da zucchero;bg8=Marroncino;bg9=Viola]]></attribute>
			<attribute name="num_verde" attributetype="Text" description="Numero verde" />
			<attribute name="link_arg" attributetype="EnumeratorMapCategory" description="Link argomento" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti" />
			<attribute name="link" attributetype="Link" description="Link titolo" />
			<list name="link_puls" attributetype="Monolist" description="Link pulsanti">
				<nestedtype>
					<attribute name="link_puls" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="CNV" typedescr="Convocazione" viewpage="convocazioni_dettaglio" listmodel="110003" defaultmodel="110002">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo convocazione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="desc" attributetype="Text" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
				</roles>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione portale" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_01_02_05" showChildsPage="false">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMap" description="Organo" searchable="true" indexingtype="TEXT" separator=";">
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[01=Consiglio;  02=Ccp Affari generali, pubblica istruzione, politiche universitarie e del diritto allo studio, politiche giovanili; 03=Ccp Attività produttive, turismo e promozione del territorio; 04=Ccp Bilancio, tributi, società partecipate, personale; 05=Ccp Cultura, spettacolo e verde pubblico; 06=Ccp Innovazione tecnologica, ambiente e politiche del mare; 07=Ccp Lavori pubblici; 08=Ccp Pari opportunità; 09=Ccp Pianificazione strategica e dello sviluppo urbanistico; 10=Ccp Politiche della sicurezza, sport e patrimonio; 11=Ccp Politiche per la mobilità, della casa e dei servizi tecnologici; 12=Ccp Politiche sociali, del benessere e della famiglia; 13=Ccp Statuto e regolamenti; 14=Ccp Valutazione delle politiche comunali e la qualità dei servizi]]>
			</attribute>
			<attribute name="numero" attributetype="Monotext" description="Numero convocazione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="data" attributetype="Timestamp" description="Data e ora" searchable="true" indexingtype="TEXT" />
			<attribute name="luogo" attributetype="Composite" description="Sede convocazione">
				<attributes>
					<attribute name="sede_link" attributetype="Link" description="Sede (collegamento luogo)" />
					<attribute name="sede_desc" attributetype="Longtext" description="Sede" />
				</attributes>
			</attribute>
			<attribute name="linkstr" attributetype="Link" description="Collegamento live streaming">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione! collegamento non valido, inserire tipo contenuto -VID-]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamanto alla diretta video (live streaming)]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="testo" attributetype="Hypertext" description="Descrizione estesa" />
			<list name="odg" attributetype="Monolist" description="Argomenti ordine del giorno">
				<nestedtype>
					<attribute name="odg" attributetype="Composite">
						<attributes>
							<attribute name="tipologia" attributetype="Enumerator" description="Tipo argomento" indexingtype="TEXT" separator=";">
								<validations>
									<required>true</required>
								</validations>
								<![CDATA[Ordine del giorno;Interrogazioni;Ordini del giorno precedenti;Altro]]>
							</attribute>
							<attribute name="paragrafo" attributetype="Longtext" description="Argomento" indexingtype="TEXT">
								<validations>
									<required>true</required>
								</validations>
							</attribute>
							<attribute name="linkAtto" attributetype="Link" description="Link atto" />
							<attribute name="allegato" attributetype="Attach" description="Allegato atto" />
							<attribute name="proponente" attributetype="Monotext" description="Proponente" indexingtype="TEXT" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="altriLink" attributetype="Monolist" description="Altri collegamenti">
				<nestedtype>
					<attribute name="altriLink" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="documenti" attributetype="Monolist" description="Documenti allegati">
				<nestedtype>
					<attribute name="documenti" attributetype="Attach" />
				</nestedtype>
			</list>
			<attribute name="video" attributetype="Link" description="Video">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione! collegamento non valido, inserire tipo contenuto -VID-]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamanto a video]]></helpmessage>
					</expression>
				</validations>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="LGO" typedescr="Luogo" viewpage="amm_07_dett" listmodel="120003" defaultmodel="120008">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="sottotit" attributetype="Text" description="Nome alternativo" indexingtype="TEXT" />
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia luoghi" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_07" showChildsPage="true">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione breve" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="descrizion" attributetype="Hypertext" description="Descrizione" indexingtype="TEXT" />
			<list name="interesse" attributetype="Monolist" description="Elementi di interesse">
				<nestedtype>
					<attribute name="interesse" attributetype="Composite">
						<attributes>
							<attribute name="titolo" attributetype="Text" description="Titolo">
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(!#parent.getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#parent.getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda LGO non è presente.]]></errormessage>
										<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione">
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(!#parent.getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#parent.getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda LGO non è presente.]]></errormessage>
										<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="sch_luogo" attributetype="Link" description="Scheda luogo">
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="galleria" attributetype="Link" description="Galleria di immagini">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="video" attributetype="Link" description="Video evento">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="servizi" attributetype="Monolist" description="Servizi presenti nel luogo">
				<nestedtype>
					<attribute name="servizi" attributetype="Composite">
						<attributes>
							<attribute name="srv_descr" attributetype="Text" description="Descrizione" />
							<attribute name="srv_link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="accesso" attributetype="Hypertext" description="Modalità di accesso">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="indirizzo" attributetype="Text" description="Indirizzo" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="gps_lat" attributetype="Monotext" description="Latitudine" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="gps_lon" attributetype="Monotext" description="Longitudine" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="cap" attributetype="Monotext" description="CAP" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="rif_tel" attributetype="Monotext" description="Riferimento telefonico del luogo" indexingtype="TEXT" />
			<attribute name="rif_email" attributetype="Monotext" description="Riferimento mail del luogo" indexingtype="TEXT" />
			<attribute name="rif_web" attributetype="Link" description="Riferimento sito web" />
			<attribute name="orario" attributetype="Composite" description="Orario per il pubblico">
				<attributes>
					<attribute name="inv_matt" attributetype="Text" description="Invernale mattina" />
					<attribute name="inv_pom" attributetype="Text" description="Invernale pomeriggio" />
					<attribute name="est_matt" attributetype="Text" description="Estivo mattina" />
					<attribute name="est_pom" attributetype="Text" description="Estivo pomeriggio" />
					<attribute name="continuo" attributetype="Text" description="Continuato" />
				</attributes>
			</attribute>
			<attribute name="s_str_link" attributetype="LinkSearchable" description="Struttura responsabile del luogo - Link" searchable="true" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="s_str_nome" attributetype="Text" description="Struttura responsabile del luogo - Nome" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''s_str_link'').destination.equals("") && text == "") || (#entity.getAttribute(''s_str_link'').destination.equals("") && text != "")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda ORG non è presente.]]></errormessage>
						<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="s_str_tel" attributetype="Monotext" description="Struttura responsabile del luogo - Riferimento telefonico" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''s_str_link'').destination.equals("") && text == "") || (#entity.getAttribute(''s_str_link'').destination.equals("") && text != "")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda ORG non è presente.]]></errormessage>
						<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="s_str_mail" attributetype="Monotext" description="Struttura responsabile del luogo - Riferimento mail" indexingtype="TEXT">
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''s_str_link'').destination.equals("") && text == "") || (#entity.getAttribute(''s_str_link'').destination.equals("") && text != "")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda ORG non è presente.]]></errormessage>
						<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="prezzo" attributetype="Monolist" description="Prezzo">
				<nestedtype>
					<attribute name="prezzo" attributetype="Composite">
						<attributes>
							<attribute name="tipo" attributetype="Enumerator" description="Tipo biglietto" separator=",">
								<validations>
									<required>true</required>
								</validations>
								<![CDATA[Intero,Ridotto,Gruppi,Cumulativo,Gratuito]]>
							</attribute>
							<attribute name="costo" attributetype="Monotext" description="Costo">
								<validations>
									<required>true</required>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[((text.matches("^([0-9]+,[0-9][0-9]$)") && !text.equals("0,00") && !#parent.getAttribute("tipo").text.equals("Gratuito")) || (text.equals("0") && #parent.getAttribute("tipo").text.equals("Gratuito")))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, inserire il valore col formato ''xx,yy'' o ''0'' in caso di tipo Gratuito.]]></errormessage>
										<helpmessage><![CDATA[Inserire un valore corretto (''0'' oppure ''100,50''.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="info_bigl" attributetype="Hypertext" description="Info biglietti" />
			<attribute name="link_bigl" attributetype="Link" description="Link acquisto biglietti" />
			<attribute name="parte_di" attributetype="Link" description="Parte di">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Luogo genitore. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="sede_di" attributetype="Link" description="Il luogo è sede di">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="categoria" attributetype="EnumeratorMapCategory" description="Categoria prevalente" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="cat_ambiti" />
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT" />
			<list name="box_aiuto" attributetype="Monolist" description="Box d''aiuto">
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo" />
							<attribute name="link" attributetype="Link" description="Link" />
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link" />
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link" />
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="STM" typedescr="Sito tematico" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="colore" attributetype="EnumeratorMap" description="Colore sfondo" separator=";">
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[bg1=Verde;bg2=Blu;bg3=Arancio;bg4=Rosso;bg5=Petrolio;bg6=Mattone;bg7=Carta da zucchero;bg8=Marroncino;bg9=Viola]]>
			</attribute>
			<attribute name="icona" attributetype="Monotext" description="Icona" />
			<attribute name="url_sito" attributetype="Link" description="URL sito" />
			<attribute name="notizia" attributetype="Link" description="Notizia">
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''NVT'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a novità non valido. Inserire un link a contenuto di tipo Novità (NVT).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Novità.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="gest_pag" attributetype="Monolist" description="Gestione pagina">
				<nestedtype>
					<attribute name="gest_pag" attributetype="Link">
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>              
 </contenttypes>  
' where item='contentTypes';
