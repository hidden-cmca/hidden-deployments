UPDATE sysconfig SET config='<?xml version="1.0" encoding="UTF-8"?>
<contenttypes>
	<contenttype typecode="APA" typedescr="Albo Pretorio Archivio" viewpage="albo_pretorio" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
        <attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">titolo</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <roles>
                <role>jacms:title</role>
                <role>jpsocial:title</role>
                <role>jprss:title</role>
            </roles>
        </attribute>
        <attribute name="immagine" attributetype="Image" description="Immagine">
            <names>
                <properties>
                    <property key="en">immagine</property>
                </properties>
            </names>
            <roles>
                <role>jpsocial:image</role>
            </roles>
        </attribute>
        <attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">descr</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
                <minlength>30</minlength>
                <maxlength>200</maxlength>
            </validations>
            <roles>
                <role>jpsocial:description</role>
                <role>jacms:description</role>
                <role>jprss:description</role>
            </roles>
        </attribute>
        <attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="documenti" showChildsPage="true">
            <names>
                <properties>
                    <property key="en">tipologia</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <roles>
                <role>jpattributeextended:sottosezione</role>
            </roles>
        </attribute>
        <attribute name="sottotip" attributetype="EnumeratorMapPage" description="Sottotipologia del documento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="doc_01;doc_04;doc_07;doc_07_02;doc_09" showChildsPage="true">
            <names>
                <properties>
                    <property key="en">sottotip</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(((!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07") &&!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09") && #attribute.getMapKey().equals(""))) || (((#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09"))) && (!#attribute.getMapKey().equals(""))) && (#attribute.getMapKey().contains(#entity.getAttribute(''tipologia'').getMapKey())))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, è necessario selezionare una Sottotipologia solo in caso di tipologia "Albo pretorio" o "Atti normativi" o "Documenti (tecnici) di supporto" o "Bandi" e che siano congruenti con il campo tipologia. Selezionare un valore differente.]]></errormessage>
                    <helpmessage><![CDATA[Inserire una diversa Sottotipologia oppure cambiare la scelta della Tipologia.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
            <names>
                <properties>
                    <property key="en">argomenti</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
        </attribute>
        <attribute name="oggetto" attributetype="Longtext" description="Oggetto" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">oggetto</property>
                </properties>
            </names>
        </attribute>
        <attribute name="descr_est" attributetype="Hypertext" description="Informazioni e modalità" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">descr_est</property>
                </properties>
            </names>
        </attribute>
        <attribute name="data_pubb" attributetype="Date" description="Data di pubblicazione" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">data_pubb</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <roles>
                <role>jpattributeextended:datapubblicazione</role>
            </roles>
        </attribute>
        <attribute name="data_scad" attributetype="Date" description="Data di scadenza" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">data_scad</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate()))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, Data di scadenza obbligatoria per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
                    <helpmessage><![CDATA[Inserire la Data di scadenza (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
                </expression>
            </validations>
            <roles>
                <role>jpattributeextended:datascadenza</role>
            </roles>
        </attribute>
        <attribute name="data_esito" attributetype="Date" description="Data esito bando (esclusivo per i bandi)" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">data_esito</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null)]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, Data esito è valida solo per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
                    <helpmessage><![CDATA[Inserire la Data esito corretta (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <list name="documenti" attributetype="Monolist" description="Documenti principali">
            <names>
                <properties>
                    <property key="en">documenti</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <nestedtype>
                <attribute name="documenti" attributetype="Attach">
                    <names>
                        <properties>
                            <property key="en">documenti</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="formati" attributetype="Monolist" description="Formati disponibili">
            <names>
                <properties>
                    <property key="en">formati</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="formati" attributetype="Enumerator" separator=",">
                    <names>
                        <properties>
                            <property key="en">formati</property>
                        </properties>
                    </names>
                    <validations>
                        <required>true</required>
                    </validations>
                    <![CDATA[PDF,PDF editabile,ODT,ODS,DOC,DOCX,XLS,XLSX,RTF,Altro,online]]>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="canal_link" attributetype="Link" description="Canale digitale servizio collegato">
            <names>
                <properties>
                    <property key="en">canal_link</property>
                </properties>
            </names>
        </attribute>
        <list name="fasi" attributetype="Monolist" description="Fasi intermedie">
            <names>
                <properties>
                    <property key="en">fasi</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="fasi" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">fasi</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="fs_accorpa" attributetype="Boolean" description="Accorpa alla riga precedente">
                            <names>
                                <properties>
                                    <property key="en">fs_accorpa</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="fs_data" attributetype="Date" description="Data fase">
                            <names>
                                <properties>
                                    <property key="en">fs_data</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[(((toString() != "") && (!#parent.getAttribute(''fs_accorpa'').booleanValue)) || ((#parent.getAttribute(''fs_accorpa'').booleanValue) && (toString() == "")))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, la data è obbligatoria se non accorpata alla precedente.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire la data della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_testo" attributetype="Text" description="Descrizione">
                            <names>
                                <properties>
                                    <property key="en">fs_testo</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[((text != "") || (#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, la descrizione è obbligatoria. Inserire un testo.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire la descrizione della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_alleg" attributetype="Attach" description="Allegato">
                            <names>
                                <properties>
                                    <property key="en">fs_alleg</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[(((attachPath != "") || (#parent.getAttribute(''fs_link'').destination != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, un allegato o link è obbligatorio. Inserire un file.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un allegato alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_link" attributetype="Link" description="Link">
                            <names>
                                <properties>
                                    <property key="en">fs_link</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[(((destination != "") || (#parent.getAttribute(''fs_alleg'').attachPath != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, un link o alllegato è obbligatorio. Inserire un link.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un link alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_datasc" attributetype="Date" description="Data scadenza file">
                            <names>
                                <properties>
                                    <property key="en">fs_datasc</property>
                                </properties>
                            </names>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <list name="allegati" attributetype="Monolist" description="Documenti allegati">
            <names>
                <properties>
                    <property key="en">allegati</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="allegati" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">allegati</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="link" attributetype="Link" description="Link scheda documento">
                            <names>
                                <properties>
                                    <property key="en">link</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="true">
                                    <ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="allegato" attributetype="Attach" description="Allegato">
                            <names>
                                <properties>
                                    <property key="en">allegato</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="descr" attributetype="Text" description="Descrizione">
                            <names>
                                <properties>
                                    <property key="en">descr</property>
                                </properties>
                            </names>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <list name="servizi" attributetype="Monolist" description="Servizi collegati">
            <names>
                <properties>
                    <property key="en">servizi</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="servizi" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">servizi</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="descr" attributetype="Hypertext" description="Descrizione">
                            <names>
                                <properties>
                                    <property key="en">descr</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="link" attributetype="Link" description="Link">
                            <names>
                                <properties>
                                    <property key="en">link</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="true">
                                    <ognlexpression><![CDATA[destination.contains(''SRV'')]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, collegamento a Servizio non valido. Inserire un link a contenuto di tipo Servizio (SRV).]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un collegamento a Servizio.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="ufficio" attributetype="LinkSearchable" description="Ufficio responsabile del documento" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">ufficio</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="true">
                    <ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
                    <helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <attribute name="area" attributetype="LinkSearchable" description="Area responsabile del documento" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">area</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
                    <helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <list name="dataset" attributetype="Monolist" description="Dataset">
            <names>
                <properties>
                    <property key="en">dataset</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="dataset" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">dataset</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="autori" attributetype="Text" description="Autore/i" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">autori</property>
                </properties>
            </names>
        </attribute>
        <attribute name="licenza" attributetype="Text" description="Licenza di distribuzione" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">licenza</property>
                </properties>
            </names>
        </attribute>
        <attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">ult_info</property>
                </properties>
            </names>
        </attribute>
        <list name="rif_norma" attributetype="Monolist" description="Riferimenti normativi">
            <names>
                <properties>
                    <property key="en">rif_norma</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="rif_norma" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">rif_norma</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="protoc" attributetype="Monotext" description="Protocollo" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">protoc</property>
                </properties>
            </names>
        </attribute>
        <attribute name="protoc_dt" attributetype="Date" description="Data protocollo">
            <names>
                <properties>
                    <property key="en">protoc_dt</property>
                </properties>
            </names>
        </attribute>
        <list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
            <names>
                <properties>
                    <property key="en">box_aiuto</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="box_aiuto" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">box_aiuto</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="testo" attributetype="Hypertext" description="Testo">
                            <names>
                                <properties>
                                    <property key="en">testo</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="link" attributetype="Link" description="Link">
                            <names>
                                <properties>
                                    <property key="en">link</property>
                                </properties>
                            </names>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="galleria" attributetype="Link" description="Immagini">
            <names>
                <properties>
                    <property key="en">galleria</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="true">
                    <ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
                    <helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
            <names>
                <properties>
                    <property key="en">corr_amm</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_amm" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_amm</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
            <names>
                <properties>
                    <property key="en">corr_serv</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_serv" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_serv</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
            <names>
                <properties>
                    <property key="en">corr_nov</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_nov" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_nov</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
            <names>
                <properties>
                    <property key="en">corr_doc</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_doc" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_doc</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
		</attributes>
	</contenttype>
	<contenttype typecode="ATA" typedescr="Amministrazione Trasparente Archivio" viewpage="amm_trasp_archivio_dett" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
        <attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">titolo</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <roles>
                <role>jacms:title</role>
                <role>jpsocial:title</role>
                <role>jprss:title</role>
            </roles>
        </attribute>
        <attribute name="immagine" attributetype="Image" description="Immagine">
            <names>
                <properties>
                    <property key="en">immagine</property>
                </properties>
            </names>
            <roles>
                <role>jpsocial:image</role>
            </roles>
        </attribute>
        <attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">descr</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
                <minlength>30</minlength>
                <maxlength>200</maxlength>
            </validations>
            <roles>
                <role>jpsocial:description</role>
                <role>jacms:description</role>
                <role>jprss:description</role>
            </roles>
        </attribute>
        <attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="documenti" showChildsPage="true">
            <names>
                <properties>
                    <property key="en">tipologia</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <roles>
                <role>jpattributeextended:sottosezione</role>
            </roles>
        </attribute>
        <attribute name="sottotip" attributetype="EnumeratorMapPage" description="Sottotipologia del documento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="doc_01;doc_04;doc_07;doc_07_02;doc_09" showChildsPage="true">
            <names>
                <properties>
                    <property key="en">sottotip</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(((!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07") &&!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09") && #attribute.getMapKey().equals(""))) || (((#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09"))) && (!#attribute.getMapKey().equals(""))) && (#attribute.getMapKey().contains(#entity.getAttribute(''tipologia'').getMapKey())))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, è necessario selezionare una Sottotipologia solo in caso di tipologia "Albo pretorio" o "Atti normativi" o "Documenti (tecnici) di supporto" o "Bandi" e che siano congruenti con il campo tipologia. Selezionare un valore differente.]]></errormessage>
                    <helpmessage><![CDATA[Inserire una diversa Sottotipologia oppure cambiare la scelta della Tipologia.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
            <names>
                <properties>
                    <property key="en">argomenti</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
        </attribute>
        <attribute name="oggetto" attributetype="Longtext" description="Oggetto" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">oggetto</property>
                </properties>
            </names>
        </attribute>
        <attribute name="descr_est" attributetype="Hypertext" description="Informazioni e modalità" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">descr_est</property>
                </properties>
            </names>
        </attribute>
        <attribute name="data_pubb" attributetype="Date" description="Data di pubblicazione" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">data_pubb</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <roles>
                <role>jpattributeextended:datapubblicazione</role>
            </roles>
        </attribute>
        <attribute name="data_scad" attributetype="Date" description="Data di scadenza" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">data_scad</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate()))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, Data di scadenza obbligatoria per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
                    <helpmessage><![CDATA[Inserire la Data di scadenza (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
                </expression>
            </validations>
            <roles>
                <role>jpattributeextended:datascadenza</role>
            </roles>
        </attribute>
        <attribute name="data_esito" attributetype="Date" description="Data esito bando (esclusivo per i bandi)" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">data_esito</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null)]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, Data esito è valida solo per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
                    <helpmessage><![CDATA[Inserire la Data esito corretta (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <list name="documenti" attributetype="Monolist" description="Documenti principali">
            <names>
                <properties>
                    <property key="en">documenti</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
            </validations>
            <nestedtype>
                <attribute name="documenti" attributetype="Attach">
                    <names>
                        <properties>
                            <property key="en">documenti</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="formati" attributetype="Monolist" description="Formati disponibili">
            <names>
                <properties>
                    <property key="en">formati</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="formati" attributetype="Enumerator" separator=",">
                    <names>
                        <properties>
                            <property key="en">formati</property>
                        </properties>
                    </names>
                    <validations>
                        <required>true</required>
                    </validations>
                    <![CDATA[PDF,PDF editabile,ODT,ODS,DOC,DOCX,XLS,XLSX,RTF,Altro,online]]>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="canal_link" attributetype="Link" description="Canale digitale servizio collegato">
            <names>
                <properties>
                    <property key="en">canal_link</property>
                </properties>
            </names>
        </attribute>
        <list name="fasi" attributetype="Monolist" description="Fasi intermedie">
            <names>
                <properties>
                    <property key="en">fasi</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="fasi" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">fasi</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="fs_accorpa" attributetype="Boolean" description="Accorpa alla riga precedente">
                            <names>
                                <properties>
                                    <property key="en">fs_accorpa</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="fs_data" attributetype="Date" description="Data fase">
                            <names>
                                <properties>
                                    <property key="en">fs_data</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[(((toString() != "") && (!#parent.getAttribute(''fs_accorpa'').booleanValue)) || ((#parent.getAttribute(''fs_accorpa'').booleanValue) && (toString() == "")))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, la data è obbligatoria se non accorpata alla precedente.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire la data della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_testo" attributetype="Text" description="Descrizione">
                            <names>
                                <properties>
                                    <property key="en">fs_testo</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[((text != "") || (#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, la descrizione è obbligatoria. Inserire un testo.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire la descrizione della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_alleg" attributetype="Attach" description="Allegato">
                            <names>
                                <properties>
                                    <property key="en">fs_alleg</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[(((attachPath != "") || (#parent.getAttribute(''fs_link'').destination != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, un allegato o link è obbligatorio. Inserire un file.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un allegato alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_link" attributetype="Link" description="Link">
                            <names>
                                <properties>
                                    <property key="en">fs_link</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="false">
                                    <ognlexpression><![CDATA[(((destination != "") || (#parent.getAttribute(''fs_alleg'').attachPath != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, un link o alllegato è obbligatorio. Inserire un link.]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un link alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="fs_datasc" attributetype="Date" description="Data scadenza file">
                            <names>
                                <properties>
                                    <property key="en">fs_datasc</property>
                                </properties>
                            </names>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <list name="allegati" attributetype="Monolist" description="Documenti allegati">
            <names>
                <properties>
                    <property key="en">allegati</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="allegati" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">allegati</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="link" attributetype="Link" description="Link scheda documento">
                            <names>
                                <properties>
                                    <property key="en">link</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="true">
                                    <ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                        <attribute name="allegato" attributetype="Attach" description="Allegato">
                            <names>
                                <properties>
                                    <property key="en">allegato</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="descr" attributetype="Text" description="Descrizione">
                            <names>
                                <properties>
                                    <property key="en">descr</property>
                                </properties>
                            </names>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <list name="servizi" attributetype="Monolist" description="Servizi collegati">
            <names>
                <properties>
                    <property key="en">servizi</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="servizi" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">servizi</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="descr" attributetype="Hypertext" description="Descrizione">
                            <names>
                                <properties>
                                    <property key="en">descr</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="link" attributetype="Link" description="Link">
                            <names>
                                <properties>
                                    <property key="en">link</property>
                                </properties>
                            </names>
                            <validations>
                                <expression evalOnValuedAttribute="true">
                                    <ognlexpression><![CDATA[destination.contains(''SRV'')]]></ognlexpression>
                                    <errormessage><![CDATA[Attenzione, collegamento a Servizio non valido. Inserire un link a contenuto di tipo Servizio (SRV).]]></errormessage>
                                    <helpmessage><![CDATA[Inserire un collegamento a Servizio.]]></helpmessage>
                                </expression>
                            </validations>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="ufficio" attributetype="LinkSearchable" description="Ufficio responsabile del documento" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">ufficio</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="true">
                    <ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
                    <helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <attribute name="area" attributetype="LinkSearchable" description="Area responsabile del documento" searchable="true" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">area</property>
                </properties>
            </names>
            <validations>
                <required>true</required>
                <expression evalOnValuedAttribute="false">
                    <ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
                    <helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <list name="dataset" attributetype="Monolist" description="Dataset">
            <names>
                <properties>
                    <property key="en">dataset</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="dataset" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">dataset</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="autori" attributetype="Text" description="Autore/i" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">autori</property>
                </properties>
            </names>
        </attribute>
        <attribute name="licenza" attributetype="Text" description="Licenza di distribuzione" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">licenza</property>
                </properties>
            </names>
        </attribute>
        <attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">ult_info</property>
                </properties>
            </names>
        </attribute>
        <list name="rif_norma" attributetype="Monolist" description="Riferimenti normativi">
            <names>
                <properties>
                    <property key="en">rif_norma</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="rif_norma" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">rif_norma</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="protoc" attributetype="Monotext" description="Protocollo" indexingtype="TEXT">
            <names>
                <properties>
                    <property key="en">protoc</property>
                </properties>
            </names>
        </attribute>
        <attribute name="protoc_dt" attributetype="Date" description="Data protocollo">
            <names>
                <properties>
                    <property key="en">protoc_dt</property>
                </properties>
            </names>
        </attribute>
        <list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
            <names>
                <properties>
                    <property key="en">box_aiuto</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="box_aiuto" attributetype="Composite">
                    <names>
                        <properties>
                            <property key="en">box_aiuto</property>
                        </properties>
                    </names>
                    <attributes>
                        <attribute name="testo" attributetype="Hypertext" description="Testo">
                            <names>
                                <properties>
                                    <property key="en">testo</property>
                                </properties>
                            </names>
                        </attribute>
                        <attribute name="link" attributetype="Link" description="Link">
                            <names>
                                <properties>
                                    <property key="en">link</property>
                                </properties>
                            </names>
                        </attribute>
                    </attributes>
                </attribute>
            </nestedtype>
        </list>
        <attribute name="galleria" attributetype="Link" description="Immagini">
            <names>
                <properties>
                    <property key="en">galleria</property>
                </properties>
            </names>
            <validations>
                <expression evalOnValuedAttribute="true">
                    <ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
                    <errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
                    <helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
                </expression>
            </validations>
        </attribute>
        <list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
            <names>
                <properties>
                    <property key="en">corr_amm</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_amm" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_amm</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
            <names>
                <properties>
                    <property key="en">corr_serv</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_serv" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_serv</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
            <names>
                <properties>
                    <property key="en">corr_nov</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_nov" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_nov</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
        <list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
            <names>
                <properties>
                    <property key="en">corr_doc</property>
                </properties>
            </names>
            <nestedtype>
                <attribute name="corr_doc" attributetype="Link">
                    <names>
                        <properties>
                            <property key="en">corr_doc</property>
                        </properties>
                    </names>
                </attribute>
            </nestedtype>
        </list>
		</attributes>
	</contenttype>
	<contenttype typecode="ARG" typedescr="Argomento" viewpage="arg_01_dett" listmodel="400011" defaultmodel="400004">
		<attributes>
			<attribute name="icona" attributetype="Text" description="Icona" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">icona</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
				</roles>
			</attribute>
			<attribute name="argomento" attributetype="EnumeratorMapCategory" description="Argomento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomento</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:argomento</role>
				</roles>
			</attribute>
			<list name="gestione" attributetype="Monolist" description="Gestione">
				<names>
					<properties>
						<property key="en">gestione</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
				<nestedtype>
					<attribute name="gestione" attributetype="Link">
						<names>
							<properties>
								<property key="en">gestione</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<list name="cont_home" attributetype="Monolist" description="Contenuti homepage">
				<names>
					<properties>
						<property key="en">cont_home</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="cont_home" attributetype="Link">
						<names>
							<properties>
								<property key="en">cont_home</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="link_pag" attributetype="Link" description="Link a pagina">
				<names>
					<properties>
						<property key="en">link_pag</property>
					</properties>
				</names>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="ATR" typedescr="Archivio Trasparenza" viewpage="amm_trasp_archivio_dett" listmodel="**NULL**" defaultmodel="1000001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="it">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="it">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
				</roles>
			</attribute>
			<list name="paragrafo" attributetype="Monolist" description="Paragrafo">
				<names>
					<properties>
						<property key="it">paragrafo</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="paragrafo" attributetype="Composite">
						<names>
							<properties>
								<property key="it">paragrafo</property>
							</properties>
						</names>
						<attributes>
							<attribute name="titolo" attributetype="Text">
								<names>
									<properties>
										<property key="it">titolo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="testo" attributetype="Hypertext" description="testo">
								<names>
									<properties>
										<property key="it">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="immagine" attributetype="Image" description="immagine">
								<names>
									<properties>
										<property key="it">immagine</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="link">
								<names>
									<properties>
										<property key="it">link</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link_txt" attributetype="Link" description="link_txt">
								<names>
									<properties>
										<property key="it">link_txt</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="allegati" attributetype="Monolist" description="Allegati">
				<names>
					<properties>
						<property key="it">allegati</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="allegati" attributetype="Attach">
						<names>
							<properties>
								<property key="it">allegati</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="link">
				<names>
					<properties>
						<property key="it">link</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="link" attributetype="Link">
						<names>
							<properties>
								<property key="it">link</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="video" attributetype="Link" description="Video">
				<names>
					<properties>
						<property key="it">video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="it">immagine</property>
					</properties>
				</names>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="it">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione di appartenenza" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_trasp_archivio" showChildsPage="true">
				<names>
					<properties>
						<property key="it">sezione</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="icona" attributetype="Text" description="Icona">
				<names>
					<properties>
						<property key="it">icona</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link_pag" attributetype="Link" description="Link a pagina di dettaglio">
				<names>
					<properties>
						<property key="it">link_pag</property>
					</properties>
				</names>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="BAN" typedescr="Banner" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
			</attribute>
			<attribute name="sfondo" attributetype="EnumeratorMap" description="Colore sfondo" separator=";">
				<names>
					<properties>
						<property key="en">sfondo</property>
					</properties>
				</names>
				<![CDATA[bg1=Verde;bg2=Blu;bg3=Arancio;bg4=Rosso;bg5=Petrolio;bg6=Mattone;bg7=Carta da zucchero;bg8=Marroncino;bg9=Viola]]>
			</attribute>
			<attribute name="num_verde" attributetype="Text" description="Numero verde">
				<names>
					<properties>
						<property key="en">num_verde</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link_arg" attributetype="EnumeratorMapCategory" description="Link argomento" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">link_arg</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link" attributetype="Link" description="Link titolo">
				<names>
					<properties>
						<property key="en">link</property>
					</properties>
				</names>
			</attribute>
			<list name="link_puls" attributetype="Monolist" description="Link pulsanti">
				<names>
					<properties>
						<property key="en">link_puls</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="link_puls" attributetype="Link">
						<names>
							<properties>
								<property key="en">link_puls</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="BNR" typedescr="Banner - Ent" viewpage="**NULL**" listmodel="10023" defaultmodel="10003">
		<attributes>
			<attribute name="title" attributetype="Text" description="Title" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">title</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
				</roles>
			</attribute>
			<attribute name="abstract" attributetype="Hypertext" description="Abstract" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">abstract</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link" attributetype="Link" description="Link">
				<names>
					<properties>
						<property key="en">link</property>
					</properties>
				</names>
			</attribute>
			<attribute name="image" attributetype="Image" description="Image">
				<names>
					<properties>
						<property key="en">image</property>
					</properties>
				</names>
			</attribute>
			<attribute name="placement" attributetype="Enumerator" description="Image Placement" separator=",">
				<names>
					<properties>
						<property key="en">placement</property>
					</properties>
				</names>
				<![CDATA[LEFT,RIGHT]]>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="CNV" typedescr="Convocazione" viewpage="convocazioni_dettaglio" listmodel="110003" defaultmodel="110002">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo convocazione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="desc" attributetype="Text" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">desc</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
				</roles>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione portale" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_01_02_05" showChildsPage="false">
				<names>
					<properties>
						<property key="en">sezione</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMap" description="Organo" searchable="true" indexingtype="TEXT" separator=";">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[01=Consiglio;  02=Ccp Affari generali, pubblica istruzione, politiche universitarie e del diritto allo studio, politiche giovanili; 03=Ccp Attività produttive, turismo e promozione del territorio; 04=Ccp Bilancio, tributi, società partecipate, personale; 05=Ccp Cultura, spettacolo e verde pubblico; 06=Ccp Innovazione tecnologica, ambiente e politiche del mare; 07=Ccp Lavori pubblici; 08=Ccp Pari opportunità; 09=Ccp Pianificazione strategica e dello sviluppo urbanistico; 10=Ccp Politiche della sicurezza, sport e patrimonio; 11=Ccp Politiche per la mobilità, della casa e dei servizi tecnologici; 12=Ccp Politiche sociali, del benessere e della famiglia; 13=Ccp Statuto e regolamenti; 14=Ccp Valutazione delle politiche comunali e la qualità dei servizi]]>
			</attribute>
			<attribute name="numero" attributetype="Monotext" description="Numero convocazione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">numero</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="data" attributetype="Timestamp" description="Data e ora" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data</property>
					</properties>
				</names>
			</attribute>
			<attribute name="luogo" attributetype="Composite" description="Sede convocazione">
				<names>
					<properties>
						<property key="en">luogo</property>
					</properties>
				</names>
				<attributes>
					<attribute name="sede_link" attributetype="Link" description="Sede (collegamento luogo)">
						<names>
							<properties>
								<property key="en">sede_link</property>
							</properties>
						</names>
					</attribute>
					<attribute name="sede_desc" attributetype="Longtext" description="Sede">
						<names>
							<properties>
								<property key="en">sede_desc</property>
							</properties>
						</names>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="linkstr" attributetype="Link" description="Collegamento live streaming">
				<names>
					<properties>
						<property key="en">linkstr</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione! collegamento non valido, inserire tipo contenuto -VID-]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamanto alla diretta video (live streaming)]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="testo" attributetype="Hypertext" description="Descrizione estesa">
				<names>
					<properties>
						<property key="en">testo</property>
					</properties>
				</names>
			</attribute>
			<list name="odg" attributetype="Monolist" description="Argomenti ordine del giorno">
				<names>
					<properties>
						<property key="en">odg</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="odg" attributetype="Composite">
						<names>
							<properties>
								<property key="en">odg</property>
							</properties>
						</names>
						<attributes>
							<attribute name="tipologia" attributetype="Enumerator" description="Tipo argomento" indexingtype="TEXT" separator=";">
								<names>
									<properties>
										<property key="en">tipologia</property>
									</properties>
								</names>
								<validations>
									<required>true</required>
								</validations>
								<![CDATA[Ordine del giorno;Interrogazioni;Ordini del giorno precedenti;Altro]]>
							</attribute>
							<attribute name="paragrafo" attributetype="Longtext" description="Argomento" indexingtype="TEXT">
								<names>
									<properties>
										<property key="en">paragrafo</property>
									</properties>
								</names>
								<validations>
									<required>true</required>
								</validations>
							</attribute>
							<attribute name="linkAtto" attributetype="Link" description="Link atto">
								<names>
									<properties>
										<property key="en">linkAtto</property>
									</properties>
								</names>
							</attribute>
							<attribute name="allegato" attributetype="Attach" description="Allegato atto">
								<names>
									<properties>
										<property key="en">allegato</property>
									</properties>
								</names>
							</attribute>
							<attribute name="proponente" attributetype="Monotext" description="Proponente" indexingtype="TEXT">
								<names>
									<properties>
										<property key="en">proponente</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="altriLink" attributetype="Monolist" description="Altri collegamenti">
				<names>
					<properties>
						<property key="en">altriLink</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="altriLink" attributetype="Link">
						<names>
							<properties>
								<property key="en">altriLink</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="documenti" attributetype="Monolist" description="Documenti allegati">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="documenti" attributetype="Attach">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="video" attributetype="Link" description="Video">
				<names>
					<properties>
						<property key="en">video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione! collegamento non valido, inserire tipo contenuto -VID-]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamanto a video]]></helpmessage>
					</expression>
				</validations>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="DOC" typedescr="Documento" viewpage="documento" listmodel="700008" defaultmodel="700007">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="documenti" showChildsPage="true">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="sottotip" attributetype="EnumeratorMapPage" description="Sottotipologia del documento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="doc_01;doc_04;doc_07;doc_07_02;doc_09" showChildsPage="true">
				<names>
					<properties>
						<property key="en">sottotip</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(((!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04") && !#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07") &&!#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09") && #attribute.getMapKey().equals(""))) || (((#entity.getAttribute(''tipologia'').getMapKey().equals("doc_01")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_04")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_07")) || (#entity.getAttribute(''tipologia'').getMapKey().equals("doc_09"))) && (!#attribute.getMapKey().equals(""))) && (#attribute.getMapKey().contains(#entity.getAttribute(''tipologia'').getMapKey())))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario selezionare una Sottotipologia solo in caso di tipologia "Albo pretorio" o "Atti normativi" o "Documenti (tecnici) di supporto" o "Bandi" e che siano congruenti con il campo tipologia. Selezionare un valore differente.]]></errormessage>
						<helpmessage><![CDATA[Inserire una diversa Sottotipologia oppure cambiare la scelta della Tipologia.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="oggetto" attributetype="Longtext" description="Oggetto" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">oggetto</property>
					</properties>
				</names>
			</attribute>
			<attribute name="descr_est" attributetype="Hypertext" description="Informazioni e modalità" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr_est</property>
					</properties>
				</names>
			</attribute>
			<attribute name="data_pubb" attributetype="Date" description="Data di pubblicazione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data_pubb</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="data_scad" attributetype="Date" description="Data di scadenza" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data_scad</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate()))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, Data di scadenza obbligatoria per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
						<helpmessage><![CDATA[Inserire la Data di scadenza (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
					</expression>
				</validations>
				<roles>
					<role>jpattributeextended:datascadenza</role>
				</roles>
			</attribute>
			<attribute name="data_esito" attributetype="Date" description="Data esito bando (esclusivo per i bandi)" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data_esito</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate() != null && #attribute.date.after(#entity.getAttribute("data_pubb").getDate())) || (!#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null) || (#entity.getAttribute("tipologia").getMapKey().equals("doc_09") && #attribute.getDate()== null)]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, Data esito è valida solo per la tipologia Bandi. Verificare inoltre che sia successiva alla Data di pubblicazione]]></errormessage>
						<helpmessage><![CDATA[Inserire la Data esito corretta (successiva alla data di pubblicazione) oppure cambiare la tipologia di documento]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Documenti principali">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<nestedtype>
					<attribute name="documenti" attributetype="Attach">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="formati" attributetype="Monolist" description="Formati disponibili">
				<names>
					<properties>
						<property key="en">formati</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="formati" attributetype="Enumerator" separator=",">
						<names>
							<properties>
								<property key="en">formati</property>
							</properties>
						</names>
						<validations>
							<required>true</required>
						</validations>
						<![CDATA[PDF,PDF editabile,ODT,ODS,DOC,DOCX,XLS,XLSX,RTF,Altro,online]]>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="canal_link" attributetype="Link" description="Canale digitale servizio collegato">
				<names>
					<properties>
						<property key="en">canal_link</property>
					</properties>
				</names>
			</attribute>
			<list name="fasi" attributetype="Monolist" description="Fasi intermedie">
				<names>
					<properties>
						<property key="en">fasi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="fasi" attributetype="Composite">
						<names>
							<properties>
								<property key="en">fasi</property>
							</properties>
						</names>
						<attributes>
							<attribute name="fs_accorpa" attributetype="Boolean" description="Accorpa alla riga precedente">
								<names>
									<properties>
										<property key="en">fs_accorpa</property>
									</properties>
								</names>
							</attribute>
							<attribute name="fs_data" attributetype="Date" description="Data fase">
								<names>
									<properties>
										<property key="en">fs_data</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(((toString() != "") && (!#parent.getAttribute(''fs_accorpa'').booleanValue)) || ((#parent.getAttribute(''fs_accorpa'').booleanValue) && (toString() == "")))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, la data è obbligatoria se non accorpata alla precedente.]]></errormessage>
										<helpmessage><![CDATA[Inserire la data della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_testo" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">fs_testo</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[((text != "") || (#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, la descrizione è obbligatoria. Inserire un testo.]]></errormessage>
										<helpmessage><![CDATA[Inserire la descrizione della fase oppure attivare la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_alleg" attributetype="Attach" description="Allegato">
								<names>
									<properties>
										<property key="en">fs_alleg</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(((attachPath != "") || (#parent.getAttribute(''fs_link'').destination != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, un allegato o link è obbligatorio. Inserire un file.]]></errormessage>
										<helpmessage><![CDATA[Inserire un allegato alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">fs_link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(((destination != "") || (#parent.getAttribute(''fs_alleg'').attachPath != "")) || (!#parent.getAttribute(''fs_accorpa'').booleanValue))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, un link o alllegato è obbligatorio. Inserire un link.]]></errormessage>
										<helpmessage><![CDATA[Inserire un link alla fase accorpata oppure togliere la spunta per Accorpare.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="fs_datasc" attributetype="Date" description="Data scadenza file">
								<names>
									<properties>
										<property key="en">fs_datasc</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="allegati" attributetype="Monolist" description="Documenti allegati">
				<names>
					<properties>
						<property key="en">allegati</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="allegati" attributetype="Composite">
						<names>
							<properties>
								<property key="en">allegati</property>
							</properties>
						</names>
						<attributes>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="allegato" attributetype="Attach" description="Allegato">
								<names>
									<properties>
										<property key="en">allegato</property>
									</properties>
								</names>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="servizi" attributetype="Monolist" description="Servizi collegati">
				<names>
					<properties>
						<property key="en">servizi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="servizi" attributetype="Composite">
						<names>
							<properties>
								<property key="en">servizi</property>
							</properties>
						</names>
						<attributes>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''SRV'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Servizio non valido. Inserire un link a contenuto di tipo Servizio (SRV).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Servizio.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ufficio" attributetype="LinkSearchable" description="Ufficio responsabile del documento" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ufficio</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="area" attributetype="LinkSearchable" description="Area responsabile del documento" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">area</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="dataset" attributetype="Monolist" description="Dataset">
				<names>
					<properties>
						<property key="en">dataset</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="dataset" attributetype="Link">
						<names>
							<properties>
								<property key="en">dataset</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="autori" attributetype="Text" description="Autore/i" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">autori</property>
					</properties>
				</names>
			</attribute>
			<attribute name="licenza" attributetype="Text" description="Licenza di distribuzione" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">licenza</property>
					</properties>
				</names>
			</attribute>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ult_info</property>
					</properties>
				</names>
			</attribute>
			<list name="rif_norma" attributetype="Monolist" description="Riferimenti normativi">
				<names>
					<properties>
						<property key="en">rif_norma</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="rif_norma" attributetype="Link">
						<names>
							<properties>
								<property key="en">rif_norma</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="protoc" attributetype="Monotext" description="Protocollo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">protoc</property>
					</properties>
				</names>
			</attribute>
			<attribute name="protoc_dt" attributetype="Date" description="Data protocollo">
				<names>
					<properties>
						<property key="en">protoc_dt</property>
					</properties>
				</names>
			</attribute>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<names>
					<properties>
						<property key="en">box_aiuto</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<names>
							<properties>
								<property key="en">box_aiuto</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="galleria" attributetype="Link" description="Immagini">
				<names>
					<properties>
						<property key="en">galleria</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="EVN" typedescr="Evento" viewpage="evento" listmodel="210003" defaultmodel="210001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="sottotit" attributetype="Text" description="Titolo alternativo/Sottotitolo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">sottotit</property>
					</properties>
				</names>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia dell evento" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="nov_03" showChildsPage="true">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="pos_img" attributetype="EnumeratorMap" description="Posizione immagine" separator=";">
				<names>
					<properties>
						<property key="en">pos_img</property>
					</properties>
				</names>
				<![CDATA[top=Allineamento in alto;center=Centrata;bottom=Allineamento in basso;]]>
			</attribute>
			<attribute name="dim_img" attributetype="Boolean" description="Immagine grande">
				<names>
					<properties>
						<property key="en">dim_img</property>
					</properties>
				</names>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="genitore" attributetype="Boolean" description="Evento genitore">
				<names>
					<properties>
						<property key="en">genitore</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="vedi_cal" attributetype="Boolean" description="Vedi il calendario eventi">
				<names>
					<properties>
						<property key="en">vedi_cal</property>
					</properties>
				</names>
			</attribute>
			<attribute name="parte_di" attributetype="Link" description="Parte di">
				<names>
					<properties>
						<property key="en">parte_di</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''EVN'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Evento genitore. Inserire un link a contenuto di tipo Evento (EVN).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Evento.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="data_pubb" attributetype="Date" description="Data di pubblicazione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data_pubb</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="intro" attributetype="Hypertext" description="Introduzione" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">intro</property>
					</properties>
				</names>
			</attribute>
			<attribute name="galleria" attributetype="Link" description="Galleria di immagini">
				<names>
					<properties>
						<property key="en">galleria</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="video" attributetype="Link" description="Video evento">
				<names>
					<properties>
						<property key="en">video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="desc_dest" attributetype="Hypertext" description="Descrizione dei destinatari" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">desc_dest</property>
					</properties>
				</names>
			</attribute>
			<list name="persone" attributetype="Monolist" description="Persone dell amministrazione">
				<names>
					<properties>
						<property key="en">persone</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="persone" attributetype="Link">
						<names>
							<properties>
								<property key="en">persone</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="luogo" attributetype="Composite" description="Luogo">
				<names>
					<properties>
						<property key="en">luogo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<attributes>
					<attribute name="sch_luogo" attributetype="Link" description="Scheda luogo">
						<names>
							<properties>
								<property key="en">sch_luogo</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="nome" attributetype="Text" description="Nome del luogo" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">nome</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata.]]></errormessage>
								<helpmessage><![CDATA[Inserire il nome del Luogo se non presente la scheda collegata.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="indirizzo" attributetype="Text" description="Indirizzo" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">indirizzo</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "" && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata oppure se Macro evento.]]></errormessage>
								<helpmessage><![CDATA[Inserire l indirizzo del Luogo se non presente la scheda collegata oppure impostare Macro evento.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="cap" attributetype="Monotext" description="CAP" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">cap</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata.]]></errormessage>
								<helpmessage><![CDATA[Inserire il CAP del Luogo se non presente la scheda collegata.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="quartiere" attributetype="Text" description="Quartiere" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">quartiere</property>
							</properties>
						</names>
					</attribute>
					<attribute name="circoscr" attributetype="Text" description="Circoscrizione" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">circoscr</property>
							</properties>
						</names>
					</attribute>
					<attribute name="gps_lat" attributetype="Monotext" description="Latitudine" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">gps_lat</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "" && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata oppure se Macro evento.]]></errormessage>
								<helpmessage><![CDATA[Inserire le coordinate GPS del Luogo se non presente la scheda collegata oppure impostare Macro evento.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="gps_lon" attributetype="Monotext" description="Longitudine" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">gps_lon</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text != "") || (#entity.getAttribute(''luogo'').getAttribute(''sch_luogo'').destination.equals("") && text == "" && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda del Luogo collegata oppure se Macro evento.]]></errormessage>
								<helpmessage><![CDATA[Inserire le coordinate GPS del Luogo se non presente la scheda collegata oppure impostare Macro evento.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="data_iniz" attributetype="Date" description="Data di inizio evento" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data_iniz</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="data_fine" attributetype="Date" description="Data di fine evento" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data_fine</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<rangestart attribute="data_iniz" />
				</validations>
				<roles>
					<role>jpattributeextended:datascadenza</role>
				</roles>
			</attribute>
			<attribute name="ricorrenza" attributetype="Composite" description="Ricorrenza settimanale">
				<names>
					<properties>
						<property key="en">ricorrenza</property>
					</properties>
				</names>
				<attributes>
					<attribute name="ric_lun" attributetype="CheckBox" description="Lunedì">
						<names>
							<properties>
								<property key="en">ric_lun</property>
							</properties>
						</names>
					</attribute>
					<attribute name="ric_mar" attributetype="CheckBox" description="Martedì">
						<names>
							<properties>
								<property key="en">ric_mar</property>
							</properties>
						</names>
					</attribute>
					<attribute name="ric_mer" attributetype="CheckBox" description="Mercoledì">
						<names>
							<properties>
								<property key="en">ric_mer</property>
							</properties>
						</names>
					</attribute>
					<attribute name="ric_gio" attributetype="CheckBox" description="Giovedì">
						<names>
							<properties>
								<property key="en">ric_gio</property>
							</properties>
						</names>
					</attribute>
					<attribute name="ric_ven" attributetype="CheckBox" description="Venerdì">
						<names>
							<properties>
								<property key="en">ric_ven</property>
							</properties>
						</names>
					</attribute>
					<attribute name="ric_sab" attributetype="CheckBox" description="Sabato">
						<names>
							<properties>
								<property key="en">ric_sab</property>
							</properties>
						</names>
					</attribute>
					<attribute name="ric_dom" attributetype="CheckBox" description="Domenica">
						<names>
							<properties>
								<property key="en">ric_dom</property>
							</properties>
						</names>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="orari" attributetype="Hypertext" description="Orari" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">orari</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="agg_cal" attributetype="Boolean" description="Aggiungi al calendario">
				<names>
					<properties>
						<property key="en">agg_cal</property>
					</properties>
				</names>
			</attribute>
			<list name="prezzo" attributetype="Monolist" description="Prezzo">
				<names>
					<properties>
						<property key="en">prezzo</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="prezzo" attributetype="Composite">
						<names>
							<properties>
								<property key="en">prezzo</property>
							</properties>
						</names>
						<attributes>
							<attribute name="tipo" attributetype="Enumerator" description="Tipo biglietto" separator=",">
								<names>
									<properties>
										<property key="en">tipo</property>
									</properties>
								</names>
								<validations>
									<required>true</required>
								</validations>
								<![CDATA[Intero,Ridotto,Gruppi,Cumulativo,Gratuito]]>
							</attribute>
							<attribute name="costo" attributetype="Monotext" description="Costo">
								<names>
									<properties>
										<property key="en">costo</property>
									</properties>
								</names>
								<validations>
									<required>true</required>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[((text.matches("^([0-9]+,[0-9][0-9]$)") && !text.equals("0,00") && !#parent.getAttribute("tipo").text.equals("Gratuito")) || (text.equals("0") && #parent.getAttribute("tipo").text.equals("Gratuito")))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, inserire il valore col formato ''xx,yy'' o ''0'' in caso di tipo Gratuito.]]></errormessage>
										<helpmessage><![CDATA[Inserire un valore corretto (''0'' oppure ''100,50''.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="info_bigl" attributetype="Hypertext" description="Info biglietti">
				<names>
					<properties>
						<property key="en">info_bigl</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link_bigl" attributetype="Link" description="Link acquisto biglietti">
				<names>
					<properties>
						<property key="en">link_bigl</property>
					</properties>
				</names>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento">
								<names>
									<properties>
										<property key="en">documento</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="organizz" attributetype="Composite" description="Organizzato da">
				<names>
					<properties>
						<property key="en">organizz</property>
					</properties>
				</names>
				<attributes>
					<attribute name="link_uff" attributetype="Link" description="Scheda ufficio">
						<names>
							<properties>
								<property key="en">link_uff</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="soggetto" attributetype="Text" description="Soggetto" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">soggetto</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(!#entity.getAttribute(''organizz'').getAttribute(''link_uff'').destination.equals("") && text == "") || (#entity.getAttribute(''organizz'').getAttribute(''link_uff'').destination.equals("") && text != "")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, questo campo deve essere compilato se non presente la scheda dell Organizzazione collegata.]]></errormessage>
								<helpmessage><![CDATA[Inserire il nome del Soggetto se non presente la scheda collegata.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
					<attribute name="persona" attributetype="Text" description="Persona" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">persona</property>
							</properties>
						</names>
					</attribute>
					<attribute name="telefono" attributetype="Monotext" description="Telefono" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">telefono</property>
							</properties>
						</names>
					</attribute>
					<attribute name="reperib" attributetype="Text" description="Reperibilità" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">reperib</property>
							</properties>
						</names>
					</attribute>
					<attribute name="email" attributetype="Monotext" description="Email" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">email</property>
							</properties>
						</names>
					</attribute>
					<attribute name="sito_web" attributetype="Link" description="Sito web">
						<names>
							<properties>
								<property key="en">sito_web</property>
							</properties>
						</names>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="supporto" attributetype="Link" description="Supportato da">
				<names>
					<properties>
						<property key="en">supporto</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="figli" attributetype="Monolist" description="Lista eventi figli">
				<names>
					<properties>
						<property key="en">figli</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="figli" attributetype="Link">
						<names>
							<properties>
								<property key="en">figli</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[(destination.contains(''EVN'') && #entity.getAttribute(''genitore'').booleanValue)]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Evento non valido o il contenuto non può avere figli. Inserire un link a contenuto di tipo Evento (EVN) oppure impostare il campo Genitore.]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Evento oppure modificare il valore del campo Genitore.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ult_info</property>
					</properties>
				</names>
			</attribute>
			<attribute name="patro_den" attributetype="Text" description="Ente che patrocina l evento" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">patro_den</property>
					</properties>
				</names>
			</attribute>
			<attribute name="patro_link" attributetype="Link" description="Ente che patrocina l evento - sito web">
				<names>
					<properties>
						<property key="en">patro_link</property>
					</properties>
				</names>
			</attribute>
			<attribute name="patro_logo" attributetype="Image" description="Ente che patrocina l evento - logo">
				<names>
					<properties>
						<property key="en">patro_logo</property>
					</properties>
				</names>
			</attribute>
			<list name="sponsor" attributetype="Monolist" description="Sponsor">
				<names>
					<properties>
						<property key="en">sponsor</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="sponsor" attributetype="Composite">
						<names>
							<properties>
								<property key="en">sponsor</property>
							</properties>
						</names>
						<attributes>
							<attribute name="denominaz" attributetype="Text" description="Denominazione">
								<names>
									<properties>
										<property key="en">denominaz</property>
									</properties>
								</names>
							</attribute>
							<attribute name="sitoweb" attributetype="Link" description="Sito web">
								<names>
									<properties>
										<property key="en">sitoweb</property>
									</properties>
								</names>
							</attribute>
							<attribute name="logo" attributetype="Image" description="Immagine logo">
								<names>
									<properties>
										<property key="en">logo</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="str_polit" attributetype="Monolist" description="Struttura politica coinvolta">
				<names>
					<properties>
						<property key="en">str_polit</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="str_polit" attributetype="Composite">
						<names>
							<properties>
								<property key="en">str_polit</property>
							</properties>
						</names>
						<attributes>
							<attribute name="pol_nome" attributetype="Text" description="Nome">
								<names>
									<properties>
										<property key="en">pol_nome</property>
									</properties>
								</names>
							</attribute>
							<attribute name="pol_scheda" attributetype="Link" description="Link a scheda">
								<names>
									<properties>
										<property key="en">pol_scheda</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<names>
					<properties>
						<property key="en">box_aiuto</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<names>
							<properties>
								<property key="en">box_aiuto</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="FCS" typedescr="Focus" viewpage="focus" listmodel="500001" defaultmodel="500001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="it">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="it">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>300</maxlength>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
				</roles>
			</attribute>
			<attribute name="icona" attributetype="Monotext" description="Icona">
				<names>
					<properties>
						<property key="it">icona</property>
					</properties>
				</names>
			</attribute>
			<attribute name="logo" attributetype="Monotext" description="Logo">
				<names>
					<properties>
						<property key="it">logo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="it">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="it">immagine</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="GAL" typedescr="Galleria immagini" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="data" attributetype="Date" description="Data" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="anteprima" attributetype="Image" description="Anteprima">
				<names>
					<properties>
						<property key="en">anteprima</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<list name="immagini" attributetype="Monolist" description="Immagini">
				<names>
					<properties>
						<property key="en">immagini</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="immagini" attributetype="Composite">
						<names>
							<properties>
								<property key="en">immagini</property>
							</properties>
						</names>
						<attributes>
							<attribute name="immagine" attributetype="Image" description="Immagine">
								<names>
									<properties>
										<property key="en">immagine</property>
									</properties>
								</names>
							</attribute>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="LGO" typedescr="Luogo" viewpage="amm_07_dett" listmodel="120003" defaultmodel="120008">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="sottotit" attributetype="Text" description="Nome alternativo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">sottotit</property>
					</properties>
				</names>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia luoghi" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_07" showChildsPage="true">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione breve" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="descrizion" attributetype="Hypertext" description="Descrizione" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descrizion</property>
					</properties>
				</names>
			</attribute>
			<list name="interesse" attributetype="Monolist" description="Elementi di interesse">
				<names>
					<properties>
						<property key="en">interesse</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="interesse" attributetype="Composite">
						<names>
							<properties>
								<property key="en">interesse</property>
							</properties>
						</names>
						<attributes>
							<attribute name="titolo" attributetype="Text" description="Titolo">
								<names>
									<properties>
										<property key="en">titolo</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(!#parent.getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#parent.getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda LGO non è presente.]]></errormessage>
										<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="false">
										<ognlexpression><![CDATA[(!#parent.getAttribute(''sch_luogo'').destination.equals("") && text == "") || (#parent.getAttribute(''sch_luogo'').destination.equals("") && text != "")]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda LGO non è presente.]]></errormessage>
										<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="sch_luogo" attributetype="Link" description="Scheda luogo">
								<names>
									<properties>
										<property key="en">sch_luogo</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="galleria" attributetype="Link" description="Galleria di immagini">
				<names>
					<properties>
						<property key="en">galleria</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="video" attributetype="Link" description="Video evento">
				<names>
					<properties>
						<property key="en">video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="servizi" attributetype="Monolist" description="Servizi presenti nel luogo">
				<names>
					<properties>
						<property key="en">servizi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="servizi" attributetype="Composite">
						<names>
							<properties>
								<property key="en">servizi</property>
							</properties>
						</names>
						<attributes>
							<attribute name="srv_descr" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">srv_descr</property>
									</properties>
								</names>
							</attribute>
							<attribute name="srv_link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">srv_link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="accesso" attributetype="Hypertext" description="Modalità di accesso">
				<names>
					<properties>
						<property key="en">accesso</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="indirizzo" attributetype="Text" description="Indirizzo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">indirizzo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="gps_lat" attributetype="Monotext" description="Latitudine" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">gps_lat</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="gps_lon" attributetype="Monotext" description="Longitudine" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">gps_lon</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="cap" attributetype="Monotext" description="CAP" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">cap</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="rif_tel" attributetype="Monotext" description="Riferimento telefonico del luogo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">rif_tel</property>
					</properties>
				</names>
			</attribute>
			<attribute name="rif_email" attributetype="Monotext" description="Riferimento mail del luogo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">rif_email</property>
					</properties>
				</names>
			</attribute>
			<attribute name="rif_web" attributetype="Link" description="Riferimento sito web">
				<names>
					<properties>
						<property key="en">rif_web</property>
					</properties>
				</names>
			</attribute>
			<attribute name="orario" attributetype="Composite" description="Orario per il pubblico">
				<names>
					<properties>
						<property key="en">orario</property>
					</properties>
				</names>
				<attributes>
					<attribute name="inv_matt" attributetype="Text" description="Invernale mattina">
						<names>
							<properties>
								<property key="en">inv_matt</property>
							</properties>
						</names>
					</attribute>
					<attribute name="inv_pom" attributetype="Text" description="Invernale pomeriggio">
						<names>
							<properties>
								<property key="en">inv_pom</property>
							</properties>
						</names>
					</attribute>
					<attribute name="est_matt" attributetype="Text" description="Estivo mattina">
						<names>
							<properties>
								<property key="en">est_matt</property>
							</properties>
						</names>
					</attribute>
					<attribute name="est_pom" attributetype="Text" description="Estivo pomeriggio">
						<names>
							<properties>
								<property key="en">est_pom</property>
							</properties>
						</names>
					</attribute>
					<attribute name="continuo" attributetype="Text" description="Continuato">
						<names>
							<properties>
								<property key="en">continuo</property>
							</properties>
						</names>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="s_str_link" attributetype="LinkSearchable" description="Struttura responsabile del luogo - Link" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">s_str_link</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="s_str_nome" attributetype="Text" description="Struttura responsabile del luogo - Nome" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">s_str_nome</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''s_str_link'').destination.equals("") && text == "") || (#entity.getAttribute(''s_str_link'').destination.equals("") && text != "")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda ORG non è presente.]]></errormessage>
						<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="s_str_tel" attributetype="Monotext" description="Struttura responsabile del luogo - Riferimento telefonico" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">s_str_tel</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''s_str_link'').destination.equals("") && text == "") || (#entity.getAttribute(''s_str_link'').destination.equals("") && text != "")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda ORG non è presente.]]></errormessage>
						<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="s_str_mail" attributetype="Monotext" description="Struttura responsabile del luogo - Riferimento mail" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">s_str_mail</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''s_str_link'').destination.equals("") && text == "") || (#entity.getAttribute(''s_str_link'').destination.equals("") && text != "")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, compilare il campo solo se il link alla scheda ORG non è presente.]]></errormessage>
						<helpmessage><![CDATA[Eliminare il collegamento alla scheda, oppure lasciare vuoto questo campo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="prezzo" attributetype="Monolist" description="Prezzo">
				<names>
					<properties>
						<property key="en">prezzo</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="prezzo" attributetype="Composite">
						<names>
							<properties>
								<property key="en">prezzo</property>
							</properties>
						</names>
						<attributes>
							<attribute name="tipo" attributetype="Enumerator" description="Tipo biglietto" separator=",">
								<names>
									<properties>
										<property key="en">tipo</property>
									</properties>
								</names>
								<validations>
									<required>true</required>
								</validations>
								<![CDATA[Intero,Ridotto,Gruppi,Cumulativo,Gratuito]]>
							</attribute>
							<attribute name="costo" attributetype="Monotext" description="Costo">
								<names>
									<properties>
										<property key="en">costo</property>
									</properties>
								</names>
								<validations>
									<required>true</required>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[((text.matches("^([0-9]+,[0-9][0-9]$)") && !text.equals("0,00") && !#parent.getAttribute("tipo").text.equals("Gratuito")) || (text.equals("0") && #parent.getAttribute("tipo").text.equals("Gratuito")))]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, inserire il valore col formato ''xx,yy'' o ''0'' in caso di tipo Gratuito.]]></errormessage>
										<helpmessage><![CDATA[Inserire un valore corretto (''0'' oppure ''100,50''.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Hypertext" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="info_bigl" attributetype="Hypertext" description="Info biglietti">
				<names>
					<properties>
						<property key="en">info_bigl</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link_bigl" attributetype="Link" description="Link acquisto biglietti">
				<names>
					<properties>
						<property key="en">link_bigl</property>
					</properties>
				</names>
			</attribute>
			<attribute name="parte_di" attributetype="Link" description="Parte di">
				<names>
					<properties>
						<property key="en">parte_di</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Luogo genitore. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="sede_di" attributetype="Link" description="Il luogo è sede di">
				<names>
					<properties>
						<property key="en">sede_di</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="categoria" attributetype="EnumeratorMapCategory" description="Categoria prevalente" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="cat_ambiti">
				<names>
					<properties>
						<property key="en">categoria</property>
					</properties>
				</names>
			</attribute>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ult_info</property>
					</properties>
				</names>
			</attribute>
			<list name="box_aiuto" attributetype="Monolist" description="Box d''aiuto">
				<names>
					<properties>
						<property key="en">box_aiuto</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<names>
							<properties>
								<property key="en">box_aiuto</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="NVT" typedescr="News" viewpage="notizia" listmodel="100013" defaultmodel="100001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo della news" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="nov_01;nov_02;nov_04" showChildsPage="false">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="allerta" attributetype="EnumeratorMap" description="Tipologia di allerta meteo" searchable="true" indexingtype="TEXT" separator=";">
				<names>
					<properties>
						<property key="en">allerta</property>
					</properties>
				</names>
				<![CDATA[1=Gialla;2=Arancione;3=Rossa;]]>
			</attribute>
			<attribute name="foto" attributetype="Image" description="Immagine in evidenza">
				<names>
					<properties>
						<property key="en">foto</property>
					</properties>
				</names>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="pos_foto" attributetype="EnumeratorMap" description="Posizione immagine" separator=";">
				<names>
					<properties>
						<property key="en">pos_foto</property>
					</properties>
				</names>
				<![CDATA[top=Allineamento in alto;center=Centrata;bottom=Allineamento in basso;]]>
			</attribute>
			<attribute name="dim_foto" attributetype="Boolean" description="Immagine grande">
				<names>
					<properties>
						<property key="en">dim_foto</property>
					</properties>
				</names>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="data" attributetype="Date" description="Data della news" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<list name="persone" attributetype="Monolist" description="Persone">
				<names>
					<properties>
						<property key="en">persone</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="persone" attributetype="Link">
						<names>
							<properties>
								<property key="en">persone</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="luoghi" attributetype="Monolist" description="Luoghi">
				<names>
					<properties>
						<property key="en">luoghi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="luoghi" attributetype="Link">
						<names>
							<properties>
								<property key="en">luoghi</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="testo" attributetype="Hypertext" description="Corpo della news" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">testo</property>
					</properties>
				</names>
			</attribute>
			<attribute name="galleria" attributetype="Link" description="Galleria di immagini">
				<names>
					<properties>
						<property key="en">galleria</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="video" attributetype="Link" description="Video evento">
				<names>
					<properties>
						<property key="en">video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento">
								<names>
									<properties>
										<property key="en">documento</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="link_est" attributetype="Monolist" description="Link utili">
				<names>
					<properties>
						<property key="en">link_est</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="link_est" attributetype="Link">
						<names>
							<properties>
								<property key="en">link_est</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<names>
					<properties>
						<property key="en">box_aiuto</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<names>
							<properties>
								<property key="en">box_aiuto</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="NWS" typedescr="News - Ent" viewpage="**NULL**" listmodel="10022" defaultmodel="10002">
		<attributes>
			<attribute name="date" attributetype="Date" searchable="true">
				<names>
					<properties>
						<property key="en">date</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="title" attributetype="Text" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">title</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
				</roles>
			</attribute>
			<attribute name="abstract" attributetype="Hypertext" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">abstract</property>
					</properties>
				</names>
			</attribute>
			<attribute name="body" attributetype="Hypertext" description="Main Body" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">body</property>
					</properties>
				</names>
			</attribute>
			<attribute name="image" attributetype="Image">
				<names>
					<properties>
						<property key="en">image</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link" attributetype="Link" description="Link">
				<names>
					<properties>
						<property key="en">link</property>
					</properties>
				</names>
			</attribute>
			<attribute name="category" attributetype="Text" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">category</property>
					</properties>
				</names>
			</attribute>
			<attribute name="direction" attributetype="Enumerator" description="Direction or orientation of the content" separator=",">
				<names>
					<properties>
						<property key="en">direction</property>
					</properties>
				</names>
				<![CDATA[VERTICAL,HORIZONTAL]]>
			</attribute>
			<attribute name="argomenti" attributetype="EnumeratorMapCategory" description="Tassonomia argomenti" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="ORG" typedescr="Organizzazione" viewpage="organizzazione" listmodel="220020" defaultmodel="220001">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_01_01;amm_01_02;amm_01_03;amm_01_04;amm_01_03_02;amm_02;amm_03" showChildsPage="false">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="competenze" attributetype="Hypertext" description="Competenze" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">competenze</property>
					</properties>
				</names>
			</attribute>
			<list name="assessor" attributetype="Monolist" description="Assessore di riferimento">
				<names>
					<properties>
						<property key="en">assessor</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="assessor" attributetype="Link">
						<names>
							<properties>
								<property key="en">assessor</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="resp" attributetype="Link" description="Responsabile">
				<names>
					<properties>
						<property key="en">resp</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="persone" attributetype="Monolist" description="Persone che compongono la struttura">
				<names>
					<properties>
						<property key="en">persone</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="persone" attributetype="Link">
						<names>
							<properties>
								<property key="en">persone</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="legami" attributetype="Monolist" description="Legami con altre strutture">
				<names>
					<properties>
						<property key="en">legami</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="legami" attributetype="Link">
						<names>
							<properties>
								<property key="en">legami</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="sede" attributetype="Link" description="Sede">
				<names>
					<properties>
						<property key="en">sede</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''LGO'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Luogo non valido. Inserire un link a contenuto di tipo Luogo (LGO).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Luogo.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="indirizzo" attributetype="Text" description="Indirizzo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">indirizzo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="ind_altro" attributetype="Text" description="Piano/Scala/Stanza" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ind_altro</property>
					</properties>
				</names>
			</attribute>
			<attribute name="orario" attributetype="Composite" description="Orario per il pubblico">
				<names>
					<properties>
						<property key="en">orario</property>
					</properties>
				</names>
				<attributes>
					<attribute name="inv_matt" attributetype="Text" description="Invernale mattina">
						<names>
							<properties>
								<property key="en">inv_matt</property>
							</properties>
						</names>
					</attribute>
					<attribute name="inv_pom" attributetype="Text" description="Invernale pomeriggio">
						<names>
							<properties>
								<property key="en">inv_pom</property>
							</properties>
						</names>
					</attribute>
					<attribute name="est_matt" attributetype="Text" description="Estivo mattina">
						<names>
							<properties>
								<property key="en">est_matt</property>
							</properties>
						</names>
					</attribute>
					<attribute name="est_pom" attributetype="Text" description="Estivo pomeriggio">
						<names>
							<properties>
								<property key="en">est_pom</property>
							</properties>
						</names>
					</attribute>
					<attribute name="continuo" attributetype="Text" description="Continuato">
						<names>
							<properties>
								<property key="en">continuo</property>
							</properties>
						</names>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="gps_lat" attributetype="Monotext" description="Latitudine" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">gps_lat</property>
					</properties>
				</names>
			</attribute>
			<attribute name="gps_lon" attributetype="Monotext" description="Longitudine" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">gps_lon</property>
					</properties>
				</names>
			</attribute>
			<attribute name="cap" attributetype="Monotext" description="CAP" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">cap</property>
					</properties>
				</names>
			</attribute>
			<attribute name="telefono" attributetype="Monotext" description="Telefono" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">telefono</property>
					</properties>
				</names>
			</attribute>
			<attribute name="email" attributetype="Monotext" description="E-mail" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">email</property>
					</properties>
				</names>
			</attribute>
			<attribute name="pec" attributetype="Monotext" description="PEC" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">pec</property>
					</properties>
				</names>
			</attribute>
			<list name="sch_persone" attributetype="Monolist" description="Persone da contattare">
				<names>
					<properties>
						<property key="en">sch_persone</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="sch_persone" attributetype="Link">
						<names>
							<properties>
								<property key="en">sch_persone</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[destination.contains("PRS")]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Persona non valido. Inserire un link a contenuto di tipo Persona (PRS).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Persona.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento">
								<names>
									<properties>
										<property key="en">documento</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ult_info</property>
					</properties>
				</names>
			</attribute>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<names>
					<properties>
						<property key="en">box_aiuto</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<names>
							<properties>
								<property key="en">box_aiuto</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="PRS" typedescr="Persona" viewpage="persona" listmodel="230014" defaultmodel="230011">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="nome" attributetype="Text" description="Nome" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">nome</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="cognome" attributetype="Text" description="Cognome" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">cognome</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Ruolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Foto della persona">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione di appartenenza" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="amm_01_01;amm_01_02;amm_01_03;amm_01_04;amm_05;" showChildsPage="false">
				<names>
					<properties>
						<property key="en">sezione</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMap" description="Tipologia persona" searchable="true" indexingtype="TEXT" separator=";">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[PP=(PP) Persona Politica;PA=(PA) Persona Amministrativa]]>
			</attribute>
			<attribute name="ruolo" attributetype="EnumeratorMap" description="Ruolo nell organizzazione" searchable="true" separator=";">
				<names>
					<properties>
						<property key="en">ruolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#attribute.getMapKey().contains(#entity.getAttribute(''tipologia'').getMapKey()))]]></ognlexpression>
						<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
						<helpmessage><![CDATA[Valorizzare il ruolo corretto in base alla scelta della Tipologia di persona]]></helpmessage>
					</expression>
				</validations>
				<![CDATA[PP1=(PP) Consigliere;PP2=(PP) Consigliere Municipalità;PP3=(PP) Assessore;PP4=(PP) Sindaco;PP5=(PP) Vicesindaco;PP6=(PP) Presidente del Consiglio; PP7=(PP) Vice Presidente del Consiglio;PP8=(PP) Vice Presidente Vicario del Consiglio;PP9=(PP) Presidente Municipalità;PA1=(PA) Assistente sociale;PA2=(PA) Avvocato;PA3=(PA) Collaboratore amministrativo;PA4=(PA) Collaboratore tecnico;PA5=(PA) Comandante polizia municipale;PA6=(PA) Direttore generale;PA7=(PA) Dirigente;PA8=(PA) Dirigente tecnico;PA9=(PA) Funzionario tecnico titolare di posizione organizzativa;PA10=(PA) Funzionario titolare di posizione organizzativa;PA11=(PA) Funzionario amministrativo addetto stampa;PA12=(PA) Funzionario amministrativo;PA13=(PA) Funzionario psicologo;PA14=(PA) Funzionario tecnico;PA15=(PA) Funzionario veterinario;PA16=(PA) Istruttore informatico programmatore;PA17=(PA) Istruttore amministrativo addetto stampa;PA18=(PA) Istruttore tecnico;PA19=(PA) Istruttore agente di polizia municipale;PA20=(PA) Istruttore amministrativo;PA21=(PA) Segretario generale;PA22=(PA) Operatore;PA23=(PA) Esecutore amministrativo;PA24=(PA) Esecutore tecnico;PA25=(PA) Esecutore guardia giurata;PA26=(PA) Titolare di posizione organizzativa con delega dirigenziale;PA27=(PA) Responsabile;]]>
			</attribute>
			<list name="coll_liv1" attributetype="Monolist" description="Collegamenti all organizzazione (I livello)">
				<names>
					<properties>
						<property key="en">coll_liv1</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="coll_liv1" attributetype="Link">
						<names>
							<properties>
								<property key="en">coll_liv1</property>
							</properties>
						</names>
						<validations>
							<required>true</required>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="coll_liv2" attributetype="Monolist" description="Collegamenti all organizzazione (II livello)">
				<names>
					<properties>
						<property key="en">coll_liv2</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="coll_liv2" attributetype="Link">
						<names>
							<properties>
								<property key="en">coll_liv2</property>
							</properties>
						</names>
						<validations>
							<required>true</required>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[destination.contains(''ORG'')]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ruolo_desc" attributetype="Hypertext" description="Competenze">
				<names>
					<properties>
						<property key="en">ruolo_desc</property>
					</properties>
				</names>
			</attribute>
			<attribute name="deleghe" attributetype="Hypertext" description="Deleghe">
				<names>
					<properties>
						<property key="en">deleghe</property>
					</properties>
				</names>
			</attribute>
			<attribute name="data_insed" attributetype="Date" description="Data inizio" searchable="true">
				<names>
					<properties>
						<property key="en">data_insed</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[((#entity.getAttribute(''tipologia'').getMapKey() == ''PP'') && #attribute.getDate() != null) || (#entity.getAttribute(''tipologia'').getMapKey() == ''PA'')]]></ognlexpression>
						<errormessage><![CDATA[Attributo obbligatorio per il tipo di persona scelto]]></errormessage>
						<helpmessage><![CDATA[Inserire la data oppure variare la scelta della Tipologia di persona]]></helpmessage>
					</expression>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="data_fine" attributetype="Date" description="Data fine" searchable="true">
				<names>
					<properties>
						<property key="en">data_fine</property>
					</properties>
				</names>
				<validations>
					<rangestart attribute="data_insed" />
				</validations>
				<roles>
					<role>jpattributeextended:datascadenza</role>
				</roles>
			</attribute>
			<attribute name="galleria" attributetype="Link" description="Foto attività politica">
				<names>
					<properties>
						<property key="en">galleria</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''GAL'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a galleria immagini non valido. Inserire un link a contenuto di tipo Galleria Immagini (GAL).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Galleria Immagini.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="telefono" attributetype="Monotext" description="Numero di telefono" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">telefono</property>
					</properties>
				</names>
			</attribute>
			<attribute name="email" attributetype="Monotext" description="Indirizzo email" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">email</property>
					</properties>
				</names>
			</attribute>
			<list name="atto_nom" attributetype="Monolist" description="Atto di nomina / proclamazione / incarico">
				<names>
					<properties>
						<property key="en">atto_nom</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="atto_nom" attributetype="Attach">
						<names>
							<properties>
								<property key="en">atto_nom</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="cv" attributetype="Attach" description="Curriculum Vitae">
				<names>
					<properties>
						<property key="en">cv</property>
					</properties>
				</names>
			</attribute>
			<list name="compensi" attributetype="Monolist" description="Compensi connessi all assunzione della carica">
				<names>
					<properties>
						<property key="en">compensi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="compensi" attributetype="Attach">
						<names>
							<properties>
								<property key="en">compensi</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="importi" attributetype="Monolist" description="Spese di missione">
				<names>
					<properties>
						<property key="en">importi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="importi" attributetype="Attach">
						<names>
							<properties>
								<property key="en">importi</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="altre_car" attributetype="Monolist" description="Altre cariche e incarichi">
				<names>
					<properties>
						<property key="en">altre_car</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="altre_car" attributetype="Attach">
						<names>
							<properties>
								<property key="en">altre_car</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="patrim_sit" attributetype="Monolist" description="Situazione patrimoniale e variazioni">
				<names>
					<properties>
						<property key="en">patrim_sit</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="patrim_sit" attributetype="Attach">
						<names>
							<properties>
								<property key="en">patrim_sit</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA26'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="redditi" attributetype="Monolist" description="Dichiarazione dei redditi titolare">
				<names>
					<properties>
						<property key="en">redditi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="redditi" attributetype="Attach">
						<names>
							<properties>
								<property key="en">redditi</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA26'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="patrim_var" attributetype="Monolist" description="Situazione patrimoniale dopo cessazione">
				<names>
					<properties>
						<property key="en">patrim_var</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="patrim_var" attributetype="Attach">
						<names>
							<properties>
								<property key="en">patrim_var</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() !=''PA26'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="redditi_pa" attributetype="Monolist" description="Dichiarazione dei redditi coniuge e parenti">
				<names>
					<properties>
						<property key="en">redditi_pa</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="redditi_pa" attributetype="Attach">
						<names>
							<properties>
								<property key="en">redditi_pa</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA5'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA7'') && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'')  && (#entity.getAttribute(''ruolo'').getMapKey() != ''PA26'') && (attachPath != '''')) || (attachPath == ''''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="spese_elet" attributetype="Monolist" description="Spese elettorali">
				<names>
					<properties>
						<property key="en">spese_elet</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="spese_elet" attributetype="Attach">
						<names>
							<properties>
								<property key="en">spese_elet</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[((((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == '')) && (#entity.getAttribute(''tipologia'').getMapKey() == ''PP''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="insussist" attributetype="Monolist" description="Dich. insussistenza cause di inconf. e incompat.">
				<names>
					<properties>
						<property key="en">insussist</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="insussist" attributetype="Attach">
						<names>
							<properties>
								<property key="en">insussist</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<list name="emolumenti" attributetype="Monolist" description="Emolumenti complessivi percepiti">
				<names>
					<properties>
						<property key="en">emolumenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="emolumenti" attributetype="Attach">
						<names>
							<properties>
								<property key="en">emolumenti</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(((#entity.getAttribute(''ruolo'').getMapKey() != ''PA10'') && (attachPath != '')) || (attachPath == ''))]]></ognlexpression>
								<errormessage><![CDATA[Attributo non valido per il tipo di persona scelto]]></errormessage>
								<helpmessage><![CDATA[Lasciare il campo vuoto oppure variare la scelta della Tipologia di persona]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="ult_info" attributetype="Hypertext" description="Ulteriori informazioni" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">ult_info</property>
					</properties>
				</names>
			</attribute>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="SRV" typedescr="Servizio" viewpage="servizio" listmodel="800010" defaultmodel="800008">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="sottotit" attributetype="Text" description="Titolo alternativo/Sottotitolo" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">sottotit</property>
					</properties>
				</names>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="Tipologia del servizio" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="servizi" showChildsPage="true">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="stato" attributetype="Enumerator" description="Stato" separator=",">
				<names>
					<properties>
						<property key="en">stato</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[Attivo,Disattivo]]>
			</attribute>
			<attribute name="stato_mot" attributetype="Longtext" description="Motivo dello stato">
				<names>
					<properties>
						<property key="en">stato_mot</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(#entity.getAttribute("stato").getText().equals("Attivo")) || (#entity.getAttribute("stato").getText().equals("Disattivo") && (! #attribute.getText().equals("")))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario inserire il Motivo dello stato Disattivo.]]></errormessage>
						<helpmessage><![CDATA[Inserire una motivazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="micro" attributetype="Boolean" description="Micro servizio" searchable="true">
				<names>
					<properties>
						<property key="en">micro</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="parte_di" attributetype="LinkSearchable" description="Parte di" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">parte_di</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!#entity.getAttribute(''micro'').booleanValue && destination.equals("")) || (destination.contains(''SRV'') && #entity.getAttribute(''micro'').booleanValue)]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Servizio macro. Inserire un link a contenuto di tipo Servizio (SRV) oppure impostare il servizio come Macro servizio.]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento ad un Macro servizio, oppure cambia il valore a Macro servizio.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<roles>
					<role>jpsocial:image</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="descr_est" attributetype="Hypertext" description="Descrizione estesa" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr_est</property>
					</properties>
				</names>
			</attribute>
			<attribute name="desc_dest" attributetype="Hypertext" description="Descrizione dei destinatari" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">desc_dest</property>
					</properties>
				</names>
			</attribute>
			<attribute name="cop_geo" attributetype="Hypertext" description="Copertura geografica" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">cop_geo</property>
					</properties>
				</names>
			</attribute>
			<attribute name="come_si_fa" attributetype="Hypertext" description="Come si fa" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">come_si_fa</property>
					</properties>
				</names>
			</attribute>
			<attribute name="esito" attributetype="Hypertext" description="Output/Cosa si ottiene" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">esito</property>
					</properties>
				</names>
			</attribute>
			<attribute name="proc_esito" attributetype="Composite" description="Procedure collegate all esito">
				<names>
					<properties>
						<property key="en">proc_esito</property>
					</properties>
				</names>
				<attributes>
					<attribute name="testo" attributetype="Hypertext" description="Testo" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">testo</property>
							</properties>
						</names>
					</attribute>
					<attribute name="link_sch" attributetype="Link" description="Link scheda">
						<names>
							<properties>
								<property key="en">link_sch</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="true">
								<ognlexpression><![CDATA[(destination.contains(''ORG'')) || (destination.contains(''LGO''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione o Luogo non valido. Inserire un link a contenuto di tipo Organizzazione (ORG) o Luogo (LGO).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione o Luogo.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="can_digit" attributetype="Composite" description="Canale digitale">
				<names>
					<properties>
						<property key="en">can_digit</property>
					</properties>
				</names>
				<attributes>
					<attribute name="testo" attributetype="Hypertext" description="Testo" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">testo</property>
							</properties>
						</names>
					</attribute>
					<attribute name="link" attributetype="Link" description="Link">
						<names>
							<properties>
								<property key="en">link</property>
							</properties>
						</names>
					</attribute>
				</attributes>
			</attribute>
			<attribute name="autenticaz" attributetype="Hypertext" description="Autenticazione" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">autenticaz</property>
					</properties>
				</names>
			</attribute>
			<attribute name="can_fisico" attributetype="Hypertext" description="Canale fisico" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">can_fisico</property>
					</properties>
				</names>
			</attribute>
			<attribute name="can_prenot" attributetype="Link" description="Canale fisico - prenotazione">
				<names>
					<properties>
						<property key="en">can_prenot</property>
					</properties>
				</names>
			</attribute>
			<list name="cosa_serve" attributetype="Monolist" description="Cosa serve (documenti richiesti)">
				<names>
					<properties>
						<property key="en">cosa_serve</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="cosa_serve" attributetype="Composite">
						<names>
							<properties>
								<property key="en">cosa_serve</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Descrizione" indexingtype="TEXT">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="documento" attributetype="Attach" description="Documento">
								<names>
									<properties>
										<property key="en">documento</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="costi" attributetype="Hypertext" description="Costi" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">costi</property>
					</properties>
				</names>
			</attribute>
			<attribute name="vincoli" attributetype="Hypertext" description="Vincoli" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">vincoli</property>
					</properties>
				</names>
			</attribute>
			<attribute name="scadenze" attributetype="Hypertext" description="Fasi e scadenze" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">scadenze</property>
					</properties>
				</names>
			</attribute>
			<list name="scad_fasi" attributetype="Monolist" description="Fasi e scadenze">
				<names>
					<properties>
						<property key="en">scad_fasi</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="scad_fasi" attributetype="Composite">
						<names>
							<properties>
								<property key="en">scad_fasi</property>
							</properties>
						</names>
						<attributes>
							<attribute name="data" attributetype="Date" description="Data">
								<names>
									<properties>
										<property key="en">data</property>
									</properties>
								</names>
							</attribute>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="micro_serv" attributetype="Link" description="Macro o Micro servizio">
								<names>
									<properties>
										<property key="en">micro_serv</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''SRV'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Servizio genitore. Inserire un link a contenuto di tipo Servizio (SRV).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Servizio.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="casi_part" attributetype="Hypertext" description="Casi particolari" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">casi_part</property>
					</properties>
				</names>
			</attribute>
			<list name="uffici" attributetype="Monolist" description="Uffici">
				<names>
					<properties>
						<property key="en">uffici</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<nestedtype>
					<attribute name="uffici" attributetype="LinkSearchable" searchable="true" indexingtype="TEXT">
						<names>
							<properties>
								<property key="en">uffici</property>
							</properties>
						</names>
						<validations>
							<required>true</required>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="area" attributetype="LinkSearchable" description="Area" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">area</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Altri documenti">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
						<attributes>
							<attribute name="documento" attributetype="Attach" description="Documento">
								<names>
									<properties>
										<property key="en">documento</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento a Documento.]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="descr" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">descr</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="Link a siti esterni">
				<names>
					<properties>
						<property key="en">link</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="link" attributetype="Link">
						<names>
							<properties>
								<property key="en">link</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="cod_ipa" attributetype="EnumeratorMap" description="Codice dell’Ente erogatore (IPA)" separator=";">
				<names>
					<properties>
						<property key="en">cod_ipa</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[c_b354=Comune di Cagliari;cmdca=Citta Metropolitana di Cagliari;r_sardeg=Regione Autonoma della Sardegna;uds_ca=Universita degli Studi di Cagliari]]>
			</attribute>
			<list name="box_aiuto" attributetype="Monolist" description="Box d aiuto">
				<names>
					<properties>
						<property key="en">box_aiuto</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="box_aiuto" attributetype="Composite">
						<names>
							<properties>
								<property key="en">box_aiuto</property>
							</properties>
						</names>
						<attributes>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_amm" attributetype="Monolist" description="Correlati: amministrazione">
				<names>
					<properties>
						<property key="en">corr_amm</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_amm" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_amm</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_serv" attributetype="Monolist" description="Correlati: servizi">
				<names>
					<properties>
						<property key="en">corr_serv</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_serv" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_serv</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_nov" attributetype="Monolist" description="Correlati: novità">
				<names>
					<properties>
						<property key="en">corr_nov</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_nov" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_nov</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="corr_doc" attributetype="Monolist" description="Correlati: documenti">
				<names>
					<properties>
						<property key="en">corr_doc</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="corr_doc" attributetype="Link">
						<names>
							<properties>
								<property key="en">corr_doc</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="STM" typedescr="Sito tematico" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<attribute name="colore" attributetype="EnumeratorMap" description="Colore sfondo" separator=";">
				<names>
					<properties>
						<property key="en">colore</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<![CDATA[bg1=Verde;bg2=Blu;bg3=Arancio;bg4=Rosso;bg5=Petrolio;bg6=Mattone;bg7=Carta da zucchero;bg8=Marroncino;bg9=Viola]]>
			</attribute>
			<attribute name="icona" attributetype="Monotext" description="Icona">
				<names>
					<properties>
						<property key="en">icona</property>
					</properties>
				</names>
			</attribute>
			<attribute name="url_sito" attributetype="Link" description="URL sito">
				<names>
					<properties>
						<property key="en">url_sito</property>
					</properties>
				</names>
			</attribute>
			<attribute name="notizia" attributetype="Link" description="Notizia">
				<names>
					<properties>
						<property key="en">notizia</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''NVT'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a novità non valido. Inserire un link a contenuto di tipo Novità (NVT).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Novità.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<list name="gest_pag" attributetype="Monolist" description="Gestione pagina">
				<names>
					<properties>
						<property key="en">gest_pag</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="gest_pag" attributetype="Link">
						<names>
							<properties>
								<property key="en">gest_pag</property>
							</properties>
						</names>
						<validations>
							<expression evalOnValuedAttribute="false">
								<ognlexpression><![CDATA[(destination.contains(''ORG''))]]></ognlexpression>
								<errormessage><![CDATA[Attenzione, collegamento a Organizzazione non valido. Inserire un link a contenuto di tipo Organizzazione (ORG).]]></errormessage>
								<helpmessage><![CDATA[Inserire un collegamento a Organizzazione.]]></helpmessage>
							</expression>
						</validations>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
	<contenttype typecode="TCL" typedescr="2 columns" viewpage="**NULL**" listmodel="10024" defaultmodel="10004">
		<attributes>
			<attribute name="image" attributetype="Image" description="Image">
				<names>
					<properties>
						<property key="en">image</property>
					</properties>
				</names>
			</attribute>
			<attribute name="title" attributetype="Text" description="Title" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">title</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
				</roles>
			</attribute>
			<attribute name="subtitle" attributetype="Hypertext" description="Subtitle" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">subtitle</property>
					</properties>
				</names>
			</attribute>
			<attribute name="col1" attributetype="Hypertext" description="Column 1 content" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">col1</property>
					</properties>
				</names>
			</attribute>
			<attribute name="col2" attributetype="Hypertext" description="Column 2 content" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">col2</property>
					</properties>
				</names>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="TGN" typedescr="Testo generico" viewpage="**NULL**" listmodel="200010" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
				</roles>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jpsocial:description</role>
					<role>jacms:description</role>
				</roles>
			</attribute>
			<list name="paragrafo" attributetype="Monolist" description="Paragrafo">
				<names>
					<properties>
						<property key="en">paragrafo</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="paragrafo" attributetype="Composite">
						<names>
							<properties>
								<property key="en">paragrafo</property>
							</properties>
						</names>
						<attributes>
							<attribute name="titolo" attributetype="Text" description="Titolo">
								<names>
									<properties>
										<property key="en">titolo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="testo" attributetype="Hypertext" description="Testo">
								<names>
									<properties>
										<property key="en">testo</property>
									</properties>
								</names>
							</attribute>
							<attribute name="immagine" attributetype="Image" description="Immagine">
								<names>
									<properties>
										<property key="en">immagine</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link_txt" attributetype="Link" description="Link testo">
								<names>
									<properties>
										<property key="en">link_txt</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="allegati" attributetype="Monolist" description="Allegati">
				<names>
					<properties>
						<property key="en">allegati</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="allegati" attributetype="Attach">
						<names>
							<properties>
								<property key="en">allegati</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="Link">
				<names>
					<properties>
						<property key="en">link</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="link" attributetype="Link">
						<names>
							<properties>
								<property key="en">link</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
			<attribute name="video" attributetype="Link" description="Video">
				<names>
					<properties>
						<property key="en">video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="true">
						<ognlexpression><![CDATA[destination.contains(''VID'')]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, collegamento a Video non valido. Inserire un link a contenuto di tipo Video (VID).]]></errormessage>
						<helpmessage><![CDATA[Inserire un collegamento a Video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Immagine">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="sezione" attributetype="EnumeratorMapPage" description="Sezione di appartenenza" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="homepage;amministrazione;amm_01;servizi;novita;documenti;argomenti;" showChildsPage="true">
				<names>
					<properties>
						<property key="en">sezione</property>
					</properties>
				</names>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="icona" attributetype="Text" description="Icona">
				<names>
					<properties>
						<property key="en">icona</property>
					</properties>
				</names>
			</attribute>
			<attribute name="link_pag" attributetype="Link" description="Link a pagina di dettaglio">
				<names>
					<properties>
						<property key="en">link_pag</property>
					</properties>
				</names>
			</attribute>
		</attributes>
	</contenttype>
	<contenttype typecode="VID" typedescr="Video" viewpage="**NULL**" listmodel="**NULL**" defaultmodel="**NULL**">
		<attributes>
			<attribute name="titolo" attributetype="Text" description="Titolo" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">titolo</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jacms:title</role>
					<role>jpsocial:title</role>
					<role>jprss:title</role>
				</roles>
			</attribute>
			<attribute name="tipologia" attributetype="EnumeratorMapPage" description="tipologia video" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapPageAttributeExtractorBeans" separator=";" codePageItems="arc_vid_01;arc_vid_02" showChildsPage="false">
				<names>
					<properties>
						<property key="en">tipologia</property>
					</properties>
				</names>
				<roles>
					<role>jpattributeextended:sottosezione</role>
				</roles>
			</attribute>
			<attribute name="argomenti" attributetype="MultiEnumeratorMapCategory" description="Tassonomia argomenti" searchable="true" indexingtype="TEXT" extractorBean="enumeratorMapCategoryAttributeExtractorBeans" separator="," codeCategoryItems="arg_argomenti">
				<names>
					<properties>
						<property key="en">argomenti</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="immagine" attributetype="Image" description="Anteprima">
				<names>
					<properties>
						<property key="en">immagine</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
			</attribute>
			<attribute name="descr" attributetype="Longtext" description="Descrizione" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">descr</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
					<minlength>30</minlength>
					<maxlength>200</maxlength>
				</validations>
				<roles>
					<role>jacms:description</role>
					<role>jpsocial:description</role>
					<role>jprss:description</role>
				</roles>
			</attribute>
			<attribute name="data" attributetype="Date" description="Data" searchable="true" indexingtype="TEXT">
				<names>
					<properties>
						<property key="en">data</property>
					</properties>
				</names>
				<validations>
					<required>true</required>
				</validations>
				<roles>
					<role>jpattributeextended:datapubblicazione</role>
				</roles>
			</attribute>
			<attribute name="cod_video" attributetype="Monotext" description="Codice video">
				<names>
					<properties>
						<property key="en">cod_video</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!text.equals("") && #entity.getAttribute(''cod_canale'').text.equals("")) || (text.equals("") && !#entity.getAttribute(''cod_canale'').text.equals(""))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario inserire solo una sorgente video.]]></errormessage>
						<helpmessage><![CDATA[Compilare solo uno dei campi sorgente video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="cod_canale" attributetype="Monotext" description="Codice canale per streaming">
				<names>
					<properties>
						<property key="en">cod_canale</property>
					</properties>
				</names>
				<validations>
					<expression evalOnValuedAttribute="false">
						<ognlexpression><![CDATA[(!text.equals("") && #entity.getAttribute(''cod_video'').text.equals("")) || (text.equals("") && !#entity.getAttribute(''cod_video'').text.equals(""))]]></ognlexpression>
						<errormessage><![CDATA[Attenzione, è necessario inserire solo una sorgente video.]]></errormessage>
						<helpmessage><![CDATA[Compilare solo uno dei campi sorgente video.]]></helpmessage>
					</expression>
				</validations>
			</attribute>
			<attribute name="testo" attributetype="Hypertext" description="Testo">
				<names>
					<properties>
						<property key="en">testo</property>
					</properties>
				</names>
			</attribute>
			<list name="documenti" attributetype="Monolist" description="Documenti">
				<names>
					<properties>
						<property key="en">documenti</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="documenti" attributetype="Composite">
						<names>
							<properties>
								<property key="en">documenti</property>
							</properties>
						</names>
						<attributes>
							<attribute name="documenti" attributetype="Attach" description="Documento">
								<names>
									<properties>
										<property key="en">documenti</property>
									</properties>
								</names>
							</attribute>
							<attribute name="link" attributetype="Link" description="Link scheda documento">
								<names>
									<properties>
										<property key="en">link</property>
									</properties>
								</names>
								<validations>
									<expression evalOnValuedAttribute="true">
										<ognlexpression><![CDATA[destination.contains(''DOC'')]]></ognlexpression>
										<errormessage><![CDATA[Attenzione, collegamento a Documento. Inserire un link a contenuto di tipo Documento (DOC).]]></errormessage>
										<helpmessage><![CDATA[Inserire un collegamento ad una scheda documento (DOC)]]></helpmessage>
									</expression>
								</validations>
							</attribute>
							<attribute name="desc" attributetype="Text" description="Descrizione">
								<names>
									<properties>
										<property key="en">desc</property>
									</properties>
								</names>
							</attribute>
						</attributes>
					</attribute>
				</nestedtype>
			</list>
			<list name="link" attributetype="Monolist" description="Altri link">
				<names>
					<properties>
						<property key="en">link</property>
					</properties>
				</names>
				<nestedtype>
					<attribute name="link" attributetype="Link">
						<names>
							<properties>
								<property key="en">link</property>
							</properties>
						</names>
					</attribute>
				</nestedtype>
			</list>
		</attributes>
	</contenttype>
</contenttypes>
' where item='contentTypes';
