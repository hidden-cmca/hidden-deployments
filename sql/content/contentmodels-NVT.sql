INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100001, 'NVT', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.tipologia.mapKey != "")
							#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
<section id="intro" data-ng-controller="FiltriController">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
						#if ($content.persone && $content.persone.size() > 0)
							<div class="mt16"></div>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PEOPLE")</h4>
							<div class="argomenti-sezione-elenco persone" data-ng-cloak>
								#foreach ($persona in $content.persone)
									#set($linkDipendente = $persona.destination.replaceAll("#|C;|!",""))
									<span data-ng-init="getContent(''$linkDipendente'',''230007'')" data-ng-bind-html="renderContent[''$linkDipendente''][''230007'']"></span>
								#end
							</div>
						#end
					</div>
				</aside>
			</div>
		</div>
		<div class="row mt40">
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 col-md-3">
				<p class="data-articolo"><span>$i18n.getLabel("CITTAMETRO_CONTENT_DATE"):</span><br /><strong>$content.data.getFormattedDate("dd MMMM yyyy")</strong></p>
			</div>
			<div class="offset-xl-1 col-xl-2 offset-lg-1 col-lg-3 offset-md-1 col-md-3">
				
			</div>
		</div>
	</div>
</section>
#if ($content.dim_foto.booleanValue)
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
	<figure>
		<img src="$content.foto.getImagePath("0")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
		<figcaption>$content.foto.text</figcaption>
	</figure>
  </div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_NVT_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_NVT_documenti")</a>
						#end
						#if ($content.luoghi && $content.luoghi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luoghi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_NVT_luoghi")">$i18n.getLabel("CITTAMETRO_CONTENT_NVT_luoghi")</a>
						#end
						#if ($content.link_est && $content.link_est.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkesterni" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_NVT_link_est")">$i18n.getLabel("CITTAMETRO_CONTENT_NVT_link_est")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8 mt24">
				<div class="articolo-paragrafi">
					#if ($content.testo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.testo.text
							</div>
						</div>
					#end
					#if ($content.video.destination == "")
						#if (! $content.dim_foto.booleanValue)
							<div class="row">
								<div class="offset-lg-1 col-lg-11 col-md-12 articolo-foto-interna">
									<figure>
										<img src="$content.foto.getImagePath("5")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
										<figcaption>$content.foto.text</figcaption>
									</figure>
								</div>
							</div>
						#end
					#end
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 mt32">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 mt32">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.video.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="videoplayer">
									#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_NVT_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.documento.attachPath != '''' || $allegato.link.destination != '''')
										<div class="col-md-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($allegato.documento.attachPath != '''')
															#set ($fileNameSplit = $allegato.documento.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$allegato.documento.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.documento.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.documento.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.documento.resource.instance.fileLength)" target="_blank">
																$allegato.documento.text
															</a>
														#end
														#if ($allegato.link.destination != '''')
														<a href="$allegato.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.link.text">
															$allegato.link.text
														</a>
														#end
														<br />
														<span>$allegato.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.luoghi && $content.luoghi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luoghi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_NVT_luoghi")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 lista-eventi-figli">
								<div class="row">
								#foreach ($luogo in $content.luoghi)
									#set($linkLuogo = $luogo.destination.replaceAll("#|C;|!",""))
									#if ($linkLuogo.contains("LGO"))
										<div class="col-md-6">
											<div data-ng-init="getContent(''$linkLuogo'',''120009'')" data-ng-bind-html="renderContent[''$linkLuogo''][''120009'']"></div>
										</div>
									#end
								#end
								</div>
							</div>
						</div>
					#end
					#if ($content.link_est && $content.link_est.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-linkesterni"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_NVT_link_est")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#set ($contalink = 1)
								#foreach ($altrilink in $content.link_est)
									#if ($altrilink.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$altrilink.destination" #if($altrilink.symbolicLink.destType == 1)target="_blank"  title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $altrilink.text" #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $altrilink.text"  #end >
                        <strong>$altrilink.text</strong></a>
											</article>
										</div>
										#set ($contalink = $contalink + 1)
									#end
									#if ($contalink % 2 != 0 || $foreach.count==$content.link_est.size())
										</div>
									#end
								#end
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" #if($aiuto.link.symbolicLink.destType == 1)target="_blank" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOSITE"): $aiuto.link.text" #else title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $aiuto.link.text" #end > 
                      <strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Servizi">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Novità">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Documenti">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100001
) LIMIT 1;

INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100013, 'NVT', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    <svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg>
    <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100013
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100005, 'NVT', 'Sezioni - In evidenza (titolo+abs)', '
<article class="scheda scheda-round scheda-news">	
	<div class="scheda-testo mt32 no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100010, 'NVT', 'Homepage - Argomenti (cat+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">
		<svg class="icon">
  	#if ($content.tipologia.mapKey != "nov01")
    	<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
    #elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
		#end
	$content.titolo.text
</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100008, 'NVT', 'Sezioni - In evidenza (img+cat+dt+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-icona-small #if ($content.foto.getImagePath("0") != "") mr114 #end">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg>
		$content.tipologia.mapValue - $content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo #if($content.foto.getImagePath("0") == "")mr-0#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100008
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100006, 'NVT', 'Sezioni - In evidenza (img+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-testo mt32 #if ($content.foto.getImagePath("0") == "")no-mr-xs no-mr-sm#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>	
	<div class="scheda-argomenti">
		#set ($argomenti = $content.argomenti.values)
		#foreach ($Category in $content.getCategories())
			#if($argomenti.contains($Category.code))
				<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
			#end
		#end
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100004, 'NVT', 'Sezioni - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small  no-mr-sm no-mr-xs">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
    	<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
    #elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
    </svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100009, 'NVT', 'Homepage - In evidenza (cat+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	#if ($content.tipologia.mapKey != "")
		<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
      <svg class="icon">
      #if ($content.tipologia.mapKey != "nov01")
        <use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
      #elseif ($content.tipologia.mapKey != "nov02")
        <use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
      #end
      </svg>
      $content.tipologia.mapValue
      </a>
    #end
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNOVITA")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100007, 'NVT', 'Sezioni - In evidenza (cat+data+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
		#if ($content.tipologia.mapKey != "nov01")
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
    #elseif ($content.tipologia.mapKey != "nov02")
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
		#end
    $content.tipologia.mapValue - $content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100003, 'NVT', 'Sezioni - In evidenza (img+cat+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-icona-small">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg> $content.tipologia.mapValue
	</div>

	<div class="scheda-testo #if ($content.foto.getImagePath("0") == "")no-mr-xs no-mr-sm#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
		<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
		</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 134567, 'NVT', 'Template per email', '
#if($content.titolo && $content.titolo.text != "")
		<p>
			$content.titolo.text
		</p>
    #if($content.testo.text)
    <div>
			$content.testo.text
    </div>
    #end
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 134567
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100012, 'NVT', 'Homepage - Allerta meteo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="allerta-meteo allerta$content.allerta.mapKey">
	<article>
  	<div class="container">
			<div class="row">
				<div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
        	<img src="$imgURL/protezione_civile.png" alt="$i18n.getLabel("CITTAMETRO_PORTAL_PROTEZIONE_CIVILE")" class="img-fluid" />
        </div>
        <div class="col-lg-11 col-md-10 col-sm-10 col-xs-9">
        	<h2><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h2>
 					<p>$content.descr.text</p>
        </div>
       </div>
      </div>
  </article>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100012
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100002, 'NVT', 'Homepage - Notizia principale (titolo+abs+arg+img)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	#if ($content.foto.getImagePath("0") != "")
	<div class="novita-foto img-fit-cover"> 
		<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" />
	</div>
  #end
	<div class="novita-testo">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<h2><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">$content.titolo.text</a></h2>
					<p>$content.descr.text</p>
					<div class="argomenti">
						#set ($argomenti = $content.argomenti.values)
						#foreach ($Category in $content.getCategories())
							#if($argomenti.contains($Category.code))
								#set($argPage = $info.getConfigParameter("pagina_argomento"))
								<a href="$content.getPageURL($argPage)?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
							#end
						#end
					</div>
				<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLNEWS") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
				</div>
			</div>
		</div>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 100014, 'NVT', '*Sezioni - In evidenza (img+ico+dt+titolo+abs+arg)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-news">
	#if ($content.foto.getImagePath("0") != "")
		<div class="scheda-foto img-fit-cover">
			<figure>
				<img src="$content.foto.getImagePath("4")" alt="$content.foto.text" class="img-fluid objpos-$content.pos_foto.mapKey" />
			</figure>
		</div>
	#end
	<div class="scheda-icona-small #if ($content.foto.getImagePath("0") != "") mr114 #end">
		<svg class="icon">
		#if ($content.tipologia.mapKey != "nov01")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use>
		#elseif ($content.tipologia.mapKey != "nov02")
			<use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use>
		#end
		</svg>
		$content.data.getFormattedDate("dd MMM yyyy")
	</div>
	<div class="scheda-testo #if($content.foto.getImagePath("0") == "")mr-0#end">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTONEWS"): $content.titolo.text">	$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-argomenti">
			#set ($argomenti = $content.argomenti.values)
			#foreach ($Category in $content.getCategories())
				#if($argomenti.contains($Category.code))
					<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
				#end
			#end
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 100014
) LIMIT 1;
