INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700011, 'DOC', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($scaduto = false)
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
					#if ($content.data_scad.fullDate != "")
						#set($scaduto = $info.getExpiredDate($content.data_scad.getFormattedDate("dd/MM/yyyy")))
						#if ($scaduto && $content.sottotip.mapKey == ''doc_04_02'')
							<div class="alert alert-warning mt16" role="alert">
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_DOC_regolamento_scaduto")
							</div>
						#end
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto" class="img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid">
		<figcaption>$content.immagine.text</figcaption>
	</figure>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.oggetto.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-oggetto" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</a>
						#end
						#if ($content.descr_est.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizion" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_descrizion")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</a>
						#end
						#if ($content.canal_link.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-istanza" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_istanza")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</a>
						#end
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-datapub" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_documento")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</a>
						#end
						#if ($content.fasi && $content.fasi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-scadenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</a>
						#end
						#if ($content.allegati && $content.allegati.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-allegati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</a>
						#end
						#if ($content.dataset && $content.dataset.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-dataset" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</a>
						#end
						#if ($content.servizi && $content.servizi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</a>
						#end
						#if ($content.area.destination != "")
							#if ($content.ufficio.destination != "")
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</a>
							#else 
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</a>
							#end
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.oggetto.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-oggetto"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p>$content.oggetto.text</p>
							</div>
						</div>
					#end
					#if ($content.descr_est.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizion"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.descr_est.text
							</div>
						</div>
					#end
					#if ($content.formati && $content.formati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-8 argomenti mb0">
								<p class="argomenti-sezione-elenco">
									<span class="articolo-titoletto mt0">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_formati")</span>:
									#foreach ($formato in $content.formati)
										<a class="badge badge-pill badge-argomenti mb0">$formato.text</a>
									#end
								</p>
							</div>
						</div>
					#end
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.canal_link.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-istanza"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="mt12">
									<a class="btn btn-default btn-iscriviti" href="$content.canal_link.destination" target="_blank">$content.canal_link.text</a>
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-datapub"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</h4>
						</div>
					</div>
					<div class="row stepper mb0">
						<div class="offset-md-1 col-md-11">
							<div class="step">
								<div class="date-step">
									<span class="date-step-giorno">$content.data_pubb.getFormattedDate("dd")</span><br />
									<span class="date-step-mese">$content.data_pubb.getFormattedDate("MMM")/$content.data_pubb.getFormattedDate("yy")</span>
									<span class="pallino"></span>
								</div>
								<div class="testo-step">
									<div class="scheda-gestione">
										<p>$i18n.getLabel("CITTAMETRO_CONTENT_PUBDATE")</p>
									</div>
								</div>
							</div>
							#if ($content.data_scad.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_scad.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_scad.getFormattedDate("MMM")/$content.data_scad.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATESCAD")</p>
										</div>
									</div>
								</div>
							#end
							#if ($content.data_esito.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_esito.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_esito.getFormattedDate("MMM")/$content.data_esito.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATEESITO")</p>
										</div>
									</div>
								</div>
							#end
						</div>
					</div>
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.attachPath != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
															aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
															$allegato.text
														</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.fasi && $content.fasi.size() > 0)
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-scadenze"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</h4>
						</div>
					</div>
					<div class="row steppertime">
						<div class="offset-md-1 col-md-11">
							#foreach ($fase in $content.fasi)
								#if (($foreach.count > 1) && ((!$fase.fs_accorpa.booleanValue) || ($fase.fs_accorpa.booleanValue && $fase.fs_testo.text != "")))
												</p>
											</div>
										</div>
									</div>
								#end
								#if ($fase.fs_accorpa.booleanValue)
									#if ($fase.fs_testo.text != "")
									<div class="step">
										<div class="date-step">
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
									#else
										#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
											<br />
											#set($filescaduto = false)
											#if ($fase.fs_datasc.fullDate != "")
												#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
											#end
											#if ($filescaduto)
												#if ($fase.fs_alleg.attachPath != "")
													#set($filescadutotxt = $fase.fs_alleg.text)
												#end
												#if ($fase.fs_link.destination != "")
													#set($filescadutotxt = $fase.fs_link.text)
												#end
												<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
											#else
												#if ($fase.fs_alleg.attachPath != "")
													#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
														$fase.fs_alleg.text
													</a>
												#end
												#if ($fase.fs_link.destination != "")
													#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
														$fase.fs_link.text
													</a>
												#end
											#end
										#end
									#end
								#else
									<div class="step">
										<div class="date-step">
											#if ($fase.fs_data.fullDate != "")
												<span class="date-step-giorno">$fase.fs_data.getFormattedDate("dd")</span>
												<span class="date-step-mese">$fase.fs_data.getFormattedDate("MMM")/$fase.fs_data.getFormattedDate("yy")</span>
											#end
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])		
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
											#end
										#end
									</div>
								</div>
							</div>
						</div>
					</div>
					#end
					#if ($content.allegati && $content.allegati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($alleg in $content.allegati)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($alleg.allegato.attachPath != '''' || $alleg.link.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($alleg.link.destination != '''')
														<a href="$alleg.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $alleg.link.text">
															$alleg.link.text
														</a>
														#else 
															#if ($alleg.allegato.attachPath != '''')
																#set ($fileNameSplit = $alleg.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])		
																<a href="$alleg.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $alleg.allegato.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $alleg.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $alleg.allegato.resource.instance.fileLength)" target="_blank">
																	$alleg.allegato.text
																</a>
															#end
														#end
														
														<br />
														<span>$alleg.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.allegati.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.dataset && $content.dataset.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($datas in $content.dataset)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($datas.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<h4>
														<a href="$datas.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $datas.text">$datas.text</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.dataset.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.servizi && $content.servizi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#foreach ($servizio in $content.servizi)
									#if ($foreach.count % 2 != 0)
										<div class="row row-eq-height allegati-riga">
									#end
									<div class="col-lg-6">
										#set($linkServizio = $servizio.link.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#else
											#set($linkServizio = $servizio.link.destination.replaceAll("#|U;|!",""))
											<article class="scheda scheda-round">
												<div class="scheda-icona-small">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
												</div>
												<div class="scheda-testo-small">
													<h4><a href="$linkServizio" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $servizio.link.text">$servizio.link.text</a></h4>
													<p>$servizio.descr.text</p>
												</div>
											</article>
										#end
									</div>
									#if ($foreach.count % 2 == 0 || $foreach.count == $content.servizi.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.area.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-area"> </a>
								#if ($content.ufficio.destination != "")
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</h4>
								#else 
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</h4>
								#end
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									#if ($content.ufficio.destination != "")
										#set($linkScheda = $content.ufficio.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-lg-6">
												<div data-ng-init="getContent(''$linkScheda'',''220004'')" data-ng-bind-html="renderContent[''$linkScheda''][''220004'']"></div>
											</div>
										#end
									#end
									<div class="col-lg-6">
										#set($linkStruttura = $content.area.destination.replaceAll("#|C;|!",""))
										#if ($linkStruttura.contains("ORG"))
											<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end					
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-11">
							#if ($content.rif_norma && $content.rif_norma.size() > 0)
								<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_rif_norma"):</span>
								#set ($contadoc = 1)
								#foreach ($norma in $content.rif_norma)
									#if ($norma.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$norma.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $norma.text" #if($norma.symbolicLink.destType == 1)target="_blank" #end><strong>$norma.text</strong></a>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
										#if ($contadoc % 2 != 0 || $foreach.count==$content.rif_norma.size())
											</div>
										#end
									#end
								#end
								</p>
							#end
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.autori.text != "" || $content.licenza.text != "" || $content.protoc.text != "" || $content.protoc_dt.fullDate != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#if ($content.autori.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_autori"):</strong> $content.autori.text</p>
								#end
								#if ($content.licenza.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_licenza"):</strong> $content.licenza.text</p>
								#end
								#if ($content.protoc.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc"):</strong>
									$content.protoc.text
									#if ($content.protoc_dt.fullDate != "")
										$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc_data") $content.protoc_dt.getFormattedDate("dd/MM/yyyy")
									#end
									</p>
								#end
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong><br />
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700010, 'DOC', 'Lista contenuti (cat+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO"): $content.titolo.text">$content.titolo.text</a></h4>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700001, 'DOC', 'Sezioni - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700014, 'DOC', 'Sezioni - In evidenza (cat+titolo+abs-area)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
    <p>
      	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area"):</strong> <a href="$content.area.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.area.text">$content.area.text</a>
    </p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700014
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700005, 'DOC', 'Argomento (ico+cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700002, 'DOC', 'Homepage - In evidenza (cat+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
		</a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
		<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_ALLDOCUMENTI")
    <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700004, 'DOC', 'Pagine interne - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round scheda-breve scheda-news">
	<div class="scheda-icona-small no-mr-sm no-mr-xs">
    	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
	</div>
	<div class="scheda-testo no-mr-sm no-mr-xs">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700007, 'DOC', 'Dettaglio completo volante', '
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if (($content.tipologia.mapKey != "") || ($content.sottotip.mapKey != ""))
							#if ($content.sottotip.mapKey != "")
								#set($codicePaginaContenuto = $content.sottotip.mapKey)
							#else 	
								#set($codicePaginaContenuto = $content.tipologia.mapKey)
							#end
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($scaduto = false)
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.descr.text</p>
					#if ($content.data_scad.fullDate != "")
						#set($scaduto = $info.getExpiredDate($content.data_scad.getFormattedDate("dd/MM/yyyy")))
						#if ($scaduto && $content.sottotip.mapKey == ''doc_04_02'')
							<div class="alert alert-warning mt16" role="alert">
								<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-error_outline"></use></svg> $i18n.getLabel("CITTAMETRO_CONTENT_DOC_regolamento_scaduto")
							</div>
						#end
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto" class="img-fit-cover">
	<figure>
		<img src="$content.immagine.getImagePath("5")" alt="$content.immagine.text" class="img-fluid">
		<figcaption>$content.immagine.text</figcaption>
	</figure>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.oggetto.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-oggetto" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</a>
						#end
						#if ($content.descr_est.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizion" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_descrizion")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</a>
						#end
						#if ($content.canal_link.destination != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-istanza" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_DOC_istanza")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</a>
						#end
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-datapub" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</a>
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-doc" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_SRV_documento")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</a>
						#end
						#if ($content.fasi && $content.fasi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-scadenze" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</a>
						#end
						#if ($content.allegati && $content.allegati.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-allegati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</a>
						#end
						#if ($content.dataset && $content.dataset.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-dataset" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</a>
						#end
						#if ($content.servizi && $content.servizi.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-servizi" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</a>
						#end
						#if ($content.area.destination != "")
							#if ($content.ufficio.destination != "")
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</a>
							#else 
								<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-area" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</a>
							#end
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					#if ($content.oggetto.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-oggetto"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_oggetto")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p>$content.oggetto.text</p>
							</div>
						</div>
					#end
					#if ($content.descr_est.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizion"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_descrizion")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.descr_est.text
							</div>
						</div>
					#end
					#if ($content.formati && $content.formati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-8 argomenti mb0">
								<p class="argomenti-sezione-elenco">
									<span class="articolo-titoletto mt0">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_formati")</span>:
									#foreach ($formato in $content.formati)
										<a class="badge badge-pill badge-argomenti mb0">$formato.text</a>
									#end
								</p>
							</div>
						</div>
					#end
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.canal_link.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-istanza"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_istanza")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								<p class="mt12">
									<a class="btn btn-default btn-iscriviti" href="$content.canal_link.destination" target="_blank">$content.canal_link.text</a>
								</p>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-datapub"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_datapubscad")</h4>
						</div>
					</div>
					<div class="row stepper mb0">
						<div class="offset-md-1 col-md-11">
							<div class="step">
								<div class="date-step">
									<span class="date-step-giorno">$content.data_pubb.getFormattedDate("dd")</span><br />
									<span class="date-step-mese">$content.data_pubb.getFormattedDate("MMM")/$content.data_pubb.getFormattedDate("yy")</span>
									<span class="pallino"></span>
								</div>
								<div class="testo-step">
									<div class="scheda-gestione">
										<p>$i18n.getLabel("CITTAMETRO_CONTENT_PUBDATE")</p>
									</div>
								</div>
							</div>
							#if ($content.data_scad.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_scad.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_scad.getFormattedDate("MMM")/$content.data_scad.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATESCAD")</p>
										</div>
									</div>
								</div>
							#end
							#if ($content.data_esito.fullDate != "")
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data_esito.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data_esito.getFormattedDate("MMM")/$content.data_esito.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
											<p>$i18n.getLabel("CITTAMETRO_CONTENT_DATEESITO")</p>
										</div>
									</div>
								</div>
							#end
						</div>
					</div>
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-doc"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.attachPath != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
															aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
															$allegato.text
														</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.fasi && $content.fasi.size() > 0)
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-scadenze"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_scadenze")</h4>
						</div>
					</div>
					<div class="row steppertime">
						<div class="offset-md-1 col-md-11">
							#foreach ($fase in $content.fasi)
								#if (($foreach.count > 1) && ((!$fase.fs_accorpa.booleanValue) || ($fase.fs_accorpa.booleanValue && $fase.fs_testo.text != "")))
												</p>
											</div>
										</div>
									</div>
								#end
								#if ($fase.fs_accorpa.booleanValue)
									#if ($fase.fs_testo.text != "")
									<div class="step">
										<div class="date-step">
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])	
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
									#else
										#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
											<br />
											#set($filescaduto = false)
											#if ($fase.fs_datasc.fullDate != "")
												#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
											#end
											#if ($filescaduto)
												#if ($fase.fs_alleg.attachPath != "")
													#set($filescadutotxt = $fase.fs_alleg.text)
												#end
												#if ($fase.fs_link.destination != "")
													#set($filescadutotxt = $fase.fs_link.text)
												#end
												<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
											#else
												#if ($fase.fs_alleg.attachPath != "")
													#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
														$fase.fs_alleg.text
													</a>
												#end
												#if ($fase.fs_link.destination != "")
													#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
													#set ($last = $fileNameSplit.size() - 1)
													#set ($fileExt = $fileNameSplit[$last])
													<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
														$fase.fs_link.text
													</a>
												#end
											#end
										#end
									#end
								#else
									<div class="step">
										<div class="date-step">
											#if ($fase.fs_data.fullDate != "")
												<span class="date-step-giorno">$fase.fs_data.getFormattedDate("dd")</span>
												<span class="date-step-mese">$fase.fs_data.getFormattedDate("MMM")/$fase.fs_data.getFormattedDate("yy")</span>
											#end
										</div>
										<div class="testo-step">
											<div class="scheda-gestione">
												<p>$fase.fs_testo.text</p>
												#if ($fase.fs_alleg.attachPath != "" || $fase.fs_link.destination != "")
													<p class="link-step">
													#set($filescaduto = false)
													#if ($fase.fs_datasc.fullDate != "")
														#set($filescaduto = $info.getExpiredDate($fase.fs_datasc.getFormattedDate("dd/MM/yyyy")))
													#end
													#if ($filescaduto)
														#if ($fase.fs_alleg.attachPath != "")
															#set($filescadutotxt = $fase.fs_alleg.text)
														#end
														#if ($fase.fs_link.destination != "")
															#set($filescadutotxt = $fase.fs_link.text)
														#end
														<a>$filescadutotxt $i18n.getLabel("CITTAMETRO_CONTENT_DOC_non_disponibile")</a>
													#else
														#if ($fase.fs_alleg.attachPath != "")
															#set ($fileNameSplit = $fase.fs_alleg.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$fase.fs_alleg.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $fase.fs_alleg.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_alleg.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $fase.fs_alleg.resource.instance.fileLength)" target="_blank">
																$fase.fs_alleg.text
															</a>
														#end
														#if ($fase.fs_link.destination != "")
															#set ($fileNameSplit = $fase.fs_link.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])		
															<a href="$fase.fs_link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase()" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $fase.fs_link.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase())">
																$fase.fs_link.text
															</a>
														#end
													#end
												#end
											#end
										#end
									</div>
								</div>
							</div>
						</div>
					</div>
					#end
					#if ($content.allegati && $content.allegati.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_allegati")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($alleg in $content.allegati)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($alleg.allegato.attachPath != '''' || $alleg.link.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($alleg.link.destination != '''')
														<a href="$alleg.link.destination" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THEFILE") $alleg.link.text">
															$alleg.link.text
														</a>
														#else 
															#if ($alleg.allegato.attachPath != '''')
																#set ($fileNameSplit = $alleg.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])		
																<a href="$alleg.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $alleg.allegato.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $alleg.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $alleg.allegato.resource.instance.fileLength)" target="_blank">
																	$alleg.allegato.text
																</a>
															#end
														#end
														
														<br />
														<span>$alleg.descr.text</span>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.allegati.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.dataset && $content.dataset.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-allegati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_dataset")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($datas in $content.dataset)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($datas.destination != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<h4>
														<a href="$datas.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $datas.text">$datas.text</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.dataset.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.servizi && $content.servizi.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-servizi"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_servizi")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#foreach ($servizio in $content.servizi)
									#if ($foreach.count % 2 != 0)
										<div class="row row-eq-height allegati-riga">
									#end
									<div class="col-lg-6">
										#set($linkServizio = $servizio.link.destination.replaceAll("#|C;|!",""))
										#if ($linkServizio.contains("SRV"))
											<div data-ng-init="getContent(''$linkServizio'',''800001'')" data-ng-bind-html="renderContent[''$linkServizio''][''800001'']"></div>
										#else
											#set($linkServizio = $servizio.link.destination.replaceAll("#|U;|!",""))
											<article class="scheda scheda-round">
												<div class="scheda-icona-small">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
												</div>
												<div class="scheda-testo-small">
													<h4><a href="$linkServizio" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $servizio.link.text">$servizio.link.text</a></h4>
													<p>$servizio.descr.text</p>
												</div>
											</article>
										#end
									</div>
									#if ($foreach.count % 2 == 0 || $foreach.count == $content.servizi.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.area.destination != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-area"> </a>
								#if ($content.ufficio.destination != "")
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_areaufficio")</h4>
								#else 
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_area")</h4>
								#end
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									#if ($content.ufficio.destination != "")
										#set($linkScheda = $content.ufficio.destination.replaceAll("#|C;|!",""))
										#if ($linkScheda.contains("ORG"))
											<div class="col-lg-6">
												<div data-ng-init="getContent(''$linkScheda'',''220004'')" data-ng-bind-html="renderContent[''$linkScheda''][''220004'']"></div>
											</div>
										#end
									#end
									<div class="col-lg-6">
										#set($linkStruttura = $content.area.destination.replaceAll("#|C;|!",""))
										#if ($linkStruttura.contains("ORG"))
											<div data-ng-init="getContent(''$linkStruttura'',''220005'')" data-ng-bind-html="renderContent[''$linkStruttura''][''220005'']"></div>
										#end
									</div>
								</div>
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					#if ($content.ult_info.text != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								$content.ult_info.text
							</div>
						</div>
					#end					
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-11">
							#if ($content.rif_norma && $content.rif_norma.size() > 0)
								<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_DOC_rif_norma"):</span>
								#set ($contadoc = 1)
								#foreach ($norma in $content.rif_norma)
									#if ($norma.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$norma.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $norma.text" #if($norma.symbolicLink.destType == 1)target="_blank" #end><strong>$norma.text</strong></a>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
										#if ($contadoc % 2 != 0 || $foreach.count == $content.rif_norma.size())
											</div>
										#end
									#end
								#end
								</p>
							#end
							#if ($content.box_aiuto && $content.box_aiuto.size() > 0)
								<div class="callout important sp">
									<div class="callout-title"><svg class="icon icon-primary"><use xlink:href="$imgURL/ponmetroca.svg#ca-info"></use></svg></div>
									#foreach ($aiuto in $content.box_aiuto)
										#if ($aiuto.testo.text != "")
											$aiuto.testo.text
										#end
										#if ($aiuto.link.destination != '''')
											<p><a href="$aiuto.link.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $aiuto.link.text"><strong>$aiuto.link.text</strong></a></p>
										#end
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.autori.text != "" || $content.licenza.text != "" || $content.protoc.text != "" || $content.protoc_dt.fullDate != "")
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-11">
								#if ($content.autori.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_autori"):</strong> $content.autori.text</p>
								#end
								#if ($content.licenza.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_licenza"):</strong> $content.licenza.text</p>
								#end
								#if ($content.protoc.text != "")
									<p><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc"):</strong>
									$content.protoc.text
									#if ($content.protoc_dt.fullDate != "")
										$i18n.getLabel("CITTAMETRO_CONTENT_DOC_protoc_data") $content.protoc_dt.getFormattedDate("dd/MM/yyyy")
									#end
									</p>
								#end
							</div>
						</div>
					#end
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong><br />
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700009, 'DOC', 'Sezioni - in evidenza (titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
						<h4>
            		<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
            </h4>
						<p>$content.descr.text</p>
					</div>
					<div class="scheda-footer">
            		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
					</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700006, 'DOC', 'Sezioni - in evidenza (cat+dt+titolo+tip+dtscad)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue - <span class="datagrigia">$content.fasi_iniz.getFormattedDate("dd MMM yyyy")</span>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>
      	<br />
      	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_TIPOLOGY"):</strong> $content.tipologia.mapValue
        #if ($content.data_scad.fullDate != '')
        <br /><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DATESCAD"):</strong>$content.data_scad.getFormattedDate("dd MMMM yyyy")
        #end
      </p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700012, 'DOC', 'Lista interna (cat+titolo+abs+pubb+scad)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTODOC"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
    <p>
      	<strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_data_pubb"):</strong> $content.data_pubb.getFormattedDate("dd MMMM yyyy")
        #if ($content.data_scad.fullDate != '')
        <br /><strong>$i18n.getLabel("CITTAMETRO_CONTENT_DOC_data_scad"):</strong> $content.data_scad.getFormattedDate("dd MMMM yyyy")
        #end
      </p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700012
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700013, 'DOC', 'Sezioni - in evidenza (ico+cat+titoletto+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.tipologia.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> $content.tipologia.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700013
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700003, 'DOC', 'Sezioni - in evidenza (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_SEE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700003
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 700008, 'DOC', 'Lista ricerca (ico+cat+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.tipologia.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.tipologia.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg> <span>$content.tipologia.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 700008
) LIMIT 1;
