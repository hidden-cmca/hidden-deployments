INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 300001, 'GAL', 'Novità (slide)', '
#if ($content.immagini.size() > 0)
	<div id="owl-galleria" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
		#foreach ($elemento in $content.immagini)
			<figure>
				<div class="galleria-foto">
					<a href="$elemento.immagine.getImagePath("0")" class="venobox" data-gall="gallery-$content.getId()" data-title="$elemento.immagine.text">
						<img src="$elemento.immagine.getImagePath("3")" alt="$elemento.testo.text" class="img-fluid">
					</a>
				</div>
				<figcaption>$elemento.immagine.text</figcaption>
			</figure>
		#end
	</div>
#end

<script>
$(document).ready(function () {
	var owl2 = $(''#owl-galleria'');
	owl2.owlCarousel({
		nav:false,
		startPosition: 0,
		autoPlay:false,
		responsiveClass:true,
		responsive:{
				0:{
					items:1,
				},
				576: {
					items:2,
				},
				768: {
					items:2,
				},
				991: {
					items:3,
				},
			}
	});
	
	$(''.venobox'').venobox({
		titleattr: ''data-title''
	});
});
</script>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 300001
) LIMIT 1;
