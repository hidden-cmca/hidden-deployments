INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 110002, 'CNV', 'Dettaglio completo volante', '
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.sezione.mapKey != "")
							#set($codicePaginaContenuto = $content.sezione.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end												
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.titolo.text</h2>
					<p>$content.desc.text</p>
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#set ($argomenti = $content.argomenti.values)
							#foreach ($Category in $content.getCategories())
								#if($argomenti.contains($Category.code))
									<a href="$content.getPageURL($info.getConfigParameter("pagina_argomento"))?categoryCode=$Category.code" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>

<section id="articolo-dettaglio-testo">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						#if ($content.testo.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-descrizion" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_CNV_descrizion")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_descrizion")</a>
						#end
						#if ($content.tipologia.mapValue != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-organo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_organo")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_organo")</a>
						#end
						#if ($content.data.fullDate != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-numerodata" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_numerodata")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_numerodata")</a>
						#end
						#if ($content.luogo.sede_link.destination != "" || $content.luogo.sede_desc.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-luogo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel(	"CITTAMETRO_CONTENT_CNV_luogo")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_luogo")</a>
						#end
						#if ($content.odg && $content.odg.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-odg" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_argtrattati")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_argtrattati")</a>
						#end
						#if ($content.documenti && $content.documenti.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_documenti")</a>
						#end
						#if ($content.altriLink && $content.altriLink.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-linkcorrelati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_linkcorrelati")">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_linkcorrelati")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi" data-ng-cloak data-ng-controller="FiltriController">
					#if ($content.testo.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-descrizion"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_descrizion")</h4>
							</div>
						</div>					
						<div class="row">
							<div class="offset-md-1 col-md-8 testolungo">
								$content.testo.text
							</div>
						</div>
					#end
					#if ($content.tipologia.mapValue != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-organo"> </a>
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_organo")</h4>
							</div>
						</div>
						<div class="row listaluoghi">
							<div class="offset-md-1 col-md-11">
								<div class="row">
									<div class="col-lg-6">
										<article class="scheda-ufficio-contatti scheda-round">
											<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
											<div class="scheda-ufficio-testo">    
												<strong>$content.tipologia.mapValue</strong>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					#end
					#if ($content.data.fullDate != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-numerodata"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_numerodata")</h4>
							</div>
						</div>
						<div class="row stepper mb0">
							<div class="offset-md-1 col-md-11">
								<div class="step">
									<div class="date-step">
										<span class="date-step-giorno">$content.data.getFormattedDate("dd")</span><br />
										<span class="date-step-mese">$content.data.getFormattedDate("MMM")/$content.data.getFormattedDate("yy")</span>
										<span class="pallino"></span>
									</div>
									<div class="testo-step">
										<div class="scheda-gestione">
										    
											<p>#if ($content.data.getFormattedDate(''HH:mm'') != "00:00") $i18n.getLabel("CITTAMETRO_CONTENT_CNV_hours") $content.data.getFormattedDate(''HH:mm'') - #end $i18n.getLabel("CITTAMETRO_CONTENT_CNV_convocazione") n. $content.numero.text</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					#end
					#if ($content.luogo.sede_link.destination != "" || $content.luogo.sede_desc.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-luogo"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_luogo")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-8 listaluoghi">
								<div class="row">
									#if ($content.luogo.sede_link.destination != "" && $content.luogo.sede_link.destination.contains("LGO"))
										#set($linkScheda = $content.luogo.sede_link.destination.replaceAll("#|C;|!",""))
										<div class="col-md-9" data-ng-init="getContent(''$linkScheda'',''120004'')" data-ng-bind-html="renderContent[''$linkScheda''][''120004'']"></div>
									#else
										<div class="col-md-9">
											<article class="scheda-ufficio-contatti scheda-round">
												<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-place"></use></svg>
												<div class="scheda-ufficio-testo">
													<h4 class="mb24"><a>$content.luogo.sede_desc.text</a></h4>
												</div>
											</article>
										</div>
									#end
								</div>
							</div>
						</div>
					#end
					#set($videoscaduto = false)
					#if ($content.data.fullDate != "")
						#set($videoscaduto = $info.getExpiredDate($content.data.getFormattedDate("dd/MM/yyyy")))
					#end
					#if (($content.linkstr.destination != "") && (!$videoscaduto))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="videoplayer">
									#set($linkVideo = $content.linkstr.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
								</div>
							</div>
						</div>
					#elseif (($content.video.destination != "") && ($videoscaduto))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="videoplayer">
									#set($linkVideo = $content.video.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkVideo'',''190002'')" data-ng-bind-html="renderContent[''$linkVideo''][''190002'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.odg && $content.odg.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-odg"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_argtrattati")</h4>
							</div>
						</div>
						#set ($contaordgg = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Ordine del giorno'')
								#if ($contaordgg==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mt0 mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_ordinedelgiorno")</div>
								#end
								#set ($contaordgg = $contaordgg + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end>$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($contaordgg >=1)
										</div>
									</div>
						#end
						
						#set ($containter = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Interrogazioni'')
								#if ($containter==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_interrogazioni")</div>
								#end
								#set ($containter = $containter + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end>$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($containter >=1)
										</div>
									</div>
						#end
						
						#set ($contaordggprec = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Ordini del giorno precedenti'')
								#if ($contaordggprec==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_ordinegiornoprec")</div>
								#end
								#set ($contaordggprec = $contaordggprec + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end >$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($contaordggprec >=1)
										</div>
									</div>
						#end
						
						#set ($contaaltro = 0)
						#foreach ($argomento in $content.odg)
							#if ($argomento.tipologia.text == ''Altro'')
								#if ($contaaltro==0)
									<div class="row steppertime">
										<div class="offset-md-1 col-md-11">
											<div class="articolo-titoletto mb24">$i18n.getLabel("CITTAMETRO_CONTENT_CNV_altro")</div>
								#end
								#set ($contaaltro = $contaaltro + 1)
											<div class="step">
												<div class="testo-step ml0">
													<div class="scheda-gestione">
														<p>$argomento.paragrafo.text
														 #if ($argomento.proponente.text != '''') <br/>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_prop"): $argomento.proponente.text #end
														</p>
								#if ($argomento.linkAtto.destination != ''''	|| $argomento.allegato.attachPath != '''')
														<p class="link-step">
															#if ($argomento.linkAtto.destination != '''')
																<a href="$argomento.linkAtto.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $argomento.linkAtto.text" #if($argomento.linkAtto.symbolicLink.destType == 1)target="_blank" #end>$argomento.linkAtto.text</a>
															#end
															#if ($argomento.allegato.attachPath != '''')
																#set ($fileNameSplit = $argomento.allegato.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$argomento.allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $argomento.allegato.resource.instance.fileLength"
																aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $argomento.allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $argomento.allegato.resource.instance.fileLength)" target="_blank">
																$argomento.allegato.text
																</a>
															#end
														</p>
								#end
													</div>
												</div>
											</div>
							#end
						#end
						#if ($contaaltro >=1)
											</ul>
										</div>
									</div>
						#end
					#end
					
					#if ($content.documenti && $content.documenti.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_documenti")</h4>
							</div>
						</div>
						<div class="row schede">
							<div class="offset-md-1 col-md-11">
								#set ($contadoc = 1)
								#foreach ($allegato in $content.documenti)
									#if ($contadoc % 2 != 0)
										<div class="row allegati-riga">
									#end
									#if ($allegato.attachPath != '''')
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#set ($fileNameSplit = $allegato.resource.instance.fileName.split("\."))
														#set ($last = $fileNameSplit.size() - 1)
														#set ($fileExt = $fileNameSplit[$last])	
														<a href="$allegato.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $allegato.resource.instance.fileLength"
															aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $allegato.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $allegato.resource.instance.fileLength)" target="_blank">
															$allegato.text
														</a>
													</h4>
												</div>
											</article>
										</div>
										#set ($contadoc = $contadoc + 1)
									#end
									#if ($contadoc % 2 != 0 || $foreach.count==$content.documenti.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					#if ($content.altriLink && $content.altriLink.size() > 0)
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-linkcorrelati"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_CNV_linkcorrelati")</h4>
							</div>
						</div>
						<div class="row articolo-ulterioriinfo">
							<div class="offset-md-1 col-md-8">
								#set ($contalink = 1)
								#foreach ($altrilink in $content.altriLink)
									#if ($altrilink.destination != '''')
										#if ($foreach.count % 2 != 0)
											<div class="row">
										#end
										<div class="col-lg-6">
											<article class="scheda-verde-link scheda-round">
												<a href="$altrilink.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTO") $altrilink.text" #if($altrilink.symbolicLink.destType == 1)target="_blank" #end><strong>$altrilink.text</strong></a>
											</article>
										</div>
										#set ($contalink = $contalink + 1)
									#end
									#if ($contalink % 2 != 0 || $foreach.count==$content.altriLink.size())
										</div>
									#end
								#end
							</div>
						</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 110002
) LIMIT 1;

INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 110004, 'CNV', 'Argomento (ico+cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
	</div>
	<div class="scheda-testo scheda-testo-large">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
 		<p>$content.desc.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 110004
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 110003, 'CNV', 'Lista ricerca (ico+cat+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.sezione.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.sezione.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.desc.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 110003
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 110001, 'CNV', 'Lista tabellare (org+num+data+ora+luogo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_ORGANO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.tipologia.mapValue</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_NUMEROANNO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_NUMEROANNO'')">$content.numero.text/$content.data.getFormattedDate(''yyyy'')</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_DATA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_DATA'')">$content.data.getFormattedDate(''dd/MM/yyyy'')</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_ORA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_ORA'')">#if ($content.data.getFormattedDate(''HH:mm'') != "00:00") $content.data.getFormattedDate(''HH:mm'')#end </td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_LUOGO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_CNV_LUOGO'')">
	#if ($content.luogo.sede_link.destination != '''')
		$content.luogo.sede_link.text
	#else
		$content.luogo.sede_desc.text
  #end
</td>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 110001
) LIMIT 1;
