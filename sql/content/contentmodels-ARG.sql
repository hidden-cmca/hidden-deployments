INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400009, 'ARG', 'In evidenza foto h144 (img+titolo)', '
<article class="scheda-app h144">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full">
          <img src="$content.immagine.getImagePath(''4'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400001, 'ARG', 'Sezioni - badge (titolo)', '
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text $i18n.getLabel("CITTAMETRO_PORTAL_FROMARGUMENTSEVIDENCE")" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" class="badge badge-pill badge-argomenti">$content.titolo.text</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400001
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400011, 'ARG', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL("argomenti")" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg> <span>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400010, 'ARG', 'Link lista (titolo)', '
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<a href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400005, 'ARG', 'Lista argomenti (ico+titolo)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<article class="scheda-brick scheda-argomento-lista scheda-round">
	<div class="scheda-argomento-lista-testo">
    <div class="scheda-icona">
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
    </div>
    <h4>
      <a href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">
        $content.titolo.text
       </a>
    </h4>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400006, 'ARG', 'In evidenza foto h488 (img+titolo)', '
<article class="scheda-app h488">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full img-fit-cover">
          <img src="$content.immagine.getImagePath(''5'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400006
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400007, 'ARG', 'In evidenza foto h264 (img+titolo)', '
<article class="scheda-app h264">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full img-fit-cover">
          <img src="$content.immagine.getImagePath(''4'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400008, 'ARG', 'In evidenza foto h184 (img+titolo)', '
<article class="scheda-app h184">
	<div class="scheda-arg-img">
    #if ($content.immagine.getImagePath(''0'') != "")
    	<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">
      	<div class="bg-full img-fit-cover">
          <img src="$content.immagine.getImagePath(''4'')" alt="$content.immagine.text" />
      	</div>
    		<div class="velo"></div>
    	</a>
    #end
    <h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $content.titolo.text">$content.titolo.text</a></h4>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400008
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400004, 'ARG', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<div class="bg-argomento img-fit-cover">
	#if ($content.immagine.getImagePath("0") != "")
		<img src="$content.immagine.getImagePath(''0'')" alt="$content.immagine.text" />
	#end
</div>
<div class="container box-argomento">
	<div class="row">
		<div class="col-sm-12" id="briciole">
			<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): Home" class=""><strong>Home</strong></a><span class="separator">/</span></li>
					<li class="breadcrumb-item"><a href="$content.getPageURL("argomenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")"><strong>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</strong></a><span class="separator">/</span></li>
					<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
				</ol>
			</nav>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6">
			<div class="icona-sezione">
				<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
			</div>
			<div class="titolo-sezione">
				<h2>$content.titolo.text</h2>
				<p>$content.descr.text</p>
			</div>
		</div>
		<div class="offset-lg-1 col-lg-5 col-md-6">
			#if($content.gestione.size() > 0)
				<aside id="menu-area" data-ng-cloak data-ng-controller="FiltriController">
					<p>$i18n.getLabel("CITTAMETRO_CONTENT_MANAGE_ARG")</p>
					#foreach ($gest in $content.gestione)
						#set($linkGest = $gest.destination.replaceAll("#|C;|!",""))
						#if ($linkGest.contains("ORG"))
							<div data-ng-init="getContent(''$linkGest'',''220005'')" data-ng-bind-html="renderContent[''$linkGest''][''220005'']"></div>
						#end
					#end
				</aside>
			#end
		</div>
	</div>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400004
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400012, 'ARG', 'Titolo', '
$content.titolo.text
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400012
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400013, 'ARG', 'Descrizione', '
$content.descr.text
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400013
) LIMIT 1;

INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400002, 'ARG', 'Sezioni - In evidenza (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<article class="scheda scheda-round">
	<div class="scheda-icona">
  	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
  </div>
  <div class="scheda-testo">
  	<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
  	<p>$content.descr.text</p>
  </div>
  <div class="scheda-footer">
  	<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400002
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 400003, 'ARG', 'Homepage - In evidenza (ico+titolo+abs+links)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($paginaArg = $info.getConfigParameter("pagina_argomento"))
<article class="scheda scheda-round" data-ng-cloak data-ng-controller="FiltriController">
	<div class="scheda-icona">
	<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#$content.icona.text"></use></svg>
  </div>
  <div class="scheda-testo">
	<h4><a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text $i18n.getLabel("CITTAMETRO_PORTAL_FROMARGUMENTSEVIDENCE")" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
	<p>$content.descr.text</p>
	<div data-ng-init="setParameters(''5'', ''5'', ''NVT,EVN'', '''', ''$content.argomento.mapKey'', '''', '''', ''(order=DESC;key=jpattributeextended:datapubblicazione)'',''multi'')">
		<ul class="scheda-lista" data-ng-if="contents.length > 0" >
			<li data-ng-repeat="elem in contents">
				<span data-ng-if="elem.indexOf(''NVT'') != -1" data-ng-init="getContent(elem,''100010'')" data-ng-bind-html="renderContent[elem][''100010'']"></span>
				<span data-ng-if="elem.indexOf(''EVN'') != -1" data-ng-init="getContent(elem,''210004'')" data-ng-bind-html="renderContent[elem][''210004'']"></span>
			</li>
		</ul>
	</div>
  </div>
  <div class="scheda-footer">
	<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $content.titolo.text" href="$content.getPageURL($paginaArg)?categoryCode=$content.argomento.mapKey" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_EXPLORE_ARGUMENT") <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
  </div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 400003
) LIMIT 1;
