INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230009, 'PRS', 'Scheda lista politica (ruolo+titolo+img) ', '
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<p class="mb8">$ruolo</p>
<article class="scheda-persona scheda-round">
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4> 		
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230009
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230012, 'PRS', 'Contatti (nome+cognome)', '
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.nome.text $content.cognome.text</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230012
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230010, 'PRS', 'Scheda lista politica (ruolo+titolo)', '
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<p class="articolo-titoletto">$ruolo</p>
<div class="row articolo-ulterioriinfo">
		<div class="col-md-12">
				<div class="argomenti persone">
						<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="badge badge-pill badge-persone">$content.nome.text $content.cognome.text</a>
				</div>
		</div>
</div>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230010
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230008, 'PRS', 'Rubrica (nome+cognome+tel+uff+serv)', '
<article class="scheda scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
	<p>
		<br/>
		#if ($content.email.text != "")
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_email"):</strong> $content.email.text<br/>
		#end
		#if ($content.telefono.text != "")
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_telefono"):</strong> $content.telefono.text<br />
		#end		
		#if ($content.coll_liv1.size() > 0)
			<strong>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_area"):</strong>								
			#foreach ($coll in $content.coll_liv1)
				<a href="$coll.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $coll.text">$coll.text</a>
			#end
		#end
	</p>
</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230008
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230011, 'PRS', 'Dettaglio completo volante', '
<section id="briciole">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-10 col-md-12">
				<nav class="breadcrumb-container" aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_YOUAREIN")">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="$content.getPageURL("homepage")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") Home"><strong>Home</strong></a><span class="separator">/</span></li>
						#if ($content.sezione.mapKey != "")
							#set($codicePaginaContenuto = $content.sezione.mapKey)
							#foreach ($codicePagina in $content.getPagesByCode($codicePaginaContenuto,false))
								#set($infoPagina = $content.getPageInfoByCode($codicePagina))
								#set($visibile = $infoPagina.isShowable())
								#set($titoloPagina = $infoPagina.getTitle($content.getLangCode()))
								#if($visibile)
									<li class="breadcrumb-item"><a href="$content.getPageURL($codicePagina)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE") $titoloPagina"><strong>$titoloPagina</strong></a><span class="separator">/</span></li>
								#end
							#end
						#end												
						<li class="breadcrumb-item active" aria-current="page"><a title="$content.titolo.text">$content.titolo.text</a></li>
					</ol>
				</nav>
			</div>
		</div>
	</div>
</section>
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($incaricoscaduto = false)
#if ($content.data_fine.fullDate != "")
	#set($incaricoscaduto = $info.getExpiredDate($content.data_fine.getFormattedDate("dd/MM/yyyy")))
#end
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.nome.text $content.cognome.text</h2>
					#if($content.ruolo.mapKey != "")
					<h4>$content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length())</h4>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#foreach ($Category in $content.getCategories())
								#if($Category.getParent().code == "arg")
									#set($argPage = $Category.code.substring(4,$Category.code.length()))
									<a href="$content.getContentOnPageLink($argPage)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
		<figure>
			<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
			<figcaption>$content.immagine.text</figcaption>
		</figure>
	</div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-ruolo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</a>
						#if ($content.telefono.text != "" || $content.email.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</a>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-ruolo"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</h4>
						</div>
					</div>
					#if ($content.ruolo_desc.text != "")
					<div class="row">
						<div class="offset-md-1 col-md-8">
							<div class="testolungo">
								$content.ruolo_desc.text
							</div>
						</div>
					</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 deleghe">
							#if ($content.deleghe.text != "")
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_deleghe")</p>
								$content.deleghe.text
							#end
							#if ($content.coll_liv1.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv1)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv1.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>								
									<div class="listaluoghi">
										<div class="row">
										#foreach ($coll in $content.coll_liv1)									
											<div class="col-lg-6">
												<article class="scheda-ufficio-contatti scheda-round">
													<div class="scheda-ufficio-testo">
														#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
														<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
													</div>
												</article>
											</div>									
										#end
										</div>
									</div>
								#end
							#end
							#if ($content.coll_liv2.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_membro")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv2)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv2.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_uffici")</p>
									<div class="listaluoghi">
										<div class="row">
											#foreach ($coll in $content.coll_liv2)
												<div class="col-lg-6">
													<article class="scheda-ufficio-contatti scheda-round">
														<div class="scheda-ufficio-testo">
															#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
														</div>
													</article>
												</div>
											#end
										</div>
									</div>
								#end
							#end
							#if ($content.data_insed.fullDate != "")
								<div class="row">
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_insed")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_incarico")</span><br />
											#end
											$content.data_insed.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#if ($incaricoscaduto)
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine_incarico")</span><br />
											#end
											$content.data_fine.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.telefono.text != "" || $content.email.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									<div class="col-md-9">
										<article class="scheda-ufficio-contatti scheda-round">
											<div class="scheda-ufficio-testo">
												<p>
													#if ($content.telefono.text != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text
													#end
													#if ($content.email.text != "")
														<br>
														$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a class="scheda-contatti-email" href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a>
													#end
												</p>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					#end
					#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</h4>
							</div>
						</div>
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != ''''))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<div class="row allegati-riga">
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													#if ($content.cv.attachPath != '''')
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#set ($fileNameSplit = $content.cv.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$content.cv.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $content.cv.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $content.cv.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $content.cv.resource.instance.mimeType, $content.cv.resource.instance.fileLength)" target="_blank">
																$content.cv.text
															</a>
														</h4>
													#else
														<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_CV_non_disponibile")<br /><span>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</span></h4>
													#end
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						#end
						#if ($content.atto_nom.size() > 0)
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_atto_nom")</p>
									#foreach ($elem in $content.atto_nom)
										#if ($foreach.count % 2 != 0)
											<div class="row allegati-riga">
										#end
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($elem.attachPath != '''')
															#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																$elem.text
															</a>
														#end
													</h4>
												</div>
											</article>
										</div>
										#if ($foreach.count % 2 == 0 || $foreach.count == $content.atto_nom.size())
											</div>
										#end
									#end								
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_spese_elet")</p>
									#if ($content.spese_elet.size() > 0)
										#foreach ($elem in $content.spese_elet)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.spese_elet.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.compensi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_compensi")</p>
									#if ($content.compensi.size() > 0)
										#foreach ($elem in $content.compensi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.compensi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.importi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_importi")</p>
									#if ($content.importi.size() > 0)
										#foreach ($elem in $content.importi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.importi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.altre_car.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_altre_car")</p>
									#if ($content.altre_car.size() > 0)
										#foreach ($elem in $content.altre_car)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.altre_car.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_sit")</p>
									#if ($content.patrim_sit.size() > 0)
										#foreach ($elem in $content.patrim_sit)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_sit.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_var")</p>
									#if ($content.patrim_var.size() > 0)
										#foreach ($elem in $content.patrim_var)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_var.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.redditi.size() > 0)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi")</p>
									#if ($content.redditi.size() > 0)
										#foreach ($elem in $content.redditi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi_pa")</p>
									#if ($content.redditi_pa.size() > 0)
										#foreach ($elem in $content.redditi_pa)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi_pa.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.insussist.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_insussist")</p>
									#if ($content.insussist.size() > 0)
										#foreach ($elem in $content.insussist)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.insussist.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.emolumenti.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_emolumenti")</p>
									#if ($content.emolumenti.size() > 0)
										#foreach ($elem in $content.emolumenti)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.emolumenti.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.ult_info.text != "")
								$content.ult_info.text
							#end
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230011
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230005, 'PRS', 'Scheda lista politica (titolo+incarico+img)', '
<article class="scheda-persona scheda-round">
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
 		<p>$content.descr.text</p>
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230005
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230007, 'PRS', 'Gestione (nome+cognome)', '
<a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="badge badge-pill badge-persone">$content.nome.text $content.cognome.text</a>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230007
) LIMIT 1;


INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230006, 'PRS', 'Argomento (ico+cat+titolo+abs+img)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-icona-small mr130">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
	</div>
	<div class="scheda-testo">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
 		$content.ruolo_desc.text
	</div>
	<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230006
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230003, 'PRS', 'Sezioni - In evidenza (cat+titolo+abs)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL("$content.sezione.mapKey")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230003
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230014, 'PRS', 'Lista ricerca (ico + cat + titolo + abs + link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article>
	<div class="cerca-risultato-item">
		<a href="$content.getPageURL($content.sezione.mapKey)" class="categoria" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> <span>$content.sezione.mapValue</span></a>
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="">$content.titolo.text</a></h4>
		<p>$content.descr.text</p>
		<a aria-label="$i18n.getLabel("CITTAMETRO_PORTAL_READMORE") - $i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_READMORE"): <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230014
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230001, 'PRS', 'Dettaglio completo', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($incaricoscaduto = false)
#if ($content.data_fine.fullDate != "")
	#set($incaricoscaduto = $info.getExpiredDate($content.data_fine.getFormattedDate("dd/MM/yyyy")))
#end
<section id="intro">
	<div class="container">
		<div class="row">
			<div class="offset-lg-1 col-lg-6 col-md-8">
				<div class="titolo-sezione">
					<h2>$content.nome.text $content.cognome.text</h2>
					#if($content.ruolo.mapKey != "")
					<h4>$content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length())</h4>
					#end
				</div>
			</div>
			<div class="offset-lg-1 col-lg-3 col-md-4">
				<aside id="argomenti-sezione">
					<div class="argomenti">
						<div class="condividi">
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREMENU")" id="shareActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" class="mr16 dropdown-toggle"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-share"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SHARE")</a>	
								<div class="dropdown-menu shadow-lg" aria-labelledby="shareActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-facebook"></use></svg>
													Facebook
												</a>
											</li>
											<li>
												#set($titoloC = $content.titolo.text.replaceAll("''","%27"))
												<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-twitter"></use></svg>
													Twitter
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-linkedin"></use></svg>
													Linkedin
												</a>
											</li>
                      <li>
												<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-whatsapp"></use></svg>
													WhatsApp
												</a>
											</li>
											<li>
												<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-telegram"></use></svg>
													Telegram
												</a>
											</li>
										 </ul>
									</div>
								</div>
							</div>
							<div class="dropdown d-inline">
								<a href="#" title="$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")" id="viewActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-more_vert"></use></svg>$i18n.getLabel("CITTAMETRO_PORTAL_SEEACTION")</a>
								<div class="dropdown-menu shadow-lg" aria-labelledby="viewActions">
									<div class="link-list-wrapper">
										<ul class="link-list">
											<li>
												#set($titoloDownload = $content.titolo.text.replaceAll("''","\\''"))
												<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-save_alt"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD")
												</a>
											</li>
											<li>
												<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-print"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_PRINT")
												</a>
											</li>
											<li>
												<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">
													<svg class="icon icon-primary left"><use xlink:href="$imgURL/ponmetroca.svg#ca-email"></use></svg>
													$i18n.getLabel("CITTAMETRO_CONTENT_SEND")
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<h4>$i18n.getLabel("CITTAMETRO_CONTENT_ARGUMENTS")</h4>
						<div class="argomenti-sezione-elenco">
							#foreach ($Category in $content.getCategories())
								#if($Category.getParent().code == "arg")
									#set($argPage = $Category.code.substring(4,$Category.code.length()))
									<a href="$content.getContentOnPageLink($argPage)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOARGUMENT"): $Category.title" class="badge badge-pill badge-argomenti">$Category.title</a>
								#end
							#end
						</div>	
					</div>
				</aside>
			</div>
		</div>
	</div>
</section>
#if ($content.immagine.getImagePath("0") != "")
<section id="articolo-dettaglio-foto">
	<div class="articolo-foto img-fit-cover">
		<figure>
			<img src="$content.immagine.getImagePath("0")" alt="$content.immagine.text" class="img-fluid" />
			<figcaption>$content.immagine.text</figcaption>
		</figure>
	</div>
</section>
#end
<section id="articolo-dettaglio-testo" data-ng-cloak data-ng-controller="FiltriController">
	<div class="container profilo-dettaglio-testo">	
		<div class="row">
			<div class="linetop-lg"></div>
			<div class="col-lg-3 col-md-4 lineright">
				<aside id="menu-sinistro">
					<h4 class="dropdown">
						<a data-toggle="collapse" href="#lista-paragrafi" role="button" aria-expanded="true" aria-controls="lista-paragrafi">
							$i18n.getLabel("CITTAMETRO_CONTENT_PAGE_INDEX")
							<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
						</a>
					</h4>
					<div class="menu-separatore"><div class="bg-oro"></div></div>
					<div id="lista-paragrafi" class="list-group collapse show">
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-ruolo" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</a>
						#if ($content.telefono.text != "" || $content.email.text != "")
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-contatti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</a>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-documenti" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</a>
						#end
						<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-info" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")">$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</a>
						#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
							<a class="list-group-item list-group-item-action" href="$content.getContentLink()#articolo-par-correlati" title="$i18n.getLabel("CITTAMETRO_CONTENT_GOTOPARAGRAPH") $i18n.getLabel("CITTAMETRO_CONTENT_RELATED")">$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</a>
						#end
					</div>
				</aside>
			</div>
			<div class="col-lg-9 col-md-8 pt8">
				<div class="articolo-paragrafi">
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-ruolo"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_ruolo")</h4>
						</div>
					</div>
					#if ($content.ruolo_desc.text != "")
					<div class="row">
						<div class="offset-md-1 col-md-8">
							<div class="testolungo">
								$content.ruolo_desc.text
							</div>
						</div>
					</div>
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 deleghe">
							#if ($content.deleghe.text != "")
								<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_deleghe")</p>
								$content.deleghe.text
							#end
							#if ($content.coll_liv1.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv1)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv1.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_partedi")</p>								
									<div class="listaluoghi">
										<div class="row">
										#foreach ($coll in $content.coll_liv1)									
											<div class="col-lg-6">
												<article class="scheda-ufficio-contatti scheda-round">
													<div class="scheda-ufficio-testo">
														#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
														<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
													</div>
												</article>
											</div>									
										#end
										</div>
									</div>
								#end
							#end
							#if ($content.coll_liv2.size() > 0)
								#if ($content.tipologia.mapKey == "PP")
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_membro")</p>
									<div class="schede">
										#foreach ($coll in $content.coll_liv2)
											#if ($foreach.count % 2 != 0)
												<div class="row row-eq-height">
											#end
											<div class="col-md-6">
												#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
												<div data-ng-init="getContent(''$linkCont'',''220019'')" data-ng-bind-html="renderContent[''$linkCont''][''220019'']"></div>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.coll_liv2.size())
												</div>
											#end
										#end
									</div>
								#else
									<p class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_uffici")</p>
									<div class="listaluoghi">
										<div class="row">
											#foreach ($coll in $content.coll_liv2)
												<div class="col-lg-6">
													<article class="scheda-ufficio-contatti scheda-round">
														<div class="scheda-ufficio-testo">
															#set($linkCont = $coll.destination.replaceAll("#|C;|!",""))
															<div data-ng-init="getContent(''$linkCont'',''220016'')" data-ng-bind-html="renderContent[''$linkCont''][''220016'']"></div>
														</div>
													</article>
												</div>
											#end
										</div>
									</div>
								#end
							#end
							#if ($content.data_insed.text != "")
								<div class="row">
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_insed")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_incarico")</span><br />
											#end
											$content.data_insed.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#if ($incaricoscaduto)
									<div class="col-md-6">
										<p>
											#if ($content.tipologia.mapKey == "PP")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine")</span><br />
											#end
											#if ($content.tipologia.mapKey == "PA")
												<span class="articolo-titoletto">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_data_fine_incarico")</span><br />
											#end
											$content.data_fine.getFormattedDate("dd/MM/yyyy")
										</p>	
									</div>
									#end
								</div>
							#end
						</div>
					</div>
					#if ($content.galleria.destination != "")
						<div class="row">
							<div class="col-md-12 paragrafo">
								<div class="galleriasfondo"></div>
							</div>
							<div class="offset-md-1 col-md-11 paragrafo">
								<div class="galleriaslide">
									<h4>$i18n.getLabel("CITTAMETRO_CONTENT_EVN_galleria")</h4>
									#set($linkGalleria = $content.galleria.destination.replaceAll("#|C;|!",""))
									<div data-ng-init="getContent(''$linkGalleria'',''300001'')" data-ng-bind-html="renderContent[''$linkGalleria''][''300001'']"></div>
								</div>
							</div>
						</div>
					#end
					#if ($content.telefono.text != "" || $content.email.text != "")
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-contatti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_contatti")</h4>
							</div>
						</div>
						<div class="row">
							<div class="offset-md-1 col-md-11 listaluoghi">
								<div class="row">
									<div class="col-md-9">
										<article class="scheda-ufficio-contatti scheda-round">
											<div class="scheda-ufficio-testo">
												<p>
													#if ($content.telefono.text != "")
														$i18n.getLabel("CITTAMETRO_PORTAL_PHONE"): $content.telefono.text
													#end
													#if ($content.email.text != "")
														<br>
														$i18n.getLabel("CITTAMETRO_PORTAL_EMAIL"): <a class="scheda-contatti-email" href="mailto:$content.email.text" title="$i18n.getLabel("CITTAMETRO_PORTAL_WRITEEMAIL")">$content.email.text</a>
													#end
												</p>
											</div>
										</article>
									</div>
								</div>
							</div>
						</div>
					#end
					#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != "") || ($content.compensi.size() > 0) || ($content.importi.size() > 0 ) || ($content.altre_car.size() > 0) || ($content.atto_nom.size() > 0) || ($content.patrim_sit.size() > 0) || ($content.redditi.size() > 0) || ($content.spese_elet.size() > 0) || ($content.patrim_var.size() > 0) || ($content.redditi_pa.size() > 0) || ($content.insussist.size() > 0) || ($content.emolumenti.size() > 0))
						<div class="row">
							<div class="offset-md-1 col-md-11 paragrafo">
								<a id="articolo-par-documenti"> </a>
								<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_documenti")</h4>
							</div>
						</div>
						#if (($content.tipologia.mapKey == "PP") || ($content.cv.attachPath != ''''))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<div class="row allegati-riga">
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													#if ($content.cv.attachPath != '''')
														<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
														<h4>
															#set ($fileNameSplit = $content.cv.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$content.cv.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $content.cv.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $content.cv.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $content.cv.resource.instance.mimeType, $content.cv.resource.instance.fileLength)" target="_blank">
																$content.cv.text
															</a>
														</h4>
													#else
														<h4>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_CV_non_disponibile")<br /><span>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</span></h4>
													#end
												</div>
											</article>
										</div>
									</div>
								</div>
							</div>
						#end
						#if ($content.atto_nom.size() > 0)
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_atto_nom")</p>
									#foreach ($elem in $content.atto_nom)
										#if ($foreach.count % 2 != 0)
											<div class="row allegati-riga">
										#end
										<div class="col-lg-6">
											<article class="allegato">
												<div class="scheda-allegato">
													<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
													<h4>
														#if ($elem.attachPath != '''')
															#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
															#set ($last = $fileNameSplit.size() - 1)
															#set ($fileExt = $fileNameSplit[$last])
															<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																$elem.text
															</a>
														#end
													</h4>
												</div>
											</article>
										</div>
										#if ($foreach.count % 2 == 0 || $foreach.count == $content.atto_nom.size())
											</div>
										#end
									#end								
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_spese_elet")</p>
									#if ($content.spese_elet.size() > 0)
										#foreach ($elem in $content.spese_elet)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.spese_elet.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.compensi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_compensi")</p>
									#if ($content.compensi.size() > 0)
										#foreach ($elem in $content.compensi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.compensi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.importi.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_importi")</p>
									#if ($content.importi.size() > 0)
										#foreach ($elem in $content.importi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.importi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (($content.tipologia.mapKey == "PP") || ($content.altre_car.size() > 0))
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_altre_car")</p>
									#if ($content.altre_car.size() > 0)
										#foreach ($elem in $content.altre_car)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.altre_car.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_sit")</p>
									#if ($content.patrim_sit.size() > 0)
										#foreach ($elem in $content.patrim_sit)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_sit.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($incaricoscaduto)
							#if	(($content.tipologia.mapKey == "PP") || ($content.ruolo.mapKey == "PA6") || ($content.ruolo.mapKey == "PA21"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_patrim_var")</p>
									#if ($content.patrim_var.size() > 0)
										#foreach ($elem in $content.patrim_var)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.patrim_var.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.redditi.size() > 0)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi")</p>
									#if ($content.redditi.size() > 0)
										#foreach ($elem in $content.redditi)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if (!$incaricoscaduto)
							#if	(($content.ruolo.mapKey != "PA7") && ($content.ruolo.mapKey != "PA10") && ($content.ruolo.mapKey != "PA26"))
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_redditi_pa")</p>
									#if ($content.redditi_pa.size() > 0)
										#foreach ($elem in $content.redditi_pa)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.redditi_pa.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.insussist.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_insussist")</p>
									#if ($content.insussist.size() > 0)
										#foreach ($elem in $content.insussist)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.insussist.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
						#if ($content.emolumenti.size() > 0)
							#if	($content.ruolo.mapKey != "PA10")
							<div class="row schede">
								<div class="offset-md-1 col-md-11">
									<p class="articolo-titoletto mb12">$i18n.getLabel("CITTAMETRO_CONTENT_PRS_emolumenti")</p>
									#if ($content.emolumenti.size() > 0)
										#foreach ($elem in $content.emolumenti)
											#if ($foreach.count % 2 != 0)
												<div class="row allegati-riga">
											#end
											<div class="col-lg-6">
												<article class="allegato">
													<div class="scheda-allegato">
														#if ($elem.attachPath != '''')
															<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-attach_file"></use></svg>
															<h4>
																#set ($fileNameSplit = $elem.resource.instance.fileName.split("\."))
																#set ($last = $fileNameSplit.size() - 1)
																#set ($fileExt = $fileNameSplit[$last])
																<a href="$elem.attachPath" title="$i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase() - $elem.resource.instance.fileLength" aria-label="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOADFILE") $elem.text ($i18n.getLabel("CITTAMETRO_CONTENT_FILE") $fileExt.toUpperCase(), $elem.resource.instance.fileLength)" target="_blank">
																	$elem.text
																</a>
															</h4>
														#end
													</div>
												</article>
											</div>
											#if ($foreach.count % 2 == 0 || $foreach.count == $content.emolumenti.size())
												</div>
											#end
										#end
									#else
										<p class="mt0"><em>$i18n.getLabel("CITTAMETRO_CONTENT_PRS_non_disponibile")</em></p>
									#end
								</div>
							</div>
							#end
						#end
					#end
					<div class="row">
						<div class="offset-md-1 col-md-11 paragrafo">
							<a id="articolo-par-info"> </a>
							<h4>$i18n.getLabel("CITTAMETRO_CONTENT_MOREINFO")</h4>
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							#if ($content.ult_info.text != "")
								$content.ult_info.text
							#end
						</div>
					</div>
					<div class="row articolo-ulterioriinfo">
						<div class="offset-md-1 col-md-8">
							<div class="row">
								<div class="col-md-12 mt16">
									<p>$i18n.getLabel("CITTAMETRO_CONTENT_LASTUPDATE")</p>
									<p class="data-articolo">
										<strong>$content.getLastModified("dd/MM/yyyy, H:mm")</strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
	</div>
</section>
#if ($content.corr_amm.size() > 0 || $content.corr_serv.size() > 0 || $content.corr_nov.size() > 0 || $content.corr_doc.size() > 0)
<a id="articolo-par-correlati"> </a>
<section id="contenuti-correlati" class="bg-grigio">
	<div class="container">
		<div class="row">
			<div class="col-md-12 paragrafo">
				<div class="titolosezione text-center"><h3>$i18n.getLabel("CITTAMETRO_CONTENT_RELATED")</h3></div>
			</div>
		</div>
		<div class="row row-eq-height">
			<div id="owl-correlati" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist">
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("amministrazione")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_AMMINISTRAZIONE")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_amm)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("servizi")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-settings"></use></svg>
									$i18n.getLabel("CITTAMETRO_PORTAL_SERVIZI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_serv)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("novita")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-today"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_NOVITA")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_nov)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
				<div class="item">
					<article class="scheda scheda-round scheda-correlati">
						<div class="scheda-testo">
							<h4>
								<a href="$content.getPageURL("documenti")" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")">
									<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-description"></use></svg>
									 $i18n.getLabel("CITTAMETRO_PORTAL_DOCUMENTI")
								</a>
							</h4>
							<ul>
								#set($nlink = 0) 
								#foreach ($correlato in $content.corr_doc)
									<li><a href="$correlato.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $correlato.text">$correlato.text</a></li>
									#set($nlink = $nlink + 1)
								#end
								#if ($nlink == 0)
									<li class="sr-only">$i18n.getLabel("CITTAMETRO_CONTENT_NORELATED")</li>
								#end
							</ul>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
#end
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230001
) LIMIT 1;




INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230013, 'PRS', 'Sezioni - in evidenza (titolo+incarico+img)', '
<article class="scheda scheda-persona scheda-round">
	<div class="scheda-testo mt16">
		<h4><a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.nome.text $content.cognome.text">$content.nome.text $content.cognome.text</a></h4>
		<p>$content.descr.text</p>
</div>
<div class="scheda-foto img-fit-cover">
 			<figure>
 				<img src="$content.immagine.getImagePath("3")" alt="$content.immagine.text" />
 			</figure>
</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230013
) LIMIT 1;




INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230004, 'PRS', 'Sezioni - in evidenza (ico+cat+titoletto+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
  	<a href="$content.getPageURL($content.sezione.mapKey)" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.sezione.mapValue">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg> $content.sezione.mapValue
    </a>
	</div>
	<div class="scheda-testo-small">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230004
) LIMIT 1;



INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230002, 'PRS', 'Sezioni - in evidenza (ico+titolo+abs+link)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
<article class="scheda scheda-round">
	<div class="scheda-icona-small">
			<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-account_balance"></use></svg>
	</div>
	<div class="scheda-testo mt16">
			<h4>
         <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text">$content.titolo.text</a>
      </h4>
			<p>$content.descr.text</p>
	</div>
	<div class="scheda-footer">
      <a href="$content.getContentLink()" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $content.titolo.text" class="tutte">$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE")
      <svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-arrow_forward"></use></svg></a>	
	</div>
</article>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230002
) LIMIT 1;




INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230016, 'PRS', 'Amm. trasparente (nominativo+carica+inizio+fine)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_NOMINATIVO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.cognome.text $content.nome.text</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_CARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_CARICA'')">$ruolo</td>

<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INIZIOCARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INIZIOCARICA'')">$content.data_insed.getFormattedDate(''dd MMM yyyy'')</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')">$content.data_fine.getFormattedDate(''dd MMM yyyy'')</td>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230016
) LIMIT 1;




INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230017, 'PRS', 'Amm. trasparente (nominativo+incarico+fine)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_NOMINATIVO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.cognome.text $content.nome.text</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')">$ruolo</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_FINECARICA'')">
	$content.data_fine.getFormattedDate(''dd MMM yyyy'')
</td>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230017
) LIMIT 1;




INSERT INTO contentmodels (modelid, contenttype, descr, model, stylesheet)
SELECT * FROM (SELECT 230015, 'PRS', 'Amm. trasparente (nominativo+incarico+area)', '
#set($imgURL = $info.getConfigParameter("resourceRootURL")+"cittametro-homepage-bundle/static/img")
#set($ruolo = $content.ruolo.mapValue.substring(4,$content.ruolo.mapValue.length()))
<th scope="row" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_NOMINATIVO'')">
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-piu" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEEALL''): $content.titolo.text" onclick="espandiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-add"></use></svg>
	</button>
	<button class="btn btn-default btn-trasparente btn-tondo ml0 mr0 tabella-resp-meno" title="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_SEELESS''): $content.titolo.text" onclick="chiudiRiga(this);">
		<svg class="icon"><use xlink:href="$imgURL/ponmetroca.svg#ca-remove"></use></svg>
	</button>
	<a href="$content.getContentLink()" title="$i18n.getLabel(''CITTAMETRO_PORTAL_GOTOPAGE''): $content.titolo.text">$content.cognome.text $content.nome.text</a>
</th>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_INCARICO'')">$ruolo</td>
<td class="tabella-campo-coll" data-th="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_AREA'')" data-thead="$i18n.getLabel(''CITTAMETRO_PORTAL_TABLE_PRS_AREA'')">
	#if ($content.coll_liv1.size() > 0)
		#foreach ($coll in $content.coll_liv1)
			<a href="$coll.destination" title="$i18n.getLabel("CITTAMETRO_PORTAL_GOTOPAGE"): $coll.text">$coll.text</a><br/>
		#end
	#end
</td>
','') AS tmp
WHERE NOT EXISTS (
    SELECT modelid FROM contentmodels WHERE modelid = 230015
) LIMIT 1;
