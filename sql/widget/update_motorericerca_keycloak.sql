/**
 * Author:  glonis
 * Author:  frafal
 * Author:  gsprega
 * Created: 27-apr-2022
 */

-- Modifica etichetta �CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE�
UPDATE localstrings SET stringvalue='Tutti i risultati' WHERE keycode='CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE';



-- Modifica etichetta �CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT�
UPDATE localstrings SET stringvalue='Solo contenuti attivi' WHERE keycode='CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT';



-- Nuova etichetta �CITTAMETRO_PORTAL_RELEVANCE� e �CITTAMETRO_PORTAL_FILTER�
INSERT INTO localstrings (keycode, langcode, stringvalue) VALUES ('CITTAMETRO_PORTAL_RELEVANCE','it','Più rilevanti');
INSERT INTO localstrings (keycode, langcode, stringvalue) VALUES ('CITTAMETRO_PORTAL_RELEVANCE','en','Più rilevanti');
INSERT INTO localstrings (keycode, langcode, stringvalue) VALUES ('CITTAMETRO_PORTAL_FILTER','it','Filtra');
INSERT INTO localstrings (keycode, langcode, stringvalue) VALUES ('CITTAMETRO_PORTAL_FILTER','en','Filtra');

--Query update body per risolvere problemi CSP in import js
UPDATE pagemodels SET templategui = REPLACE(templategui, '<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style">', '<body class="push-body" data-ng-app="ponmetroca" data-ng-csp="no-inline-style;no-unsafe-eval">');


--Query update widgetcatalog
UPDATE widgetcatalog SET parameters = '<config>
    <parameter name="facetRootNodes">Facet Category Root</parameter>
    <parameter name="contentTypesFilter">Content Type (optional)</parameter>
    <action name="solrFacetNavTreeConfig"/>
</config>' WHERE code = 'cagliari_widget_advsearch_results';


--Query bonifica modelli con Condividi e Vedi azioni
UPDATE contentmodels SET model = REPLACE(model,'<a href="#" onclick="javascript:condividiSocial(''facebook'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">','<a data-socialtype="facebook" class="list-item left-icon btn-condividi" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Facebook">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="#" onclick="javascript:condividiSocial(''twitter'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">','<a data-socialtype="twitter" data-socialtitle="$titoloC" class="list-item left-icon btn-condividi" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Twitter">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="#" onclick="javascript:condividiSocial(''linkedin'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">','<a data-socialtype="linkedin" class="list-item left-icon btn-condividi" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Linkedin">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="#" onclick="javascript:condividiSocial(''whatsapp'','''');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">','<a data-socialtype="whatsapp" class="list-item left-icon btn-condividi" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): WhatsApp">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="#" onclick="javascript:condividiSocial(''telegram'','''',''$titoloC'');return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">','<a data-socialtype="telegram" data-socialtitle="$titoloC" class="list-item left-icon btn-condividi" title="$i18n.getLabel("CITTAMETRO_PORTAL_SHAREON"): Telegram">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="" onclick="saveAs(new Blob([$(''html'').html()], {type: ''text/plain;charset=utf-8''}), ''$titoloDownload''+''.html'');" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">','<a id="btn-salva" data-titolo="$titoloDownload" class="list-item left-icon btn-azioni" title="$i18n.getLabel("CITTAMETRO_CONTENT_DOWNLOAD") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="#" onclick="window.print();return false;" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">','<a class="list-item left-icon btn-azioni" id="btn-stampa" title="$i18n.getLabel("CITTAMETRO_CONTENT_PRINT") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">');
UPDATE contentmodels SET model = REPLACE(model,'<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">','<a href="mailto:?subject=$i18n.getLabel("CITTAMETRO_PORTAL_SEEPAGE")&amp;body=$content.getContentLink()" class="list-item left-icon btn-azioni" title="$i18n.getLabel("CITTAMETRO_CONTENT_SEND") $i18n.getLabel("CITTAMETRO_CONTENT_THECONTENT")">');




-- Modifiche fragment per motore ricerca
UPDATE guifragment SET gui='<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.categories var="systemCategories" root="arg_argomenti" />
<@wp.currentPage param="code" var="code" />
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="jpfacetnav_results" listResult=false />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />
<@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" />
<#if (!tipiContenuto??)><#assign tipiContenuto = ""/></#if>

<!-- Intestazione -->
<div class="container header" data-ng-controller="ctrlRicerca as ctrl" >
	<div class="row clearfix header-row">
		<div class="col-xl-6 col-lg-6 col-md-7 col-sm-12 comune">
			<div class="logoprint">
				<h1>
					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/STEMMA_CMDCA_20170329_TRATTO.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
					<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" />
				</h1>
			</div>
			<div class="logoimg">
				<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />">
					<img src="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/STEMMA_CMDCA_20170329_TRATTO.svg" alt="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_IMGALT" />"/>
				</a>
			</div>
			<div class="logotxt">
				<h1>
					<a href="<@wp.url page="homepage" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" />"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_TEXT" /></a>
				</h1>
			</div>
			<!-- pulsante ricerca mobile -->
			<div class="p_cercaMobile clearfix">
				<button aria-label="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" type="button"
					data-ng-click="visible=true; ctrl.Ricerca.setFocusRicerca($event)">
					<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- pulsante ricerca mobile -->
		</div>

		<div class="col-xl-4 col-lg-4 d-none d-lg-block d-md-none pull-right text-right">
			<!-- social-->
			<ul class="list-inline text-right social">
				<li class="small list-inline-item"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL" /></li>
				
				<li class="list-inline-item"><a target="_blank" href="https://www.facebook.com/cittametropolitanadicagliari/" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - Facebook - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" />"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-facebook"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_FB" /></span></a></li>
				<li class="list-inline-item"><a target="_blank" href="https://www.youtube.com/channel/UCuhKPaORUxFGD6rY5DLhIzg" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> - YouTube - <@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-youtube"></use></svg><span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span></a></li>
				<li class="list-inline-item">
					<a target="_blank" 
						href="https://www.instagram.com/cittametroca/?utm_source=ig_profile_share&igshid=vz1lk7zotn77" 
						aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_EXT_SITE" /> 
						- Instagram - 
						<@wp.i18n key="CITTAMETRO_PORTAL_NEW_WIN" />" 
						title="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" />">
							<svg class="icon">
								<use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-instagram"></use>
							</svg>
							<span class="hidden"><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SOCIAL_YT" /></span>
					</a>
				</li>
				<li class="list-inline-item"><a aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> - RSS" href="<@wp.url page="rss" />" title="RSS"><svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-rss"></use></svg><span class="hidden">RSS</span></a></li>
			</ul>
			<!-- social-->
		</div>

		<div class="col-xl-2 col-lg-2 col-md-5 d-md-block d-none text-right">
			<!-- ricerca -->
			<div class="cerca float-right">
				<span><@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" /></span>
				<button aria-label="<@wp.i18n key="CITTAMETRO_HEADER_INTESTAZIONE_SEARCH" />" class="btn btn-default btn-cerca pull-right" type="button"
					data-ng-click="visible=true; ctrl.Ricerca.setFocusRicerca($event)">
					<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
				</button>
			</div>
			<!-- ricerca -->
		</div>

		<div id="ricerca" class="col-xl-6 col-lg-6 col-md-5 col-sm-12 d-md-block d-lg-block pull-right text-right header-col" data-ng-cloak
			data-ng-show="visible" off-click="visible=false" off-click-activator="visible">
			<form action="<@wp.url page="${searchWidgetPage.code}"/>" method="post" data-ng-if="ctrl.Ricerca.getHeaderSearch()">
				<div class="form-group" id="searchPopup" data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'')">
					<label for="cerca-txt" class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /></label>
					<md-autocomplete type="search" data-ng-focus="$event.preventDefault()" md-input-name="cercatxt" md-input-id="cerca-txt" md-delay="1000" md-no-cache="true" data-ng-trim="false"
						placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />" md-selected-item="ctrl.Ricerca.cercatxt"
						md-search-text="ctrl.Ricerca.searchStringRicercaTxt" md-selected-item-change="ctrl.Ricerca.selectedRicercaTxtItemChanged(item)" md-items="item in ctrl.Ricerca.getAutocompleteResults(ctrl.Ricerca.searchStringRicercaTxt)" md-item-text="item.contentName" md-clear-button="true">
						<md-item-template>
							<div>
								<a data-ng-href="{{item.href}}" data-ng-if="!item.searchButton && item.searchButton != ''hidden''">
									<div>
										<svg class="icon icon-sm" data-ng-bind-html="item.icon"></svg>
										<span class="autocomplete-list-text">
											<span data-md-highlight-text="ctrl.Ricerca.searchStringRicercaTxt" data-md-highlight-flags="i">{{item.contentName}}</span>
											<em>{{item.category}}</em>
										</span>
									</div>
								</a>
								<div class="search-start" data-ng-if="item.searchButton && item.searchButton != ''hidden''" data-ng-click="item.contentName = ctrl.Ricerca.searchStringRicercaTxt; ctrl.Ricerca.startSearch()">
									<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE" />
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg>
								</div>
								<div class="search-start-hidden" data-ng-if="item.searchButton == ''hidden''"> </div>
							</div>
						</md-item-template>
					</md-autocomplete>
					<svg class="icon ico-prefix"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
					<button id="search-button" class="search-start ico-postfix" >
						<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg>
					</button>
				</div>
			</form>
		</div>
		<#list systemCategories?sort_by("value") as systemCategory>
			<data-ng-container data-ng-init="ctrl.Ricerca.setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
		</#list>
		<@wp.nav var="pagina" spec="code(homepage).subtree(4)">
			<#if (pagina.level == 1)>
				<#assign padre = pagina.code />
			</#if>
			<#if (pagina.level == 2)>
				<#assign padre2 = pagina.code />
				<#assign categoria = pagina.title />
				<#assign categoriaCode = pagina.code />
				<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria?replace("''", "\\''")}'', ''${categoriaCode}'', ''${padre}'')"></data-ng-container>
			</#if>
			<#if (pagina.level > 2)>
				<#assign categoria = pagina.title />
				<#assign categoriaCode = pagina.code />
				<data-ng-container data-ng-init="ctrl.Ricerca.setCategorie(''${categoria?replace("''", "\\''")}'', ''${categoriaCode}'', ''${padre}'', ''${padre2}'')"></data-ng-container>
			</#if>
		</@wp.nav>

		<#if (code == ''amministrazione'')><data-ng-container data-ng-init="ctrl.Ricerca.ammins_toggleAll()"></data-ng-container></#if>
		<#if (code == ''servizi'')><data-ng-container data-ng-init="ctrl.Ricerca.servizi_toggleAll()"></data-ng-container></#if>
		<#if (code == ''novita'')><data-ng-container data-ng-init="ctrl.Ricerca.novita_toggleAll()"></data-ng-container></#if>
		<#if (code == ''documenti'')><data-ng-container data-ng-init="ctrl.Ricerca.docs_toggleAll()"></data-ng-container></#if>
	</div>
</div>
<!-- Intestazione -->' WHERE code='cittametro_template_header_intestazione';

UPDATE guifragment SET gui='<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.currentPage param="title" var="titolo" />
<@wp.currentPage param="code" var="page" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />
<@cw.configWidget pageCode="${searchWidgetPage.code}" widgetCode="cagliari_widget_advsearch_results" configParam="contentTypesFilter" var="tipiContenuto" />

<form data-ng-cloak data-ng-controller="ctrlRicerca as ctrl" action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
	<div class="form-group" data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'')">
		<label class="sr-only" for="cerca-intro"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> ${titolo}</label>
		<input type="text" id="cerca-intro" name="cercatxt" class="form-control" data-ng-model="ctrl.Ricerca.searchStringRicercaTxt" placeholder="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> ${titolo?lower_case}">
		<button type="submit" class="ico-sufix" value="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" />" title="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" />">
			<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
		</button>
	</div>
	<input type="hidden" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
	<input type="hidden" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
	<input type="hidden" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
	<input type="hidden" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
</form>' WHERE code='cittametro_widget_cerca_contenuti';

UPDATE guifragment SET gui='<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
        <#assign wp=JspTaglibs["/aps-core"]>
        <@wp.categories var="systemCategories" titleStyle="full" />
        <link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/jquery-ui.min.css" rel="stylesheet" type="text/css" />
        <@wp.outputHeadInfo type="CSS_CA_BTM"><link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /></@wp.outputHeadInfo>
        <@wp.outputHeadInfo type="CSS_CA_BTM_EXT"><link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" /></@wp.outputHeadInfo>
        <@wp.outputHeadInfo type="CSS_CA_BTM_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>


        <link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.min.css" rel="stylesheet" type="text/css" />

        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/popper.min.js"></script>

        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/bootstrap-italia_1.2.0sf.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/cagliari.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-ui.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/i18n/datepicker-it.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-animate.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-aria.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-messages.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-sanitize.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-material.cagliari.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/angular-touch.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/app.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/general-service.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/ricerca.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/ricerca-service.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery.cookie.min.js"></script>
        <script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery.cookiecuttr.min.js"></script>

        <@wp.outputHeadInfo type="JS_CA_BTM_EXT"><script nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
        <@wp.outputHeadInfo type="JS_CA_BTM_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
        <@wp.outputHeadInfo type="JS_CA_BTM"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
        <@wp.outputHeadInfo type="JS_CA_BTM_PLGN"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>

        <script nonce="<@wp.cspNonce />" >
          <@wp.outputHeadInfo type="JS_CA_BTM_INC">
          <@wp.printHeadInfo />
          </@wp.outputHeadInfo>
        </script>

        <#-- COOKIE BAR, ANALITICS e HOTJAR 
        <script nonce="<@wp.cspNonce />" >
          (function ($) {
            $(document).ready(function () {
              // activate cookie cutter
              $.cookieCuttr({
                cookieAnalytics: false,
                cookieMessage: ''<p><@wp.i18n key="CITTAMETRO_FOOTER_COOKIES_TEXT" />&nbsp;<a href="<@wp.url page="privacy" />" class="text-white" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTO" /> <@wp.i18n key="CITTAMETRO_PORTAL_PRIVACYPOLICY" />"><@wp.i18n key="CITTAMETRO_PORTAL_PRIVACYPOLICY" /></a>.<br/><@wp.i18n key="CITTAMETRO_FOOTER_COOKIES_QUESTION" /></p>'',
                cookieAcceptButtonText: ''<@wp.i18n key="CITTAMETRO_FOOTER_ACCEPTS_COOKIES" />'',
                cookieDiscreetPosition: "bottomleft",
                cookieDeclineButton: true,
                //cookieResetButton: true,
                cookieDeclineButtonText: ''<@wp.i18n key="CITTAMETRO_FOOTER_DECLINES_COOKIES" />'',
                //cookieResetButtonText: ''Cambia scelta cookies'',
                cookieDomain: ''comune.cittametro.it''
              });
            });
          })(jQuery);

          if (jQuery.cookie(''cc_cookie_accept'') == "cc_cookie_accept") {
            (function(i,s,o,g,r,a,m){i[''GoogleAnalyticsObject'']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,''script'',''https://www.google-analytics.com/analytics.js'',''ga'');
            ga(''create'', ''UA-27957186-1'', ''auto'');
            ga(''set'', ''anonymizeIp'', true);
            ga(''send'', ''pageview'');

            (function(h,o,t,j,a,r){
              h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
              h._hjSettings={hjid:553641,hjsv:6};
              a=o.getElementsByTagName(''head'')[0];
              r=o.createElement(''script'');r.async=1;
              r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
              a.appendChild(r);
            })(window,document,''https://static.hotjar.com/c/hotjar-'',''.js?sv='');
          }
		   -->
        </script>' WHERE code='cittametro_template_footer_js';

UPDATE guifragment SET gui='<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.categories var="systemCategories" root="arg_argomenti" />
<@wp.currentPage param="code" var="code" />
<@wp.pageWithWidget var="searchResultPageVar" widgetTypeCode="jpfacetnav_results" listResult=false />
<@wp.currentWidget param="config" configParam="contentTypesFilter" var="tipiContenuto" />

<#if (RequestParameters[''cercatxt'']??)><#assign cercatxt = RequestParameters[''cercatxt'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign cercatxt = ""></#if>
<#if (RequestParameters[''argomenti'']??)><#assign argomenti = RequestParameters[''argomenti'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign argomenti = ""></#if>
<#if (RequestParameters[''amministrazione'']??)><#assign amministrazione = RequestParameters[''amministrazione'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign amministrazione = ""></#if>
<#if (RequestParameters[''servizi'']??)><#assign servizi = RequestParameters[''servizi'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign servizi = ""></#if>
<#if (RequestParameters[''novita'']??)><#assign novita = RequestParameters[''novita'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign novita = ""></#if>
<#if (RequestParameters[''documenti'']??)><#assign documenti = RequestParameters[''documenti'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign documenti = ""></#if>
<#if (RequestParameters[''attivi'']??)><#assign attivi = RequestParameters[''attivi'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign attivi = ""></#if>
<#if (attivi == "")><#assign attivi = ''false''/></#if>
<#if (RequestParameters[''inizio'']??)><#assign inizio = RequestParameters[''inizio'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign inizio = ""></#if>
<#if (RequestParameters[''fine'']??)><#assign fine = RequestParameters[''fine'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign fine = ""></#if>

<div class="progress-spinner progress-spinner-active" data-ng-show="::false">
	<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
</div>

<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">
	<data-ng-container data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'');
		ctrl.Ricerca.setArgsSelected(''${argomenti?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''amministrazione'',''${amministrazione?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''servizi'',''${servizi?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''novita'',''${novita?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''documenti'',''${documenti?replace(''"'', '''')}'');
		ctrl.Ricerca.setCercatxt(''${cercatxt}'');
		ctrl.Ricerca.activeChk = ${attivi};
		ctrl.Ricerca.dataInizio = ''${inizio}'';
		ctrl.Ricerca.dataFine = ''${fine}''">
	</data-ng-container>

	<section id="introricerca" data-ng-init="setSearchCookie(true); ctrl.Ricerca.getContents(); showall=false; showfilter=false">
		<div class="container">
			<div class="row">
				<div class="offset-lg-1 col-lg-10 col-md-12">
					<div class="titolo-sezione">
						<form action="<@wp.url page="${code}"/>" method="post">
							<div class="form-group" id="searchRisultati" data-ng-init="ctrl.Ricerca.setHeaderSearch(false)">
								<label for="cerca-txt" class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /></label>
								<md-autocomplete type="search" md-input-name="cercatxt" md-input-id="cerca-txt" md-delay="1000" md-no-cache="true" data-ng-trim="false"
									placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />" md-selected-item="ctrl.Ricerca.cercatxt"
									md-search-text="ctrl.Ricerca.searchStringRicercaTxt" md-selected-item-change="ctrl.Ricerca.selectedRicercaTxtItemChanged(item)" md-items="item in ctrl.Ricerca.getAutocompleteResults(ctrl.Ricerca.searchStringRicercaTxt)" md-item-text="item.contentName" md-clear-button="true">
									<md-item-template>
										<div>
											<a data-ng-href="{{item.href}}" data-ng-if="!item.searchButton && item.searchButton != ''hidden''">
												<div>
													<svg class="icon icon-sm" data-ng-bind-html="item.icon"></svg>
													<span class="autocomplete-list-text">
														<span data-md-highlight-text="ctrl.Ricerca.searchStringRicercaTxt" data-md-highlight-flags="i">{{item.contentName}}</span>
														<em>{{item.category}}</em>
													</span>
												</div>
											</a>
											<div class="search-start" data-ng-if="item.searchButton && item.searchButton != ''hidden''" data-ng-click="item.contentName = ctrl.Ricerca.searchStringRicercaTxt; ctrl.Ricerca.startSearch()">
												<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_THROUGHOUT_SITE" />
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg>
											</div>
											<div class="search-start-hidden" data-ng-if="item.searchButton == ''hidden''"> </div>
										</div>
									</md-item-template>
								</md-autocomplete>
								<svg class="icon ico-prefix"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
								<button id="search-button" class="search-start ico-postfix" >
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg>
								</button>
							</div>
							<input type="hidden" id="amministrazione" name="amministrazione" value="{{ctrl.Ricerca.get_ammins_selected()}}">
							<input type="hidden" id="servizi" name="servizi" value="{{ctrl.Ricerca.get_servizi_selected()}}">
							<input type="hidden" id="novita" name="novita" value="{{ctrl.Ricerca.get_novita_selected()}}">
							<input type="hidden" id="documenti" name="documenti" value="{{ctrl.Ricerca.get_docs_selected()}}">
							<input type="hidden" id="argomenti" name="argomenti" value="{{ctrl.Ricerca.get_args_selected()}}">
							<input type="hidden" id="attivi" name="attivi" value="{{ctrl.Ricerca.activeChk}}">
							<input type="hidden" id="inizio" name="inizio" value="{{ctrl.Ricerca.dataInizio}}">
							<input type="hidden" id="fine" name="fine" value="{{ctrl.Ricerca.dataFine}}">
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="articolo-dettaglio-testo">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="cerca-risultati d-block d-sm-block d-md-none d-lg-none d-xl-none">
						<#assign morethan><@wp.i18n key="CITTAMETRO_PORTAL_MORETHAN" /></#assign>
						<span data-ng-if="ctrl.Ricerca.getContentsVar().length > 0">
							<span data-ng-show="ctrl.Ricerca.getTotalItems() == ''100000''">${morethan?cap_first} </span>
							{{ctrl.Ricerca.getTotalItems()}}
							<span data-ng-if="ctrl.Ricerca.getTotalItems()!=1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /></span>
							<span data-ng-if="ctrl.Ricerca.getTotalItems()==1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULT" /></span>
						</span>
						<span data-ng-if="ctrl.Ricerca.getContentsVar().length == 0"><@wp.i18n key="LIST_VIEWER_EMPTY" /></span>
						<a class="show-filters" data-ng-click="showfilter=false" data-ng-show="showfilter" href=""><strong><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></strong></a>
						<a class="show-filters" data-ng-click="showfilter=true" data-ng-hide="showfilter" href=""><strong><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></strong></a>
						<span class="float-right" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0 && !showfilter">
							<select class="ordinamento" name="ordinamento" data-ng-model="ctrl.Ricerca.orderBy" data-ng-change="ctrl.Ricerca.getContents()" title="<@wp.i18n key="CITTAMETRO_PORTAL_CHOOSE_OPTION" />">
								<option value="relevance"><@wp.i18n key="CITTAMETRO_PORTAL_RELEVANCE" /></option>
								<option value="data_last_mod"><@wp.i18n key="CITTAMETRO_PORTAL_LAST_MODIFIED" /></option>
								<option value="data_pubb"><@wp.i18n key="CITTAMETRO_PORTAL_PUBLICATION_DATE" /></option>
								<option value="titolo_asc"><@wp.i18n key="CITTAMETRO_PORTAL_INCREASING_TITLE" /></option>
								<option value="titolo_desc"><@wp.i18n key="CITTAMETRO_PORTAL_DESCENDING_TITLE" /></option>
							</select>
						</span>
						<button data-ng-show="showfilter" data-ng-click="showfilter=false" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CITTAMETRO_PORTAL_CLOSE" /></button>
					</div>
					<aside id="menu-sinistro-cerca" class="d-md-block" data-ng-class="showfilter ? ''d-sm-block d-block'' : ''d-sm-none d-none''">
						<h3><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></h3>
						<h4><@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" /></h4>
						<div class="search-filter-ckgroup">
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" />"
									data-ng-checked="ctrl.Ricerca.ammins_isChecked()"
									md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.ammins_toggleAll(); ctrl.Ricerca.getContents()">
									<label><strong><@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></strong></label>
								</md-checkbox>
								<a href="" data-ng-hide="showallamm" data-ng-click="showallamm=!showallamm" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_down"></use></svg>
								</a>
								<a href="" data-ng-show="showallamm" data-ng-click="showallamm=!showallamm" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
								</a>
								<div class="mb24 ml12" data-ng-if="showallamm">
									<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_ammins_chk()">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_ammins_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_ammins_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
											<label data-ng-bind-html="item.name"></label>
										</md-checkbox>
									</div>
								</div>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" />"
									data-ng-checked="ctrl.Ricerca.servizi_isChecked()"
									md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.servizi_toggleAll(); ctrl.Ricerca.getContents()">
									<label><strong><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></strong></label>
								</md-checkbox>
								<a href="" data-ng-hide="showallser" data-ng-click="showallser=!showallser" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_down"></use></svg>
								</a>
								<a href="" data-ng-show="showallser" data-ng-click="showallser=!showallser" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
								</a>
								<div class="mb24 ml12" data-ng-if="showallser">
									<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_servizi_chk()">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_servizi_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_servizi_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
											<label data-ng-bind-html="item.name"></label>
										</md-checkbox>
									</div>
								</div>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" />"
									data-ng-checked="ctrl.Ricerca.novita_isChecked()"
									md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.novita_toggleAll(); ctrl.Ricerca.getContents()">
									<label><strong><@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></strong></label>
								</md-checkbox>
								<a href="" data-ng-hide="showallnov" data-ng-click="showallnov=!showallnov" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_down"></use></svg>
								</a>
								<a href="" data-ng-show="showallnov" data-ng-click="showallnov=!showallnov" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
								</a>
								<div class="mb24 ml12" data-ng-if="showallnov">
									<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_novita_chk()">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_novita_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, 		ctrl.Ricerca.get_novita_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
											<label data-ng-bind-html="item.name"></label>
										</md-checkbox>
									</div>
								</div>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" />"
									data-ng-checked="ctrl.Ricerca.docs_isChecked()"
									md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.docs_toggleAll(); ctrl.Ricerca.getContents()">
									<label><strong><@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></strong></label>
								</md-checkbox>
								<a href="" data-ng-hide="showalldoc" data-ng-click="showalldoc=!showalldoc" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_down"></use></svg>
								</a>
								<a href="" data-ng-show="showalldoc" data-ng-click="showalldoc=!showalldoc" title="<@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" />">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_up"></use></svg>
								</a>
								<div class="mb24 ml12" data-ng-if="showalldoc">
									<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_docs_chk()">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_docs_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_docs_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
											<label data-ng-bind-html="item.name"></label>
										</md-checkbox>
									</div>
								</div>
							</div>
						</div>

						<h4><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></h4>
						<div class="search-filter-ckgroup">
							<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti() | orderBy : ''-occ''">
								<div data-ng-if = "$index <= 9">
									<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
										<label data-ng-bind-html="item.name"></label><span data-ng-show="item.occ">&nbsp;({{item.occ}})</span>
									</md-checkbox>
								</div>
								<div data-ng-if = "$index >= 10">
									<div data-ng-show="showallarg">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, 		ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
											<label data-ng-bind-html="item.name"></label><span data-ng-show="item.occ">&nbsp;({{item.occ}})</span>
										</md-checkbox>
									</div>
								</div>
							</div>
							<a href="" class="search-mostra" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
							<a href="" class="search-mostra" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
						</div>

						<h4><@wp.i18n key="CITTAMETRO_CONTENT_DATE" /></h4>
						<div class="search-filter-dategroup">
							<div class="form-group">
								<label for="datepicker_start"><@wp.i18n key="CITTAMETRO_CONTENT_PUBDATE" /> - <@wp.i18n key="DATE_FROM" /></label>
								<input type="text" class="form-control" id="datepicker_start" data-ng-model="ctrl.Ricerca.dataInizio" placeholder="gg/mm/aaaa" data-ng-init="ctrl.Ricerca.initDatepicker(''datepicker_start'')"/>
								<button aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" data-ng-click="ctrl.Ricerca.openDatepicker(''datepicker_start'')">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg>
								</button>
							</div>
							<div class="form-group mb16">
								<label for="datepicker_end"><@wp.i18n key="CITTAMETRO_CONTENT_PUBDATE" /> - <@wp.i18n key="DATE_TO" /></label>
								<input type="text" class="form-control" id="datepicker_end" data-ng-model="ctrl.Ricerca.dataFine" placeholder="gg/mm/aaaa" data-ng-init="ctrl.Ricerca.initDatepicker(''datepicker_end'')"/>
								<button aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_OPEN_CALENDAR" />" type="button" class="ico-sufix" data-ng-click="ctrl.Ricerca.openDatepicker(''datepicker_end'')"">
									<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-event"></use></svg>
								</button>
							</div>
							<a href="" data-ng-click="ctrl.Ricerca.getContents()" class="tutte mb32" title="<@wp.i18n key="CITTAMETRO_PORTAL_FILTER" />">
								<@wp.i18n key="CITTAMETRO_PORTAL_FILTER" /> <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg>
							</a>
						</div>
						<div class="search-filter-ckgroup">
							<div class="flex-100">
								<md-checkbox data-ng-checked="ctrl.Ricerca.activeChk" data-ng-click="ctrl.Ricerca.activeChk = !ctrl.Ricerca.activeChk; ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
									<label><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT" /></label>
								</md-checkbox>
							</div>
							<p class="small"><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_ONLY_ACTIVE_CONTENT_INFO" /></p>
						</div>
					</aside>
				</div>
				<div class="col-lg-9 col-md-8 d-md-block" data-ng-class="showfilter ? ''d-sm-none d-none'' : ''d-sm-block d-block''">
					<div class="articolo-paragrafi">
						<div class="row">
							<div class="col-md-12 cerca-risultati d-md-block d-none">
								<span data-ng-if="ctrl.Ricerca.getContentsVar().length > 0">
									<span data-ng-if="ctrl.Ricerca.getTotalItems()!=1"><@wp.i18n key="CITTAMETRO_PORTAL_FOUND" /></span>
									<span data-ng-if="ctrl.Ricerca.getTotalItems()==1"><@wp.i18n key="CITTAMETRO_PORTAL_FOUND_ONE" /></span>
									<span data-ng-show="ctrl.Ricerca.getTotalItems() == ''100000''"><@wp.i18n key="CITTAMETRO_PORTAL_MORETHAN" /> </span>
									{{ctrl.Ricerca.getTotalItems()}}
									<span data-ng-if="ctrl.Ricerca.getTotalItems()!=1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /></span>
									<span data-ng-if="ctrl.Ricerca.getTotalItems()==1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULT" /></span>
								</span>
								<span data-ng-if="ctrl.Ricerca.getContentsVar().length == 0"><@wp.i18n key="LIST_VIEWER_EMPTY" /></span>
								<div class="float-right d-sm-none d-md-block" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0">
									<label for="ordinamento"><@wp.i18n key="CITTAMETRO_PORTAL_SORT_BY" /></label>
									<select id="ordinamento" class="ordinamento" name="ordinamento" data-ng-model="ctrl.Ricerca.orderBy" data-ng-change="ctrl.Ricerca.getContents()" title="<@wp.i18n key="CITTAMETRO_PORTAL_CHOOSE_OPTION" />">
										<option value="relevance"><@wp.i18n key="CITTAMETRO_PORTAL_RELEVANCE" /></option>
										<option value="data_last_mod"><@wp.i18n key="CITTAMETRO_PORTAL_LAST_MODIFIED" /></option>
										<option value="data_pubb"><@wp.i18n key="CITTAMETRO_PORTAL_PUBLICATION_DATE" /></option>
										<option value="titolo_asc"><@wp.i18n key="CITTAMETRO_PORTAL_INCREASING_TITLE" /></option>
										<option value="titolo_desc"><@wp.i18n key="CITTAMETRO_PORTAL_DESCENDING_TITLE" /></option>
									</select>
								</div>
							</div>
							<div class="linetop-lg"></div>
						</div>

						<div data-ng-if="ctrl.Ricerca.getContentsVar().length > 0" data-ng-repeat="elem in ctrl.Ricerca.getContentsVar()">
							<div class="row" data-ng-if="($index+1)%3 == 0 || ($index+1) == ctrl.Ricerca.getContentsVar().length || ($index+1) == 15 || (($index+1) + (15* ctrl.Ricerca.getCurrentPage)) == ctrl.Ricerca.getContentsVar().length">
								<div class="col-lg-4 col-md-12" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="ctrl.Ricerca.getRenderContent(ctrl.Ricerca.getContentsVar((ctrl.Ricerca.getContentsVar().indexOf(elem)-2)))"></div>
								<div class="col-lg-4 col-md-12" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="ctrl.Ricerca.getRenderContent(ctrl.Ricerca.getContentsVar((ctrl.Ricerca.getContentsVar().indexOf(elem)-1)))"></div>
								<div class="col-lg-4 col-md-12" data-ng-bind-html="ctrl.Ricerca.getRenderContent(elem)"></div>
							</div>
						</div>

						<div class="row" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0 && ctrl.Ricerca.getPages().length > 1">
							<div class="col-md-12">
								<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" />">
									<ul class="pagination">
										<li class="page-item">
											<button class="page-link" data-ng-disabled="ctrl.Ricerca.getCurrentPage() == 0" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getCurrentPage()); ctrl.Ricerca.getContents(ctrl.Ricerca.getCurrentPage()+1)"
												title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
												<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
											</button>
										</li>
										<li data-ng-if="ctrl.Ricerca.getCurrentPage()>2" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> 1" class="page-item">
											<button class="page-link" data-ng-click="ctrl.Ricerca.goToPage(1); ctrl.Ricerca.getContents(1)">1</button>
										</li>
										<li data-ng-if="ctrl.Ricerca.getCurrentPage()>3" class="page-item disabled"><button class="page-link">...</button></li>
										<li data-ng-repeat="elem in ctrl.Ricerca.getPages()" data-ng-if="elem-1<ctrl.Ricerca.getCurrentPage()+3 && elem-1>ctrl.Ricerca.getCurrentPage()-3" title="{{elem}}"
											data-ng-class="ctrl.Ricerca.getCurrentPage() == (elem-1) ? ''page-item active'' : ''page-item''">
											<button data-ng-show="ctrl.Ricerca.getCurrentPage() == (elem-1)" aria-current="page" class="page-link" data-ng-click="ctrl.Ricerca.goToPage(elem); ctrl.Ricerca.getContents(elem)">{{elem}}</button>
											<button data-ng-show="ctrl.Ricerca.getCurrentPage() != (elem-1)" class="page-link" data-ng-click="ctrl.Ricerca.goToPage(elem); ctrl.Ricerca.getContents(elem)">{{elem}}</button>
										</li>
										<li data-ng-if="ctrl.Ricerca.getCurrentPage()<ctrl.Ricerca.getPages().length-4" class="page-item disabled"><button class="page-link">...</button></li>
										<li data-ng-if="ctrl.Ricerca.getCurrentPage()<ctrl.Ricerca.getPages().length-3" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> {{ctrl.Ricerca.getPages().length}}" class="page-item">
											<button class="page-link" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getPages().length); ctrl.Ricerca.getContents(ctrl.Ricerca.getPages().length)">{{ctrl.Ricerca.getPages().length}}</button>
										</li>
										<li class="page-item">
											<button class="page-link" data-ng-disabled="ctrl.Ricerca.getCurrentPage()+1 == ctrl.Ricerca.getNumberOfPages()" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getCurrentPage()+1); ctrl.Ricerca.getContents(ctrl.Ricerca.getCurrentPage()+2)"
												title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
												<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
											</button>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>' WHERE code='cagliari_widget_advsearch_results';

UPDATE guifragment SET gui='<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<@wp.currentPage param="title" var="titolo" />
<@wp.pageWithWidget widgetTypeCode="cagliari_widget_advsearch_results" var="searchWidgetPage" />

<div class="widget" data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></h3>
			</div>
		</div>
		<div class="row">
			<div class="offset-lg-2 col-lg-8 offset-lg-2 offset-md-1 col-md-10 offset-md-1 col-sm-12">
				<div class="box-servizi">
					<form action="<@wp.url page="${searchWidgetPage.code}"/>" method="post">
						<div class="form-group">
							<label class="sr-only" for="cerca-txt-intro"><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" /></label>
							<input type="text" id="cerca-txt-intro" name="cercatxt" class="form-control" data-ng-model="ctrl.Ricerca.searchStringRicercaTxt" placeholder="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_INFORMATIONPEOPLESERVICES" />">
							<button type="submit" class="ico-sufix">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-search"></use></svg>
							</button>
						</div>
						<div>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_01" info="title" />"><@wp.pageInfo pageCode="doc_01" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="amm_03" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_03" info="title" />"><@wp.pageInfo pageCode="amm_03" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="organizzazione" />?contentId=ORG9607" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: URP">URP </a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="persona" />?contentId=PRS11084" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: Sindaco">Sindaco</a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="amm_01_02" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="amm_01_02" info="title" />"><@wp.pageInfo pageCode="amm_01_02" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_09" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_09" info="title" />"><@wp.pageInfo pageCode="doc_09" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="doc_09_02" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="doc_09_02" info="title" />"><@wp.pageInfo pageCode="doc_09_02" info="title" /></a>
							<a class="btn btn-default btn-verde" href="<@wp.url page="ts_10_01" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: <@wp.pageInfo pageCode="ts_10_01" info="title" />"><@wp.pageInfo pageCode="ts_10_01" info="title" /></a>
<a class="btn btn-default btn-verde" href="<@wp.url page="servizio" />?contentId=SRV59324" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />:Gestione manutenzione strade">Manutenzione strade: numero verde 800 116 444</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>' WHERE code='cittametro_widget_cerca_servizi_home';



-- Modifiche fragment per keycloak
UPDATE guifragment SET gui='<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<@wp.currentWidget param="config" configParam="contents" var="contenuti" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />

<#if (!elementiMaxItem??)><#assign elementiMaxItem = 0 /></#if>
<#if (!contenuti??)><#assign contenuti = "" /></#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', '''', '''', '''', '''', '''', '''', '''', ''${contenuti}'')">
		<#if (titleVar??)>
			<div class="row">
				<div class="col-md-12">
					<div class="titolosezione">
						<h3>
							<#if (pageLinkVar??)>
								<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
							<#else>
								${titleVar}
							</#if>
						</h3>
					</div>
				</div>
			</div>
		</#if>
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} == 0" data-ng-repeat="elem in contents | startFrom:currentPage*contents.length | limitTo: contents.length">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && contents.length > ${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" />">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
</div>' WHERE code='cittametro_widget_inevidenza';

UPDATE guifragment SET gui='<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />
<@wp.categories var="systemCategories" titleStyle="prettyFull" />

<#assign tipoContenuto = ''ARG'' />
<#assign idModello = ''400006'' />
<#assign elementiMax = 999 />
<#assign elementiMaxItem = 7 />
<#assign ordinamento = "(order=ASC;attributeFilter=true;key=titolo)" />
<#assign categorie = "evd" />

<div class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', ''${tipoContenuto}'', '''', ''${ordinamento}'', '''')">
		<div class="row">
			<div class="col-md-12">
				<div class="titolosezione">
					<h3><@wp.i18n key="CITTAMETRO_PORTAL_INEVIDENCE" /></h3>
				</div>
			</div>
		</div>

		<div data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
			<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
				<div class="col-md-4" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 1">
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 1" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 2">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(elem.$, ''400008'')" data-ng-bind-html="renderContent[elem.$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 6">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-5].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-5].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''400008'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-6].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-6].$][''${idModello}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-5].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-5].$][''400008'']"></div>
					</div>
				</div>

				<div class="col-md-4" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 2">
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3" data-ng-init="getContent(elem.$, ''400007'')" data-ng-bind-html="renderContent[elem.$][''400007'']"></div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400007'']"></div>
						<div data-ng-init="getContent(elem.$, ''400009'')" data-ng-bind-html="renderContent[elem.$][''400009'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400007'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400009'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400009'']"></div>
						<div data-ng-init="getContent(elem.$, ''400007'')" data-ng-bind-html="renderContent[elem.$][''400007'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 6">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''400007'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400009'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400009'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400007'']"></div>
					</div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''400007'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''400009'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''400009'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''400007'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''400007'']"></div>
					</div>
				</div>

				<div class="col-md-4" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 6">
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 6" data-ng-init="getContent(elem.$, ''400008'')" data-ng-bind-html="renderContent[elem.$][''400008'']"></div>
					<div data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 7">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''400008'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''400008'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					</div>
				</div>

			</div>
		</div>

		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /> - <@wp.i18n key="CITTAMETRO_PORTAL_INEVIDENCE" />">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1)"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1)">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1)">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1)"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>
	<#assign contentList="">
</div>' WHERE code='cittametro_widget_argomenti_inevidenza_img';

UPDATE guifragment SET gui='<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<@ca.widgetFrameNumber frameid="numeroFrame" />
<@cw.currentArgument var="currentArg" />
<@wp.currentWidget param="config" configParam="contentType" var="tipoContenuto" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="modelId2" var="idModello2" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="ordinamento" />
<@wp.currentWidget param="config" configParam="layout" var="layout" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@wp.categories var="systemCategories" root="arg_argomenti" />

<@jacms.contentList listName="contentList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />

<#if (!elementiMax??)><#assign elementiMax>99999</#assign></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem>99999</#assign></#if>
<#if (!ordinamento??)><#assign ordinamento = "" /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>
<#if (!idModello??)><#assign idModello = "list" /></#if>
<#if (!idModello2??)><#assign idModello2 = "list" /></#if>
<#if (currentArg??)>
	<#assign categorie = ''${currentArg},${categorie}'' />
<#else>
	<#assign currentArg = '''' />
</#if>

<#assign altriFiltri = false />
<#assign allCategories = false/>
<#assign numTipologie = 0 />
<#assign categoryIndex = 999 />
<#assign categoryEnumIndex = 999 />
<#assign tipologieIndex = 999 />
<#assign tipologiaName = '''' />
<#assign enumCategName = '''' />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
	<#if (userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)>
		<#list userFilterOptionsVar as userFilterOptionVar>
			<#if (userFilterOptionVar.attribute??)>
				<#if (userFilterOptionVar.attribute.mapItems??)>
					<#if (userFilterOptionVar.attribute.type != ''MultiEnumeratorMapCategory'')>
						<#if (tipologieIndex == 999)>
							<#assign tipologieIndex = userFilterOptionVar?index/>
						</#if>
					<#elseif (userFilterOptionVar.attribute.type == ''MultiEnumeratorMapCategory'')>
						<#assign categoryEnumIndex = userFilterOptionVar?index/>
					</#if>
				</#if>
			</#if>
		</#list>
	</#if>

	<#if (filtri??)>
		<#assign filtriRicerca = [] />
		<#list filtri?split("+") as filtro>
			<#if (filtro?contains("key=category"))>
				<#if (filtro?remove_beginning("(attributeFilter=false;key=category)") == "") || (filtro?contains("categoryCode=arg_argomenti"))>
					<#assign allCategories = true/>
				</#if>
				<#assign categoryIndex = filtro?index/>
			<#else>
				<#assign filtriRicerca = filtriRicerca + [filtro] />
			</#if>
		</#list>
		<#if (categoryIndex <= categoryEnumIndex)>
			<#if (allCategories) && (categoryIndex < categoryEnumIndex)>
				<#list systemCategories?sort_by("value") as systemCategory>
					<data-ng-container data-ng-init="setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
				</#list>
			<#else>
				<#assign filtriCategorie = [] />
				<#assign filtriRicerca = [] />
				<#list filtri?split("+") as filtro>
					<#if (filtro?contains("key=category"))>
						<#assign value>${filtro?remove_beginning("(attributeFilter=false;key=category;categoryCode=")}</#assign>
						<#assign value>${value?remove_ending(")")}</#assign>
						<#assign filtriCategorie = filtriCategorie + [value] />
					<#else>
						<#assign filtriRicerca = filtriRicerca + [filtro] />
					</#if>
				</#list>
				<#list filtriCategorie as filtro>
					<#list systemCategories as systemCategory>
						<#if (filtro == systemCategory.key)>
							<data-ng-container data-ng-init="setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
						</#if>
					</#list>
				</#list>
			</#if>
		<#elseif (categoryEnumIndex < categoryIndex)>
			<#list userFilterOptionsVar as userFilterOptionVar>
				<#if (userFilterOptionVar.attribute??)>
					<#if (userFilterOptionVar.attribute.mapItems??)>
						<#if (userFilterOptionVar?index == categoryEnumIndex)>
							<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
								<data-ng-container data-ng-init="setArgomenti(''${enumeratorMapItemVar.value?replace("''", "\\''")}'', ''${enumeratorMapItemVar.key}'')"></data-ng-container>
							</#list>
						</#if>
					</#if>
				</#if>
			</#list>
		</#if>
	</#if>

	<div class="progress-spinner progress-spinner-active" data-ng-show="::false">
		<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
	</div>
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', '' '', ''${idModello2}'', ''${ordinamento}'', '''')">
		<div class="row">
			<div class="col-md-12">
				<#if ((userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)) || ((filtriCategorie??) && (filtriCategorie?size > 0) || (allCategories))>
					<div class="titolosezione filtrisezione">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<div class="filtro">
							<span class="tabella_risultati">
								{{contents.length}}
								<span data-ng-if="contents.length!=1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_2" /></span>
								<span data-ng-if="contents.length==1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULT" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_1" /></span>
							</span>
							<button id="${tipoContenuto}" class="btn btn-default btn-trasparente active" data-ng-class="class_tutti()" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
								data-ng-click="toggleAll(); cleanSearchParameter(''all''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), ''${tipoContenuto}'', ''${ordinamento}'')">
								<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
							</button>
							<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (categoryIndex<tipologieIndex) && (categoryIndex<categoryEnumIndex) >
								<button data-ng-repeat="item in argomenti" data-ng-if="$index <= 2" id="{{item.code}}" class="btn btn-default btn-trasparente" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" title="{{item.name}}" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
									{{item.name}}
								</button>
								<button data-ng-repeat="item in argomenti" data-ng-if="$index >= 3" id="{{item.code}}" class="btn btn-default btn-trasparente" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" title="{{item.name}}" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')" data-ng-show="getArgomentiList().indexOf(item.code) != -1 && item.code != ''${currentArg}''">
									{{item.name}}
								</button>
							</#if>
							<#if (userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)>
								<#list userFilterOptionsVar as userFilterOptionVar>
									<#if (userFilterOptionVar.attribute??)>
										<#if (userFilterOptionVar.attribute.mapItems??)>
											<#if (userFilterOptionVar?index == tipologieIndex)>
												<#assign tipologiaName = userFilterOptionVar.attribute.name />
												<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
													<#if (enumeratorMapItemVar?index <= 1) && (tipologieIndex<categoryIndex) && (tipologieIndex<categoryEnumIndex)>
														<#if (enumeratorMapItemVar?index == 0)><#assign tipologia1 = enumeratorMapItemVar.key/></#if>
														<#if (enumeratorMapItemVar?index == 1)><#assign tipologia2 = enumeratorMapItemVar.key/></#if>
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index <= 1) && ((categoryIndex<tipologieIndex) || (categoryEnumIndex<tipologieIndex)) >
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
															data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index >= 2)>
														<#if (enumeratorMapItemVar?index == 2) && (tipologieIndex<categoryIndex) && (tipologieIndex<categoryEnumIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" title="${enumeratorMapItemVar.value}" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" data-ng-hide="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' && searchParameters.${tipologiaName} != ''${tipologia1}'' && searchParameters.${tipologiaName} != ''${tipologia2}'' && searchParametersLength != 0"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index == 2) && ((categoryIndex<tipologieIndex) || (categoryEnumIndex<tipologieIndex)) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index > 2)>
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
													</#if>
													<#assign numTipologie = enumeratorMapItemVar?index + 1 />
												</#list>
											<#elseif (userFilterOptionVar?index == categoryEnumIndex) && (categoryEnumIndex<categoryIndex)>
												<#assign enumCategName = userFilterOptionVar.attribute.name />
												<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
													<#if (enumeratorMapItemVar?index <= 1) && (categoryEnumIndex<tipologieIndex)>
														<#if (enumeratorMapItemVar?index == 0)><#assign categoria1 = enumeratorMapItemVar.key/></#if>
														<#if (enumeratorMapItemVar?index == 1)><#assign categoria2 = enumeratorMapItemVar.key/></#if>
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-class="(searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}'')? ''active'': ''''"
															data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index <= 1) && (categoryEnumIndex>tipologieIndex) >
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
															data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index >= 2)>
														<#if (enumeratorMapItemVar?index == 2) && (tipologieIndex>categoryEnumIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" title="${enumeratorMapItemVar.value}" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" data-ng-hide="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' && searchParameters.${enumCategName} != ''${categoria1}'' && searchParameters.${enumCategName} != ''${categoria2}'' && searchParametersLength != 0"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index == 2) && (categoryEnumIndex>tipologieIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index > 2)>
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
													</#if>
												</#list>
											<#else>
												<#assign altriFiltri = true />
											</#if>
										</#if>
									</#if>
								</#list>
							</#if>
							<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (tipologieIndex<categoryIndex) && (categoryIndex<categoryEnumIndex)>
								<button data-ng-repeat="item in argomenti" id="{{item.code}}" class="btn btn-default btn-trasparente" title="{{item.name}}" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')" data-ng-show="getArgomentiList().indexOf(item.code) != -1 && item.code != ''${currentArg}''">
									{{item.name}}
								</button>
							</#if>
							<button data-ng-if="key != ''${tipologiaName}'' && key != ''${enumCategName}''" data-ng-show="(value.value != null && value.value != '''') || (value.start != null && value.start != '''') || (value.end != null && value.end != '''')" data-ng-repeat="(key, value) in searchParameters"  class="btn btn-default btn-trasparente active" data-ng-click="cleanSearchParameter(key); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
								<span data-ng-if="value.type==''date''">
									<span data-ng-if="value.start && value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{value.start}}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{value.end}} </span>
									<span data-ng-if="value.start && !value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{value.start}}</span>
									<span data-ng-if="!value.start && value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{value.end}}</span>
								</span>
								<span data-ng-if="value.type==''text''">
									<span data-ng-if="key == ''fulltext''">{{value.value}}</span>
									<span data-ng-if="key != ''fulltext''">{{value.title}}: {{value.value}}</span>
								</span>
								<span data-ng-if="value.type==''select''">
									<span>{{value.title}}: {{value.value}}</span>
								</span>
								<span data-ng-if="value.type==''boolean''">
									<span>{{value.title}}</span>
								</span>
							</button>
							<#if (filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)>
								<#list filtriRicerca as filtro>
									<#if !((filtro?contains("key=${tipologiaName}") && tipologiaName != '''') || ((filtro?contains("key=${enumCategName}") && enumCategName != '''')))>
										<#assign altriFiltri = true />
									</#if>
								</#list>
							</#if>
							<#if (numTipologie > 3) || (allCategories) || ((filtriCategorie??) && (filtriCategorie?size > 3)) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
								<#if (tipologieIndex<categoryIndex)>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''categorie''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								<#elseif (tipologieIndex>categoryIndex) || (categoryEnumIndex<categoryIndex)>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''argomenti''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								<#else>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''opzioni''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								</#if>
							</#if>
						</div>
					</div>
				<#else>
					<div class="titolosezione" data-ng-class="{''filtrisezione'' : ${elementiMax}>${elementiMaxItem}}">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<div class="filtro mb8" data-ng-if="${elementiMax}>${elementiMaxItem}">
							<div class="tabella_risultati pt8 mr0">
								{{contents.length}}
								<span data-ng-if="contents.length!=1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_2" /></span>
								<span data-ng-if="contents.length==1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULT" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_1" /></span>
							</div>
						</div>
					</div>
				</#if>
			</div>
		</div>

		<#if (layout == ''1'')>
			<div class="row row-eq-height widget-mono">
				<div class="col-lg-4 col-md-6 animation-if" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}"></div>
			</div>
		</#if>
		<#if (layout == ''2'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-md-4" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''3'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height widget-multi" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-6 mb16" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''4'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-12 1a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 4" data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 3" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 3a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 4a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) <= 3 && contents.length - (''${elementiMaxItem}''* currentPage) >= 2"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 5a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) <= 2" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 6a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3" data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''5'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-12 1b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 5" data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 3b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 5b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4" data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 6b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 7b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) == 3" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 8b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) <= 3 && contents.length - (''${elementiMaxItem}''* currentPage) >= 2"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 9b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) <= 3" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>

		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /><#if (titleVar??)> - ${titleVar}</#if>">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1); scrollTo(''frame${numeroFrame}'')" title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-if="currentPage>2" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> 1" class="page-item">
							<button class="page-link" data-ng-click="goToPage(0); scrollTo(''frame${numeroFrame}'')">1</button>
						</li>
						<li data-ng-if="currentPage>3" class="page-item disabled"><button class="page-link">...</button></li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+3 && elem-1>currentPage-3" title="{{elem}}" data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li data-ng-if="currentPage<pages.length-4" class="page-item disabled"><button class="page-link">...</button></li>
						<li data-ng-if="currentPage<pages.length-3" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> {{pages.length}}" class="page-item">
							<button class="page-link" data-ng-click="goToPage(pages.length-1); scrollTo(''frame${numeroFrame}'')">{{pages.length}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1); scrollTo(''frame${numeroFrame}'')" title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">

	<#if (numTipologie > 3) || (allCategories) || ((filtriCategorie??) && (filtriCategorie?size > 3)) || ((filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)) || (categoryEnumIndex<categoryIndex) >
		<div class="modal fade searchWidgetModal" id="searchWidgetModal${numeroFrame}" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle${numeroFrame}" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca-interna-${numeroFrame}">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !General.getFiltriMode().filtri}">
										<button type="button" class="close" data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}''); General.setFiltriMode(false, false)"
											data-dismiss="modal" aria-label="Chiudi filtri di ricerca">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle${numeroFrame}">
											<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />
										</h1>
										<button data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}''); ctrl.General.setFiltriMode(false, false)" class="confirm btn btn-default btn-trasparente float-right" data-dismiss="modal">Conferma</button>
									</div>
								</div>
								<#if (filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)>
									<#list filtriRicerca as filtro>
										<#if (filtro?contains("key=titolo"))>
											<div class="row">
												<div class="col-lg-12 col-md-12 col-sm-12">
													<div class="form-group-free">
														<label class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></label>
														<input type="text" class="cerca-txt-free form-control" name="cercatxt" id="cerca-txt${numeroFrame}" placeholder="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> <#if (titleVar??)>${titleVar?lower_case}</#if>" data-ng-model=''searchParameters.titolo.value'' data-ng-change="searchParameters.titolo.title = ''Titolo''; searchParameters.titolo.type = ''text''; searchParameters.titolo.value == ''''? cleanSearchParameter(''titolo''):''''">
													</div>
												</div>
											</div>
										<#elseif !((filtro?contains("key=${tipologiaName}") && tipologiaName != '''') || ((filtro?contains("key=${enumCategName}") && enumCategName != '''')))>
											<#assign altriFiltri = true />
										</#if>
									</#list>
								</#if>
								<#if (numTipologie > 0) || ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="search-filter-type ng-hide" data-ng-show="General.getFiltriMode().filtri">
												<ul class="nav nav-tabs" role="tablist">
													<#if (numTipologie > 0)>
														<li role="presentation">
															<a href="" aria-controls="categoriaTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''categorie'')" data-ng-class="categoriaTab"><@wp.i18n key="CITTAMETRO_CONTENT_TYPES" /></a>
														</li>
													</#if>
													<#if ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (categoryEnumIndex<categoryIndex)>
														<li role="presentation" data-ng-if="argomenti.length > 0">
															<a href="" aria-controls="argomentoTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''argomenti'')" data-ng-class="argomentoTab"><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></a>
														</li>
													</#if>
													<#if (altriFiltri)>
														<li role="presentation">
															<a href="" aria-controls="opzioniTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''opzioni'')" data-ng-class="opzioniTab"><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></a>
														</li>
													</#if>
												</ul>
											</div>
										</div>
									</div>
								</#if>
							</div>
						</div>

						<#if (numTipologie > 0) || ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
							<div class="modal-body-search">
								<div class="container">
									<div role="tabpanel" data-ng-show="General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
										<div class="tab-content">
											<#if (numTipologie > 0)>
												<div role="tabpanel" data-ng-class="categoriaTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<#assign mostraTutto = false />
																<#list userFilterOptionsVar as userFilterOptionVar>
																	<#if (userFilterOptionVar.attribute??)>
																		<#if (userFilterOptionVar.attribute.mapItems??)>
																			<#if (userFilterOptionVar?index == tipologieIndex)>
																				<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
																					<#if (enumeratorMapItemVar?index <= 11)>
																						<div class="flex-100">
																							<md-checkbox data-ng-checked="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																					<#if (enumeratorMapItemVar?index >= 12)>
																						<#assign mostraTutto = true />
																						<div class="flex-100" data-ng-show="showalltipol">
																							<md-checkbox data-ng-checked="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																				</#list>
																			</#if>
																		</#if>
																	</#if>
																</#list>
																<#if (mostraTutto)>
																	<a href="" data-ng-hide="showalltipol" data-ng-click="showalltipol=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																	<a href="" data-ng-show="showalltipol" data-ng-click="showalltipol=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
																</#if>
															</div>
														</div>
													</div>
												</div>
											</#if>
											<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (categoryIndex<categoryEnumIndex)>
												<div data-ng-if="argomenti.length > 0" role="tabpanel" data-ng-class="argomentoTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<div class="flex-100" data-ng-repeat="item in argomenti">
																	<div data-ng-if = "$index <= 11 && item.code != ''${currentArg}''">
																		<md-checkbox data-ng-checked="exists(item, args_selected)" data-ng-click="toggle(item, args_selected)" aria-label="{{item.name}}">
																		<label data-ng-bind-html="item.name"></label></md-checkbox>
																	</div>
																	<div data-ng-if = "$index >= 12 && item.code != ''${currentArg}''">
																		<div data-ng-show="showallarg">
																			<md-checkbox data-ng-checked="exists(item, args_selected)" data-ng-click="toggle(item, args_selected)" aria-label="{{item.name}}">
																			<label data-ng-bind-html="item.name"></label></md-checkbox>
																		</div>
																	</div>
																</div>
																<a href="" data-ng-hide="showallarg || argomenti.length <= 12" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
															</div>
														</div>
													</div>
												</div>
											<#elseif (categoryEnumIndex<categoryIndex)>
												<div role="tabpanel" data-ng-class="argomentoTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<#assign mostraTuttoCat = false />
																<#list userFilterOptionsVar as userFilterOptionVar>
																	<#if (userFilterOptionVar.attribute??)>
																		<#if (userFilterOptionVar.attribute.mapItems??)>
																			<#if (userFilterOptionVar?index == categoryEnumIndex)>
																				<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
																					<#if (enumeratorMapItemVar?index <= 11)>
																						<div class="flex-100">
																							<md-checkbox data-ng-checked="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																					<#if (enumeratorMapItemVar?index >= 12)>
																						<#assign mostraTuttoCat = true />
																						<div class="flex-100" data-ng-show="showallarg">
																							<md-checkbox data-ng-checked="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																				</#list>
																			</#if>
																		</#if>
																	</#if>
																</#list>
																<#if (mostraTuttoCat)>
																	<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																	<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
																</#if>
															</div>
														</div>
													</div>
												</div>
											</#if>
											<#if (altriFiltri)>
												<div role="tabpanel" data-ng-class="opzioniTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="opzioniTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12 mt48">
															<div class="row">
															<@wp.freemarkerTemplateParameter var="userFilterOptionsVar" valueName="userFilterOptionsVar" removeOnEndTag=true >
																<@wp.fragment code="cittametro_widget_lista_multilayout_uf" escapeXml=false />
															</@wp.freemarkerTemplateParameter>
															</div>
														</div>
													</div>
												</div>
											</#if>
										</div>
									</div>
								</div>
							</div>
						</#if>
					</form>
				</div>
			</div>
		</div>
	</#if>
</div>' WHERE code='cittametro_widget_lista_multilayout';

UPDATE guifragment SET gui='<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<@wp.info key="currentLang" var="lingua" />
<@ca.widgetFrameNumber frameid="numeroFrame" />
<@cw.currentArgument var="currentArg" />
<@wp.currentWidget param="config" configParam="contentTypes" var="contentTypes" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="modelId2" var="idModello2" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="orClauseCategoryFilter" var="orCategory" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="filters" />
<@wp.currentWidget param="config" configParam="layout" var="layout" />
<@wp.currentWidget param="config" configParam="title_${lingua}" var="titleVar" />
<@wp.currentWidget param="config" configParam="pageLink" var="pageLinkVar" />
<@wp.currentWidget param="config" configParam="linkDescr_${lingua}" var="pageLinkDescriptionVar" />


<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@wp.categories var="systemCategories" root="arg_argomenti" />


<#if (!elementiMax??)><#assign elementiMax>99999</#assign></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem>99999</#assign></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>
<#if (!orCategory??)><#assign orCategory = "false" /></#if>
<#if (!filters??)><#assign filters = "" /></#if>
<#if (!idModello??)><#assign idModello = "list" /></#if>
<#if (!idModello2??)><#assign idModello2 = "list" /></#if>
<#if (currentArg??)>
	<#assign categorie = ''${currentArg},${categorie}'' />
<#else>
	<#assign currentArg = '''' />
</#if>

<#assign altriFiltri = false />
<#assign allCategories = false/>
<#assign numTipologie = 0 />
<#assign categoryIndex = 999 />
<#assign categoryEnumIndex = 999 />
<#assign tipologieIndex = 999 />
<#assign tipologiaName = '''' />
<#assign enumCategName = '''' />

<#assign contentTypes = contentTypes?keep_after("[")?keep_before("]")?split("},") />

<#assign contents = [] />
<#list contentTypes as contentType>
	<#assign contents = contents + [contentType?keep_after("{")?keep_before("}")] />
</#list>

<#assign tipiContenuto = "" />
<#list contents as content>
	<#assign typeCode = content?keep_after("contentType=")?keep_before(",")/>
	<#if (content?index == 0)>
		<#assign tipiContenuto += typeCode />
	<#else>
		<#assign tipiContenuto += '','' + typeCode />
	</#if>
</#list>

<#assign userFilters = [] />
<#list contents as content>
	<#list content?keep_after("userFilters=")?keep_before("}")?split("+") as userFilter>
		<#if (userFilter != '''')>
			<#assign userFilters = userFilters + [{''contentType'': content?keep_after("contentType=")?keep_before(","), ''filter'':userFilter}] />
		</#if>
	</#list>
</#list>

<#assign listaFiltriTip = "" />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
    <div class="progress-spinner progress-spinner-active" data-ng-show="::false">
		<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
	</div>
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipiContenuto}'', ''${idModello}'', ''${categorie}'', '' '', ''${orCategory}'', ''${filters}'', ''multi'')">
		<div class="row">
			<div class="col-md-12">
				<#if (userFilters?size > 0)>
					<div class="titolosezione filtrisezione">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<div class="filtro">
							<span class="tabella_risultati">
								{{totalItems}}
								<span data-ng-if="totalItems!=1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_2" /></span>
								<span data-ng-if="totalItems==1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULT" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_1" /></span>
							</span>
							<button id="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />${numeroFrame}" class="btn btn-default btn-trasparente active" data-ng-class="class_tutti()" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
								data-ng-click="toggleAll(); cleanSearchParameter(''all''); getMultiContents(''${tipiContenuto}'', getArgomentiList(), ''<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />${numeroFrame}'', ''${filters}'')">
								<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
							</button>
							<#assign numTipologie = 0/>
							<#list userFilters as userFilter>
								<#assign typeCode= userFilter.filter?keep_after("value=")?keep_before(")")/>
								<#assign typeKey= userFilter.filter?keep_after("key=")?keep_before(";")/>
								<#if (typeCode!= '''')>
									<#if (!listaFiltriTip?contains(typeCode))>
										<#if (listaFiltriTip == "")>
											<#assign listaFiltriTip += typeCode />
										<#else>
											<#assign listaFiltriTip += '','' + typeCode />
										</#if>
										<@wp.pageInfo pageCode="${typeCode}" info="title" var="typeName" />
										<#if (numTipologie == 0)><#assign tip1 = typeCode/></#if>
										<#if (numTipologie == 1)><#assign tip2 = typeCode/></#if>
										<button id="${typeCode}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${typeCode}''? ''active'': ''''" title="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)>data-ng-hide="searchParameters.key != ''${typeCode}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2}'' && searchParametersLength != 0"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${typeCode}''"</#if> >
											${typeName}
										</button>
										<#assign numTipologie ++/>
									</#if>
								<#else>
									<#if (typeKey?contains("contentType"))>
										<#if (numTipologie == 0)><#assign tip1 = userFilter.contentType/></#if>
										<#if (numTipologie == 1)><#assign tip2 = userFilter.contentType/></#if>
										<button id="${userFilter.contentType}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${userFilter.contentType}''? ''active'': ''''" title="<@wp.i18n key=''CITTAMETRO_CONTENT_${userFilter.contentType}''/>" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)> data-ng-hide="searchParameters.key != ''${userFilter.contentType}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2}'' && searchParametersLength != 0"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${userFilter.contentType}''"</#if>>
											<@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}"/>
										</button>
										<#assign numTipologie ++/>
									<#else>
										<#assign pageStart= ''homepage''/>
										<#if (userFilter.contentType== ''NVT'')><#assign pageStart= ''novita''/></#if>
										<#if (userFilter.contentType== ''EVN'')><#assign pageStart= ''nov_03''/></#if>
										<#if (userFilter.contentType== ''DOC'')><#assign pageStart= ''documenti''/></#if>
										<#if (userFilter.contentType== ''SRV'')><#assign pageStart= ''servizi''/></#if>
										<@wp.nav var="pagina" spec="code(${pageStart}).subtree(3)">
											<#if (pagina.level >= 1)>
												<#if (numTipologie == 0)><#assign tip1 = pagina.code/></#if>
												<#if (numTipologie == 1)><#assign tip2 = pagina.code/></#if>
												<button id="${pagina.code}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${pagina.code}''? ''active'': ''''" title="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)>data-ng-hide="searchParameters.key != ''${pagina.code}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2} && searchParametersLength != 0''"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${pagina.code}''"</#if>>
													${pagina.title}
												</button>
												<#assign numTipologie ++/>
											</#if>
										</@wp.nav>
									</#if>
								</#if>
							</#list>
							<#if (numTipologie > 3)>
								<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''categorie''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
							</#if>
						</div>
					</div>
				<#else>
					<div class="titolosezione" data-ng-class="{''filtrisezione'' : ${elementiMax}>${elementiMaxItem}}">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<div class="filtro mb8" data-ng-if="${elementiMax}>${elementiMaxItem}">
							<div class="tabella_risultati pt8 mr0">
								{{totalItems}}
								<span data-ng-if="totalItems!=1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_2" /></span>
								<span data-ng-if="totalItems==1"><@wp.i18n key="CITTAMETRO_PORTAL_RESULT" /> <@wp.i18n key="pagerInfo_SEARCH_OUTRO_1" /></span>
							</div>
						</div>
					</div>
				</#if>
			</div>
		</div>

		<#if (layout == ''1'')>
			<div class="row row-eq-height widget-mono">
				<#list contents as content>
					<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
					<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
					<div class="col-lg-4 col-md-6 animation-if" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents"></div>
				</#list>
			</div>
		</#if>
		<#if (layout == ''2'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<div class="col-md-4" data-ng-if="($index+1)%3 == 0 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-md-4" data-ng-if="(($index+1)%3 == 0 || ($index+1)%3 == 2) && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-md-4" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''3'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height widget-multi" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="(($index+1)%3 == 0 || ($index+1)%3 == 2) && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''4'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 1a" data-ng-if="contents.length >= 4 && contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 2a" data-ng-if="contents.length >= 3 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
					</#list>
						<div class="col-lg-4 col-md-12 3a" data-ng-if="contents.length >= 4">
							<#list contents as content>
								<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
								<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
								<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
								<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
								<div data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
							</#list>
						</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 4a" data-ng-if="contents.length <= 3 && contents.length >= 2 && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 5a" data-ng-if="contents.length <= 2 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 6a" data-ng-if="contents.length == 3 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''5'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 1b" data-ng-if="contents.length >= 5 && contents[contents.indexOf(elem)-4].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-4], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4]][''${idModello}'']"></div>
					</#list>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="contents.length >= 5">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello2}'']"></div>
							<div data-ng-if="contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello2}'']"></div>
						</#list>
					</div>
					<div class="col-lg-4 col-md-12 3b" data-ng-if="contents.length >= 5">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
							<div data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
						</#list>
					</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 5b" data-ng-if="contents.length == 4 && contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello}'']"></div>
					</#list>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="contents.length == 4">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello2}'']"></div>
							<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
						</#list>
					</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 6b" data-ng-if="contents.length == 4 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
						<div class="col-lg-4 col-md-12 7b" data-ng-if="contents.length == 3 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 8b" data-ng-if="contents.length <= 3 && contents.length >= 2 && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 9b" data-ng-if="contents.length <= 3 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>

		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /><#if (titleVar??)> - ${titleVar}</#if>">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, currentPage); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-if="currentPage>2" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> 1" class="page-item">
							<button class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, 1); scrollTo(''frame${numeroFrame}'')">1</button>
						</li>
						<li data-ng-if="currentPage>3" class="page-item disabled"><button class="page-link">...</button></li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+3 && elem-1>currentPage-3" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, elem); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, elem); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li data-ng-if="currentPage<pages.length-4" class="page-item disabled"><button class="page-link">...</button></li>
						<li data-ng-if="currentPage<pages.length-3" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" /> {{pages.length}}" class="page-item">
							<button class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, pages.length); scrollTo(''frame${numeroFrame}'')">{{pages.length}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage+1 == numberOfPages" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, currentPage+2); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>

		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">

	<#if (numTipologie > 3)>
		<div class="modal fade searchWidgetModal" id="searchWidgetModal${numeroFrame}" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle${numeroFrame}" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca-interna-${numeroFrame}">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !General.getFiltriMode().filtri}">
										<button type="button" class="close" data-dismiss="modal" aria-label="Chiudi filtri di ricerca" data-ng-click="General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle${numeroFrame}">
											<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />
										</h1>
										<button class="confirm btn btn-default btn-trasparente float-right" data-dismiss="modal" data-ng-click="General.setFiltriMode(false, false)">Conferma</button>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="search-filter-type ng-hide" data-ng-show="General.getFiltriMode().filtri">
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation">
													<a href="" aria-controls="categoriaTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''categorie'')" data-ng-class="categoriaTab"><@wp.i18n key="CITTAMETRO_CONTENT_TYPES" /></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal-body-search">
							<div class="container">
								<div role="tabpanel" data-ng-show="General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
									<div class="tab-content">
										<div role="tabpanel" data-ng-class="categoriaTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab${numeroFrame}">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<#assign mostraTutto = false />
														<#assign numTipologie = 0/>
														<#assign listaFiltriTip = ""/>
														<#list userFilters as userFilter>
															<#assign typeCode= userFilter.filter?keep_after("value=")?keep_before(")")/>
															<#assign typeKey= userFilter.filter?keep_after("key=")?keep_before(";")/>
															<#if (typeCode!= '''')>
																<#if (!listaFiltriTip?contains(typeCode))>
																	<#if (listaFiltriTip == "")>
																		<#assign listaFiltriTip += typeCode />
																	<#else>
																		<#assign listaFiltriTip += '','' + typeCode />
																	</#if>
																	<@wp.pageInfo pageCode="${typeCode}" info="title" var="typeName" />
																	<#if (numTipologie <= 11)>
																		<div class="flex-100">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${typeCode}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label>${typeName}</label></md-checkbox>
																		</div>
																	<#else>
																		<#assign mostraTutto = true />
																		<div class="flex-100" data-ng-show="showalltipol">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${typeCode}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label>${typeName}</label></md-checkbox>
																		</div>
																	</#if>
																	<#assign numTipologie ++/>
																</#if>
															<#else>
																<#if (typeKey?contains("contentType"))>
																	<#if (numTipologie <= 11)>
																		<div class="flex-100">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${userFilter.contentType}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label><@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}" /></label></md-checkbox>
																		</div>
																	<#else>
																		<#assign mostraTutto = true />
																		<div class="flex-100" data-ng-show="showalltipol">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${userFilter.contentType}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label><@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}" /></label></md-checkbox>
																		</div>
																	</#if>
																	<#assign numTipologie ++/>
																<#else>
																	<#assign pageStart= ''homepage''/>
																	<#if (userFilter.contentType== ''NVT'')><#assign pageStart= ''novita''/></#if>
																	<#if (userFilter.contentType== ''EVN'')><#assign pageStart= ''nov_03''/></#if>
																	<#if (userFilter.contentType== ''DOC'')><#assign pageStart= ''documenti''/></#if>
																	<#if (userFilter.contentType== ''SRV'')><#assign pageStart= ''servizi''/></#if>
																	<@wp.nav var="pagina" spec="code(${pageStart}).subtree(3)">
																		<#if (pagina.level >= 1)>
																			<#if (numTipologie <= 11)>
																				<div class="flex-100">
																					<md-checkbox data-ng-checked="searchParameters.key == ''${pagina.code}''" aria-label="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																					<label>${pagina.title}</label></md-checkbox>
																				</div>
																			<#else>
																				<#assign mostraTutto = true />
																				<div class="flex-100" data-ng-show="showalltipol">
																					<md-checkbox data-ng-checked="searchParameters.key == ''${pagina.code}''" aria-label="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																					<label>${pagina.title}</label></md-checkbox>
																				</div>
																			</#if>
																			<#assign numTipologie ++/>
																		</#if>
																	</@wp.nav>
																</#if>
															</#if>
														</#list>
														<#if (mostraTutto)>
															<a href="" data-ng-hide="showalltipol" data-ng-click="showalltipol=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
															<a href="" data-ng-show="showalltipol" data-ng-click="showalltipol=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
														</#if>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</#if>
</div>' WHERE code='cittametro_widget_lista_multicontent';

UPDATE guifragment SET gui='<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cc=JspTaglibs["/cagliari-core"]>

<@wp.currentWidget param="config" configParam="contentType" var="tipoContenuto" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="ordinamento" />
<@wp.categories var="systemCategories" titleStyle="prettyFull" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />


<@jacms.contentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign pathimg><@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg</#assign>
<#setting locale="it_IT">
<#assign actualdate = .now>
<#assign monthst = actualdate?string["MMMM"]>
<#assign day = actualdate?string["d"]>
<#assign dayNum = day?number />
<#assign month = actualdate?string["MM"]>
<#assign monthNum = month?number />
<#assign yearst = actualdate?string["yyyy"]>
<#assign finemese><@cc.lastDayOfMonth /></#assign>
<#assign finemese = finemese?number />
<#assign dataend = "${finemese}/${month}/${yearst}"  />
<#assign datatoday = "${day}/${month}/${yearst}"/>

<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!ordinamento??)><#assign ordinamento = "" /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>

<#if (filtri??)>
	<#assign filtriUtente = [] />
	<#list filtri?split("+") as filtro>
		<#assign value>${filtro?remove_beginning("(attributeFilter=false;key=category;categoryCode=")}</#assign>
		<#assign value>${value?remove_ending(")")}</#assign>
		<#assign filtriUtente = filtriUtente + [value] />
	</#list>
	<#assign filtriCodeName = [] />
	<#list filtriUtente as filtro>
		<#list systemCategories as systemCategory>
			<#if (filtro == systemCategory.key)>
				<#assign filtriCodeName = filtriCodeName + [filtro + "/" + systemCategory.value?keep_after_last("/ ")] />
			</#if>
		</#list>
	</#list>
</#if>

<div class="widget" data-ng-cloak data-ng-controller="FiltriController" >
	<div class="container" data-ng-init="setCalendarParameters(''${dayNum}'',''${finemese}'',false, ''${elementiMaxItem}'', ''${elementiMax}'')">
		<div class="row">
			<#if (titleVar??)>
				<div class="col-md-3">
					<h3>${titleVar}</h3>
				</div>
			</#if>
			<#if (filtriCodeName??) && (filtriCodeName?has_content) && (filtriCodeName?size > 0)>
				<div class="col-md-9 calendario-filtro">
					<button id="${tipoContenuto}" class="btn btn-default btn-trasparente" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
						data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', ''${tipoContenuto}''); categorie=''${categorie}''">
						<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
					</button>
					<#list filtriCodeName as filtro>
						<#assign categoryCode>${filtro?keep_before_last("/")}</#assign>
						<#assign categoryName>${filtro?keep_after_last("/")}</#assign>
						<button id="${categoryCode}" class="btn btn-default btn-trasparente" title="${categoryName}"
							data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie},${categoryCode}'', ''${categoryCode}''); categorie=''${categoryCode}''">
							${categoryName}
						</button>
					</#list>
				</div>
			</#if>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h4 data-ng-show="oneMonth">${monthst} ${yearst}</h4>
				<h4 data-ng-hide="oneMonth">${monthst}/{{nextMonth}} ${yearst}</h4>
				<div id="owl-calendario" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist" data-ng-cloak>
					<data-ng-container data-ng-repeat="day in calendar" data-ng-cloak data-ng-if="full[day]" data-ng-show="!empty">
						<div class="scheda-calendario" data-ng-if="day >= ${dayNum}" data-ng-init="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'',
							''${tipoContenuto}'', getFilterDay(day, ''${month}'',''${yearst}''), day)">
							<div class="scheda-calendario-data">
								<strong>{{day}}</strong> {{dayName[day]}}
							</div>
							<div class="progress-spinner progress-spinner-active" data-ng-show="loader[day]">
								<span class="sr-only">Caricamento...</span>
							</div>
							<ul class="scheda-calendario-lista" data-ng-if="events[day].length > 0">
								<li data-ng-cloak data-ng-repeat="elem in events[day]"
									data-ng-show="categoryContent[day][elem.$].indexOf(categorie)>-1"
									data-ng-bind-html="renderEventContent[day][elem.$]">
								</li>
							</ul>
						</div>
						<div class="scheda-calendario" data-ng-if="day < ${dayNum}" data-ng-init="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'',
							''${tipoContenuto}'', getFilterDay(day, ''${monthNum + 1}'',''${yearst}''), day)">
							<div class="scheda-calendario-data">
								<strong>{{day}}</strong> {{dayName[day]}}
							</div>
							<div class="progress-spinner progress-spinner-active" data-ng-show="loader[day]">
								<span class="sr-only">Caricamento...</span>
							</div>
							<ul class="scheda-calendario-lista" data-ng-if="events[day].length > 0">
								<li data-ng-repeat="elem in events[day]"
									data-ng-show="categoryContent[day][elem.$].indexOf(categorie)>-1"
									data-ng-bind-html="renderEventContent[day][elem.$]">
								</li>
							</ul>
						</div>
					</data-ng-container>
				</div>
				<div data-ng-show="empty" class="mt24">
					<@wp.i18n key="CITTAMETRO_PORTAL_EVENTLISTEMPTY" />
				</div>
			</div>
		</div>
	</div>
</div>' WHERE code='cittametro_widget_calendario_homepage';

UPDATE guifragment SET gui='<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.currentWidget param="config" configParam="contents" var="contenuti" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />

<#assign elementiMaxItem = 3 />
<#if (!contenuti??)><#assign contenuti = "" /></#if>

<div class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', '''', '''', '''', '''', '''', '''', '''', ''${contenuti}'')">
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>

		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} == 0" data-ng-repeat="elem in contents | startFrom:currentPage*contents.length | limitTo: contents.length">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
	</div>
	<#assign contentList="">
</div>' WHERE code='cittametro_widget_inevidenza_homepage';

DELETE FROM guifragment WHERE code='keycloak_auth';
INSERT INTO guifragment (code, widgettypecode, plugincode, gui, defaultgui, locked) VALUES ('keycloak_auth', NULL, NULL, '<#assign wp=JspTaglibs[ "/aps-core"]>
<#assign wp = JspTaglibs["/aps-core"] >
<script nonce="<@wp.cspNonce />" src="http://metroca.mshome.net/auth/js/keycloak.js"></script>
<script nonce="<@wp.cspNonce />">
        const consolePrefix = ''[ENTANDO-KEYCLOAK]'';
        let keycloakConfig = {
			"realm":"entando",
			"auth-server-url":"http://metroca.mshome.net/auth",
			"ssl-required":"external",
			"resource":"entando-web",
			"clientId":"entando-web",
			"public-client":true
		};

		window.keycloak = new Keycloak(keycloakConfig);
		window.keycloak
			.init({
				onLoad: ''check-sso'',
				promiseType: ''native'',
				silentCheckSsoRedirectUri: ''<@wp.info key="systemParam" paramName="applicationBaseURL" />cmsresources/static/html/silent-check-sso.html'',
				enableLogging: true,
				responseMode: ''query''
			})
			.then(
				function (auht) {
					window.keycloak.isInitialized = true;
					if (auht)
						localStorage.setItem(''token'', window.keycloak.token);
					else
						localStorage.removeItem(''token'');
				}
			)
			.catch(function(e) {
				localStorage.removeItem(''token'');
			});

		window.keycloak.onReady = function() {
			dispatchKeycloakEvent(''onReady'');
		};
		window.keycloak.onAuthSuccess = function() {
			dispatchKeycloakEvent(''onAuthSuccess'');
			localStorage.setItem(''token'', keycloak.token);
		};
		window.keycloak.onAuthError = function() {
			dispatchKeycloakEvent(''onAuthError'');
			localStorage.removeItem(''token'');
		};
		window.keycloak.onAuthRefreshSuccess = function() {
			dispatchKeycloakEvent(''onAuthRefreshSuccess'');
			localStorage.setItem(''token'', keycloak.token);
		};
		window.keycloak.onAuthRefreshError = function() {
			dispatchKeycloakEvent(''onAuthRefreshError'');
			localStorage.removeItem(''token'');
		};
		window.keycloak.onAuthLogout = function() {
			dispatchKeycloakEvent(''onAuthLogout'');
			localStorage.removeItem(''token'');
		};
		window.keycloak.onTokenExpired = function() {
			dispatchKeycloakEvent(''onTokenExpired'');
			localStorage.removeItem(''token'');
		};

        function dispatchKeycloakEvent(eventType) {
            //console.info(consolePrefix, ''Dispatching'', eventType, ''custom event'');
            return window.dispatchEvent(new CustomEvent(''keycloak'', {
                detail: {
                    eventType
                }
            }));
        };
 </script>', NULL, 0);

 UPDATE guifragment SET gui='<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>
<#assign jpseo=JspTaglibs["/jpseo-aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign jacms=JspTaglibs["/jacms-aps-core"]>

 	<html lang="<@wp.info key="currentLang" />">
 	<head>
		<base href="<@wp.info key="systemParam" paramName="applicationBaseURL" />">
 		<!--[if IE]><script type="text/javascript">
 			(function() {
 				var baseTag = document.getElementsByTagName(''base'')[0];
 				baseTag.href = baseTag.href;
 			})();
 		</script><![endif]-->
		<@wp.i18n key="CITTAMETRO_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />
  		<@wp.currentPage param="title" var="pageTitle" />
		<@ca.cagliariPageInfoTag pageCode="${pageCode}" info="isPublishOnFly" var="isPublishOnFly"/>
		<#if (isPublishOnFly == "true")>
			<@ca.cagliariMainFrameContentTag var="content"/>
			<#if content??>
				<#if (content.getAttributeByRole(''jpsocial:title''))??>
					<#assign title=content.getAttributeByRole(''jpsocial:title'').text>
					<#if title??>
						<#assign pageTitle = title>
					</#if>
				</#if>
				<#if (content.getAttributeByRole(''jpsocial:description''))??>
					<#assign description=content.getAttributeByRole(''jpsocial:description'').text>
				</#if>
				<#if (content.getAttributeByRole(''jpsocial:image''))??>
					<#assign image=content.getAttributeByRole(''jpsocial:image'').getImagePath("4")>
				</#if>
			</#if>
		</#if>
		<#if (pageCode == "homepage")>
			<title>${siteName}</title>
		<#else>
			<#if (pageCode == "arg_01_dett")>
				<@cw.currentArgument var="currentArg" />
				<#if (currentArg??)>
					<@cw.contentList listName="contentList" contentType="ARG" category="${currentArg}"/>
					<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
						<#list contentList as contentId>
							<title>${siteName} | <@jacms.content contentId="${contentId}" modelId="400012" /></title>
							<#assign description><@jacms.content contentId="${contentId}" modelId="400013" /></#assign>
						</#list>
					</#if>
					<#assign contentList="">
				</#if>
			<#else>
				<title>${siteName} | ${pageTitle}</title>
			</#if>
		</#if>
 		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 		<@jpseo.currentPage param="description" var="metaDescr" />
 		<#if metaDescr?? && metaDescr?has_content>
			<meta name="description" content="${metaDescr}" />
		<#else>
			<#if description??>
				<meta name="description" content="${description}" />
			<#else>
				<@ca.cagliariMainFrameContentTag var="content"/>
				<#if content??>
					<#assign description=content.getAttributeByRole(''jacms:description'').text>
					<meta name="description" content="${description}" />
				</#if>
			</#if>
		</#if>
		<@jpseo.currentPage param="keywords" var="metaKeyword" />
		<#if metaKeyword??>
			<meta name="keywords" content="${metaKeyword}" />
		</#if>

 		<link rel="apple-touch-icon" sizes="57x57" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-57x57.png" />
 		<link rel="apple-touch-icon" sizes="60x60" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-60x60.png" />
 		<link rel="apple-touch-icon" sizes="72x72" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-72x72.png" />
 		<link rel="apple-touch-icon" sizes="76x76" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-76x76.png" />
 		<link rel="apple-touch-icon" sizes="114x114" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-114x114.png" />
 		<link rel="apple-touch-icon" sizes="120x120" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-120x120.png" />
 		<link rel="apple-touch-icon" sizes="144x144" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-144x144.png" />
 		<link rel="apple-touch-icon" sizes="152x152" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-152x152.png" />
 		<link rel="apple-touch-icon" sizes="180x180" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/apple-icon-180x180.png" />
 		<link rel="icon" type="image/png" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/favicon-32x32.png" sizes="32x32" />
 		<link rel="icon" type="image/png" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/android-chrome-192x192.png" sizes="192x192" />
 		<link rel="icon" type="image/png" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/favicon-96x96.png" sizes="96x96" />
 		<link rel="icon" type="image/png" href="<@wp.resourceURL />cittametro-homepage-bundle/static/img/favicon-16x16.png" sizes="16x16" />

 		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/font/fonts.min.css" />
 		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/bootstrap-italia_1.2.0.min.css" />
 		<link rel="stylesheet" href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/angular-material.cagliari.css"  />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cittametro.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/areapersonale.css" rel="stylesheet" type="text/css" />
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/cagliari-print.css" rel="stylesheet" type="text/css" media="print" />
 		<@wp.outputHeadInfo type="CSS_CA_INT">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/css/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT">
 		<link href="<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="CSS_CA_PLGN">
 		<link href="<@wp.resourceURL />cittametro-homepage-bundle/static/<@wp.printHeadInfo />" rel="stylesheet" type="text/css" />
 		</@wp.outputHeadInfo>

 		<script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/jquery-3.3.1.min.js"></script>
 		<@wp.fragment code="keycloak_auth" escapeXml=false />
 		<@wp.outputHeadInfo type="JS_CA_TOP_EXT"><script nonce="<@wp.cspNonce />" src="<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_EXT_GLB"><@wp.printHeadInfo /></@wp.outputHeadInfo>
 		<@wp.outputHeadInfo type="JS_CA_TOP"><script nonce="<@wp.cspNonce />" src="<@wp.resourceURL />cittametro-homepage-bundle/static/js/<@wp.printHeadInfo />"></script></@wp.outputHeadInfo>

  		<@wp.i18n key="CITTAMETRO_PORTAL_TITLE" var="siteName"/>
  		<@wp.currentPage param="code" var="pageCode" />
  		<@wp.currentPage param="title" var="pageTitle" />
		<#if pageCode?? && pageCode != "">
		<#else>
			<#assign pageCode=''homepage''>
		</#if>
  		<#if pageCode == ''homepage''>
  		 	<#assign pageTitle=siteName>
  		</#if>

  		<@wp.i18n key="CITTAMETRO_PORTAL_DESCRIPTION" var="description"/>

  		<@wp.info key="systemParam" paramName="applicationBaseURL" var="baseURL"/>
		<#if (baseURL?last_index_of("/portal-ui") > -1)>
		  <#assign baseURL = baseURL?substring(0,baseURL?last_index_of("/portal-ui"))>
		<#elseif (baseURL?last_index_of("/entando-de-app") > -1)>
		  <#assign baseURL = baseURL?substring(0,baseURL?last_index_of("/entando-de-app"))>
		<#else>
		  <#assign baseURL = baseURL?substring(0,baseURL?last_index_of("/portale"))>
		</#if>

		<!-- FACEBOOK -->
		<meta property="og:site_name" content="${siteName}" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="${pageTitle}" />
		<#if description??>
			<meta property="og:description" content="${description}"/>
		<#else>
			<meta property="og:description" content="-"/>
		</#if>
		<#if image?? && image != "">
			<meta property="og:image" content="${baseURL}${image}" />
		<#else>
			<meta property="og:image" content="${baseURL}<@wp.resourceURL/>cittametro-homepage-bundle/static/img/STEMMA_CMDCA_20170329_TRATTO.svg" />
		</#if>
		<meta property="fb:app_id" content="409032892503811" />

		<!-- TWITTER -->
		<meta name="twitter:card" content="summary_large_image" />
		<meta name="twitter:site" content="@Comune_Cagliari" />
		<meta name="twitter:title" content="${pageTitle}" />
		<#if description??>
			<meta name="twitter:description" content="${description}"/>
		<#else>
			<meta name="twitter:description" content="-"/>
		</#if>
		<#if image?? && image != "">
			<meta name="twitter:image" content="${baseURL}${image}" />
		<#else>
			<meta name="twitter:image" content="${baseURL}<@wp.resourceURL/>cittametro-homepage-bundle/static/img/STEMMA_CMDCA_20170329_TRATTO.svg" />
		</#if>
		<@wp.fragment code="cittametro_template_googletagmanager" escapeXml=false />
 	</head>' WHERE code='cittametro_template_head';
