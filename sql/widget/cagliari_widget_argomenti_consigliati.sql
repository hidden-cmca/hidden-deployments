insert into widgetcatalog (code, titles, locked) values ('cagliari_widget_argomenti_consigliati', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - Argomenti consigliati (piccoli)</property><property key="it">Cittametro - Argomenti consigliati (piccoli)</property></properties>', 1);


UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfig"/>
</config>' WHERE code = 'cagliari_widget_argomenti_consigliati';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cagliari_widget_argomenti_consigliati';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cagliari_widget_argomenti_consigliati', 'cagliari_widget_argomenti_consigliati', 'whitelabel','',1);


UPDATE guifragment SET gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@jacms.contentList listName="contentList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />
<div class="widget">
<#if (titleVar??)>
	<div class="row">
		<div class="col-md-12">
			<div class="titolosezione">
				<h3>
				<#if (pageLinkVar??)>
					<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
				<#else>
					${titleVar}
				</#if>
				</h3>
			</div>
		</div>
	</div>
</#if>

<#if (contentList??) && (contentList?has_content) && (contentList?size > 0)>
	<@wp.pager listName="contentList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
		<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
			<div class="row">
				<div class="offset-md-2 col-md-8 offset-md-2 col-sm-12">
					<div class="argomenti altri-argomenti">
						<div class="altri-argomenti-elenco">
							<#list contentList as contentId>
								<#if (contentId_index >= groupContent.begin) && (contentId_index <= groupContent.end)>
									<@jacms.content contentId="${contentId}" />
								</#if>
							</#list>
						</div>
					</div>
				</div>
			</div>
		</@wp.freemarkerTemplateParameter>
	</@wp.pager>
<#else>
	<div class="row">
		<div class="offset-md-2 col-md-8 offset-md-2 col-sm-12">
			<div class="argomenti altri-argomenti">
				<div class="altri-argomenti-elenco">
					<a class="badge badge-pill badge-argomenti"><@wp.i18n key="CITTAMETRO_PORTAL_NOARGUMENTS" /></a>
				</div>
			</div>
		</div>
	</div>
</#if>
<#assign contentList="">
</div>' where code = 'cagliari_widget_argomenti_consigliati';
