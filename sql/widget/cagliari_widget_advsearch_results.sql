insert into widgetcatalog (code, titles, locked) values ('cagliari_widget_advsearch_results', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">CITTAMETRO - Ricerca avanzata - Risultati</property><property key="it">CITTAMETRO - Ricerca avanzata - Risultati</property></properties>', 1);


UPDATE widgetcatalog SET parameters = '<config>
    <parameter name="facetRootNodes">Facet Category Root</parameter>
    <parameter name="contentTypesFilter">Content Type (optional)</parameter>
    <action name="solrFacetNavTreeConfig"/>
</config>' WHERE code = 'cagliari_widget_advsearch_results';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cagliari_widget_advsearch_results';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cagliari_widget_advsearch_results', 'cagliari_widget_advsearch_results', 'whitelabel','',1);


UPDATE guifragment SET gui = '<#assign wp=JspTaglibs["/aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>

<@wp.currentWidget param="config" configParam="contentTypesFilter" var="tipiContenuto" />
<#if (RequestParameters[''cercatxt'']??)><#assign cercatxt = RequestParameters[''cercatxt'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign cercatxt = ""></#if>
<#if (RequestParameters[''argomenti'']??)><#assign argomenti = RequestParameters[''argomenti'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign argomenti = ""></#if>
<#if (RequestParameters[''amministrazione'']??)><#assign amministrazione = RequestParameters[''amministrazione'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign amministrazione = ""></#if>
<#if (RequestParameters[''servizi'']??)><#assign servizi = RequestParameters[''servizi'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign servizi = ""></#if>
<#if (RequestParameters[''novita'']??)><#assign novita = RequestParameters[''novita'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign novita = ""></#if>
<#if (RequestParameters[''documenti'']??)><#assign documenti = RequestParameters[''documenti'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign documenti = ""></#if>
<#if (RequestParameters[''attivi'']??)><#assign attivi = RequestParameters[''attivi'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign attivi = ""></#if>
<#if (attivi == "")><#assign attivi = ''false''/></#if>
<#if (RequestParameters[''inizio'']??)><#assign inizio = RequestParameters[''inizio'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign inizio = ""></#if>
<#if (RequestParameters[''fine'']??)><#assign fine = RequestParameters[''fine'']?replace("&#x5b;","")?replace("&#x5d;","")?replace("&quot;","\"")/><#else><#assign fine = ""></#if>

<div class="progress-spinner progress-spinner-active" data-ng-show="::false">
	<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
</div>

<div data-ng-cloak data-ng-controller="ctrlRicerca as ctrl">
	<data-ng-container data-ng-init="ctrl.Ricerca.setContentTypes(''${tipiContenuto}'');
		ctrl.Ricerca.setArgsSelected(''${argomenti?replace(''"'', '''')}''); 
		ctrl.Ricerca.setCatSelected(''amministrazione'',''${amministrazione?replace(''"'', '''')}''); 
		ctrl.Ricerca.setCatSelected(''servizi'',''${servizi?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''novita'',''${novita?replace(''"'', '''')}'');
		ctrl.Ricerca.setCatSelected(''documenti'',''${documenti?replace(''"'', '''')}'');
		ctrl.Ricerca.setCercatxt(''${cercatxt}'');
		ctrl.Ricerca.activeChk = ${attivi};
		ctrl.Ricerca.dataInizio = ''${inizio}'';
		ctrl.Ricerca.dataFine = ''${fine}''">
	</data-ng-container>
	
	<section id="introricerca" data-ng-init="setSearchCookie(true); ctrl.Ricerca.getContents(); showall=false; showfilter=false">
		<div class="container">
			<div class="row">
				<div class="offset-lg-1 col-lg-10 col-md-12">
					<div class="titolo-sezione">
						<h2><#if (cercatxt!="")>${cercatxt}<#else><@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT" /></#if></h2>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="articolo-dettaglio-testo">
		<div class="container">
			<div class="row">
				<div class="linetop-lg"></div>
				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="cerca-risultati d-block d-sm-block d-md-none d-lg-none d-xl-none">
						<#assign morethan><@wp.i18n key="CITTAMETRO_PORTAL_MORETHAN" /></#assign>
						<span data-ng-if="ctrl.Ricerca.getContentsVar().length > 0"><span data-ng-show="ctrl.Ricerca.getTotalItems() == ''100000''">${morethan?cap_first} </span>{{ctrl.Ricerca.getTotalItems()}} <@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /></span>
						<span data-ng-if="ctrl.Ricerca.getContentsVar().length == 0"><@wp.i18n key="LIST_VIEWER_EMPTY" /></span>
						<a class="show-filters" data-ng-click="showfilter=false" data-ng-show="showfilter" href=""><strong><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></strong></a>
						<a class="show-filters" data-ng-click="showfilter=true" data-ng-hide="showfilter" href=""><strong><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></strong></a>
						<span class="float-right" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0 && !showfilter">
							<select class="ordinamento" name="ordinamento" data-ng-model="ctrl.Ricerca.orderBy" data-ng-change="ctrl.Ricerca.getContents()" title="<@wp.i18n key="CITTAMETRO_PORTAL_CHOOSE_OPTION" />">
								<option value="data_last_mod"><@wp.i18n key="CITTAMETRO_PORTAL_LAST_MODIFIED" /></option>
								<option value="data_pubb"><@wp.i18n key="CITTAMETRO_PORTAL_PUBLICATION_DATE" /></option>
								<option value="titolo_asc"><@wp.i18n key="CITTAMETRO_PORTAL_INCREASING_TITLE" /></option>
								<option value="titolo_desc"><@wp.i18n key="CITTAMETRO_PORTAL_DESCENDING_TITLE" /></option>
							</select>
						</span>
						<button data-ng-show="showfilter" data-ng-click="showfilter=false" class="confirm btn btn-default btn-trasparente float-right"><@wp.i18n key="CITTAMETRO_PORTAL_CONFIRM" /></button>
					</div>
					<aside id="menu-sinistro-cerca" class="d-md-block" data-ng-class="showfilter ? ''d-sm-block d-block'' : ''d-sm-none d-none''">
						<h4><@wp.i18n key="CITTAMETRO_PORTAL_CATEGORIES" /></h4>
						<div class="search-filter-ckgroup">
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" />"
									data-ng-checked="ctrl.Ricerca.ammins_isChecked()"
									md-indeterminate="ctrl.Ricerca.ammins_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.ammins_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_AMMINISTRAZIONE" /></label>
								</md-checkbox>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" />"
									data-ng-checked="ctrl.Ricerca.servizi_isChecked()"
									md-indeterminate="ctrl.Ricerca.servizi_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.servizi_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_SERVIZI" /></label>
								</md-checkbox>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" />"
									data-ng-checked="ctrl.Ricerca.novita_isChecked()"
									md-indeterminate="ctrl.Ricerca.novita_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.novita_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_NOVITA" /></label>
								</md-checkbox>
							</div>
							<div class="flex-100">
								<md-checkbox aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" />"
									data-ng-checked="ctrl.Ricerca.docs_isChecked()"
									md-indeterminate="ctrl.Ricerca.docs_isIndeterminate()"
									data-ng-click="ctrl.Ricerca.docs_toggleAll(); ctrl.Ricerca.getContents()"><label><@wp.i18n key="CITTAMETRO_PORTAL_DOCUMENTI" /></label>
								</md-checkbox>
							</div>
						</div>
						
						<h4><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></h4>
						<div class="search-filter-ckgroup">
							<div class="flex-100" data-ng-repeat="item in ctrl.Ricerca.get_argomenti()">
								<div data-ng-if = "$index <= 9">
									<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
									<label data-ng-bind-html="item.name"></label></md-checkbox>
								</div>
								<div data-ng-if = "$index >= 10">									
									<div data-ng-show="showallarg">
										<md-checkbox data-ng-checked="ctrl.Ricerca.exists(item.code, ctrl.Ricerca.get_args_selected())" data-ng-click="ctrl.Ricerca.toggle(item.code, ctrl.Ricerca.get_args_selected()); ctrl.Ricerca.getContents()" aria-label="{{item.name}}">
										<label data-ng-bind-html="item.name"></label></md-checkbox>
									</div>
								</div>
							</div>
							<a href="" class="search-mostra" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
							<a href="" class="search-mostra" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
						</div>
					</aside>
				</div>
				<div class="col-lg-9 col-md-8 d-md-block" data-ng-class="showfilter ? ''d-sm-none d-none'' : ''d-sm-block d-block''">
					<div class="articolo-paragrafi">
						<div class="row">
							<div class="col-md-12 cerca-risultati d-md-block d-none">
								<span data-ng-if="ctrl.Ricerca.getContentsVar().length > 0"><@wp.i18n key="CITTAMETRO_PORTAL_FOUND" /> <span data-ng-show="ctrl.Ricerca.getTotalItems() == ''100000''"><@wp.i18n key="CITTAMETRO_PORTAL_MORETHAN" /> </span>{{ctrl.Ricerca.getTotalItems()}} <@wp.i18n key="CITTAMETRO_PORTAL_RESULTS" /></span>
								<span data-ng-if="ctrl.Ricerca.getContentsVar().length == 0"><@wp.i18n key="LIST_VIEWER_EMPTY" /></span>
								<div class="float-right d-sm-none d-md-block" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0">
									<label for="ordinamento"><@wp.i18n key="CITTAMETRO_PORTAL_SORT_BY" /></label>
									<select id="ordinamento" class="ordinamento" name="ordinamento" data-ng-model="ctrl.Ricerca.orderBy" data-ng-change="ctrl.Ricerca.getContents()" title="<@wp.i18n key="CITTAMETRO_PORTAL_CHOOSE_OPTION" />">
										<option value="data_last_mod"><@wp.i18n key="CITTAMETRO_PORTAL_LAST_MODIFIED" /></option>
										<option value="data_pubb"><@wp.i18n key="CITTAMETRO_PORTAL_PUBLICATION_DATE" /></option>
										<option value="titolo_asc"><@wp.i18n key="CITTAMETRO_PORTAL_INCREASING_TITLE" /></option>
										<option value="titolo_desc"><@wp.i18n key="CITTAMETRO_PORTAL_DESCENDING_TITLE" /></option>
									</select>
								</div>
							</div>
							<div class="linetop-lg"></div>
						</div>
						
						<div data-ng-if="ctrl.Ricerca.getContentsVar().length > 0" data-ng-repeat="elem in ctrl.Ricerca.getContentsVar()">
							<div class="row" data-ng-if="($index+1)%3 == 0 || ($index+1) == ctrl.Ricerca.getContentsVar().length || ($index+1) == 15 || (($index+1) + (15* ctrl.Ricerca.getCurrentPage)) == ctrl.Ricerca.getContentsVar().length">
								<div class="col-lg-4 col-md-12" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="ctrl.Ricerca.getRenderContent(ctrl.Ricerca.getContentsVar((ctrl.Ricerca.getContentsVar().indexOf(elem)-2)))"></div>
								<div class="col-lg-4 col-md-12" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="ctrl.Ricerca.getRenderContent(ctrl.Ricerca.getContentsVar((ctrl.Ricerca.getContentsVar().indexOf(elem)-1)))"></div>
								<div class="col-lg-4 col-md-12" data-ng-bind-html="ctrl.Ricerca.getRenderContent(elem)"></div>
							</div>
						</div>
						
						<div class="row" data-ng-if="ctrl.Ricerca.getContentsVar().length > 0 && ctrl.Ricerca.getPages().length > 1">
							<div class="col-md-12">
								<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" />">
									<ul class="pagination">
										<li class="page-item">
											<button class="page-link" data-ng-disabled="ctrl.Ricerca.getCurrentPage() == 0" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getCurrentPage()); ctrl.Ricerca.getContents(ctrl.Ricerca.getCurrentPage()+1)" 
												title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
												<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
											</button>
										</li>
										<li data-ng-repeat="elem in ctrl.Ricerca.getPages()" data-ng-if="elem-1<ctrl.Ricerca.getCurrentPage()+5 && elem-1>ctrl.Ricerca.getCurrentPage()-5" title="{{elem}}"
											data-ng-class="ctrl.Ricerca.getCurrentPage() == (elem-1) ? ''page-item active'' : ''page-item''">
											<button data-ng-show="ctrl.Ricerca.getCurrentPage() == (elem-1)" aria-current="page" class="page-link" data-ng-click="ctrl.Ricerca.goToPage(elem); ctrl.Ricerca.getContents(elem)">{{elem}}</button>
											<button data-ng-show="ctrl.Ricerca.getCurrentPage() != (elem-1)" class="page-link" data-ng-click="ctrl.Ricerca.goToPage(elem); ctrl.Ricerca.getContents(elem)">{{elem}}</button>
										</li>
										<li class="page-item">
											<button class="page-link" data-ng-disabled="ctrl.Ricerca.getCurrentPage()+1 == ctrl.Ricerca.getNumberOfPages()" data-ng-click="ctrl.Ricerca.goToPage(ctrl.Ricerca.getCurrentPage()+1); ctrl.Ricerca.getContents(ctrl.Ricerca.getCurrentPage()+2)"
												title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
												<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
												<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
											</button>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>' where code = 'cagliari_widget_advsearch_results';
