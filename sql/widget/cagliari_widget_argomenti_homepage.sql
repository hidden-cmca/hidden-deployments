insert into widgetcatalog (code, titles, locked) values ('cittametro_widget_argomenti_homepage', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - Argomenti homepage</property><property key="it">Cittametro - Argomenti homepage</property></properties>', 1);


UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>' WHERE code = 'cittametro_widget_argomenti_homepage';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cittametro_widget_argomenti_homepage';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cittametro_widget_argomenti_homepage', 'cittametro_widget_argomenti_homepage', 'whitelabel','',1);


UPDATE guifragment SET gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<div class="widget">
<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />
	<div class="argomenti-foto">
		<div class="container">
			<#if (titleVar??)>
				<div class="row">
					<div class="col-md-12">
						<h3 class="titolosezione">
							${titleVar}
						</h3>
					</div>
				</div>
			</#if>
		</div>
	</div>
	<div class="container">
		<#if (contentInfoList??) && (contentInfoList?has_content) && (contentInfoList?size > 0)>
			<@wp.pager listName="contentInfoList" objectName="groupContent" pagerIdFromFrame=true advanced=true offset=5>
				<@wp.freemarkerTemplateParameter var="group" valueName="groupContent" removeOnEndTag=true >
				<#assign count= 0>
				<div class="row row-eq-height mt-128n">
					<#list contentInfoList as contentInfoVar>
						<#if (count < 3)>
							<div class="col-md-4">
								<#assign cid = contentInfoVar[''contentId'']>
								<#if (cid?contains("ARG"))>
									<#if (count < 3)>
										<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="400003" />
									</#if>
									<#assign count = count + 1>
								</#if>
							</div>
						</#if>	
					</#list>
				</div>
				<div class="row">
					<div class="offset-md-2 col-md-8 offset-md-2 col-sm-12">
						<div class="argomenti altri-argomenti">
							<div class="altri-argomenti-titolo">
								<h4><@wp.i18n key="CITTAMETRO_PORTAL_ARGUMENTS_OTHER" /></h4>
							</div>
							<#assign count= 0>
							<div class="altri-argomenti-elenco">
								<#list contentInfoList as contentInfoVar>
									<#if (count > 2)>
										<#assign cid = contentInfoVar[''contentId'']>
										<#if (cid?contains("ARG"))>
											<@jacms.content contentId="${contentInfoVar[''contentId'']}" modelId="400001" />
											<#assign count = count + 1>
										</#if>
									</#if>
									<#assign count = count + 1>
								</#list>
								<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOARGUMENT" />" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="badge badge-pill badge-argomenti">${pageLinkDescriptionVar}</a>
								</#if>
							</div>
						</div>
					</div>
				</div>
				</@wp.freemarkerTemplateParameter>
			</@wp.pager>
		</#if>
	</div>
<#assign contentInfoList="">
</div>' where code = 'cittametro_widget_argomenti_homepage';
