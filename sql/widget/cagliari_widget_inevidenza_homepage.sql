insert into widgetcatalog (code, titles, locked) values ('cittametro_widget_inevidenza_homepage', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - In evidenza homepage</property><property key="it">Cittametro - In evidenza homepage</property></properties>', 1);


UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>' WHERE code = 'cittametro_widget_inevidenza_homepage';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cittametro_widget_inevidenza_homepage';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cittametro_widget_inevidenza_homepage', 'cittametro_widget_inevidenza_homepage', 'whitelabel','',1);


UPDATE guifragment SET gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>

<@wp.currentWidget param="config" configParam="contents" var="contenuti" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />

<#assign elementiMaxItem = 3 />
<#if (!contenuti??)><#assign contenuti = "" /></#if>

<div class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', '''', '''', '''', '''', '''', '''', '''', ''${contenuti}'')"> 	
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">	
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} == 0" data-ng-repeat="elem in contents | startFrom:currentPage*contents.length | limitTo: contents.length">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
	</div>
	<#assign contentList="">
</div>' where code = 'cittametro_widget_inevidenza_homepage';
