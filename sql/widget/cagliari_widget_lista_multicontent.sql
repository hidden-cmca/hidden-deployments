insert into widgetcatalog (code, titles, locked) values ('cagliari_widget_lista_multicontent', '<?xml version="1.0" encoding="UTF-8"?>
<properties>
<property key="en">Cittametro - Lista multi-content</property>
<property key="it">Cittametro - Lista multi-content</property>
</properties>', 1);


UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contentTypes">Content Type (mandatory)</parameter>
    <parameter name="layout">Layout of content list</parameter>	
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />	
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="multiContentListViewerConfig"/>
</config>' WHERE code = 'cagliari_widget_lista_multicontent';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cagliari_widget_lista_multicontent';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cagliari_widget_lista_multicontent', 'cagliari_widget_lista_multicontent', 'whitelabel','',1);


UPDATE guifragment SET gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
	<#if (Session.currentUser.accessToken??)>
		<#assign utenteToken = Session.currentUser.accessToken />
	</#if>
</#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />
<@cw.currentArgument var="currentArg" />
<@wp.currentWidget param="config" configParam="contentTypes" var="contentTypes" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="modelId2" var="idModello2" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="orClauseCategoryFilter" var="orCategory" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="filters" />
<@wp.currentWidget param="config" configParam="layout" var="layout" />
<@wp.currentWidget param="config" configParam="title_it" var="titleVar" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@wp.categories var="systemCategories" root="arg_argomenti" />

	
<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>
<#if (!orCategory??)><#assign orCategory = "false" /></#if>
<#if (!filters??)><#assign filters = "" /></#if>
<#if (!idModello??)><#assign idModello = "list" /></#if>
<#if (!idModello2??)><#assign idModello2 = "list" /></#if>
<#if (currentArg??)>
	<#assign categorie = ''${currentArg},${categorie}'' />
<#else>
	<#assign currentArg = '''' />
</#if>

<#assign altriFiltri = false />
<#assign allCategories = false/>
<#assign numTipologie = 0 />
<#assign categoryIndex = 999 />
<#assign categoryEnumIndex = 999 />
<#assign tipologieIndex = 999 />
<#assign tipologiaName = '''' />
<#assign enumCategName = '''' />

<#assign contentTypes = contentTypes?keep_after("[")?keep_before("]")?split("},") />

<#assign contents = [] />
<#list contentTypes as contentType>
	<#assign contents = contents + [contentType?keep_after("{")?keep_before("}")] />
</#list>

<#assign tipiContenuto = "" />
<#list contents as content>
	<#assign typeCode = content?keep_after("contentType=")?keep_before(",")/>
	<#if (content?index == 0)>
		<#assign tipiContenuto += typeCode />
	<#else>
		<#assign tipiContenuto += '','' + typeCode />
	</#if>
</#list>

<#assign userFilters = [] />
<#list contents as content>
	<#list content?keep_after("userFilters=")?keep_before("}")?split("+") as userFilter>
		<#if (userFilter != '''')>
			<#assign userFilters = userFilters + [{''contentType'': content?keep_after("contentType=")?keep_before(","), ''filter'':userFilter}] />
		</#if>
	</#list>
</#list>

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
    <div class="progress-spinner progress-spinner-active" data-ng-show="::false">
		<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
	</div>
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipiContenuto}'', ''${idModello}'', ''${categorie}'', '' '', ''${orCategory}'', ''${filters}'', ''multi'', ''${utenteToken}'')"> 	
		<div class="row">
			<div class="col-md-12">
				<#if (userFilters?size > 0)>
					<div class="titolosezione filtrisezione">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<#if (titleVar??)>
								
                            <#assign "different_all"= "CITTAMETRO_PORTAL_ALL" + titleVar?upper_case/>
                        <#else>
                            <#assign "different_all"= "CITTAMETRO_PORTAL_ALL" + paginaCorrente ?upper_case/>
						</#if>
                        

						<div class="filtro">
							<button id="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />${numeroFrame}" class="btn btn-default btn-trasparente active" data-ng-class="class_tutti()" title="<@wp.i18n key=different_all />"
								data-ng-click="toggleAll(); cleanSearchParameter(''all''); getMultiContents(''${tipiContenuto}'', getArgomentiList(), ''<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />${numeroFrame}'', ''${filters}'')">
								<@wp.i18n key=different_all />
							</button>
							<#assign numTipologie = 0/>
							<#list userFilters as userFilter>
								<#assign typeCode= userFilter.filter?keep_after("value=")?keep_before(";")/>
								<#assign typeKey= userFilter.filter?keep_after("key=")?keep_before(")")/>
								<#if (typeCode!= '''')>
									<@wp.pageInfo pageCode="${typeCode}" info="title" var="typeName" />	
									<#if (numTipologie == 0)><#assign tip1 = typeCode/></#if>
									<#if (numTipologie == 1)><#assign tip2 = typeCode/></#if>
									<button id="${typeCode}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${typeCode}''? ''active'': ''''" title="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)>data-ng-hide="searchParameters.key != ''${typeCode}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2}'' && searchParametersLength != 0"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${typeCode}''"</#if> >
										${typeName}
									</button>
									<#assign numTipologie ++/>
								<#else>
									<#if (typeKey?contains("contentType"))>
										<#if (numTipologie == 0)><#assign tip1 = userFilter.contentType/></#if>
										<#if (numTipologie == 1)><#assign tip2 = userFilter.contentType/></#if>
										<button id="${userFilter.contentType}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${userFilter.contentType}''? ''active'': ''''" title="<@wp.i18n key=''CITTAMETRO_CONTENT_${userFilter.contentType}''/>" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)> data-ng-hide="searchParameters.key != ''${userFilter.contentType}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2}'' && searchParametersLength != 0"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${userFilter.contentType}''"</#if>>
											<@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}"/>
										</button>
										<#assign numTipologie ++/>
									<#else>
										<#assign pageStart= ''homepage''/>
										<#if (userFilter.contentType== ''NVT'')><#assign pageStart= ''novita''/></#if>
										<#if (userFilter.contentType== ''EVN'')><#assign pageStart= ''nov_03''/></#if>
										<#if (userFilter.contentType== ''DOC'')><#assign pageStart= ''documenti''/></#if>
										<#if (userFilter.contentType== ''SRV'')><#assign pageStart= ''servizi''/></#if>
										<@wp.nav var="pagina" spec="code(${pageStart}).subtree(3)">
											<#if (pagina.level >= 1)>
												<#if (numTipologie == 0)><#assign tip1 = pagina.code/></#if>
												<#if (numTipologie == 1)><#assign tip2 = pagina.code/></#if>
												<button id="${pagina.code}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.key == ''${pagina.code}''? ''active'': ''''" title="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')" <#if (numTipologie == 2)>data-ng-hide="searchParameters.key != ''${pagina.code}'' && searchParameters.key != ''${tip1}'' && searchParameters.key != ''${tip2} && searchParametersLength != 0''"</#if> <#if (numTipologie > 2)>data-ng-if="searchParameters.key == ''${pagina.code}''"</#if>>
													${pagina.title}
												</button>
												<#assign numTipologie ++/>
											</#if>
										</@wp.nav>
									</#if>
								</#if>
							</#list>
							<#if (numTipologie > 3)>
								<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''categorie''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
							</#if>
						</div>
					</div>
				<#else>
					<div class="titolosezione">
						<h3>
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
					</div>
				</#if>
			</div>
		</div>
		
		<#if (layout == ''1'')>
			<div class="row row-eq-height widget-mono">
				<#list contents as content>
					<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
					<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
					<div class="col-lg-4 col-md-6 animation-if" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents"></div>
				</#list>
			</div>
		</#if>
		<#if (layout == ''2'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<div class="col-md-4" data-ng-if="($index+1)%3 == 0 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-md-4" data-ng-if="(($index+1)%3 == 0 || ($index+1)%3 == 2) && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-md-4" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''3'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height widget-multi" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="(($index+1)%3 == 0 || ($index+1)%3 == 2) && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-6 mb16" data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''4'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />						
						<div class="col-lg-4 col-md-12 1a" data-ng-if="contents.length >= 4 && contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 2a" data-ng-if="contents.length >= 3 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>
					</#list>
						<div class="col-lg-4 col-md-12 3a" data-ng-if="contents.length >= 4">
							<#list contents as content>
								<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
								<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
								<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />	
								<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
								<div data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
							</#list>
						</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 4a" data-ng-if="contents.length <= 3 && contents.length >= 2 && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 5a" data-ng-if="contents.length <= 2 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 6a" data-ng-if="contents.length == 3 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
					</#list>
				</div>
			</div>
		</#if>
		<#if (layout == ''5'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents">
				<div class="row row-eq-height" data-ng-if="($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}''">
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 1b" data-ng-if="contents.length >= 5 && contents[contents.indexOf(elem)-4].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-4], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4]][''${idModello}'']"></div>
					</#list>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="contents.length >= 5">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello2}'']"></div>						
							<div data-ng-if="contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello2}'']"></div>
						</#list>
					</div>
					<div class="col-lg-4 col-md-12 3b" data-ng-if="contents.length >= 5">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
							<div data-ng-if="elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
						</#list>
					</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 5b" data-ng-if="contents.length == 4 && contents[contents.indexOf(elem)-3].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-3], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3]][''${idModello}'']"></div>
					</#list>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="contents.length == 4">
						<#list contents as content>
							<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
							<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
							<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
							<div data-ng-if="contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello2}'']"></div>
							<div data-ng-if="contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello2}'']"></div>
						</#list>
					</div>
					<#list contents as content>
						<#assign tipoContenuto = content?keep_after("contentType=")?keep_before(",") />
						<#assign idModello = content?keep_after("modelId=")?keep_before(",") />
						<#assign idModello2 = content?keep_after("modelId2=")?keep_before(",") />
						<div class="col-lg-4 col-md-12 6b" data-ng-if="contents.length == 4 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello2}'')" data-ng-bind-html="renderContent[elem][''${idModello2}'']"></div>
						<div class="col-lg-4 col-md-12 7b" data-ng-if="contents.length == 3 && contents[contents.indexOf(elem)-2].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-2], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2]][''${idModello}'']"></div>					
						<div class="col-lg-4 col-md-12 8b" data-ng-if="contents.length <= 3 && contents.length >= 2 && contents[contents.indexOf(elem)-1].indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(contents[contents.indexOf(elem)-1], ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1]][''${idModello}'']"></div>
						<div class="col-lg-4 col-md-12 9b" data-ng-if="contents.length <= 3 && elem.indexOf(''${tipoContenuto}'') != -1" data-ng-init="getContent(elem, ''${idModello}'')" data-ng-bind-html="renderContent[elem][''${idModello}'']"></div>	
					</#list>	
				</div>
			</div>
		</#if>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /><#if (titleVar??)> - ${titleVar}</#if>">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, currentPage); scrollTo(''frame${numeroFrame}'')" 
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, elem); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, elem); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage+1 == numberOfPages" data-ng-click="getMultiContents(undefined, ''${categorie}'', '''', undefined, currentPage+2); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		
		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
	
	<#if (numTipologie > 3)>
		<div class="modal fade searchWidgetModal" id="searchWidgetModal${numeroFrame}" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle${numeroFrame}" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca-interna-${numeroFrame}">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !General.getFiltriMode().filtri}">
										<button type="button" class="close" data-dismiss="modal" aria-label="Chiudi filtri di ricerca" data-ng-click="General.setFiltriMode(false, false)">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle${numeroFrame}">
											<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />
										</h1>
										<button class="confirm btn btn-default btn-trasparente float-right" data-dismiss="modal" data-ng-click="General.setFiltriMode(false, false)">Conferma</button>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="search-filter-type ng-hide" data-ng-show="General.getFiltriMode().filtri">
											<ul class="nav nav-tabs" role="tablist">
												<li role="presentation">
													<a href="" aria-controls="categoriaTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''categorie'')" data-ng-class="categoriaTab"><@wp.i18n key="CITTAMETRO_CONTENT_TYPES" /></a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="modal-body-search">
							<div class="container">
								<div role="tabpanel" data-ng-show="General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
									<div class="tab-content">
										<div role="tabpanel" data-ng-class="categoriaTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab${numeroFrame}">
											<div class="row">
												<div class="offset-md-1 col-md-10 col-sm-12">
													<div class="search-filter-ckgroup">
														<#assign mostraTutto = false />
														<#assign numTipologie = 0/>
														<#list userFilters as userFilter>
															<#assign typeCode= userFilter.filter?keep_after("value=")?keep_before(";")/>
															<#assign typeKey= userFilter.filter?keep_after("key=")?keep_before(")")/>
															<#if (typeCode!= '''')>
																<@wp.pageInfo pageCode="${typeCode}" info="title" var="typeName" />		
																<#if (numTipologie <= 11)>
																	<div class="flex-100">
																		<md-checkbox data-ng-checked="searchParameters.key == ''${typeCode}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																		<label>${typeName}</label></md-checkbox>
																	</div>
																<#else>
																	<#assign mostraTutto = true />
																	<div class="flex-100" data-ng-show="showalltipol">
																		<md-checkbox data-ng-checked="searchParameters.key == ''${typeCode}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${typeCode}'' ? searchParameters = {type:''tipologia'', key:''${typeCode}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																		<label>${typeName}</label></md-checkbox>
																	</div>
																</#if>
																<#assign numTipologie ++/>
															<#else>
																<#if (typeKey?contains("contentType"))>
																	<#if (numTipologie <= 11)>
																		<div class="flex-100">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${userFilter.contentType}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label><@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}" /></label></md-checkbox>
																		</div>
																	<#else>
																		<#assign mostraTutto = true />
																		<div class="flex-100" data-ng-show="showalltipol">
																			<md-checkbox data-ng-checked="searchParameters.key == ''${userFilter.contentType}''" aria-label="${typeName}" data-ng-click="searchParameters.key != ''${userFilter.contentType}'' ? searchParameters = {type:''contentType'', key:''${userFilter.contentType}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																			<label><@wp.i18n key="CITTAMETRO_CONTENT_${userFilter.contentType}" /></label></md-checkbox>
																		</div>
																	</#if>
																	<#assign numTipologie ++/>
																<#else>
																	<#assign pageStart= ''homepage''/>
																	<#if (userFilter.contentType== ''NVT'')><#assign pageStart= ''novita''/></#if>
																	<#if (userFilter.contentType== ''EVN'')><#assign pageStart= ''nov_03''/></#if>
																	<#if (userFilter.contentType== ''DOC'')><#assign pageStart= ''documenti''/></#if>
																	<#if (userFilter.contentType== ''SRV'')><#assign pageStart= ''servizi''/></#if>
																	<@wp.nav var="pagina" spec="code(${pageStart}).subtree(3)">
																		<#if (pagina.level >= 1)>
																			<#if (numTipologie <= 11)>
																				<div class="flex-100">
																					<md-checkbox data-ng-checked="searchParameters.key == ''${pagina.code}''" aria-label="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																					<label>${pagina.title}</label></md-checkbox>
																				</div>
																			<#else>
																				<#assign mostraTutto = true />
																				<div class="flex-100" data-ng-show="showalltipol">
																					<md-checkbox data-ng-checked="searchParameters.key == ''${pagina.code}''" aria-label="${pagina.title}" data-ng-click="searchParameters.key != ''${pagina.code}'' ? searchParameters = {type:''tipologia'', key:''${pagina.code}''}: cleanSearchParameter(''all''); getMultiContents(undefined, getArgomentiList(), '''', ''${filters}'')">
																					<label>${pagina.title}</label></md-checkbox>
																				</div>
																			</#if>
																			<#assign numTipologie ++/>
																		</#if>
																	</@wp.nav>
																</#if>
															</#if>
														</#list>
														<#if (mostraTutto)>
															<a href="" data-ng-hide="showalltipol" data-ng-click="showalltipol=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
															<a href="" data-ng-show="showalltipol" data-ng-click="showalltipol=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
														</#if>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</#if>
</div>' where code = 'cagliari_widget_lista_multicontent';
