insert into widgetcatalog (code, titles, locked) values ('cagliari_widget_lista_multilayout', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - Lista multi-layout</property><property key="it">Cittametro - Lista multi-layout</property></properties>', 1);

UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
    <parameter name="layout">Layout of content list</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="modelId2">Content Model 2</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />	
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfigExt"/>
</config>' WHERE code = 'cagliari_widget_lista_multilayout';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cagliari_widget_lista_multilayout';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cagliari_widget_lista_multilayout', 'cagliari_widget_lista_multilayout', 'whitelabel','',1);

UPDATE guifragment set gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cw=JspTaglibs["/comuneCagliari-whitelabel"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />
<@cw.currentArgument var="currentArg" />
<@wp.currentWidget param="config" configParam="contentType" var="tipoContenuto" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="modelId2" var="idModello2" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="ordinamento" />
<@wp.currentWidget param="config" configParam="layout" var="layout" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@wp.categories var="systemCategories" root="arg_argomenti" />

<@jacms.contentList listName="contentList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />
    
<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!ordinamento??)><#assign ordinamento = "" /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>
<#if (!idModello??)><#assign idModello = "list" /></#if>
<#if (!idModello2??)><#assign idModello2 = "list" /></#if>
<#if (currentArg??)>
	<#assign categorie = ''${currentArg},${categorie}'' />
<#else>
	<#assign currentArg = '''' />
</#if>

<#assign altriFiltri = false />
<#assign allCategories = false/>
<#assign numTipologie = 0 />
<#assign categoryIndex = 999 />
<#assign categoryEnumIndex = 999 />
<#assign tipologieIndex = 999 />
<#assign tipologiaName = '''' />
<#assign enumCategName = '''' />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
	<#if (userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)>
		<#list userFilterOptionsVar as userFilterOptionVar>
			<#if (userFilterOptionVar.attribute??)>
				<#if (userFilterOptionVar.attribute.mapItems??)>
					<#if (userFilterOptionVar.attribute.type != ''MultiEnumeratorMapCategory'')>
						<#if (tipologieIndex == 999)>
							<#assign tipologieIndex = userFilterOptionVar?index/>
						</#if>
					<#elseif (userFilterOptionVar.attribute.type == ''MultiEnumeratorMapCategory'')>
						<#assign categoryEnumIndex = userFilterOptionVar?index/>
					</#if>
				</#if>
			</#if>
		</#list>
	</#if>
	
	<#if (filtri??)>
		<#assign filtriRicerca = [] />
		<#list filtri?split("+") as filtro>
			<#if (filtro?contains("key=category"))>
				<#if (filtro?remove_beginning("(attributeFilter=false;key=category)") == "") || (filtro?contains("categoryCode=arg_argomenti"))>
					<#assign allCategories = true/>
				</#if>
				<#assign categoryIndex = filtro?index/>
			<#else>
				<#assign filtriRicerca = filtriRicerca + [filtro] />
			</#if>
		</#list>
		<#if (categoryIndex <= categoryEnumIndex)>
			<#if (allCategories) && (categoryIndex < categoryEnumIndex)>
				<#list systemCategories?sort_by("value") as systemCategory>
					<data-ng-container data-ng-init="setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
				</#list>
			<#else>
				<#assign filtriCategorie = [] />
				<#assign filtriRicerca = [] />
				<#list filtri?split("+") as filtro>
					<#if (filtro?contains("key=category"))>
						<#assign value>${filtro?remove_beginning("(attributeFilter=false;key=category;categoryCode=")}</#assign>
						<#assign value>${value?remove_ending(")")}</#assign>
						<#assign filtriCategorie = filtriCategorie + [value] />
					<#else>
						<#assign filtriRicerca = filtriRicerca + [filtro] />
					</#if>
				</#list>
				<#list filtriCategorie as filtro>
					<#list systemCategories as systemCategory>
						<#if (filtro == systemCategory.key)>
							<data-ng-container data-ng-init="setArgomenti(''${systemCategory.value?replace("''", "\\''")}'', ''${systemCategory.key}'')"></data-ng-container>
						</#if>
					</#list>	
				</#list>
			</#if>
		<#elseif (categoryEnumIndex < categoryIndex)>
			<#list userFilterOptionsVar as userFilterOptionVar>
				<#if (userFilterOptionVar.attribute??)>
					<#if (userFilterOptionVar.attribute.mapItems??)>
						<#if (userFilterOptionVar?index == categoryEnumIndex)>
							<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
								<data-ng-container data-ng-init="setArgomenti(''${enumeratorMapItemVar.value?replace("''", "\\''")}'', ''${enumeratorMapItemVar.key}'')"></data-ng-container>
							</#list>
						</#if>
					</#if>
				</#if>
			</#list>
		</#if>
	</#if>

	<div class="progress-spinner progress-spinner-active" data-ng-show="::false">
		<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_LOADING" />...</span>
	</div>
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', ''${elementiMax}'', ''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', '' '', ''${idModello2}'', ''${ordinamento}'', '''', ''${utenteToken}'')"> 	
		<div class="row">
			<div class="col-md-12">
				<#if ((userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)) || ((filtriCategorie??) && (filtriCategorie?size > 0) || (allCategories))>
					<div class="titolosezione filtrisezione">
						<h3 class="float-left">
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
						<div class="filtro">
							<button id="${tipoContenuto}" class="btn btn-default btn-trasparente active" data-ng-class="class_tutti()" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
								data-ng-click="toggleAll(); cleanSearchParameter(''all''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), ''${tipoContenuto}'', ''${ordinamento}'')">
								<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
							</button>
							<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (categoryIndex<tipologieIndex) && (categoryIndex<categoryEnumIndex) >
								<button data-ng-repeat="item in argomenti" data-ng-if="$index <= 2" id="{{item.code}}" class="btn btn-default btn-trasparente" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" title="{{item.name}}" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
									{{item.name}}
								</button>
								<button data-ng-repeat="item in argomenti" data-ng-if="$index >= 3" id="{{item.code}}" class="btn btn-default btn-trasparente" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" title="{{item.name}}" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')" data-ng-show="getArgomentiList().indexOf(item.code) != -1 && item.code != ''${currentArg}''">
									{{item.name}}
								</button>
							</#if>
							<#if (userFilterOptionsVar??) && (userFilterOptionsVar?has_content) && (userFilterOptionsVar?size > 0)>
								<#list userFilterOptionsVar as userFilterOptionVar>
									<#if (userFilterOptionVar.attribute??)>
										<#if (userFilterOptionVar.attribute.mapItems??)>
											<#if (userFilterOptionVar?index == tipologieIndex)>
												<#assign tipologiaName = userFilterOptionVar.attribute.name />
												<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
													<#if (enumeratorMapItemVar?index <= 1) && (tipologieIndex<categoryIndex) && (tipologieIndex<categoryEnumIndex)>
														<#if (enumeratorMapItemVar?index == 0)><#assign tipologia1 = enumeratorMapItemVar.key/></#if>
														<#if (enumeratorMapItemVar?index == 1)><#assign tipologia2 = enumeratorMapItemVar.key/></#if>
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index <= 1) && ((categoryIndex<tipologieIndex) || (categoryEnumIndex<tipologieIndex)) >
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
															data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index >= 2)>
														<#if (enumeratorMapItemVar?index == 2) && (tipologieIndex<categoryIndex) && (tipologieIndex<categoryEnumIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" title="${enumeratorMapItemVar.value}" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" data-ng-hide="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' && searchParameters.${tipologiaName} != ''${tipologia1}'' && searchParameters.${tipologiaName} != ''${tipologia2}'' && searchParametersLength != 0"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index == 2) && ((categoryIndex<tipologieIndex) || (categoryEnumIndex<tipologieIndex)) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index > 2)>
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${tipologiaName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${tipologiaName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
													</#if>
													<#assign numTipologie = enumeratorMapItemVar?index + 1 />
												</#list>
											<#elseif (userFilterOptionVar?index == categoryEnumIndex) && (categoryEnumIndex<categoryIndex)>
												<#assign enumCategName = userFilterOptionVar.attribute.name />
												<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
													<#if (enumeratorMapItemVar?index <= 1) && (categoryEnumIndex<tipologieIndex)>
														<#if (enumeratorMapItemVar?index == 0)><#assign categoria1 = enumeratorMapItemVar.key/></#if>
														<#if (enumeratorMapItemVar?index == 1)><#assign categoria2 = enumeratorMapItemVar.key/></#if>
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-class="(searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}'')? ''active'': ''''"
															data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index <= 1) && (categoryEnumIndex>tipologieIndex) >
														<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
															data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
															${enumeratorMapItemVar.value}
														</button>
													</#if>
													<#if (enumeratorMapItemVar?index >= 2)>
														<#if (enumeratorMapItemVar?index == 2) && (tipologieIndex>categoryEnumIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" title="${enumeratorMapItemVar.value}" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" data-ng-hide="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' && searchParameters.${enumCategName} != ''${categoria1}'' && searchParameters.${enumCategName} != ''${categoria2}'' && searchParametersLength != 0"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index == 2) && (categoryEnumIndex>tipologieIndex) >
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
														<#if (enumeratorMapItemVar?index > 2)>
															<button id="${enumeratorMapItemVar.key}" class="btn btn-default btn-trasparente" data-ng-class="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''? ''active'': ''''" title="${enumeratorMapItemVar.value}" data-ng-show="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''"
																data-ng-click="searchParameters.${enumCategName} != ''${enumeratorMapItemVar.key}'' ? searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}'': cleanSearchParameter(''${enumCategName}''); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																${enumeratorMapItemVar.value}
															</button>
														</#if>
													</#if>
												</#list>
											<#else>
												<#assign altriFiltri = true />
											</#if>
										</#if>
									</#if>
								</#list>
							</#if>
							<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (tipologieIndex<categoryIndex) && (categoryIndex<categoryEnumIndex)>
								<button data-ng-repeat="item in argomenti" id="{{item.code}}" class="btn btn-default btn-trasparente" title="{{item.name}}" data-ng-class="(getArgomentiList().indexOf(item.code) != -1)? ''active'': ''''" data-ng-click="toggle(item, args_selected); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')" data-ng-show="getArgomentiList().indexOf(item.code) != -1 && item.code != ''${currentArg}''">
									{{item.name}}
								</button>
							</#if>
							<button data-ng-if="key != ''${tipologiaName}'' && key != ''${enumCategName}''" data-ng-show="(value.value != null && value.value != '''') || (value.start != null && value.start != '''') || (value.end != null && value.end != '''')" data-ng-repeat="(key, value) in searchParameters"  class="btn btn-default btn-trasparente active" data-ng-click="cleanSearchParameter(key); getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
								<span data-ng-if="value.type==''date''">
									<span data-ng-if="value.start && value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{value.start}}&nbsp;<@wp.i18n key="CITTAMETRO_PORTAL_TO" />&nbsp;{{value.end}} </span>
									<span data-ng-if="value.start && !value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_FROM" />&nbsp;{{value.start}}</span>
									<span data-ng-if="!value.start && value.end">{{value.title}}: <@wp.i18n key="CITTAMETRO_PORTAL_UNTIL" />&nbsp;{{value.end}}</span>
								</span>
								<span data-ng-if="value.type==''text''">
									<span data-ng-if="key == ''fulltext''">{{value.value}}</span>
									<span data-ng-if="key != ''fulltext''">{{value.title}}: {{value.value}}</span>
								</span>
								<span data-ng-if="value.type==''select''">
									<span>{{value.title}}: {{value.value}}</span>
								</span>
								<span data-ng-if="value.type==''boolean''">
									<span>{{value.title}}</span>
								</span>
							</button>
							<#if (filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)>
								<#list filtriRicerca as filtro>
									<#if !((filtro?contains("key=${tipologiaName}") && tipologiaName != '''') || ((filtro?contains("key=${enumCategName}") && enumCategName != '''')))>
										<#assign altriFiltri = true />
									</#if>
								</#list>
							</#if>
							<#if (numTipologie > 3) || (allCategories) || ((filtriCategorie??) && (filtriCategorie?size > 3)) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
								<#if (tipologieIndex<categoryIndex)>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''categorie''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								<#elseif (tipologieIndex>categoryIndex) || (categoryEnumIndex<categoryIndex)>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''argomenti''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								<#else>
									<button class="btn btn-default btn-trasparente" data-ng-click="setActive(''opzioni''); General.setFiltriMode(true, true);" aria-label="Dettaglio filtri" data-toggle="modal" data-target="#searchWidgetModal${numeroFrame}">...</button>
								</#if>
							</#if>
						</div>
					</div>
				<#else>
					<div class="titolosezione">
						<h3>
							<#if (titleVar??)>
								<#if (pageLinkVar??)>
									<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
								<#else>
									${titleVar}
								</#if>
							</#if>
						</h3>
					</div>
				</#if>
			</div>
		</div>
		
		<#if (layout == ''1'')>
			<div class="row row-eq-height widget-mono">
				<div class="col-lg-4 col-md-6 animation-if" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}"></div>
			</div>
		</#if>
		<#if (layout == ''2'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-md-4" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''3'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height widget-multi" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-6 mb16" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-6 mb16" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''4'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-12 1a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 4" data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 3" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 3a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 4a" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) <= 3 && contents.length - (''${elementiMaxItem}''* currentPage) >= 2"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 5a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) <= 2" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 6a" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 3" data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
				</div>
			</div>
		</#if>
		<#if (layout == ''5'')>
			<div class="animation-if" data-ng-if="contents.length > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem} ">
				<div class="row row-eq-height" data-ng-if="($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">
					<div class="col-lg-4 col-md-12 1b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) >= 5" data-ng-init="getContent(contents[contents.indexOf(elem)-4].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-4].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello2}'']"></div>						
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 3b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) >= 5">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 5b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4" data-ng-init="getContent(contents[contents.indexOf(elem)-3].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-3].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 2b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello2}'']"></div>
						<div data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello2}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 6b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) == 4">
						<div data-ng-init="getContent(elem.$, ''${idModello2}'')" data-ng-bind-html="renderContent[elem.$][''${idModello2}'']"></div>
					</div>
					<div class="col-lg-4 col-md-12 7b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) == 3" data-ng-init="getContent(contents[contents.indexOf(elem)-2].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 8b" data-ng-if="contents.length - (''${elementiMaxItem}''* currentPage) <= 3 && contents.length - (''${elementiMaxItem}''* currentPage) >= 2"
						data-ng-init="getContent(contents[contents.indexOf(elem)-1].$, ''${idModello}'')" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].$][''${idModello}'']"></div>
					<div class="col-lg-4 col-md-12 9b" data-ng-if="(contents.length - (''${elementiMaxItem}''* currentPage)) <= 3" data-ng-init="getContent(elem.$, ''${idModello}'')" data-ng-bind-html="renderContent[elem.$][''${idModello}'']"></div>	
				</div>
			</div>
		</#if>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && ${elementiMax}>${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" /><#if (titleVar??)> - ${titleVar}</#if>">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1); scrollTo(''frame${numeroFrame}'')" title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}" data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1); scrollTo(''frame${numeroFrame}'')" title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
		
		<div class="row row-eq-height" data-ng-if="contents.length == 0">
			<div class="col-md-12">
				<p><@wp.i18n key="LIST_VIEWER_EMPTY" /></p>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
	
	<#if (numTipologie > 3) || (allCategories) || ((filtriCategorie??) && (filtriCategorie?size > 3)) || ((filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)) || (categoryEnumIndex<categoryIndex) >
		<div class="modal fade searchWidgetModal" id="searchWidgetModal${numeroFrame}" tabindex="-1" role="dialog" aria-labelledby="searchModalTitle${numeroFrame}" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="ricerca-interna-${numeroFrame}">
						<div class="modal-header-fullsrc">
							<div class="container">
								<div class="row">
									<div class="col-sm-1" data-ng-class="{pb12: !General.getFiltriMode().filtri}">
										<button type="button" class="close" data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}''); General.setFiltriMode(false, false)" 
											data-dismiss="modal" aria-label="Chiudi filtri di ricerca">
											<svg class="icon"><use xlink:href="<@wp.resourceURL/>cittametro-homepage-bundle/static/img/ponmetroca.svg#ca-arrow_back"></use></svg>
										</button>
									</div>
									<div class="col-sm-11">
										<h1 class="modal-title" id="searchModalTitle${numeroFrame}">
											<@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" />
										</h1>
										<button data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}''); ctrl.General.setFiltriMode(false, false)" class="confirm btn btn-default btn-trasparente float-right" data-dismiss="modal">Conferma</button>
									</div>
								</div>
								<#if (filtriRicerca??) && (filtriRicerca?has_content) && (filtriRicerca?size > 0)>
									<#list filtriRicerca as filtro>
										<#if (filtro?contains("key=titolo"))>
											<div class="row">
												<div class="col-lg-12 col-md-12 col-sm-12">
													<div class="form-group-free">
														<label class="sr-only active"><@wp.i18n key="CITTAMETRO_HEADER_FILTER_TITLE" /></label>
														<input type="text" class="cerca-txt-free form-control" name="cercatxt" id="cerca-txt${numeroFrame}" placeholder="<@wp.i18n key="CITTAMETRO_HEADER_SEARCH_TITLE" /> <#if (titleVar??)>${titleVar?lower_case}</#if>" data-ng-model=''searchParameters.titolo.value'' data-ng-change="searchParameters.titolo.title = ''Titolo''; searchParameters.titolo.type = ''text''; searchParameters.titolo.value == ''''? cleanSearchParameter(''titolo''):''''">
													</div>
												</div>
											</div>
										<#elseif !((filtro?contains("key=${tipologiaName}") && tipologiaName != '''') || ((filtro?contains("key=${enumCategName}") && enumCategName != '''')))>
											<#assign altriFiltri = true />
										</#if>
									</#list>
								</#if>
								<#if (numTipologie > 0) || ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12">
											<div class="search-filter-type ng-hide" data-ng-show="General.getFiltriMode().filtri">
												<ul class="nav nav-tabs" role="tablist">
													<#if (numTipologie > 0)>
														<li role="presentation">
															<a href="" aria-controls="categoriaTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''categorie'')" data-ng-class="categoriaTab"><@wp.i18n key="CITTAMETRO_CONTENT_TYPES" /></a>
														</li>
													</#if>
													<#if ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (categoryEnumIndex<categoryIndex)>
														<li role="presentation" data-ng-if="argomenti.length > 0">
															<a href="" aria-controls="argomentoTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''argomenti'')" data-ng-class="argomentoTab"><@wp.i18n key="CITTAMETRO_CONTENT_ARGUMENTS" /></a>
														</li>
													</#if>
													<#if (altriFiltri)>
														<li role="presentation">
															<a href="" aria-controls="opzioniTab${numeroFrame}" role="tab" data-toggle="tab" data-ng-click="setActive(''opzioni'')" data-ng-class="opzioniTab"><@wp.i18n key="CITTAMETRO_CONTENT_OPTIONS" /></a>
														</li>
													</#if>
												</ul>
											</div>
										</div>
									</div>
								</#if>
							</div>
						</div>

						<#if (numTipologie > 0) || ((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories) || (altriFiltri) || (categoryEnumIndex<categoryIndex)>
							<div class="modal-body-search">
								<div class="container">
									<div role="tabpanel" data-ng-show="General.getFiltriMode().filtri" class="ng-hide" data-ng-init="showallcat = false; showallarg = false">
										<div class="tab-content">
											<#if (numTipologie > 0)>
												<div role="tabpanel" data-ng-class="categoriaTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="categoriaTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<#assign mostraTutto = false />
																<#list userFilterOptionsVar as userFilterOptionVar>
																	<#if (userFilterOptionVar.attribute??)>
																		<#if (userFilterOptionVar.attribute.mapItems??)>
																			<#if (userFilterOptionVar?index == tipologieIndex)>
																				<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
																					<#if (enumeratorMapItemVar?index <= 11)>
																						<div class="flex-100">
																							<md-checkbox data-ng-checked="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																					<#if (enumeratorMapItemVar?index >= 12)>
																						<#assign mostraTutto = true />
																						<div class="flex-100" data-ng-show="showalltipol">
																							<md-checkbox data-ng-checked="searchParameters.${tipologiaName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${tipologiaName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																				</#list>
																			</#if>
																		</#if>
																	</#if>
																</#list>
																<#if (mostraTutto)>
																	<a href="" data-ng-hide="showalltipol" data-ng-click="showalltipol=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																	<a href="" data-ng-show="showalltipol" data-ng-click="showalltipol=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
																</#if>
															</div>
														</div>
													</div>
												</div>
											</#if>
											<#if (((filtriCategorie??) && (filtriCategorie?size > 0)) || (allCategories)) && (categoryIndex<categoryEnumIndex)>
												<div data-ng-if="argomenti.length > 0" role="tabpanel" data-ng-class="argomentoTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<div class="flex-100" data-ng-repeat="item in argomenti">
																	<div data-ng-if = "$index <= 11 && item.code != ''${currentArg}''">
																		<md-checkbox data-ng-checked="exists(item, args_selected)" data-ng-click="toggle(item, args_selected)" aria-label="{{item.name}}">
																		<label data-ng-bind-html="item.name"></label></md-checkbox>
																	</div>
																	<div data-ng-if = "$index >= 12 && item.code != ''${currentArg}''">									
																		<div data-ng-show="showallarg">
																			<md-checkbox data-ng-checked="exists(item, args_selected)" data-ng-click="toggle(item, args_selected)" aria-label="{{item.name}}">
																			<label data-ng-bind-html="item.name"></label></md-checkbox>
																		</div>
																	</div>
																</div>
																<a href="" data-ng-hide="showallarg || argomenti.length <= 12" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
															</div>
														</div>
													</div>
												</div>
											<#elseif (categoryEnumIndex<categoryIndex)>
												<div role="tabpanel" data-ng-class="argomentoTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="argomentoTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12">
															<div class="search-filter-ckgroup">
																<#assign mostraTuttoCat = false />
																<#list userFilterOptionsVar as userFilterOptionVar>
																	<#if (userFilterOptionVar.attribute??)>
																		<#if (userFilterOptionVar.attribute.mapItems??)>
																			<#if (userFilterOptionVar?index == categoryEnumIndex)>
																				<#list userFilterOptionVar.attribute.mapItems as enumeratorMapItemVar>
																					<#if (enumeratorMapItemVar?index <= 11)>
																						<div class="flex-100">
																							<md-checkbox data-ng-checked="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																					<#if (enumeratorMapItemVar?index >= 12)>
																						<#assign mostraTuttoCat = true />
																						<div class="flex-100" data-ng-show="showallarg">
																							<md-checkbox data-ng-checked="searchParameters.${enumCategName} == ''${enumeratorMapItemVar.key}''" aria-label="${enumeratorMapItemVar.value}"
																							data-ng-click="searchParameters.${enumCategName} = ''${enumeratorMapItemVar.key}''; getContents(''${tipoContenuto}'', ''${idModello}'', getArgomentiList(), '' '', ''${ordinamento}'')">
																							<label>${enumeratorMapItemVar.value}</label></md-checkbox>
																						</div>
																					</#if>
																				</#list>
																			</#if>
																		</#if>
																	</#if>
																</#list>
																<#if (mostraTuttoCat)>
																	<a href="" data-ng-hide="showallarg" data-ng-click="showallarg=true"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_ALL" /></strong></a>
																	<a href="" data-ng-show="showallarg" data-ng-click="showallarg=false"><strong><@wp.i18n key="CITTAMETRO_PORTAL_SHOW_LESS" /></strong></a>
																</#if>
															</div>
														</div>
													</div>
												</div>
											</#if>
											<#if (altriFiltri)>
												<div role="tabpanel" data-ng-class="opzioniTab == ''active''? ''tab-pane active'' : ''tab-pane''" id="opzioniTab${numeroFrame}">
													<div class="row">
														<div class="offset-md-1 col-md-10 col-sm-12 mt48">
															<div class="row">
															<@wp.freemarkerTemplateParameter var="userFilterOptionsVar" valueName="userFilterOptionsVar" removeOnEndTag=true >
																<@wp.fragment code="cagliari_widget_lista_multilayout_uf" escapeXml=false />
															</@wp.freemarkerTemplateParameter>
															</div>
														</div>
													</div>
												</div>
											</#if>
										</div>
									</div>
								</div>
							</div>
						</#if>
					</form>
				</div>
			</div>
		</div>
	</#if>
</div>' where code = 'cagliari_widget_lista_multilayout';
