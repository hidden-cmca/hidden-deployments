insert into widgetcatalog (code, titles, locked) values ('cagliari_widget_calendario_homepage', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cittametro - Calendario homepage con filtri</property><property key="it">Cittametro - Calendario homepage con filtri</property></properties>', 1);

UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contentType">Content Type (mandatory)</parameter>
	<parameter name="modelId">Content Model</parameter>
	<parameter name="userFilters">Front-End user filter options</parameter>
	<parameter name="category">Content Category **deprecated**</parameter>
	<parameter name="categories">Content Category codes (comma separeted)</parameter>
	<parameter name="orClauseCategoryFilter" />
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="maxElements">Number of contents</parameter>
	<parameter name="filters" />
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="listViewerConfig"/>
</config>' WHERE code = 'cagliari_widget_calendario_homepage';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cagliari_widget_calendario_homepage';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cagliari_widget_calendario_homepage', 'cagliari_widget_calendario_homepage', 'whitelabel', '', 1);

UPDATE guifragment SET gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign cc=JspTaglibs["/cagliari-core"]>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@wp.currentWidget param="config" configParam="contentType" var="tipoContenuto" />
<@wp.currentWidget param="config" configParam="maxElements" var="elementiMax" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />
<@wp.currentWidget param="config" configParam="modelId" var="idModello" />
<@wp.currentWidget param="config" configParam="categories" var="categorie" />
<@wp.currentWidget param="config" configParam="userFilters" var="filtri" />
<@wp.currentWidget param="config" configParam="filters" var="ordinamento" />
<@wp.categories var="systemCategories" titleStyle="prettyFull" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />


<@jacms.contentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" userFilterOptionsVar="userFilterOptionsVar" />

<@wp.headInfo type="JS_CA_BTM" info="owl.carousel.min.js" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.carousel.min.css" />
<@wp.headInfo type="CSS_CA_BTM" info="owl.theme.default.min.css" />

<#assign pathimg><@wp.imgURL/>ponmetroca.svg</#assign>
<#setting locale="it_IT">
<#assign actualdate = .now>
<#assign monthst = actualdate?string["MMMM"]?cap_first>
<#assign day = actualdate?string["d"]>
<#assign dayNum = day?number />
<#assign month = actualdate?string["MM"]>
<#assign monthNum = month?number />
<#assign yearst = actualdate?string["yyyy"]>
<#assign finemese><@cc.lastDayOfMonth /></#assign>
<#assign finemese = finemese?number />
<#assign dataend = "${finemese}/${month}/${yearst}"  />
<#assign datatoday = "${day}/${month}/${yearst}"/>

<#if (!elementiMax??)><#assign elementiMax = 999 /></#if>
<#if (!elementiMaxItem??)><#assign elementiMaxItem = 999 /></#if>
<#if (!ordinamento??)><#assign ordinamento = "" /></#if>
<#if (!categorie??)><#assign categorie = "" /></#if>

<#if (filtri??)>
	<#assign filtriUtente = [] />
	<#list filtri?split("+") as filtro>
		<#assign value>${filtro?remove_beginning("(attributeFilter=false;key=category;categoryCode=")}</#assign>
		<#assign value>${value?remove_ending(")")}</#assign>
		<#assign filtriUtente = filtriUtente + [value] />
	</#list>
	<#assign filtriCodeName = [] />
	<#list filtriUtente as filtro>
		<#list systemCategories as systemCategory>
			<#if (filtro == systemCategory.key)>
				<#assign filtriCodeName = filtriCodeName + [filtro + "/" + systemCategory.value?keep_after_last("/ ")] />
			</#if>
		</#list>
	</#list>
</#if>

<div class="widget" data-ng-cloak data-ng-controller="FiltriController" >
	<div class="container" data-ng-init="setCalendarParameters(''${dayNum}'',''${finemese}'',false, ''${elementiMaxItem}'', ''${elementiMax}'', ''${utenteToken}'')">
		<div class="row">
			<#if (titleVar??)>
				<div class="col-md-3">
					<h3>${titleVar}</h3>
				</div>
			</#if>
			<#if (filtriCodeName??) && (filtriCodeName?has_content) && (filtriCodeName?size > 0)>
				<div class="col-md-9 calendario-filtro">
					<button id="${tipoContenuto}" class="btn btn-default btn-trasparente" title="<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />"
						data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'', ''${tipoContenuto}''); categorie=''${categorie}''">
						<@wp.i18n key="CITTAMETRO_PORTAL_ALL" />
					</button>
					<#list filtriCodeName as filtro>
						<#assign categoryCode>${filtro?keep_before_last("/")}</#assign>
						<#assign categoryName>${filtro?keep_after_last("/")}</#assign>
						<button id="${categoryCode}" class="btn btn-default btn-trasparente" title="${categoryName}"
							data-ng-click="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie},${categoryCode}'', ''${categoryCode}''); categorie=''${categoryCode}''">
							${categoryName}
						</button>
					</#list>
				</div>
			</#if>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<h4 data-ng-show="oneMonth">${monthst} ${yearst}</h4>
				<h4 data-ng-hide="oneMonth">${monthst}/{{nextMonth}} ${yearst}</h4>
				<div id="owl-calendario" class="owl-carousel owl-center owl-theme owl-loaded owl-drag" role="tablist" data-ng-cloak>
					<data-ng-container data-ng-repeat="day in calendar" data-ng-cloak data-ng-if="full[day]" data-ng-show="!empty">
						<div class="scheda-calendario" data-ng-if="day >= ${dayNum}" data-ng-init="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'',
							''${tipoContenuto}'', getFilterDay(day, ''${month}'',''${yearst}''), day)">
							<div class="scheda-calendario-data">
								<strong>{{day}}</strong> {{dayName[day]}}
							</div>
							<div class="progress-spinner progress-spinner-active" data-ng-show="loader[day]">
								<span class="sr-only">Caricamento...</span>
							</div>
							<ul class="scheda-calendario-lista" data-ng-if="events[day].length > 0">
								<li data-ng-cloak data-ng-repeat="elem in events[day]"
									data-ng-show="categoryContent[day][elem.$].indexOf(categorie)>-1"
									data-ng-bind-html="renderEventContent[day][elem.$]">
								</li>
							</ul>
						</div>
						<div class="scheda-calendario" data-ng-if="day < ${dayNum}" data-ng-init="getContents(''${tipoContenuto}'', ''${idModello}'', ''${categorie}'',
							''${tipoContenuto}'', getFilterDay(day, ''${monthNum + 1}'',''${yearst}''), day)">
							<div class="scheda-calendario-data">
								<strong>{{day}}</strong> {{dayName[day]}}
							</div>
							<div class="progress-spinner progress-spinner-active" data-ng-show="loader[day]">
								<span class="sr-only">Caricamento...</span>
							</div>
							<ul class="scheda-calendario-lista" data-ng-if="events[day].length > 0">
								<li data-ng-repeat="elem in events[day]"
									data-ng-show="categoryContent[day][elem.$].indexOf(categorie)>-1"
									data-ng-bind-html="renderEventContent[day][elem.$]">
								</li>
							</ul>
						</div>
					</data-ng-container>
				</div>
				<div data-ng-show="empty" class="mt24">
					<@wp.i18n key="CITTAMETRO_PORTAL_EVENTLISTEMPTY" />
				</div>
			</div>
		</div>
	</div>
</div>' where code = 'cagliari_widget_calendario_homepage';
