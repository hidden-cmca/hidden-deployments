insert into widgetcatalog (code, titles, locked) values ('cittametro_widget_inevidenza', '<?xml version="1.0" encoding="UTF-8"?><properties><property key="en">Cagliari - Contenuti in 3 colonne</property><property key="it">Cittametro - Contenuti in 3 colonne</property></properties>', 1);


UPDATE widgetcatalog SET parameters = '<config>
	<parameter name="contents">Contents to Publish (mandatory)</parameter>
	<parameter name="maxElemForItem">Contents for each page</parameter>
	<parameter name="title_{lang}">Widget Title in lang {lang}</parameter>
	<parameter name="pageLink">The code of the Page to link</parameter>
	<parameter name="linkDescr_{lang}">Link description in lang {lang}</parameter>
	<action name="rowListViewerConfig" />
</config>' WHERE code = 'cittametro_widget_inevidenza';

UPDATE widgetcatalog SET plugincode = 'whitelabel' WHERE code = 'cittametro_widget_inevidenza';

insert into guifragment (code, widgettypecode, plugincode, gui, locked) values ('cittametro_widget_inevidenza', 'cittametro_widget_inevidenza', 'whitelabel','',1);


UPDATE guifragment SET gui = '<#assign jacms=JspTaglibs["/jacms-aps-core"]>
<#assign c=JspTaglibs["http://java.sun.com/jsp/jstl/core"]>
<#assign wp=JspTaglibs["/aps-core"]>
<#assign ca=JspTaglibs["/WEB-INF/tld/cagliari-core.tld"]>

<@wp.currentWidget param="config" configParam="contents" var="contenuti" />
<@wp.currentWidget param="config" configParam="maxElemForItem" var="elementiMaxItem" />

<@wp.headInfo type="JS_CA_BTM" info="filtri-controller.min.js" />
<@wp.headInfo type="JS_CA_BTM" info="general-service.js" />
<@wp.headInfo type="JS_CA_BTM" info="filtri-service.min.js" />

<@jacms.rowContentList listName="contentInfoList" titleVar="titleVar" pageLinkVar="pageLinkVar" pageLinkDescriptionVar="pageLinkDescriptionVar" />

<#if (!elementiMaxItem??)><#assign elementiMaxItem = 0 /></#if>
<#if (!contenuti??)><#assign contenuti = "" /></#if>

<#assign utenteToken = '''' />
<#if (Session.currentUser?? && Session.currentUser != "guest")>
<#if (Session.currentUser.accessToken??)>
<#assign utenteToken = Session.currentUser.accessToken />
</#if>
</#if>

<@ca.widgetFrameNumber frameid="numeroFrame" />

<div id="frame${numeroFrame}" class="widget" data-ng-controller="FiltriController">
	<div data-ng-cloak data-ng-init="setParameters(''${elementiMaxItem}'', '''', '''', '''', '''', '''', '''', '''', ''${contenuti}'',''${utenteToken}'')"> 
		<#if (titleVar??)>
			<div class="row">
				<div class="col-md-12">
					<div class="titolosezione">
						<h3>
							<#if (pageLinkVar??)>
								<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}">${titleVar}</a>
							<#else>
								${titleVar}
							</#if>
						</h3>
					</div>
				</div>
			</div>
		</#if>
		
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} > 0" data-ng-repeat="elem in contents | startFrom:currentPage*${elementiMaxItem} | limitTo: ${elementiMaxItem}">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == ''${elementiMaxItem}'' || (($index+1) + (''${elementiMaxItem}''* currentPage)) == contents.length">	
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		
		<div data-ng-if="contents.length > 0 && ${elementiMaxItem} == 0" data-ng-repeat="elem in contents | startFrom:currentPage*contents.length | limitTo: contents.length">
			<div class="row row-eq-height" data-ng-if="($index+1)%3 == 0 || ($index+1) == contents.length || ($index+1) == contents.length">
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-2].contentId]"></div>
				<div class="col-md-4" data-ng-if="($index+1)%3 == 0 || ($index+1)%3 == 2" data-ng-bind-html="renderContent[contents[contents.indexOf(elem)-1].contentId]"></div>
				<div class="col-md-4" data-ng-bind-html="renderContent[elem.contentId]"></div>
			</div>
		</div>
		
		<div class="row" data-ng-if="contents.length > 0 && pages.length >1 && contents.length > ${elementiMaxItem}">
			<div class="col-md-12">
				<nav class="pagination-wrapper justify-content-center" aria-label="<@wp.i18n key="CITTAMETRO_PORTAL_SEARCH_RESULT_PAGINATION" />">
					<ul class="pagination">
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage == 0" data-ng-click="goToPage(currentPage-1); scrollTo(''frame${numeroFrame}'')" 
								title="<@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-keyboard_arrow_left"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_PREVIOUSPAGE" /></span>
							</button>
						</li>
						<li data-ng-repeat="elem in pages" data-ng-if="elem-1<currentPage+5 && elem-1>currentPage-5" title="{{elem}}"
							data-ng-class="currentPage == (elem-1) ? ''page-item active'' : ''page-item''">
							<button data-ng-show="currentPage == (elem-1)" aria-current="page" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
							<button data-ng-show="currentPage != (elem-1)" class="page-link" data-ng-click="goToPage(elem-1); scrollTo(''frame${numeroFrame}'')">{{elem}}</button>
						</li>
						<li class="page-item">
							<button class="page-link" data-ng-disabled="currentPage >= contents.length/pageSize - 1" data-ng-click="goToPage(currentPage+1); scrollTo(''frame${numeroFrame}'')"
								title="<@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" />">
								<svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-keyboard_arrow_right"></use></svg>
								<span class="sr-only"><@wp.i18n key="CITTAMETRO_PORTAL_NEXTPAGE" /></span>
							</button>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<#if (pageLinkVar??) && (pageLinkDescriptionVar??)>
		<div class="row">
			<div class="col-md-12 veditutti">
				<a href="<@wp.url page="${pageLinkVar}"/>" title="<@wp.i18n key="CITTAMETRO_PORTAL_GOTOPAGE" />: ${titleVar}" class="btn btn-default btn-verde">${pageLinkDescriptionVar} <svg class="icon"><use xlink:href="<@wp.imgURL/>ponmetroca.svg#ca-arrow_forward"></use></svg></a>
			</div>
		</div>
	</#if>
	<#assign contentList="">
</div>' where code = 'cittametro_widget_inevidenza';
