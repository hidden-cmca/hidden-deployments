dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)


kubectl cp content/content-default-lang.sql entando/$dbpod:/opt/app-root/src/content-default-lang.sql

kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/content-default-lang.sql";
exit 1
