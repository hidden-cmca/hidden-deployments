dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)


kubectl cp subsites/subsites-port.sql entando/$dbpod:/opt/app-root/src/subsites-port.sql
kubectl cp subsites/subsites-serv.sql entando/$dbpod:/opt/app-root/src/subsites-serv.sql

kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/subsites-port.sql";
kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_servdb -d metroca_db -a -f /opt/app-root/src/subsites-serv.sql"; 
exit 1
