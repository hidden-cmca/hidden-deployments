bundleName="bundle-ui-metroca"

dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)
kubectl -n entando delete EntandoDeBundle/$bundleName
kubectl exec -it $dbpod -n entando -- bash -c "psql -U metroca_dedb -d metroca_db -c $'delete from installed_entando_bundles where id=\'$bundleName\';'"
sleep 1
ent bundler from-git --repository=https://gitlab.com/hidden-cmca/bundle-ui-metroca.git --namespace=entando --name=$bundleName
