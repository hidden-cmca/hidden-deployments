Attenzione: file di deploy con ingressHostName: metroca.mshome.net

## Per far girare in locale con multipass:

(da powershell admin `Remove-Item -path C:\WINDOWS\system32\drivers\etc\hosts.ics`)

`multipass launch --name metroca --cpus 6 --mem 8G --disk 40G`

`multipass shell metroca`
### oneline setup con ent-dev

`curl -L https://gitlab.com/hidden-cmca/hidden-deployments/-/raw/develop-local/dip.sh | bash`

dopo di che uscire dalla vm e rientrare.

(per lens `cat .kube/config`)

### Installazione manuale

    git clone https://gitlab.com/hidden-cmca/hidden-deployments.git -b develop-local

    curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION="v1.19.12+k3s1" sh -s -

    kubectl create namespace entando

    kubectl apply -f https://raw.githubusercontent.com/entando/entando-releases/v6.3.2/dist/ge-1-1-6/namespace-scoped-deployment/cluster-resources.yaml

    kubectl -n entando apply -f hidden-deployments/dist/namespace-resources.yaml

    kubectl -n entando apply -f hidden-deployments/configmaps/metroca-configmaps.yaml

    kubectl -n entando apply -f hidden-deployments/dist/deploy-entando-postgres.yaml

## bundle-Ui-metroca

#### installazione
`ent bundler from-git --repository=https://gitlab.com/hidden-cmca/bundle-ui-metroca.git --namespace=entando --name=bundle-ui-metroca`
#### aggiornamento
`bash bundleUiMetrocaUpdate.sh`

## ilpatto-bundle

#### installazione
`ent bundler from-git --repository=https://gitlab.com/hidden-cmca/ilpatto-bundle.git --namespace=entando --name=ilpatto-bundle`
##### autoupdate dopo reset cmca-ilpatto-microservice-server-deployment
`kubectl -n entando get deployments/cmca-ilpatto-microservice-server-deployment -o yaml > cmca-ilpatto-microservice-server-deployment.yaml`

`sed -i s/'imagePullPolicy: IfNotPresent'/'imagePullPolicy: Always'/ cmca-ilpatto-microservice-server-deployment.yaml`

`sudo kubectl apply -f cmca-ilpatto-microservice-server-deployment.yaml -n entando`


#### aggiornamento versione
`bash ilPattoBundleUpdate.sh`
##### aggiornamento ingress dopo aggiornamento versione
`kubectl get ingress/metroca-ingress -n entando -o yaml > metroca-ingres.yaml`

`sed -i s/0-1-0-snapshot/0-1-1-snapshot/ metroca-ingres.yaml`

`kubectl -n entando apply -f metroca-ingres.yaml`


## aggiornamento immagine de-app-wildfly

`kubectl -n entando scale deployments/metroca-server-deployment --replicas=0`

`kubectl -n entando get deployments/metroca-server-deployment -o yaml > metroca-server-deployment.yaml`

`sed -i s/0.1.5-SNAPSHOT/0.1.6-SNAPSHOT/ metroca-server-deployment.yaml`

`kubectl -n entando apply -f metroca-server-deployment.yaml`

`kubectl -n entando scale deployments/metroca-server-deployment --replicas=1`

##### autoupdate dopo reset metroca-server-deployment
`kubectl -n entando get deployments/metroca-server-deployment -o yaml > metroca-server-deployment.yaml`

`sed -i s/'imagePullPolicy: IfNotPresent'/'imagePullPolicy: Always'/ metroca-server-deployment.yaml`

`sudo kubectl apply -f metroca-server-deployment.yaml -n entando`

### ricreazione core solr dopo aggiornamento de-app-wildfly

`solrpod=$(kubectl -n entando get pods  | grep solr | grep -Po '.*(?=1/1)' | xargs)`

`kubectl exec -n entando $solrpod  -- solr create_core -c entando`



 
