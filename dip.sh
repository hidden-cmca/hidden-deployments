sudo apt update && sudo apt -y upgrade
sudo apt -y install openjdk-11-jdk jq
git clone https://gitlab.com/hidden-cmca/hidden-deployments.git -b develop-local
sleep 1
curl -sfL https://get.k3s.io | K3S_KUBECONFIG_MODE="644" INSTALL_K3S_VERSION="v1.19.12+k3s1" sh -s -
sleep 1
# sudo chmod 644 /etc/rancher/k3s/k3s.yaml
# echo "sudo chmod 644 /etc/rancher/k3s/k3s.yaml" >> .bashrc
ready=$(kubectl get nodes | grep -Po '(?<=(metroca )).*(?= control)?(?= master)'| xargs)
while [ "$ready" != "Ready" ]
do
  echo
  echo "###########Nodo not ready############"
  echo
  echo
  sleep 2
  ready=$(kubectl get nodes | grep -Po '(?<=(metroca )).*(?= control)?(?= master)'| xargs)
done
sleep 1
kubectl create namespace entando
sleep 1
kubectl config set-context --current --namespace=entando
sleep 1
kubectl apply -f https://raw.githubusercontent.com/entando/entando-releases/v6.3.2/dist/ge-1-1-6/namespace-scoped-deployment/cluster-resources.yaml
sleep 1
kubectl -n entando apply -f hidden-deployments/dist/namespace-resources.yaml
# kubectl -n entando apply -f https://raw.githubusercontent.com/entando-k8s/entando-k8s-operator-bundle/v6.3.2/manifests/k8s-116-and-later/namespace-scoped-deployment/namespace-resources.yaml
sleep 1
kubectl -n entando apply -f hidden-deployments/configmaps/configmap-metroca.yaml
sleep 1
kubectl -n entando apply -f hidden-deployments/dist/solr.yaml
sleep 2
#readysolr=$(kubectl -n entando get deployments/solr | grep -Po '(?<=(solr)).*(?=1)'| xargs)
#readysolr=$(echo $readysolr | sed 's/ .*//' )
#while [ "$readysolr" != "1/1" ]
#do
#  echo
#  echo "###########Solr not ready############"
#  echo
#  echo
#  sleep 3
#  readysolr=$(kubectl -n entando get deployments/solr | grep -Po '(?<=(solr)).*(?=1)'| xargs)
#  readysolr=$(echo $readysolr | sed 's/ .*//' )
#done
sleep 35
solrpod=$(kubectl -n entando get pods  | grep solr | grep -Po '.*(?=1/1)' | xargs)
#solrentando=$(kubectl exec -n entando $solrpod  -- solr create_core -c entando)
#echo "$solrentando"
#sleep 5
kubectl -n entando apply -f hidden-deployments/dist/deploy-entando-postgres.yaml
sleep 1
# curl -L https://get.entando.org/cli | bash
# sleep 1
# source "$HOME/.entando/activate"
# sleep 1
# checkdev=$(ent check-env develop --yes | grep -Po '(?<=( ent )).*' | xargs)
# while [ "$checkdev" != "completed" ]
# do
#   echo
#   echo "###########dip ent not ready############"
#   echo
#   echo
#   sleep 1
#   checkdev=$(ent check-env develop --yes | grep -Po '(?<=( ent )).*' | xargs)
# done
# echo
# echo "###########ent dev READY############"
# echo
# echo
mkdir /home/ubuntu/.kube
cp /etc/rancher/k3s/k3s.yaml /home/ubuntu/.kube/config
sed -i s/127.0.0.1/metroca.mshome.net/ /home/ubuntu/.kube/config
# ent attach-kubeconfig /home/ubuntu/.kube/config
# ent profile new metro metroca entando
# watch ent app-info
