dbpod=$(kubectl get pods -n entando | grep metroca-db-deployment | grep -Po '.*(?=1/1)' | xargs)


kubectl cp port_data_production.sql entando/$dbpod:/opt/app-root/src/jpsubistes_port_data_production.sql
kubectl cp serv_data_production.sql entando/$dbpod:/opt/app-root/src/jpsubistes_serv_data_production.sql

kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_portdb -d metroca_db -a -f /opt/app-root/src/jpsubistes_port_data_production.sql";
kubectl -n entando exec -it $dbpod  -- bash -c "psql -U metroca_servdb -d metroca_db -a -f /opt/app-root/src/jpsubistes_serv_data_production.sql";
exit 1
